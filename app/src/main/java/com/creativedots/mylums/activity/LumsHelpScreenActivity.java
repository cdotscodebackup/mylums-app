package com.creativedots.mylums.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;


import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.login.LoginActivity;
import com.creativedots.mylums.adapter.HelpPagerAdapter;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;
import com.viewpagerindicator.CirclePageIndicator;


public class LumsHelpScreenActivity extends FragmentActivity{

    private ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lums_help_view_pager);

        pager = (ViewPager) findViewById(R.id.help_view_pager);
        pager.setAdapter(new HelpPagerAdapter(this, getSupportFragmentManager()));

        CirclePageIndicator titleIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        titleIndicator.setViewPager(pager);

        findViewById(R.id.skip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SessionManager(LumsHelpScreenActivity.this).setShowWizard(false);
                Intent intent = new Intent(LumsHelpScreenActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                finishAffinity();
            }
        });
    }

}
