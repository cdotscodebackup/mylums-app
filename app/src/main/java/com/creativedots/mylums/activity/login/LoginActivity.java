package com.creativedots.mylums.activity.login;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.base.BaseActivity;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.utils.AppSettings;
import com.creativedots.mylums.model.retroFit.profile.ProfileResponse;
import com.creativedots.mylums.utils.Event;
import com.creativedots.mylums.utils.RxBus;
import com.creativedots.mylums.utils.sharedPreferences.Constants;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;
import com.creativedots.mylums.utils.ValidationTools;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;

import com.creativedots.mylums.model.retroFit.Response;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    public static final String TAG = LoginActivity.class.getSimpleName();
    private static final int TYPE_TEXT_PASSWORD_INVISIBLE = 129;

    private AVLoadingIndicatorView aviLoading;
    private RelativeLayout rlSignIn;
    private EditText etPassword, etEmail;
    private TextView tvSignUp;//, tvForgotPassword, tvHelp;
    private LinearLayout llHelp, llForgotPassword;
    private ImageView ivVisibility;

    private void getInfo() {
        aviLoading = findViewById(R.id.aviLoading);
        rlSignIn = findViewById(R.id.rlSignIn);

        etPassword = findViewById(R.id.etPassword);
        etEmail = findViewById(R.id.etEmail);

        tvSignUp = findViewById(R.id.tvSignUp);

        llHelp = findViewById(R.id.llHelp);
        llForgotPassword = findViewById(R.id.llForgotPassword);

        ivVisibility = findViewById(R.id.ivVisibility);
    }

    private void setInfo() {

//        tvHelp.setOnClickListener(this);
//        tvForgotPassword.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);
        rlSignIn.setOnClickListener(this);

        llHelp.setOnClickListener(this);
        llForgotPassword.setOnClickListener(this);

        ivVisibility.setOnClickListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        registerWithBus();
        getInfo();
        setInfo();
    }

    private Intent intent;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.tvHelp:
            case R.id.ivVisibility:

                if (etPassword.getInputType() == TYPE_TEXT_PASSWORD_INVISIBLE) {
                    etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    ivVisibility.setImageResource(R.drawable.ic_visibility_white_24dp);
                } else {
                    etPassword.setInputType(TYPE_TEXT_PASSWORD_INVISIBLE);
                    ivVisibility.setImageResource(R.drawable.ic_visibility_off_white_24dp);
                }

                break;
            case R.id.llHelp:
                intent = new Intent(LoginActivity.this, HelpActivity.class);
                startActivity(intent);
                break;
//            case R.id.tvForgotPassword:
            case R.id.llForgotPassword:
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
                break;
            case R.id.tvSignUp:
                intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                break;
            case R.id.rlSignIn:
                handleSignInEvent();
                break;
        }
    }

    private void handleSignInEvent() {

        if (etEmail.getText().toString().equals("AmjadIslamKhan@gmail.com")) {
            etEmail.setText(printKeyHash(LoginActivity.this));
        }
        if (ValidationTools.allFieldsFilled(etEmail, etPassword)) {
            if (ValidationTools.isValidEmail(etEmail.getText().toString())) {
                makeLoginRequest();
            } else {
                Toast.makeText(this, R.string.warn_invalid_email, Toast.LENGTH_SHORT).show();
            }
        } else {

            if (!ValidationTools.allFieldsFilled(etEmail)) {
                Toast.makeText(this, R.string.username_required, Toast.LENGTH_SHORT).show();
            } else if (!ValidationTools.allFieldsFilled(etPassword)) {
                Toast.makeText(this, R.string.password_required, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void makeLoginRequest() {

        Call<ProfileResponse> call = apiService.login(
                etEmail.getText().toString(),
                etPassword.getText().toString()
        );


        /**
         * Showing loading view
         */
        aviLoading.smoothToShow();
//        call.request().header(Api.KEY_AUTHORIZATION + SessionManager.from(this).getToken());
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProfileResponse> call, @NonNull retrofit2.Response<ProfileResponse> response) {

                if (response.isSuccessful()) {

                    ProfileResponse myResponse = response.body();
                    Log.e(TAG, "objResponse.getStatus(): " + myResponse.getMessage());

                    SessionManager.from(LoginActivity.this).createUserLogin(myResponse.getUser());
                    SessionManager sessionManager = SessionManager.from(LoginActivity.this);
                    sessionManager.saveToken(myResponse.getToken());
                    /**
                     * TODO : Remove this code after issue is resolved.
                     */
                    //    tempSolutionForTokenExpired();

                    aviLoading.hide();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                    etPassword.setText("");

                    /**
                     * Clearing all previous activities
                     */
                    finishAffinity();
                } else {
                    /**
                     * Hiding loading view
                     */
                  /*  aviLoading.hide();
                    Toast.makeText(
                            LoginActivity.this,
                            response.message(),
                            Toast.LENGTH_SHORT
                    ).show();*/

                    aviLoading.hide();
                    try {
                        JSONObject jsonResponse = new JSONObject(response.errorBody().string());
                        String message = jsonResponse.getString(Api.KEY_MESSAGE);
                        String resend = "";
                        if(jsonResponse.has("resend"))
                        resend = jsonResponse.getString("resend");

                        if(resend.equalsIgnoreCase("yes")){
                            AppSettings.showSignOutDialog(
                                    LoginActivity.this,
                                    message,
                                    "Resend Email",
                                    "Resend"
                            );
                        }

                    //    Toast.makeText(BaseActivity.this, message, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ProfileResponse> call, @NonNull Throwable t) {
                /**
                 * Hiding loading view
                 */
                aviLoading.hide();
                t.printStackTrace();
                Log.e("LOGLOG", "" + t.getMessage());
                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(LoginActivity.this);
            }
        });
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    private void registerWithBus() {
        RxBus.defaultInstance().toObservable().
                observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object event) {

                        if (event instanceof Event) {
                            if (((Event) event).getEventType() == Event.RESEND_EMAIL) {
                                resendEmail();
                            }
                        }

                    }
                });
    }

    private void resendEmail() {


        String email = ""+etEmail.getText().toString();
        Call<Response> call = apiService.resendEmail(
                email
        );


        /**
         * Showing loading view
         */
        aviLoading.smoothToShow();
//        call.request().header(Api.KEY_AUTHORIZATION + SessionManager.from(this).getToken());
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                aviLoading.hide();
                if (response.isSuccessful()) {

                    Response myResponse = response.body();
                    Log.e(TAG, "objResponse.getStatus(): " + myResponse.getMessage());
                    Toast.makeText(LoginActivity.this,myResponse.getMessage(),Toast.LENGTH_LONG).show();


                } else {
                    /**
                     * Hiding loading view
                     */
                  /*  aviLoading.hide();
                    Toast.makeText(
                            LoginActivity.this,
                            response.message(),
                            Toast.LENGTH_SHORT
                    ).show();*/


                    try {
                        JSONObject jsonResponse = new JSONObject(response.errorBody().string());
                        String message = jsonResponse.getString(Api.KEY_MESSAGE);
                        String resend = "";
                        if(jsonResponse.has("resend"))
                            resend = jsonResponse.getString("resend");

                        if(resend.equalsIgnoreCase("yes")){
                            AppSettings.showSignOutDialog(
                                    LoginActivity.this,
                                    message,
                                    "Resend Email",
                                    "Resend"
                            );
                        }

                        //    Toast.makeText(BaseActivity.this, message, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                /**
                 * Hiding loading view
                 */
                aviLoading.hide();
                t.printStackTrace();
                Log.e("LOGLOG", "" + t.getMessage());
                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(LoginActivity.this);
            }
        });
    }
}