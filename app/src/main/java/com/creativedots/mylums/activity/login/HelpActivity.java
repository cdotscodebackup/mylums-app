package com.creativedots.mylums.activity.login;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.base.BaseActivity;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.model.retroFit.helpLogin.HelpLoginResponse;
import com.creativedots.mylums.utils.ValidationTools;
import com.hbb20.CountryCodePicker;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;

public class HelpActivity extends BaseActivity implements View.OnClickListener {

    public static final String TAG = HelpActivity.class.getSimpleName();

    private AVLoadingIndicatorView aviLoading;
    private ImageView ivBack;
    private EditText etFirstname, etLastName, etRollNo, etPhoneNo, etEmail, etMessage;
    private RelativeLayout rlSubmit;
    private CountryCodePicker cc_picker;

    private void getInfo() {
        ivBack = findViewById(R.id.ivBack);

        aviLoading = findViewById(R.id.aviLoading);

        etFirstname = findViewById(R.id.etFirstname);
        etLastName = findViewById(R.id.etLastName);
        etRollNo = findViewById(R.id.etRollNo);
        etPhoneNo = findViewById(R.id.etPhoneNo);
        etEmail = findViewById(R.id.etEmail);
        etMessage = findViewById(R.id.etMessage);
        cc_picker = findViewById(R.id.ccp);
        rlSubmit = findViewById(R.id.rlSubmit);
    }

    private void setInfo() {

        ivBack.setOnClickListener(this);
        rlSubmit.setOnClickListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        getInfo();
        setInfo();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.rlSubmit:
                handleSubmitEventValidation();
                break;
        }
    }

    private void handleSubmitEventValidation() {
        if (ValidationTools.allFieldsFilled(etFirstname, etLastName, etRollNo, etPhoneNo, etEmail, etMessage)) {

            String firstName = etFirstname.getText().toString().trim();
            String lastName = etLastName.getText().toString().trim();
            String rollNo = etRollNo.getText().toString().trim();
            String phoneNo = cc_picker.getSelectedCountryCode() + etPhoneNo.getText().toString().trim();
            String email = etEmail.getText().toString().trim();
            String message = etMessage.getText().toString().trim();

            if (ValidationTools.isValidPhone(etPhoneNo)) {

                if (ValidationTools.isValidEmail(etEmail)) {

                    /**
                     * Calling Help API here
                     */
                    makeHelpRequest(firstName, lastName, rollNo, phoneNo, email, message);
                } else {
                    Toast.makeText(this, R.string.warn_invalid_email, Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(this, R.string.warn_invalid_phone, Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(this, R.string.warn_fill_all_fields, Toast.LENGTH_SHORT).show();
        }
    }

    private void makeHelpRequest(String firstName, String lastName, String rollNo, String phoneNo, String email, String message) {
        Call<HelpLoginResponse> call = apiService.help(firstName, lastName, rollNo, phoneNo, email, message);

        /**
         * Showing loading view
         */
        aviLoading.smoothToShow();


        call.enqueue(new Callback<HelpLoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<HelpLoginResponse> call, @NonNull retrofit2.Response<HelpLoginResponse> response) {

                if (response.isSuccessful()) {
                    /**
                     * hiding loading view
                     */
                    aviLoading.hide();

                    HelpLoginResponse myResponse = response.body();

                    if (myResponse.getStatus().equals(Api.KEY_SUCCESSFULL)) {
                        Toast.makeText(HelpActivity.this,
                                myResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(HelpActivity.this,
                                myResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    /**
                     * hiding loading view
                     */
                    aviLoading.hide();
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<HelpLoginResponse> call, @NonNull Throwable t) {
                /**
                 * hiding loading view
                 */
                aviLoading.hide();
                t.printStackTrace();

                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(HelpActivity.this);
            }
        });
    }


}
