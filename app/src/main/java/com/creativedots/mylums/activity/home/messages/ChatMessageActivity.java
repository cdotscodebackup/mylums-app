package com.creativedots.mylums.activity.home.messages;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.base.BaseActivity;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.adapter.messages.ChatMessageAdapter;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.fragment.dashboardFragments.ViewAlumniProfileFragment;
import com.creativedots.mylums.model.NotificationModel;
import com.creativedots.mylums.model.notification.Message;
import com.creativedots.mylums.model.retroFit.Message.MessageBoardResponse;
import com.creativedots.mylums.model.retroFit.Message.MessageChannel;
import com.creativedots.mylums.model.retroFit.Message.Messages;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.utils.Event;
import com.creativedots.mylums.utils.RxBus;
import com.creativedots.mylums.utils.sharedPreferences.Constants;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;

public class ChatMessageActivity extends BaseActivity implements View.OnClickListener {

    public static final String TAG = ChatMessageActivity.class.getSimpleName();

    private ImageView ivBack, civProfileImage, imgSend;
    private TextView tvName, tvCompany, tvDesignation;
    private EditText etMessage;
    private MessageChannel mChannel;
    private ArrayList<Messages> mMessages;
    private LinearLayout lnProfile;

    private ChatMessageAdapter chatMessageAdapter;
    private RecyclerView rvChatMessages;
    private AVLoadingIndicatorView aviLoading;


    private void getInfo() {
        ivBack = findViewById(R.id.ivBack);
        lnProfile = findViewById(R.id.lnProfile);

//        ivSend = findViewById(R.id.ivSend);

        etMessage = findViewById(R.id.etMessage);

        civProfileImage = findViewById(R.id.civProfileImage);
        rvChatMessages = findViewById(R.id.rvChatMessages);

        civProfileImage = findViewById(R.id.civProfileImage);
        tvCompany = findViewById(R.id.tvCompany);
        tvDesignation = findViewById(R.id.tvDesignation);
        tvName = findViewById(R.id.tvName);

        imgSend = findViewById(R.id.imgSend);
        etMessage = findViewById(R.id.etMessage);

        aviLoading = findViewById(R.id.aviLoading);
    }


    private void setInfo() {

        mChannel = new MessageChannel();
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("channel")) {
                mChannel = getIntent().getExtras().getParcelable("channel");
                if (mChannel.getProfile().getImagepath() != null)
                    Glide.with(this)
                            .load(mChannel.getProfile().getImagepath())
                            .apply(new RequestOptions().centerCrop().circleCrop().error(R.drawable.ic_user_tab_primary).placeholder(R.drawable.ic_user_tab_primary))
                            .into(civProfileImage);
                tvName.setText(mChannel.getProfile().getFirstName() + " " + mChannel.getProfile().getLastName());
                tvDesignation.setText(mChannel.getProfile().getCurrentDesignation());
                tvCompany.setText(mChannel.getProfile().getCurrentCompany());
                if (mChannel.getId() != null) {
                    markAsRead(mChannel.getId());
                }
            }
        }
        registerWithBus();
        ivBack.setOnClickListener(this);
        lnProfile.setOnClickListener(this);
        imgSend.setOnClickListener(this);


        /**
         * initializing ChatMessageAdapter
         */
        initChatMessagesAdapter();

        fetchMessagesList(true);
    }

    private void initChatMessagesAdapter() {
        mMessages = new ArrayList<>();
        chatMessageAdapter = new ChatMessageAdapter(ChatMessageActivity.this, mMessages, mChannel.getProfile().getFirstName());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setReverseLayout(true);
        rvChatMessages.setLayoutManager(linearLayoutManager);
        rvChatMessages.setAdapter(chatMessageAdapter);


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_message);
        getInfo();
        setInfo();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.lnProfile:
                //ViewAlumniProfileFragment viewAlumniProfileFragment = new ViewAlumniProfileFragment();
                //viewAlumniProfileFragment.setAlmuniUserID(mChannel.getProfile().getUserId(), "profile");
                SessionManager.openChat = true;
                NotificationModel notificationModel = new NotificationModel();
                notificationModel.setTypeId(mChannel.getId());
                notificationModel.setProfile(mChannel.getProfile());
                SessionManager.from(this).setNotificationModel(notificationModel);
                Intent intent = getIntent();
                intent.putExtra("userId", mChannel.getProfile().getUserId());
                setResult(RESULT_OK, intent);
                finish();
                break;
            case R.id.imgSend:
                if (!etMessage.getText().toString().isEmpty())
                    sendMessage();
                break;

        }
    }

    private void sendMessage() {

        Messages messages = new Messages();
        messages.setSenderId(SessionManager.from(this).getUser().getUserId());
        messages.setMessage(etMessage.getText().toString());
        messages.setSenderId(SessionManager.from(this).getUser().getUserId());
        messages.setChatroomId(mChannel.getId());
        mMessages.add(0, messages);
        chatMessageAdapter.notifyDataSetChanged();
        rvChatMessages.scrollToPosition(0);
        String msg = etMessage.getText().toString();
        etMessage.setText("");
        Call<Response> call;
        call = apiService.sendMessage(
                msg,
                mChannel.getId()
        );

        // aviLoading.smoothToShow();
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                //  handleSearchApiResponse(response);
                //  aviLoading.hide();

                if (response.isSuccessful()) {
                    Response myResponse = response.body();
                    Toast.makeText(ChatMessageActivity.this, myResponse.getMessage() + "", Toast.LENGTH_SHORT).show();
                    if (myResponse.getStatus().equalsIgnoreCase("Successful")) {
                        setResult(RESULT_OK);
                    }


                } else {
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                //  aviLoading.hide();
                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(ChatMessageActivity.this);
            }
        });
    }


    private void fetchMessagesList(boolean flag) {

        Call<MessageBoardResponse> call;
        call = apiService.MessagesList(Api.Message.GET_URL_MESSAGE_BOARD + mChannel.getId());

        if (flag)
            aviLoading.smoothToShow();
        call.enqueue(new Callback<MessageBoardResponse>() {
            @Override
            public void onResponse(@NonNull Call<MessageBoardResponse> call, @NonNull retrofit2.Response<MessageBoardResponse> response) {
                //  handleSearchApiResponse(response);
                aviLoading.hide();

                if (response.isSuccessful()) {
                    MessageBoardResponse myResponse = response.body();
                    mMessages.clear();
                    mMessages.addAll(myResponse.getData());
                    chatMessageAdapter.notifyDataSetChanged();
                    rvChatMessages.scrollToPosition(0);

                } else {
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<MessageBoardResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                aviLoading.hide();
                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(ChatMessageActivity.this);
            }
        });
    }

    private void markAsRead(String id) {

        Call<Response> call;
        call = apiService.readMessageStatus(id);

        // aviLoading.smoothToShow();
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                //  handleSearchApiResponse(response);
                aviLoading.hide();

                if (response.isSuccessful()) {
                    Response myResponse = response.body();

                } else {
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                aviLoading.hide();
            }
        });
    }

    private void registerWithBus() {
        RxBus.defaultInstance().toObservable().
                observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object event) {

                        if (event instanceof NotificationModel) {
                            NotificationModel notificationModel = (NotificationModel) event;
                            if (notificationModel.getType().equalsIgnoreCase(Constants.KEY_MESSAGE_TYPE)) {

                                Messages message = new Messages();
                                message.setMessage(notificationModel.getMessage().getBody());
                                mMessages.add(0, message);
                                fetchMessagesList(false);
                            }
                        }

                    }
                });
    }
}
