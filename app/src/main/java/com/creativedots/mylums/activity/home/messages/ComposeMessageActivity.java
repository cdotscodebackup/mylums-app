package com.creativedots.mylums.activity.home.messages;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.base.BaseActivity;
import com.creativedots.mylums.adapter.AlumniListAdapter;
import com.creativedots.mylums.adapter.autoComplete.DirectoryAutoCompleteArrayAdapter;
import com.creativedots.mylums.adapter.messages.ChatMessageAdapter;
import com.creativedots.mylums.fragment.dashboardFragments.MessagesFragment;
import com.creativedots.mylums.model.retroFit.Data;
import com.creativedots.mylums.model.retroFit.Message.MessageChannel;
import com.creativedots.mylums.model.retroFit.Message.Messages;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.utils.AppSettings;
import com.creativedots.mylums.utils.ValidationTools;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class ComposeMessageActivity extends BaseActivity implements View.OnClickListener {

    public static final String TAG = ComposeMessageActivity.class.getSimpleName();

    private DirectoryAutoCompleteArrayAdapter directoryAutoCompleteArrayAdapter;

    private Data selectedUserData;
    private ArrayList<Messages> mMessages;
    private ImageView ivBack, ivSend;
    private AutoCompleteTextView actvQuery;
    private TextView tvClearQuery;
    private EditText etMessage;
    private ChatMessageAdapter chatMessageAdapter;
    private RecyclerView rvChatMessages;
    private ArrayList<MessageChannel> messageChannels;

    private void getInfo() {
        ivBack = findViewById(R.id.ivBack);

        ivSend = findViewById(R.id.ivSend);

        etMessage = findViewById(R.id.etMessage);

        actvQuery = findViewById(R.id.actvQuery);
        tvClearQuery = findViewById(R.id.tvClearQuery);
        rvChatMessages = findViewById(R.id.rvChatMessages);
    }

    private void setInfo() {
        ivBack.setOnClickListener(this);
        ivSend.setOnClickListener(this);
        tvClearQuery.setOnClickListener(this);

        /**
         * initializing autoCompleteAdapter
         */
        initAutoCompleteAdapter();
        initChatMessagesAdapter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose_message);
        messageChannels = new ArrayList<>();
        if (getIntent() != null && getIntent().hasExtra("channel_list")) {
            messageChannels = getIntent().getParcelableArrayListExtra("channel_list");
        }
        getInfo();
        setInfo();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.tvClearQuery:
                actvQuery.setText("");
                tvClearQuery.setVisibility(View.GONE);
                break;
            case R.id.ivSend:
                if (ValidationTools.allFieldsFilled(etMessage)) {
                    if (selectedUserData != null && selectedUserData.userId != null)
                        sendMessage();
                    else {
                        Toast.makeText(this, R.string.warn_select_a_contact, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, R.string.warn_enter_message, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void initAutoCompleteAdapter() {
        directoryAutoCompleteArrayAdapter = new DirectoryAutoCompleteArrayAdapter(
                ComposeMessageActivity.this,
                R.layout.actv_directory,
                new ArrayList<Data>(),
                apiService,
                null
        );

        directoryAutoCompleteArrayAdapter.setOnItemClickListener(new DirectoryAutoCompleteArrayAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(Data data) {
                Log.e(TAG, "onItemClicked: " + data.toString());

                isChatExist(data.userId);
                selectedUserData = data;

                actvQuery.setText(data.getFirstName() + " " + data.getLastName());
                actvQuery.dismissDropDown();
                AppSettings.hideKeyboard(ComposeMessageActivity.this, actvQuery);
            }
        });

        actvQuery.setAdapter(directoryAutoCompleteArrayAdapter);

        actvQuery.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    tvClearQuery.setVisibility(View.VISIBLE);
                } else {
                    tvClearQuery.setVisibility(View.GONE);
                }
            }
        });
    }

    private void initChatMessagesAdapter() {
        mMessages = new ArrayList<>();
        chatMessageAdapter = new ChatMessageAdapter(ComposeMessageActivity.this, mMessages, "");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        rvChatMessages.setLayoutManager(linearLayoutManager);
        rvChatMessages.setAdapter(chatMessageAdapter);
    }

    private void sendMessage() {

        AppSettings.hideKeyboard(ComposeMessageActivity.this, getWindow().getDecorView());

        Messages messages = new Messages();
        messages.setMessage(etMessage.getText().toString());
        messages.setSenderId(SessionManager.from(this).getUser().getUserId());
        messages.setChatroomId("");
        mMessages.add(messages);
        chatMessageAdapter.notifyDataSetChanged();
        String msg = etMessage.getText().toString();
        etMessage.setText("");
        Call<Response> call;
        call = apiService.sayHiMessage(
                msg,
                selectedUserData.userId
        );

        // aviLoading.smoothToShow();
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@io.reactivex.annotations.NonNull Call<Response> call, @io.reactivex.annotations.NonNull retrofit2.Response<Response> response) {
                //  handleSearchApiResponse(response);
                //  aviLoading.hide();

                if (response.isSuccessful()) {
                    Response myResponse = response.body();

                    if (myResponse.getStatus().equalsIgnoreCase("Successful")) {
                        setResult(RESULT_OK);
                    }

                } else {
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@io.reactivex.annotations.NonNull Call<Response> call, @io.reactivex.annotations.NonNull Throwable t) {
                t.printStackTrace();
                //  aviLoading.hide();
                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(ComposeMessageActivity.this);
            }
        });
    }

    public void isChatExist(String userID) {
        for (int i = 0; i < messageChannels.size(); i++) {

            if (messageChannels.get(i).getProfile().getUserId().equalsIgnoreCase(userID)) {
                MessageChannel messageChannel = messageChannels.get(i);
                messageChannel.setId(messageChannel.getId());
                messageChannel.setProfile(messageChannel.getProfile());

                Intent startChatMessageActivity = new Intent(this, ChatMessageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable("channel", messageChannel);
                startChatMessageActivity.putExtras(bundle);
                startActivityForResult(startChatMessageActivity, MessagesFragment.RC_CHAT_ACTIVITY_REQUEST);
                finish();
            }
        }
    }
}
