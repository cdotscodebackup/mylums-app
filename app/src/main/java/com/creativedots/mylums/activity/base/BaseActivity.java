package com.creativedots.mylums.activity.base;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.dialogFragments.NoInternetDialog;
import com.creativedots.mylums.network.ApiClient;
import com.creativedots.mylums.network.ApiInterface;
import com.creativedots.mylums.utils.AppSettings;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;
import com.novoda.merlin.Bindable;
import com.novoda.merlin.Connectable;
import com.novoda.merlin.Disconnectable;
import com.novoda.merlin.Merlin;
import com.novoda.merlin.NetworkStatus;

import org.json.JSONException;
import org.json.JSONObject;


public abstract class BaseActivity extends AppCompatActivity {

    public static final String TAG = BaseActivity.class.getSimpleName();

    private AlertDialog alertDialog;

    private DialogInterface.OnClickListener onPositiveClickedListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            if(!AppSettings.isNetworkConnected(BaseActivity.this)){
                Toast.makeText(BaseActivity.this, R.string.warn_no_internet, Toast.LENGTH_SHORT).show();
            }else{
                alertDialog.dismiss();
            }
        }
    };

    private Merlin merlin;
    private NoInternetDialog noInternetDialog;

    private void getInfo(){

    }

    private void setInfo(){

        /**
         * initializing Retro-Fit.
         */
        initRetroFit();

        /**
         * initializing No Internet Dialog.
         */
        initNoInternetDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getInfo();
        setInfo();
    }

    @Override
    protected void onResume() {
        super.onResume();

        /**
         * Setting No Internet Dialog
         */
        setupNoInternetDialog();
    }

    private void initNoInternetDialog() {
        noInternetDialog = new NoInternetDialog();
        noInternetDialog.setCancelable(false);

        merlin = new Merlin.Builder()
                .withConnectableCallbacks()
                .withBindableCallbacks()
                .withDisconnectableCallbacks()
                .build(this);
    }

    private void setupNoInternetDialog() {
        merlin.bind();

        merlin.registerConnectable(new Connectable() {
            @Override
            public void onConnect() {
                if(noInternetDialog.isVisible())
                    noInternetDialog.dismiss();
            }
        });

        merlin.registerBindable(new Bindable() {
            @Override
            public void onBind(NetworkStatus networkStatus) {
                if(!networkStatus.isAvailable()){
                    showNoInternetDialog();
                }
            }
        });

        merlin.registerDisconnectable(new Disconnectable() {
            @Override
            public void onDisconnect() {
                showNoInternetDialog();
            }
        });


        if(!AppSettings.isNetworkConnected(this)){
            showNoInternetDialog();
        }
    }

    private void showNoInternetDialog() {
        if(noInternetDialog.isAdded()) {
            return;
        }
        noInternetDialog.show(getSupportFragmentManager(), NoInternetDialog.TAG);
    }

    @Override
    protected void onPause() {
        merlin.unbind();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            noInternetDialog.onDestroy();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    protected ApiInterface apiService;

    private void initRetroFit() {
        Log.e("BaseActivity",SessionManager.from(this).getToken());
        apiService = ApiClient.getRetrofitClient(SessionManager.from(BaseActivity.this).getToken()).create(ApiInterface.class);
    }

    protected void displayErrorMessage(String errorBody) {
        try {
            JSONObject jsonResponse = new JSONObject(errorBody);
            String message = jsonResponse.getString(Api.KEY_MESSAGE);

            Toast.makeText(BaseActivity.this, message, Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void showNoNetworkDialog(final Context context){
        alertDialog = new AlertDialog.Builder(context)
                .setTitle("No Internet")
                .setMessage("Please check you network!")
                .setPositiveButton("OK", null)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
//                        if(!AppSettings.isNetworkConnected(context)){
//                            alertDialog.show();
//
//                        }
                    }
                })
                .show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(android.R.color.black));
    }

}
