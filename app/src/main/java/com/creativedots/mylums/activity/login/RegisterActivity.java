package com.creativedots.mylums.activity.login;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.WebViewActivity;
import com.creativedots.mylums.activity.base.BaseActivity;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.fragment.WebViewFragment;
import com.creativedots.mylums.model.NotificationModel;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.utils.AppSettings;
import com.creativedots.mylums.utils.Event;
import com.creativedots.mylums.utils.RxBus;
import com.creativedots.mylums.utils.ValidationTools;
import com.creativedots.mylums.utils.sharedPreferences.Constants;
import com.wang.avi.AVLoadingIndicatorView;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    public static final String TAG = RegisterActivity.class.getSimpleName();

    public static final String CONST_TERMS_AND_CONDITIONS = "User Agreement";
    public static final String CONST_PRIVACY_POLICY = "Privacy Policy";

    private View llEmailFoundView, llEmailNotFoundView;
    private EditText etFirstname, etLastName, etRollNo, etEmail,
            etPassword, etConfirmPassword;
    private TextView tvHelp1, tvHelp2, tvSignIn, tvEmailNotFound, tvTermsAndPrivacy;
    private ImageView ivRollStatus, ivEmailStatus;
    private RelativeLayout rlSignIn;
    private AppCompatCheckBox cbAgreement;

    private AVLoadingIndicatorView avi;

    private Boolean emailVerified = false, rollNoVerified = false;

    private void getInfo() {
        avi = findViewById(R.id.avi);

        llEmailFoundView = findViewById(R.id.llEmailFoundView);
        llEmailNotFoundView = findViewById(R.id.llEmailNotFoundView);
        tvEmailNotFound = findViewById(R.id.tvEmailNotFound);

        etFirstname = findViewById(R.id.etFirstname);
        etLastName = findViewById(R.id.etLastName);
        etRollNo = findViewById(R.id.etRollNo);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);

        ivRollStatus = findViewById(R.id.ivRollStatus);
        ivEmailStatus = findViewById(R.id.ivEmailStatus);

        tvHelp1 = findViewById(R.id.tvHelp1);
        tvHelp2 = findViewById(R.id.tvHelp2);
        tvSignIn = findViewById(R.id.tvSignIn);

        rlSignIn = findViewById(R.id.rlSignIn);
        cbAgreement = findViewById(R.id.cbAgreement);

        tvTermsAndPrivacy = findViewById(R.id.tvTermsAndPrivacy);
    }

    private void setInfo() {

        avi.hide();

        tvHelp1.setOnClickListener(this);
        tvHelp2.setOnClickListener(this);
        tvSignIn.setOnClickListener(this);
        rlSignIn.setOnClickListener(this);
        ivRollStatus.setOnClickListener(this);
        ivEmailStatus.setOnClickListener(this);

        KeyboardVisibilityEvent.setEventListener(
                RegisterActivity.this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        // some code depending on keyboard visiblity status
                        if (!isOpen)
                            emailValidation(etEmail.getText().toString());
                    }
                });

        cbAgreement.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    cbAgreement.setBackground(getResources().getDrawable(R.drawable.ic_blue_check_box));
                } else {
                    cbAgreement.setBackground(getResources().getDrawable(R.drawable.ic_circle));
                }
            }
        });

        etRollNo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String rollNo = etRollNo.getText().toString().trim();
                    if (rollNo.length() > 1) {

                        ivRollStatus.setImageResource(R.drawable.ic_checker_loader);

                        Call<Response> call = apiService.verifyRollNo(rollNo);

                        call.enqueue(new Callback<Response>() {
                            @Override
                            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                                Response callResponse = response.body();
                                AppSettings.hideKeyboard(getApplicationContext(), etRollNo);
                                if (response.isSuccessful()) {

                                    if (callResponse.getStatus().equals(Api.KEY_SUCCESSFULL)) {
                                        /**
                                         * Email Found!
                                         */

                                        if (tvEmailNotFound.getText().toString().equalsIgnoreCase(
                                                getResources().getString(R.string.warn_incorrect_roll_number)
                                        )) {
                                            llEmailNotFoundView.setVisibility(View.GONE);
                                        }
                                        ivRollStatus.setImageResource(R.drawable.ic_green_check_true);
                                        rollNoVerified = true;

                                        if (emailVerified) {

                                            /**
                                             * Hiding EmailFoundView just in case.
                                             */
                                            llEmailNotFoundView.setVisibility(View.GONE);
                                            /**
                                             * Showing Help_1 button
                                             */
                                            tvHelp1.setVisibility(View.VISIBLE);
                                            /**
                                             * Showing EmailFoundView.
                                             */
                                            llEmailFoundView.setVisibility(View.VISIBLE);
                                        } /*else {

                                         *//**
                                         * Hiding EmailFoundView just in case.
                                         *//*
                                            llEmailFoundView.setVisibility(View.GONE);
                                            *//**
                                         * Showing ErrorMessageView
                                         *//*
                                            llEmailNotFoundView.setVisibility(View.VISIBLE);

                                            tvEmailNotFound.setText(R.string.try_again_or_contact);
                                            *//**
                                         * Hiding Help_1 button as there is another
                                         * Help_2 button in llEmailNotFoundView.
                                         *//*
                                            tvHelp1.setVisibility(View.GONE);

                                        *//*Toast.makeText(
                                                RegisterActivity.this,
                                                response.message(),
                                                Toast.LENGTH_SHORT
                                        ).show();*//*
                                            ivEmailStatus.setImageResource(R.drawable.ic_red_check_false);
                                            emailVerified = false;
                                        }*/
                                    } else {

                                        /**
                                         * Email not Found!
                                         */
                                        invalidMessages("rollno");
                                        ivRollStatus.setImageResource(R.drawable.ic_red_check_false);
                                        rollNoVerified = false;
                                    }

                                } else {

                                    /**
                                     * Hiding EmailFoundView just in case.
                                     */
                                    llEmailFoundView.setVisibility(View.GONE);
                                    /**
                                     * Showing ErrorMessageView
                                     */
                                    llEmailNotFoundView.setVisibility(View.VISIBLE);

                                    invalidMessages("rollno");
                                    //tvEmailNotFound.setText(R.string.warn_incorrect_roll_number);
                                    /**
                                     * Hiding Help_1 button as there is another
                                     * Help_2 button in llEmailNotFoundView.
                                     */
                                    tvHelp1.setVisibility(View.GONE);

                                /*Toast.makeText(
                                        RegisterActivity.this,
                                        response.message(),
                                        Toast.LENGTH_SHORT
                                ).show();*/
                                    ivRollStatus.setImageResource(R.drawable.ic_red_check_false);
                                    rollNoVerified = false;
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                                t.printStackTrace();
                                ivRollStatus.setImageResource(R.drawable.ic_red_check_false);
                                rollNoVerified = false;

                                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                                    showNoNetworkDialog(RegisterActivity.this);
                            }
                        });
                    } else {
                        if (!etRollNo.getText().toString().trim().isEmpty())
                            resetEmailFoundView(true);
                    }
                }
            }
        });


        etRollNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                ivRollStatus.setImageResource(R.drawable.ic_checker_loader);

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().isEmpty()){
                    ivRollStatus.setImageResource(android.R.color.transparent);
                }
            }
        });

        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
               // ivRollStatus.setImageResource(R.drawable.ic_checker_loader);

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().isEmpty()){
                    ivEmailStatus.setImageResource(android.R.color.transparent);
                }
            }
        });

        etEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    emailValidation(etEmail.getText().toString());
            }
        });

        /**
         * Setting text clicks to open browser.
         */
        setupTermsAndPrivacyLinks();

    }

    private void resetEmailFoundView(Boolean rollNo) {
        if (rollNo) {
            ivRollStatus.setImageResource(R.drawable.ic_red_check_false);
            rollNoVerified = false;
        } else {
            ivEmailStatus.setImageResource(R.drawable.ic_red_check_false);
            emailVerified = false;
        }
        llEmailFoundView.setVisibility(View.GONE);
        etConfirmPassword.setText("");
        etPassword.setText("");
        cbAgreement.setChecked(false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getInfo();
        setInfo();
        registerWithBus();
    }

    private Intent intent;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvHelp1:
            case R.id.tvHelp2:
                intent = new Intent(RegisterActivity.this, HelpActivity.class);
                startActivity(intent);
                break;
            case R.id.tvSignIn:
                intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
                /**
                 * Finishes all previous activities.
                 */
                finishAffinity();
                break;
            case R.id.rlSignIn:
                handleSignUpEvent(view);
                break;
            case R.id.ivRollStatus:
                if (!rollNoVerified) {
                    etRollNo.setText("");
                    ivRollStatus.setImageResource(android.R.color.transparent);
                    invalidMessages("clearrollno");
                }
                break;
            case R.id.ivEmailStatus:
                if (!emailVerified) {
                    etEmail.setText("");
                    ivEmailStatus.setImageResource(android.R.color.transparent);
                    invalidMessages("clearemail");
                }
        }
    }

    private void handleSignUpEvent(View view) {
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();

        if (ValidationTools.allFieldsFilled(etFirstname, etLastName, etRollNo,
                etEmail, etPassword, etConfirmPassword)) {

            if (password.equals(confirmPassword)) {
                if (cbAgreement.isChecked()) {
                    String firstName = etFirstname.getText().toString().trim();
                    String lastName = etLastName.getText().toString().trim();
                    String rollNo = etRollNo.getText().toString().trim();
                    String email = etEmail.getText().toString().trim();

                    Call<Response> call = apiService.register(firstName, lastName, rollNo,
                            email, password, confirmPassword, Api.KEY_SUCCESSFULL);

                    avi.show();
                    call.enqueue(new Callback<Response>() {
                        @Override
                        public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                            avi.hide();

                            Response myResponse = response.body();

                            if (response.isSuccessful()) {

                                if (myResponse.getStatus().equals(Api.KEY_SUCCESSFULL)) {
                                   /* Toast.makeText(RegisterActivity.this,
                                            myResponse.getMessage(), Toast.LENGTH_LONG).show();*/
                                    AppSettings.showSignOutDialog(
                                            RegisterActivity.this,
                                            myResponse.getMessage(),
                                            "Success",
                                            "OK"
                                    );
                                } else {
                                    Toast.makeText(RegisterActivity.this,
                                            myResponse.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                String message;

                                try {
                                    JSONObject jsonResponse = new JSONObject(response.errorBody().string());
                                    message = jsonResponse.getString(Api.KEY_MESSAGE);

                                    Toast.makeText(RegisterActivity.this,
                                            message, Toast.LENGTH_LONG).show();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                        }

                        @Override
                        public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                            avi.hide();
                            t.printStackTrace();

                            if (t.getLocalizedMessage().contains("No address associated with hostname"))
                                showNoNetworkDialog(RegisterActivity.this);
                        }
                    });
                } else {
                    Snackbar.make(view, R.string.warn_terms_and_conditions, Snackbar.LENGTH_SHORT)
                            .show();
                }
            } else {
                Toast.makeText(this, R.string.warn_password_mismatch, Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(this, R.string.warn_fill_all_fields, Toast.LENGTH_SHORT).show();
        }
    }

    public void emailValidation(String email) {

        if (ValidationTools.isValidEmail(email.trim())) {
            ivEmailStatus.setImageResource(R.drawable.ic_checker_loader);

            Call<Response> call = apiService.verifyEmail(email);

            call.enqueue(new Callback<Response>() {
                @Override
                public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                    Response callResponse = response.body();
                    AppSettings.hideKeyboard(getApplicationContext(), etEmail);
                    if (response.isSuccessful()) {

                        if (callResponse.getStatus().equals(Api.KEY_SUCCESSFULL)) {
                            /**
                             * Email Found!
                             */
                            ivEmailStatus.setImageResource(R.drawable.ic_green_check_true);
                            emailVerified = true;

                            if (rollNoVerified) {

                                /**
                                 * Hiding EmailFoundView just in case.
                                 */
                                llEmailNotFoundView.setVisibility(View.GONE);
                                /**
                                 * Showing Help_1 button
                                 */
                                tvHelp1.setVisibility(View.VISIBLE);
                                /**
                                 * Showing EmailFoundView.
                                 */
                                llEmailFoundView.setVisibility(View.VISIBLE);

                            } else {

                                /**
                                 * Hiding EmailFoundView just in case.
                                 */
                                llEmailFoundView.setVisibility(View.GONE);
                                /**
                                 * Showing ErrorMessageView
                                 */
                                llEmailNotFoundView.setVisibility(View.VISIBLE);

                                tvEmailNotFound.setText(R.string.warn_incorrect_roll_number);
                                /**
                                 * Hiding Help_1 button as there is another
                                 * Help_2 button in llEmailNotFoundView.
                                 */
                                tvHelp1.setVisibility(View.GONE);


                            }
                        } else {

                            try {
                                displayErrorMessage(response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            invalidMessages("email");
                            /**
                             * Email not Found!
                             */
                            ivEmailStatus.setImageResource(R.drawable.ic_red_check_false);
                            emailVerified = false;
                        }

                    } else {

                        try {
                            displayErrorMessage(response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        /**
                         * Hiding EmailFoundView just in case.
                         */
                        llEmailFoundView.setVisibility(View.GONE);
                        /**
                         * Showing ErrorMessageView
                         */
                        llEmailNotFoundView.setVisibility(View.VISIBLE);

                        invalidMessages("email");
                        /**
                         * Hiding Help_1 button as there is another
                         * Help_2 button in llEmailNotFoundView.
                         */
                        tvHelp1.setVisibility(View.GONE);

                                /*Toast.makeText(
                                        RegisterActivity.this,
                                        response.message(),
                                        Toast.LENGTH_SHORT
                                ).show();*/
                        ivEmailStatus.setImageResource(R.drawable.ic_red_check_false);
                        emailVerified = false;
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    ivEmailStatus.setImageResource(R.drawable.ic_red_check_false);
                    emailVerified = false;

                    if (t.getLocalizedMessage().contains("No address associated with hostname"))
                        showNoNetworkDialog(RegisterActivity.this);
                }
            });
        } else {
            if (!etEmail.getText().toString().trim().isEmpty()) {
                resetEmailFoundView(false);
                invalidMessages("email");
            }
        }
    }

    private void setupTermsAndPrivacyLinks() {
        ClickableSpan csTermsAndConditions = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                startBrowserForUrl(getString(R.string.const_terms_and_conditions), Api.URL_TERMS_AND_CONDITIONS);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(Color.YELLOW); // specific color for this link
            }
        };
        ClickableSpan csPrivacyPolicy = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                startBrowserForUrl(getString(R.string.label_privacy_policy), Api.URL_PRIVACY_POLICY);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(Color.YELLOW); // specific color for this link
            }
        };

        makeLinks(tvTermsAndPrivacy, new String[]{
                CONST_TERMS_AND_CONDITIONS, CONST_PRIVACY_POLICY
        }, new ClickableSpan[]{
                csTermsAndConditions, csPrivacyPolicy
        });
    }

    public void makeLinks(TextView textView, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(textView.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            int startIndexOfLink = textView.getText().toString().indexOf(link);
            spannableString.setSpan(clickableSpan, startIndexOfLink,
                    startIndexOfLink + link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        textView.setHighlightColor(
                Color.TRANSPARENT); // prevent TextView change background when highlight
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
    }

    private void startBrowserForUrl(String title, String url) {
        Intent intent = new Intent(RegisterActivity.this, WebViewActivity.class);
        intent.putExtra(WebViewActivity.KEY_TITLE, title);
        intent.putExtra(WebViewActivity.KEY_URL, url);
        startActivity(intent);
    }

    private void invalidMessages(String type) {
        if (type.equalsIgnoreCase("email")) {
            if (ivRollStatus.getDrawable() != null)
                if (ivRollStatus.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_red_check_false).getConstantState() && !rollNoVerified) {
                    tvEmailNotFound.setText(R.string.try_again_or_contact_both);
                } else {
                    tvEmailNotFound.setText(R.string.try_again_or_contact);
                }
            else
                tvEmailNotFound.setText(R.string.try_again_or_contact);
        } else if (type.equalsIgnoreCase("clearrollno")) {
            if (ivEmailStatus.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_red_check_false).getConstantState()) {
                tvEmailNotFound.setText(R.string.try_again_or_contact);
            } else {
                tvEmailNotFound.setText("");
            }
        } else if (type.equalsIgnoreCase("clearemail")) {
            if (ivRollStatus.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_red_check_false).getConstantState()) {
                tvEmailNotFound.setText(R.string.warn_incorrect_roll_number);
            } else {
                tvEmailNotFound.setText("");
            }
        } else {
            if (ivEmailStatus.getDrawable() != null) {
                if (ivEmailStatus.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_red_check_false).getConstantState() && !emailVerified) {
                    tvEmailNotFound.setText(R.string.try_again_or_contact_both);
                } else {
                    tvEmailNotFound.setText(R.string.warn_incorrect_roll_number);
                }
            } else {
                tvEmailNotFound.setText(R.string.warn_incorrect_roll_number);
            }
        }
    }


    private void registerWithBus() {
        RxBus.defaultInstance().toObservable().
                observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object event) {

                        if (event instanceof Event) {
                            if (((Event) event).getEventType() == Event.FINISH_ACTIVITY) {
                                finish();
                            }
                        }

                    }
                });
    }
}
