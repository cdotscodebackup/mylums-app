package com.creativedots.mylums.activity.home;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.base.BaseActivity;
import com.creativedots.mylums.activity.home.messages.ChatMessageActivity;
import com.creativedots.mylums.activity.login.LoginActivity;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.fragment.DashboardFragment;
import com.creativedots.mylums.fragment.HelpAndSupportFragment;
import com.creativedots.mylums.fragment.bottomBarFragments.NotificationsFragment;
import com.creativedots.mylums.fragment.bottomBarFragments.QrCodeFragment;
import com.creativedots.mylums.fragment.bottomBarFragments.QrCodeInfoFragment;
import com.creativedots.mylums.fragment.dashboardFragments.DirectorySearchFragment;
import com.creativedots.mylums.fragment.dashboardFragments.DiscountPartnersFragments;
import com.creativedots.mylums.fragment.dashboardFragments.EventDetailsFragment;
import com.creativedots.mylums.fragment.dashboardFragments.MessagesFragment;
import com.creativedots.mylums.fragment.dashboardFragments.SettingsFragment;
import com.creativedots.mylums.fragment.dashboardFragments.UpcomingEventsFragment;
import com.creativedots.mylums.fragment.dashboardFragments.UpdateProfileFragment;
import com.creativedots.mylums.fragment.dashboardFragments.ViewAlumniProfileFragment;
import com.creativedots.mylums.model.NotificationModel;
import com.creativedots.mylums.model.retroFit.Data;
import com.creativedots.mylums.model.retroFit.Message.MessageChannel;
import com.creativedots.mylums.model.retroFit.Message.Messages;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.model.retroFit.notification.Notification;
import com.creativedots.mylums.model.retroFit.notification.NotificationResponse;
import com.creativedots.mylums.service.LocationUpdateWorker;
import com.creativedots.mylums.utils.AppSettings;
import com.creativedots.mylums.utils.Event;
import com.creativedots.mylums.utils.RxBus;
import com.creativedots.mylums.utils.ValidationTools;
import com.creativedots.mylums.utils.sharedPreferences.Constants;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.linkedin.platform.LISessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import androidx.work.BackoffPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private int height;
    private int width;

    private DrawerLayout drawer;
    private NavigationView navigationView;
    private DashboardFragment dashboardFragment;


    private static final int FRAGMENT_CONTAINER_ID = R.id.flContainer;

    private ImageView ivCloseDrawer;
    private CircleImageView civProfileImage;
    private TextView tvName, tvEmail;
    private TextView tvEvents, tvDirectory, tvProfile, tvDiscountPartners, tvInvites,
            tvMessages, tvHelpAndSupport, tvSettings, tvLogout;
    private RelativeLayout rlMenu, rlHelp, rlQrCode, rlNotifications;
    public TextView tvNotifications;
    private String unreadNotifications = "";

    private ImageView ivMenu, ivHelp, ivQrCode, ivNotifications;

    private void getInfo() {

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        ivCloseDrawer = findViewById(R.id.ivCloseDrawer);
        civProfileImage = findViewById(R.id.civProfileImage);
        tvName = findViewById(R.id.tvName);
        tvEmail = findViewById(R.id.tvEmail);

        tvEvents = findViewById(R.id.tvEvents);
        tvDirectory = findViewById(R.id.tvDirectory);
        tvProfile = findViewById(R.id.tvProfile);
        tvDiscountPartners = findViewById(R.id.tvDiscountPartners);
        tvInvites = findViewById(R.id.tvInvites);
        tvMessages = findViewById(R.id.tvMessages);
        tvHelpAndSupport = findViewById(R.id.tvHelpAndSupport);
        tvSettings = findViewById(R.id.tvSettings);
        tvLogout = findViewById(R.id.tvLogout);

        rlMenu = findViewById(R.id.rlMenu);
        rlHelp = findViewById(R.id.rlHelp);
        rlQrCode = findViewById(R.id.rlQrCode);
        rlNotifications = findViewById(R.id.rlNotifications);

        ivMenu = findViewById(R.id.ivMenu);
        ivHelp = findViewById(R.id.ivHelp);
        ivQrCode = findViewById(R.id.ivQrCode);
        ivNotifications = findViewById(R.id.ivNotifications);

        tvNotifications = findViewById(R.id.tvNotifications);
    }

    private void setInfo() {

        dashboardFragment = new DashboardFragment();
        dashboardFragment.setOnItemClickedListener(onItemClickedListener);


        initUserInfo();

        /**
         * initializing Click Listeners
         */
        initClickListeners();

        /**
         * Setting fade effect Transparent
         */
//        drawer.setScrimColor(getResources().getColor(android.R.color.transparent));

        getSupportFragmentManager().beginTransaction()
                .replace(FRAGMENT_CONTAINER_ID, dashboardFragment, DashboardFragment.TAG)
                .commit();

        if (!ValidationTools.isPermissionGranted(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION))
            ValidationTools.requestPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        checkNotifications();
    }

    public void initUserInfo() {
        Data data = SessionManager.from(this).getUser();

        Glide.with(this).load(data.getImage())
                .apply(new RequestOptions()
                        .centerCrop()
                        .dontTransform()
                        .placeholder(R.drawable.ic_user_place_holder)
                ).into(civProfileImage);

        tvName.setText(data.getFirstName() + " " + data.getLastName());
        tvEmail.setText(data.getEmail());
        tvNotifications.setText(unreadNotifications);
    }

    private void initClickListeners() {
        ivCloseDrawer.setOnClickListener(this);

        tvEvents.setOnClickListener(this);
        tvDirectory.setOnClickListener(this);
        tvProfile.setOnClickListener(this);
        tvDiscountPartners.setOnClickListener(this);
        tvInvites.setOnClickListener(this);
        tvMessages.setOnClickListener(this);
        tvHelpAndSupport.setOnClickListener(this);
        tvSettings.setOnClickListener(this);
        tvLogout.setOnClickListener(this);

        rlMenu.setOnClickListener(this);
        rlHelp.setOnClickListener(this);
        rlQrCode.setOnClickListener(this);
        rlNotifications.setOnClickListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        printKeyHash(this);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        registerWithBus();
        sendTokenToServer(FirebaseInstanceId.getInstance().getToken());
        fetchNotifications();
        startLocationUpdates();
        /**
         * Setting Background Image
         */

        setBackground(getResources().getDrawable(R.drawable.bg_dashboard));
        setContentView(R.layout.activity_main);

        getInfo();
        setInfo();
    }

    private void setSelectedTab(ImageView imageViews) {
        SessionManager.openChat = false;
        changeBottomTabs(true);
        if (imageViews.getId() == R.id.ivHelp)
            ivHelp.setImageDrawable(getResources().getDrawable(R.drawable.ic_help_support_blue));
        else if (imageViews.getId() == R.id.ivQrCode)
            ivQrCode.setImageDrawable(getResources().getDrawable(R.drawable.ic_qr_code_blue));
        else if (imageViews.getId() == R.id.ivNotifications)
            ivNotifications.setImageDrawable(getResources().getDrawable(R.drawable.ic_notificaitons_blue));
    }

    private void clearSelectedTabs(ImageView... imageViews) {
        for (ImageView view : imageViews) {
            view.setColorFilter(android.R.color.white);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivCloseDrawer:
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.rlMenu:
                clearSelectedTabs(ivHelp, ivQrCode, ivNotifications);
                drawer.openDrawer(GravityCompat.START);
                break;
            case R.id.rlHelp:
                setSelectedTab(ivHelp);
                replaceFragment(new HelpAndSupportFragment(), HelpAndSupportFragment.TAG, true, true);
                break;
            case R.id.rlQrCode:
                setSelectedTab(ivQrCode);
                QrCodeFragment qrCodeFragment = new QrCodeFragment();
                replaceFragment(qrCodeFragment, QrCodeFragment.TAG, true, true);
                break;
            case R.id.rlNotifications:
                //   Toast.makeText(this, "Notifications", Toast.LENGTH_SHORT).show();
                // TODO : Not Ready
                setSelectedTab(ivNotifications);
                replaceFragment(new NotificationsFragment(), NotificationsFragment.TAG, true, true);
                break;
            case R.id.tvEvents:
            case R.id.tvDirectory:
            case R.id.tvProfile:
            case R.id.tvDiscountPartners:
            case R.id.tvInvites:
            case R.id.tvMessages:
            case R.id.tvHelpAndSupport:
            case R.id.tvSettings:
            case R.id.tvLogout:
                SessionManager.openChat = false;
                handleNavigationClicks(view);
                break;
        }
    }

    /**
     * NavBar Controls
     */
    private void handleNavigationClicks(View view) {
        switch (view.getId()) {
            case R.id.tvEvents:
                replaceFragment(new UpcomingEventsFragment(), UpcomingEventsFragment.TAG, true, true);
                break;
            case R.id.tvDirectory:
                replaceFragment(new DirectorySearchFragment(), DirectorySearchFragment.TAG, true, true);
                break;
            case R.id.tvProfile:
                replaceFragment(new UpdateProfileFragment(), UpdateProfileFragment.TAG, true, true);
                break;
            case R.id.tvDiscountPartners:
                replaceFragment(new DiscountPartnersFragments(), DiscountPartnersFragments.TAG, true, true);
                break;
            case R.id.tvInvites:
                showShareDialog();
                break;
            case R.id.tvMessages:
                replaceFragment(new MessagesFragment(), MessagesFragment.TAG, true, true);
                break;
            case R.id.tvHelpAndSupport:
                replaceFragment(new HelpAndSupportFragment(), HelpAndSupportFragment.TAG, true, true);
                break;
            case R.id.tvSettings:
                replaceFragment(new SettingsFragment(), SettingsFragment.TAG, true, true);
//                replaceFragment(new MeetRequestFragment(), MeetRequestFragment.TAG, true,false);
                break;
            case R.id.tvLogout:
                showSignOutDialog();
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
    }

    /**
     * Fragment Controls
     */
    private DashboardFragment.OnItemClickedListener onItemClickedListener = new DashboardFragment.OnItemClickedListener() {
        @Override
        public void onItemClicked(View view) {
            switch (view.getId()) {
                case R.id.cvEvents:
                    // TODO : Not Ready.
                    replaceFragment(new UpcomingEventsFragment(), UpcomingEventsFragment.TAG, true, false);
                    break;
                case R.id.cvDirectory:
                    // TODO : Not Ready.
                    replaceFragment(new DirectorySearchFragment(), DirectorySearchFragment.TAG, true, false);
                    break;
                case R.id.cvProfile:
                    // TODO : Not Ready.
                    replaceFragment(new UpdateProfileFragment(), UpdateProfileFragment.TAG, true, false);
                    break;
                case R.id.cvDiscounts:
                    replaceFragment(new DiscountPartnersFragments(), DiscountPartnersFragments.TAG, true, false);
                    break;
                case R.id.cvInvites:
                    showShareDialog();
                    break;
                case R.id.cvMessages:
                    // TODO : Not Ready.
                    replaceFragment(new MessagesFragment(), MessagesFragment.TAG, true, false);
                    break;
            }
        }
    };

    private FragmentManager fmanager;

    public void replaceFragment(Fragment f, String tag, Boolean addOnTop, Boolean clearBackStack) {


        fmanager = getSupportFragmentManager();
        try {


            if (clearBackStack) {
                fmanager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }

            if (fmanager.getBackStackEntryCount() > 0)
                if ((fmanager.getBackStackEntryAt(fmanager.getBackStackEntryCount() - 1)).getName().equalsIgnoreCase(tag))
                    return;
            FragmentTransaction ft = fmanager.beginTransaction();
            /*ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right, android.R.anim.slide_out_right);*/

            ft.replace(FRAGMENT_CONTAINER_ID, f, tag);
            ft.addToBackStack(tag);
            ft.commit();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("LOGFRAG", e.toString());

        }
    }

    public void addFragment(Fragment f, String tag, Boolean addOnTop, Boolean clearBackStack) {
        fmanager = getSupportFragmentManager();
        try {
            if (clearBackStack) {
                fmanager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
            FragmentTransaction ft = fmanager.beginTransaction();
            //ft.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out);
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out,
                    android.R.animator.fade_in, android.R.animator.fade_out);

            ft.add(FRAGMENT_CONTAINER_ID, f, tag);
            ft.addToBackStack(tag);
            ft.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showShareDialog() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getInviteMessage(/*getString(R.string.sample_invite_code), Constants.INVITE_URL*/));
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @NonNull
    private String getInviteMessage(/*String inviteCode, String url*/) {
        return SessionManager.from(this).getUser().getFirstName().trim() + " " + SessionManager.from(this).getUser().getLastName().trim() + " has invited you to download "
                + getString(R.string.app_name)
                + " application. Please download the app from following links:\n" +
                "Google Play store: https://play.google.com/store/apps/details?id=com.creativedots.mylums\n" +
                "iOS App Store: https://itunes.apple.com/pk/app/mylums/id1444768354?mt=8";

    }

    private void showSignOutDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_dialog);
        TextView no = dialog.findViewById(R.id.txtNo);
        TextView yes = dialog.findViewById(R.id.txtYes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeLogoutRequest();
                dialog.dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.show();
    }

    private void makeLogoutRequest() {

        Call<Response> call = apiService.logout();
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                if (response.isSuccessful()) {

                    SessionManager.from(MainActivity.this).clear();
                    Response myResponse = response.body();
                    Log.e(TAG, "myResponse.getStatus(): " + myResponse.toString());

                    if (myResponse.getStatus().equals(Api.KEY_SUCCESSFULL)) {
                        Toast.makeText(MainActivity.this,
                                myResponse.getMessage(), Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                        /**
                         * Clearing out all previous Activities
                         */
                        finishAffinity();
                    } else {
                        Toast.makeText(MainActivity.this,
                                myResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.errorBody().string());
                        String message = jsonResponse.getString(Api.KEY_MESSAGE);

                        Toast.makeText(MainActivity.this,
                                message, Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(MainActivity.this);
            }
        });

    }

    private void setBackground(Drawable backgroundDrawable) {

        Glide.with(MainActivity.this).asDrawable()
                .load(backgroundDrawable)
                .apply(new RequestOptions().centerCrop().placeholder(new ColorDrawable(getResources().getColor(R.color.colorPrimary))))
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        getWindow().setBackgroundDrawable(resource);
                        return true;
                    }
                }).submit(width, height);
    }

    @Override
    public void onBackPressed() {
        String tag = "";
        if (fmanager == null)
            fmanager = getSupportFragmentManager();
        int backStackCount = fmanager.getBackStackEntryCount();
        if (backStackCount > 0) {
            tag = fmanager.getBackStackEntryAt(backStackCount - 1).getName();

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else if (SessionManager.openChat) {
                if (tag != null &&
                        tag.equalsIgnoreCase(ViewAlumniProfileFragment.TAG)) {
                    SessionManager.openChat = false;
                    if (SessionManager.getNotificationModel().getTypeId() != null)
                        moveToMessageScreen(SessionManager.getNotificationModel());
                    getSupportFragmentManager().popBackStack();

                } else {
                    super.onBackPressed();
                }
            } else if (tag.equalsIgnoreCase(UpdateProfileFragment.TAG)) {
                Event event = new Event();
                event.setEventType(Event.UPDATE_PROFILE_BACK);
                RxBus.defaultInstance().send(event);

            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    private void sendTokenToServer(final String token) {

        Call<Response> call = apiService.sendToken(
                token, "android"
        );
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                if (response.isSuccessful()) {

                    //SessionManager.from(MainActivity.this).clear();
                    Response myResponse = response.body();
                    Log.e(TAG, "fcm token: " + token);


                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.errorBody().string());
                        String message = jsonResponse.getString(Api.KEY_MESSAGE);

                        Toast.makeText(MainActivity.this,
                                message, Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(MainActivity.this);
            }
        });
    }

    private void fetchNotifications() {

        Call<NotificationResponse> call = apiService.notifcationList();

        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(@NonNull Call<NotificationResponse> call, @NonNull retrofit2.Response<NotificationResponse> response) {

                if (response.isSuccessful()) {

                    NotificationResponse myResponse = response.body();
                    Log.e(TAG, "myResponse.getStatus(): " + myResponse.toString());

                    if (myResponse.getStatus().equals(Api.KEY_SUCCESSFULL)) {
                        ArrayList<Notification> notifications = myResponse.getData();
                        int unread = 0;
                        for (int i = 0; i < notifications.size(); i++) {
                            if (!notifications.get(i).getStatus().equalsIgnoreCase("read")) {
                                unread++;
                            }
                        }
                        unreadNotifications = "" + unread;
                        tvNotifications.setText(unreadNotifications);
                        if (unread > 0)
                            tvNotifications.setVisibility(View.VISIBLE);
                        else
                            tvNotifications.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<NotificationResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });

    }


    public void moveToMessageScreen(NotificationModel notificationModel) {

        if (notificationModel != null) {


            MessageChannel messageChannel = new MessageChannel();
            messageChannel.setId(notificationModel.getTypeId());
            messageChannel.setProfile(notificationModel.getProfile());

            Intent startChatMessageActivity = new Intent(this, ChatMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelable("channel", messageChannel);
            startChatMessageActivity.putExtras(bundle);
            startActivityForResult(startChatMessageActivity, MessagesFragment.RC_CHAT_ACTIVITY_REQUEST);
        }
    }

    private void moveToEventDetail(NotificationModel notificationModel) {

        EventDetailsFragment eventDetailsFragment = new EventDetailsFragment();
        eventDetailsFragment.setEventID(notificationModel.getTypeId());
        replaceFragment(eventDetailsFragment, EventDetailsFragment.TAG, true, false);
    }

    private void moveToDiscountDetail(NotificationModel notificationModel) {
        DiscountPartnersFragments discountPartnersFragments = new DiscountPartnersFragments();
        Bundle bundle = new Bundle();
        bundle.putBoolean("notification", true);
        discountPartnersFragments.setArguments(bundle);
        replaceFragment(discountPartnersFragments, DiscountPartnersFragments.TAG, true, false);
    }

    private void moveToTinderDetail(NotificationModel notificationModel) {

        NotificationsFragment Notification = new NotificationsFragment();
        replaceFragment(Notification, NotificationsFragment.TAG, true, false);
    }

    private void moveToAlumniProfile(String userId) {
        ViewAlumniProfileFragment viewAlumniProfileFragment = new ViewAlumniProfileFragment();
        viewAlumniProfileFragment.setAlmuniUserID(userId, "profile");
        replaceFragment(
                viewAlumniProfileFragment,
                ViewAlumniProfileFragment.TAG,
                true,
                false
        );
    }

    private void checkNotifications() {
        if (getIntent().getExtras() != null) {
            NotificationModel notification = null;
            if (getIntent().getExtras().containsKey("notification")) {
                notification = (NotificationModel) getIntent().getExtras().getParcelable("notification");
            } else if (getIntent().getExtras().containsKey("type_id")) {
                notification = new NotificationModel().convertToNotification(getIntent().getExtras());
            }
            if (notification != null) {

                if (notification.getType().equalsIgnoreCase(Constants.KEY_MESSAGE_TYPE)) {
                    markNotificaitonAsRead(notification.getNotificatoinId());
                    moveToMessageScreen(notification);
                } else if (notification.getType().equalsIgnoreCase(Constants.KEY_EVENT_TYPE)) {
                    markNotificaitonAsRead(notification.getNotificatoinId());
                    moveToEventDetail(notification);
                } else if (notification.getType().equalsIgnoreCase(Constants.KEY_DISCOUNT_TYPE)) {
                    markNotificaitonAsRead(notification.getNotificatoinId());
                    moveToDiscountDetail(notification);
                } else if (notification.getType().equalsIgnoreCase(Constants.KEY_TINDER_TYPE)) {
                    moveToTinderDetail(notification);
                } else if (notification.getType().equalsIgnoreCase(Constants.KEY_PROFILE_TYPE)) {
                    markNotificaitonAsRead(notification.getNotificatoinId());
                    moveToAlumniProfile(notification.getProfile().getUserId());
                } else {
                    moveToTinderDetail(notification);
                }
                Log.e("quote", "" + notification.getBody());
            }
        } else if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            // Activity was brought to front and not created,
            // Thus finishing this will get us to the last viewed activity
            finish();
            return;
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        Log.d(TAG, "onSaveInstanceState");
    }

    private void registerWithBus() {
        RxBus.defaultInstance().toObservable().
                observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object event) {

                        if (event instanceof Event) {

                            if (((Event) event).getEventType() == Event.REFRESH_NOTIFICATION_LIST) {
                                fetchNotifications();
                            }
                        }

                    }
                });
    }

    public void changeNotificationCount(String unreadNotifications) {
        if (unreadNotifications.isEmpty())
            tvNotifications.setVisibility(View.GONE);
        else {
            tvNotifications.setVisibility(View.VISIBLE);
        }
        tvNotifications.setText(unreadNotifications);
    }


    public void changeBottomTabs(boolean flag) {
        if (flag) {

            ivHelp.setImageDrawable(getResources().getDrawable(R.drawable.ic_help_support));
            ivQrCode.setImageDrawable(getResources().getDrawable(R.drawable.ic_qr_code));
            ivNotifications.setImageDrawable(getResources().getDrawable(R.drawable.ic_notifications));

        } else {
            String tag = "";
            if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                tag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();

            if (tag.equalsIgnoreCase(NotificationsFragment.TAG)) {
                ivNotifications.setImageDrawable(getResources().getDrawable(R.drawable.ic_notificaitons_blue));
            } else if (tag.equalsIgnoreCase(QrCodeFragment.TAG)) {
                ivQrCode.setImageDrawable(getResources().getDrawable(R.drawable.ic_qr_code_blue));
            } else if (tag.equalsIgnoreCase(HelpAndSupportFragment.TAG)) {
                ivHelp.setImageDrawable(getResources().getDrawable(R.drawable.ic_help_support_blue));
            }
        }

        Log.e("changeBottomTabs", "changeBottomTabs called");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        LISessionManager.getInstance(getApplicationContext()).onActivityResult(this, requestCode, resultCode, data);

    }

    public void startLocationUpdates() {
        String uuidStr = SessionManager.from(this).getWorkManagerID();
        if (uuidStr != null && !uuidStr.isEmpty()) {
            WorkManager.getInstance().cancelWorkById(UUID.fromString(uuidStr));
        }
        //Task is not scheduled, start it
        PeriodicWorkRequest.Builder photoCheckBuilder = new PeriodicWorkRequest.Builder(LocationUpdateWorker.class, 10, TimeUnit.MINUTES);
        photoCheckBuilder.setBackoffCriteria(BackoffPolicy.LINEAR, 1, TimeUnit.MINUTES);
        // Create the actual work object:
        PeriodicWorkRequest photoCheckWork = photoCheckBuilder.build();
        SessionManager.from(this).setWorkManagerID(photoCheckWork.getId().toString());
        // Then enqueue the recurring task:
        WorkManager.getInstance().enqueue(photoCheckWork);
    }

    private void markNotificaitonAsRead(String id) {

        Call<Response> call;
        call = apiService.readNotificationStatus(id);

        // aviLoading.smoothToShow();
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@io.reactivex.annotations.NonNull Call<Response> call, @io.reactivex.annotations.NonNull retrofit2.Response<Response> response) {
                //  handleSearchApiResponse(response);

                if (response.isSuccessful()) {

                    MainActivity activity = MainActivity.this;
                    Response myResponse = response.body();
                    if (activity != null) {
                        if (!tvNotifications.getText().toString().isEmpty()) {
                            int num = Integer.parseInt(tvNotifications.getText().toString());
                            if (num <= 1)
                                changeNotificationCount("");
                            else
                                changeNotificationCount((num - 1) + "");
                        }
                    }
                } else {
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(@io.reactivex.annotations.NonNull Call<Response> call, @io.reactivex.annotations.NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }
}
