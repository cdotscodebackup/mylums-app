package com.creativedots.mylums.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.base.BaseActivity;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.activity.login.LoginActivity;
import com.creativedots.mylums.model.NotificationModel;
import com.creativedots.mylums.utils.AppSettings;
import com.creativedots.mylums.utils.AutoResizeTextView;
import com.creativedots.mylums.utils.Typewriter;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;

import java.util.Map;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends BaseActivity {

    public static final String TAG = SplashActivity.class.getSimpleName();
    private static final int SPLASH_TIME_IN_SECS = 3;
    Animation anim;
    View blueLogo, view;
    LinearLayout lnLogotxt, lnLast;
    TextView txtCGL;
    NotificationModel mNotificationModel;
    private AlertDialog alertDialog;
    private final Handler mHandler = new Handler();
    /*private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            SessionManager sessionManager = SessionManager.from(SplashActivity.this);
            if(sessionManager.isLoggedIn() && !sessionManager.getToken().isEmpty()){
                *//**
     * Taking user to Home Screen.
     *//*
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                finishAffinity();
            }else{
                */
    /**
     * Taking user to Login Screen.
     *//*
                Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
//                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up);
                finishAffinity();
            }

        }
    };*/

    private DialogInterface.OnClickListener onPositiveClickedListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            if (!AppSettings.isNetworkConnected(SplashActivity.this)) {
                Toast.makeText(SplashActivity.this, R.string.warn_no_internet, Toast.LENGTH_SHORT).show();
            } else {
                alertDialog.dismiss();
                // makeTokenRequest();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Fabric.with(this, new Crashlytics());

        view = findViewById(R.id.whiteLogo);
        blueLogo = findViewById(R.id.blueLogo);
        lnLogotxt = findViewById(R.id.lnLogotxt);
        txtCGL = findViewById(R.id.txtCLG);
        lnLast = findViewById(R.id.lnLast);


        if (getIntent().getExtras() != null) {
            /*for (String key: bundle.keySet()) {
                Log.e ("myApplication", key + " is a key in the bundle");
            }*/

            if (getIntent().getExtras().containsKey("notification")) {
                 mNotificationModel = (NotificationModel) getIntent().getExtras().getParcelable("notification");
                 checkNextScreen();
            } else if (getIntent().getExtras().containsKey("type_id")) {
                NotificationModel notificationModel = new NotificationModel();
                mNotificationModel = notificationModel.convertToNotification(getIntent().getExtras());
                checkNextScreen();
            }else if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
                // Activity was brought to front and not created,
                // Thus finishing this will get us to the last viewed activity
                finish();
                return;
            } else {
                loadAnimation();
            }
        }else if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            // Activity was brought to front and not created,
            // Thus finishing this will get us to the last viewed activity
            finish();
            return;
        } else {
            loadAnimation();
        }


        //   showNoInternetDialog();
    }


  /*  private void showNoInternetDialog() {
        if(!AppSettings.isNetworkConnected(this)){
            alertDialog = new AlertDialog.Builder(this)
                    .setTitle("No Internet")
                    .setMessage("Please try again!")
                    .setPositiveButton("Refresh", onPositiveClickedListener)
                    .setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            if(!AppSettings.isNetworkConnected(SplashActivity.this)){
                                alertDialog.show();

                            }
                        }
                    })
                    .show();
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(android.R.color.black));
        }else{
            if(SessionManager.from(SplashActivity.this).isLoggedIn()) {
                makeTokenRequest();
            }else {
                Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                startActivity(intent);
                overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up);
                finishAffinity();
            }
        }
    }
*/
   /* private void makeTokenRequest() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Call<Response> call = apiService.getToken(SessionManager.USERNAME,SessionManager.PASSWORD);
                call.enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                        if(response.isSuccessful()){

                            Response res = response.body();

                            if (res != null) {

                                *//**
     * Saving Token for later use.
     *//*
                                SessionManager sessionManager = SessionManager.from(SplashActivity.this);
                                if(sessionManager.isLoggedIn()){
                                    *//**
     * Taking user to Home Screen.
     *//*
                                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                                    finishAffinity();
                                }else{
                                    */

    /**
     * Taking user to Login Screen.
     *//*
                                    Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
//                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up);
                                    finishAffinity();
                                }

                            }else{
                                Log.e(TAG, "Not Token found!");
                            }

                        }else{
                            try {
                                if (response.errorBody() != null) {
                                    Log.e(TAG, "response.errorBody() : " + response.errorBody().string());
                                    SessionManager.from(SplashActivity.this).clear();
                                    Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up);
                                    finishAffinity();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {

                    }
                });

            }
        }, 1500);
    }*/
    public void loadAnimation() {

        blueLogo.setVisibility(View.GONE);
        anim = AnimationUtils.loadAnimation(this, R.anim.scale);
        view.startAnimation(anim);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                blueLogoAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void blueLogoAnimation() {
        anim = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.fade_in);
        blueLogo.startAnimation(anim);
        blueLogo.setVisibility(View.VISIBLE);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                lnLogotxtAnim();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


    }

    public void lnLogotxtAnim() {
        lnLogotxt.setVisibility(View.VISIBLE);
        anim = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.slide_down_animation);
        lnLogotxt.startAnimation(anim);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                Typewriter writer = findViewById(R.id.txtCLG);
                writer.setCharacterDelay(5);
                writer.animateText("Connecting Luminities Globally");

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void txtLast() {

        lnLast.setVisibility(View.VISIBLE);
        anim = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.slide_up_animation);
        lnLast.startAnimation(anim);

        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                checkNextScreen();

           /*     txtCGL.setVisibility(View.VISIBLE);
                anim = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.anim_ltr);
                txtCGL.startAnimation(anim);*/

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        //    mHandler.postDelayed(mRunnable, 2000);
    }

    @Override
    public void onPause() {
        super.onPause();
        //  mHandler.removeCallbacks(mRunnable);
    }

    private void checkNextScreen(){
        SessionManager sessionManager = SessionManager.from(SplashActivity.this);
        if (sessionManager.isLoggedIn() && !sessionManager.getToken().isEmpty()) {

            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            if(mNotificationModel != null){
                intent.putExtra("notification",mNotificationModel);
            }

            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            finishAffinity();
        } else {

            /**
             * Taking user to Login Screen.
             */

            if (!sessionManager.getShowWizard()) {
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                finishAffinity();
            } else {
                startActivity(new Intent(SplashActivity.this, LumsHelpScreenActivity.class));
            }
        }
    }

}
