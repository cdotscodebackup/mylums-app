/*
 * Copyright 2012 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.creativedots.mylums.activity;

import com.creativedots.mylums.R;
import com.creativedots.mylums.model.eventDetail.GalleryProduct;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;

import android.os.Bundle;

import java.util.ArrayList;

/**
 * A simple YouTube Android API demo application which shows how to create a simple application that
 * shows a YouTube Video in a {@link YouTubePlayerFragment}.
 * <p>
 * Note, this sample app extends from {@link YouTubeFailureRecoveryActivity} to handle errors, which
 * itself extends {@link YouTubeBaseActivity}. However, you are not required to extend
 * {@link YouTubeBaseActivity} if using {@link YouTubePlayerFragment}s.
 */
public class YoutubePlayActivity extends YouTubeFailureRecoveryActivity {

    public static final String TAG = YoutubePlayActivity.class.getSimpleName();

    public static final String KEY_BUNDLE = "bundle";
    public static final String KEY_VIDEOS = "videos";
    public static final String KEY_SELECTED_VIDEO = "selectedVideoPosition";
    private static final String SAMPLE_YOUTUBE_VIDEO_ID = "nCgQDjiotG0";

    private Bundle bundle;
    private int SELECTED_VIDEO_POSITION = 0;
    private String YOUTUBE_VIDEO_ID = SAMPLE_YOUTUBE_VIDEO_ID;

    private ArrayList<GalleryProduct> mGalleryProducts = new ArrayList<>();

    private void getInfo(){

        bundle = getIntent().getBundleExtra(KEY_BUNDLE);

        if(bundle != null){
            if(bundle.containsKey(KEY_VIDEOS)){
                mGalleryProducts = bundle.getParcelableArrayList(KEY_VIDEOS);
                SELECTED_VIDEO_POSITION = bundle.getInt(KEY_SELECTED_VIDEO);
            }
        }

    }

    private void setInfo() {

        String videoPath = mGalleryProducts.get(SELECTED_VIDEO_POSITION).getVideoPath();

        YOUTUBE_VIDEO_ID = videoPath.split("v=")[1];

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_youtube);

        getInfo();
        setInfo();

        YouTubePlayerFragment youTubePlayerFragment =
            (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
        youTubePlayerFragment.initialize("AIzaSyAOaz8qA9WhLthbdTur5lxuVRpjS93lfz0", this);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {

        if (!wasRestored) {
          player.cueVideo(YOUTUBE_VIDEO_ID);
        }
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
    }

}
