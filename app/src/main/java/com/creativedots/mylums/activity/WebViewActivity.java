package com.creativedots.mylums.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.databinding.ActivityWebViewBinding;
import com.creativedots.mylums.utils.sharedPreferences.Constants;

public class WebViewActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = WebViewActivity.class.getSimpleName();

    public static final String KEY_TITLE = "title";
    public static final String KEY_URL = "url";

    private ActivityWebViewBinding binding;

    private String TITLE, URL;

    private void getInfo(){
        TITLE = getIntent().getStringExtra(KEY_TITLE);
        URL = getIntent().getStringExtra(KEY_URL);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setInfo(){

        /**
         * Showing loading view
         */
        binding.aviLoading.show();

        binding.tvTitle.setText(TITLE);

        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(WebViewActivity.this, description, Toast.LENGTH_SHORT).show();
            }
            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });
        binding.webView.loadUrl(URL);

        binding.webView.setWebViewClient(new WebViewClient(){

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                /**
                 * Hiding Loading view on finished webpage loading
                 */
                binding.aviLoading.hide();
            }
        });

        binding.ivBack.setOnClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_web_view);

        getInfo();
        setInfo();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivBack:
                finish();
                break;
        }
    }
}
