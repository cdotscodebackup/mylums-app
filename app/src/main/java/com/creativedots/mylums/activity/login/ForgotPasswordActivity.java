package com.creativedots.mylums.activity.login;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.base.BaseActivity;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.utils.AppSettings;
import com.creativedots.mylums.utils.ValidationTools;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener {

    public static final String TAG = ForgotPasswordActivity.class.getSimpleName();

    private AVLoadingIndicatorView aviLoading;
    private ImageView ivBack;
    private EditText etEmail;
    private TextView tvHelp;
    private RelativeLayout rlSubmit;

    private void getInfo(){
        aviLoading = findViewById(R.id.aviLoading);
        ivBack = findViewById(R.id.ivBack);
        etEmail = findViewById(R.id.etEmail);
        tvHelp = findViewById(R.id.tvHelp);
        rlSubmit = findViewById(R.id.rlSubmit);
    }

    private void setInfo(){
        ivBack.setOnClickListener(this);
        tvHelp.setOnClickListener(this);
        rlSubmit.setOnClickListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        getInfo();
        setInfo();
    }

    @Override
    protected void onResume() {
        super.onResume();

        AppSettings.showKeyboard(this,etEmail);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppSettings.hideKeyboard(this,etEmail);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivBack:
                finish();
                break;
            case R.id.tvHelp:
                startActivity(new Intent(ForgotPasswordActivity.this, HelpActivity.class));
                break;
            case R.id.rlSubmit:
                handleSubmitEvent();
                break;
        }
    }

    private void handleSubmitEvent() {

        /**
         * Hiding Keyboard
         */
        AppSettings.hideKeyboard(ForgotPasswordActivity.this, getWindow().getDecorView());

        if(ValidationTools.allFieldsFilled(etEmail)){
            if(ValidationTools.isValidEmail(etEmail)){

                makeForgotPasswordRequest();

            }else{
                Toast.makeText(this, R.string.warn_invalid_email, Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this, R.string.warn_enter_email, Toast.LENGTH_SHORT).show();
        }
    }

    private void makeForgotPasswordRequest() {
        Call<Response> call = apiService.forgotPassword(etEmail.getText().toString().trim());

        /**
         * Showing loading view
         */
        aviLoading.smoothToShow();

        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                if(response.isSuccessful()){
                    /**
                     * Hiding loading view
                     */
                    aviLoading.hide();

                    Response myResponse = response.body();
                    Log.e(TAG, "myResponse.getStatus(): " + myResponse.getMessage());
                    Toast.makeText(ForgotPasswordActivity.this,
                            myResponse.getMessage(), Toast.LENGTH_SHORT
                    ).show();

                    /**
                     * Finishing Activity
                     */
                    finish();

                }else{

                    /**
                     * Hiding loading view
                     */
                    aviLoading.hide();

                    try {
                        JSONObject jsonResponse = new JSONObject(response.errorBody().string());
                        String message = jsonResponse.getString(Api.KEY_MESSAGE);

                        Toast.makeText(ForgotPasswordActivity.this,
                                message, Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                /**
                 * Hiding loading view
                 */
                aviLoading.hide();
                t.printStackTrace();

                if(t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(ForgotPasswordActivity.this);
            }
        });
    }
}
