package com.creativedots.mylums.utils;

import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/*
 * Created by m.imran
 * Senior Software Engineer at
 * BhimSoft on 07/08/2017.
 */

public class DateConversion {

    public static final String[] TIME_FORMATS = new String[]{
            "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ",
            "HH:mm:ss.SSSSSS",
            "HH:mm:ss'Z'"
    };
    public static Locale locale = Locale.ENGLISH;


    //Used Below

    // 14 Dec 2018 02:00 PM
    public static final SimpleDateFormat sENGLISH_MONTH_TIME = new SimpleDateFormat("dd MMM yy hh:mm a", locale);

    // 14 Dec 2018
    public static final SimpleDateFormat sENGLISH_MONTH = new SimpleDateFormat("dd MMM yy", locale);
    // Patternt e,g 14 December 2018
    public static final SimpleDateFormat sDATE_ENGLISH_MONTH = new SimpleDateFormat("dd MMMM yyyy", locale);
    //14-12-2018
    public static final SimpleDateFormat sSIMPLE_DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy", locale);
    //    30-10-2018 06:04 AM
    public static final SimpleDateFormat sDATE_AND_TIME_FORMAT = new SimpleDateFormat("dd-MM-yyyy hh:mm a", locale);
    //2018-12-14
    public static final SimpleDateFormat sDATE_TIME_FORMAT1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ", locale);//2017-07-31T02:38:56.086057

    //USED ABOVE
    private static final SimpleDateFormat sDATE_TIME_FORMAT = new SimpleDateFormat("yyy-MM-dd'T'HH:mm:ss.SSSSSS", locale);//2017-07-31T02:38:56.086057
    public static final SimpleDateFormat sSTANDARD_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", locale);
    private static final SimpleDateFormat sDAY_FORMAT = new SimpleDateFormat("d", locale);
    private static final SimpleDateFormat sMONTH_FORMAT = new SimpleDateFormat("MMMM", locale);
    public static final SimpleDateFormat sTIME_FORMAT = new SimpleDateFormat("HH:mm:ss", locale);
    private static final SimpleDateFormat sTIME_FORMAT2 = new SimpleDateFormat("HH:mm:ss.SSSSSS", locale);
    public static final SimpleDateFormat sTIME_FORMAT_AM_PM = new SimpleDateFormat("hh:mm a", locale);
    private static final SimpleDateFormat sTIME_FORMAT_AM_PM_WITHOUT_AMPM = new SimpleDateFormat("hh:mm", locale);
    private static final SimpleDateFormat sAM_PM = new SimpleDateFormat("a", locale);
    private static final SimpleDateFormat sTIME_FORMAT_HOURS_MINS = new SimpleDateFormat("HH 'Hours' ,mm 'Mins'", locale);
    private static final SimpleDateFormat sTIME_MINUTES = new SimpleDateFormat("mm", locale);
    private static final SimpleDateFormat sTIME_HOURS = new SimpleDateFormat("HH", locale);
    public static final SimpleDateFormat sSIMPLE_DATE_FORMAT_IOS_STANDARD = new SimpleDateFormat("dd / MM / yy", locale);



    /////////////////

    public static Date sDATE_TIME_FORMAT1parse(String dateTime) throws ParseException {
        return sDATE_TIME_FORMAT1.parse(removeMicroSeconds(dateTime));
    }

    public static String sDATE_TIME_FORMAT1format(Date date) {
        return sDATE_TIME_FORMAT1.format(date);
    }

    public static String sDATE_TIME_FORMAT1format(long time) {
        return sDATE_TIME_FORMAT1.format(time);
    }

    //////////////

    public static Date sDATE_TIME_FORMATparse(String dateTime) throws ParseException {
        return sDATE_TIME_FORMAT.parse(removeMicroSeconds(dateTime));
    }

    public static String sDATE_TIME_FORMATformat(Date date) {
        return sDATE_TIME_FORMAT.format(date);
    }

    public static String sDATE_TIME_FORMATformat(long time) {
        return sDATE_TIME_FORMAT.format(time);
    }

    ////////////////////

    public static Date sTIME_FORMAT2parse(String dateTime) throws ParseException {
        return sTIME_FORMAT2.parse(removeMicroSeconds(dateTime));
    }

    public static String sTIME_FORMAT2format(Date date) {
        return sTIME_FORMAT2.format(date);
    }

    public static String sTIME_FORMAT2format(long time) {
        return sTIME_FORMAT2.format(time);
    }


    public static String formatTime24hrs(String time /*01:23 am*/) {
        try {
            return sTIME_FORMAT.format(sTIME_FORMAT_AM_PM.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }


    /**
     * @param dateTime LIke 2017-07-31T02:38:56.086057
     * @return Like 01:23 (actually in AM/Pm
     */
    public static String formatTimeAmPm(String dateTime) {
        if (TextUtils.isEmpty(dateTime)) {
            return "";
        }
        try {
            return sTIME_FORMAT_AM_PM.format(sDATE_TIME_FORMATparse(dateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static String formateTimeAmPm(Date date) {
        String retVal = formatTimeWithOutAmPm(date) + " " + formatAmPm(date);
        return retVal;
        //return sTIME_FORMAT_AM_PM.format(date);

    }

    public static String format24HrInto12(String time /*01:23 am*/) {
        if (TextUtils.isEmpty(time)) {
            return "";
        }
        try {
            return sTIME_FORMAT_AM_PM.format(sTIME_FORMAT.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }


    /**
     * @param dateTime LIke 2017-07-31T02:38:56.086057
     * @return Like 01:23 (actually in AM/Pm
     */
    public static String formatTimeWithOutAmPm(String dateTime) {
        if (TextUtils.isEmpty(dateTime)) {
            return "";
        }
        try {
            if (dateTime.contains("Z")) {
                return sTIME_FORMAT_AM_PM_WITHOUT_AMPM.format(sDATE_TIME_FORMAT1parse(dateTime));
            } else {
                return sTIME_FORMAT_AM_PM_WITHOUT_AMPM.format(sDATE_TIME_FORMATparse(dateTime));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static Date parseDate(String dateTime) {
        if (TextUtils.isEmpty(dateTime)) {
            return new Date();
        }
        try {
            if (dateTime.contains("Z")) {
                return sDATE_TIME_FORMAT1parse(dateTime);
            } else {
                return sDATE_TIME_FORMATparse(dateTime);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }


    public static String day(Date dateTime) {
        if (dateTime == null) {
            return "";
        }
        return sDAY_FORMAT.format(dateTime);
    }

    public static String month(Date dateTime) {
        if (dateTime == null) {
            return "";
        }
        String retVal = sMONTH_FORMAT.format(dateTime);
        Locale locale = Locale.getDefault();
        if (locale.getLanguage().equalsIgnoreCase("ar")) {
            switch (retVal.toLowerCase()) {
                case "january":
                    retVal = "كانون الثاني";
                    break;
                case "february":
                    retVal = "شباط";
                    break;
                case "march":
                    retVal = "آذار";
                    break;
                case "april":
                    retVal = "نيسان";
                    break;
                case "may":
                    retVal = "أيار";
                    break;
                case "june":
                    retVal = "حزيران";
                    break;
                case "july":
                    retVal = "تموز";
                    break;
                case "august":
                    retVal = "آب";
                    break;
                case "september":
                    retVal = "أيلول";
                    break;
                case "october":
                    retVal = "تشرين الأول";
                    break;
                case "november":
                    retVal = "تشرين الثاني";
                    break;
                case "december":
                    retVal = "كانون الأول";
                    break;
            }
        }
        return retVal;
    }


    /**
     * @param dateTime LIke 2017-07-31T02:38:56.086057
     * @return Like AM/PM
     */
    public static String formatAmPm(String dateTime) {
        if (TextUtils.isEmpty(dateTime)) {
            return "";
        }
        try {
            if (dateTime.contains("Z")) {
                return sAM_PM.format(sDATE_TIME_FORMAT1parse(dateTime));
            } else {
                return sAM_PM.format(sDATE_TIME_FORMATparse(dateTime));
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * @param dateTime LIke 2017-07-31T02:38:56.086057
     * @return Like 01:23 (actually in AM/Pm
     */
    public static String formatTimeWithOutAmPm(Date dateTime) {
        if (dateTime == null) {
            return "";
        }
        return sTIME_FORMAT_AM_PM_WITHOUT_AMPM.format(dateTime);
    }

    /**
     * @param dateTime LIke 2017-07-31T02:38:56.086057
     * @return Like AM/PM
     */
    public static String formatAmPm(Date dateTime) {
        if (dateTime == null) {
            return "";
        }
        String retVal = sAM_PM.format(dateTime);
        Locale locale = Locale.getDefault();
        if (locale.getLanguage().equalsIgnoreCase("ar")) {
            retVal = toArabicAmPm(retVal);
        }
        return retVal;
    }

    public static String toArabicAmPm(String value) {
        String retVal = value;
        if (retVal.toLowerCase().contains("a")) {
            retVal = "صباحا";
        } else if (retVal.toLowerCase().contains("p")) {
            retVal = "مساء";
        }

        return retVal;
    }

    public static String getRelativeDateTime(String dateTime) {
        return getRelativeDateTime(parseDate(dateTime));
    }

    private static String getRelativeDateTime(Date dateTime) {
        long now = System.currentTimeMillis();
        return DateUtils.getRelativeTimeSpanString(dateTime.getTime(), now, DateUtils.DAY_IN_MILLIS).toString();
    }

    public static String removeMicroSeconds(String time) {
        if (time.contains(".")) {
            String[] parts = time.split("\\.");
            if (parts.length == 2) {
                if (time.length() >= 30) {
                    String part1 = parts[0];
                    String part2 = ".000000";
                    String part3 = parts[1].substring(6, parts[1].length());
                    time = part1 + part2 + part3;
                } else if (parts[1].length() == 6) {
                    time = parts[0] + ".000000";
                }
            }
        }
        return time;
    }

    public static Date parseTime(String time) {
        if (TextUtils.isEmpty(time)) {
            return new Date();
        }


        for (String s : TIME_FORMATS) {
            try {

                time = removeMicroSeconds(time);

                SimpleDateFormat utcTimeFormat = new SimpleDateFormat(s, locale);
                utcTimeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date date = utcTimeFormat.parse(time);
                Calendar calendar = Calendar.getInstance();
                TimeZone timeZone = calendar.getTimeZone();
                utcTimeFormat.setTimeZone(timeZone);
                String formattedDate = utcTimeFormat.format(date);
                return utcTimeFormat.parse(formattedDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return new Date();
    }


/*
    public static Date parseTime2(String time) throws Exception{

        //2018-08-06T01:24:00.999999+05:00

        String[] tokens = time.split(".");
        String[] subTokens = null;
        String seprator = "";
        if(tokens.length == 2 && tokens[1].contains("+")){
            subTokens = tokens[1].split("\\+");
            seprator = "+";
        }else if(tokens.length == 2 && tokens[0].contains("-")){
            subTokens = tokens[1].split("-");
            seprator = "-";
        }

        if(subTokens.length == 2){
            String formmattedDate =  tokens[0] + "." + String.format("%06d", Long.parseLong(subTokens[0])%1000000)
        }



        for (String s : TIME_FORMATS) {
            try {
                SimpleDateFormat utcTimeFormat = new SimpleDateFormat(s, locale);
                utcTimeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date date = utcTimeFormat.parse(time);
                Calendar calendar = Calendar.getInstance();
                TimeZone timeZone = calendar.getTimeZone();
                utcTimeFormat.setTimeZone(timeZone);
                String formattedDate = utcTimeFormat.format(date);
                return utcTimeFormat.parse(formattedDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return new Date();
    }
*/


    public static String parseTime(long time) {
        return sDATE_TIME_FORMAT1format(time);
    }


    public static String getHoursMinutesTime(String time) {
        return sTIME_FORMAT_HOURS_MINS.format(parseTime(time));
    }

    public static String getMinutes(String time) {
        return sTIME_MINUTES.format(parseTime(time));
    }

    public static String getHours(String time) {
        return sTIME_HOURS.format(parseTime(time));
    }


    public static String parseDateInReviewFormat(String date) {
        try {
            Date jdate = parseDate(date);
            SimpleDateFormat format = new SimpleDateFormat("MMM dd,yyyy", locale);
            return format.format(jdate);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String parsePaymentTimeFormat(Date input) {
        try {
            return sDATE_TIME_FORMATformat(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String parsePaymentTimeToLocalDateFormat(String input) {
        try {
            return sSIMPLE_DATE_FORMAT.format(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String parsePaymentTimeToLocalTimeFormat(Date input) {
        try {
            return sTIME_FORMAT_AM_PM.format(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String convertMillisecondToTime(long millisec) {
        return sTIME_FORMAT_AM_PM.format(millisec);

    }

    public static String getNotificationDateHeader(long timeInMillisec) {
        SimpleDateFormat format = new SimpleDateFormat("d", locale);
        String date = format.format(timeInMillisec);

        if (date.endsWith("1") && !date.endsWith("11"))
            format = new SimpleDateFormat("EE MMM d'st', yyyy", locale);
        else if (date.endsWith("2") && !date.endsWith("12"))
            format = new SimpleDateFormat("EE MMM d'nd', yyyy", locale);
        else if (date.endsWith("3") && !date.endsWith("13"))
            format = new SimpleDateFormat("EE MMM d'rd', yyyy", locale);
        else
            format = new SimpleDateFormat("EE MMM d'th', yyyy", locale);

        return format.format(timeInMillisec);

    }

    public static String getNotificationDateHeaderArabic(long timeInMillisec) {
        SimpleDateFormat format = new SimpleDateFormat("EE MMM d, yyyy", Locale.getDefault());

        String date = format.format(timeInMillisec);
        date = date.toLowerCase().replace("١", "1")
                .replace("٢", "2")
                .replace("٣", "3")
                .replace("٤", "4")
                .replace("٥", "5")
                .replace("٦", "6")
                .replace("٧", "7")
                .replace("٨", "8")
                .replace("٩", "9")
                .replace("٠", "0");

        return date;
    }


//    public static String getHrsFromDate(Date date) {
//
//        try {
//            return findDifferenceInHrs(currentDate(), date);
//        } catch (Exception e) {
//            return "";
//        }
//
//
//    }
//
//
//    public static String getMinsFromDate(Date date) {
//
//        try {
//            return findDifferenceInHrs(currentDate(), date);
//        } catch (Exception e) {
//            return "";
//        }
//    }
//
//    public static String getDaysFromDate(Date date) {
//
//        try {
//            return findDifferenceInHrs(currentDate(), date);
//        } catch (Exception e) {
//            return "";
//        }
//    }
//
//    public static String findDifferenceInMins(Date startDate, Date endDate) {
//        //milliseconds
//        long different = endDate.getTime() - startDate.getTime();
//
//        System.out.println("startDate : " + startDate);
//        System.out.println("endDate : " + endDate);
//        System.out.println("different : " + different);
//
//        long secondsInMilli = 1000;
//        long minutesInMilli = secondsInMilli * 60;
//
//        different = different % minutesInMilli;
//
//        return String.valueOf(different);
//
//
//    }

//
//    public static String findDifferenceInHrs(Date startDate, Date endDate) {
//        //milliseconds
//        long different = endDate.getTime() - startDate.getTime();
//
//        System.out.println("startDate : " + startDate);
//        System.out.println("endDate : " + endDate);
//        System.out.println("different : " + different);
//
//        long secondsInMilli = 1000;
//        long minutesInMilli = secondsInMilli * 60;
//        long hoursInMilli = minutesInMilli * 60;
//
//        long elapsedHours = different / hoursInMilli;
//        different = different % hoursInMilli;
//        return String.valueOf(different);
//
//    }
//
//    public static String findDifferenceInDays(Date startDate, Date endDate) {
//        //milliseconds
//        long different = endDate.getTime() - startDate.getTime();
//
//        System.out.println("startDate : " + startDate);
//        System.out.println("endDate : " + endDate);
//        System.out.println("different : " + different);
//
//        long secondsInMilli = 1000;
//        long minutesInMilli = secondsInMilli * 60;
//        long hoursInMilli = minutesInMilli * 60;
//        long daysInMilli = hoursInMilli * 24;
//
//        long elapsedDays = different / daysInMilli;
//        different = different % daysInMilli;
//
//        return String.valueOf(different);
//    }

    public static String findDifference(Date startDate, Date endDate) {

        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        if (elapsedDays < 0) {
            elapsedDays = 0;
        }
        if (elapsedHours < 0) {
            elapsedHours = 0;
        }
        if (elapsedMinutes < 0) {
            elapsedMinutes = 0;
        }


        return (elapsedDays < 10 ? "0" + elapsedDays : elapsedDays) + "&" + (elapsedHours < 10 ? "0"
                + elapsedHours : elapsedHours) + "&" + (elapsedMinutes < 10 ? "0" + elapsedMinutes : elapsedMinutes);
    }


//    private static Date currentDate() {
//        try {
//            Calendar c = Calendar.getInstance();
//            String date = sDATE_TIME_FORMATformat(c.getTime());
//            // SimpleDateFormat df = new SimpleDateFormat(sDATE_TIME_FORMAT);
//            return sDATE_TIME_FORMATparse(date);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    public static Date currentDateWithZ() {
        try {
            Calendar c = Calendar.getInstance();

            String date = sDATE_TIME_FORMAT1format(c.getTime());
            // SimpleDateFormat df = new SimpleDateFormat(sDATE_TIME_FORMAT);
            return sDATE_TIME_FORMATparse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date currentDateWithZ(Long additionInMilliSeconds) {
        try {
            Calendar c = Calendar.getInstance();
            c.add(Calendar.SECOND, (int) (additionInMilliSeconds / 1000));

            String date = sDATE_TIME_FORMAT1format(c.getTime());
            // SimpleDateFormat df = new SimpleDateFormat(sDATE_TIME_FORMAT);
            return sDATE_TIME_FORMATparse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDayNameOfWeek() {
        Calendar sCalendar = Calendar.getInstance();
        String dayShortName = sCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, locale);
        return dayShortName.toLowerCase();
    }


    public static String convert24HrTimeTo12Hr(String time) {
        try {
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss", locale);
            // _24HourSDF.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = _24HourSDF.parse(time);


            Calendar calendar = Calendar.getInstance();
            TimeZone timeZone = calendar.getTimeZone();

            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a", locale);
            _12HourSDF.setTimeZone(timeZone);

            return _12HourSDF.format(date);

        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return sTIME_FORMAT_AM_PM.format(new Date());

    }


    /*public static String uTCtoLocalDate(String time) {

        try {
            String timeZone = Calendar.getInstance().getTimeZone().getID();
            Date date = sTIME_FORMAT.parse(time);
            Date local = new Date(date.getTime() + TimeZone.getTimeZone(timeZone).getOffset(date.getTime()));
            return sTIME_FORMAT.format(local);

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
          return "";
    }

    public static String localToUTC(String time) {
        Date date = null;
        String s;
        try {
            date = sTIME_FORMAT.parse(time);
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            s = sdf.format(date);
            Log.e("DATEUTC",""+s);
            return s;
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }*/

    public static String calculateReachTime(String arriveTime, String driveTimes, int mintWindows) {
        Date date = DateConversion.parseTime(arriveTime);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);


        if (driveTimes != null) {
            String driveTime[] = driveTimes.split(":");

            if (driveTime.length > 0) {
                int hours = Integer.parseInt(driveTime[0]);

                calendar.add(Calendar.HOUR, -hours);

            }
            if (driveTime.length > 1) {
                int min = Integer.parseInt(driveTime[1]);

                calendar.add(Calendar.MINUTE, -min);
            }

            if (driveTime.length > 2) {
                int seconds = Integer.parseInt(driveTime[2]);
                calendar.add(Calendar.SECOND, -seconds);
            }
        }

        calendar.add(Calendar.MINUTE, -mintWindows);

        return DateConversion.parseTime(calendar.getTimeInMillis());

    }

    public static String changeTimeStandard(String dateTime) {
        try {
            return sDATE_TIME_FORMAT1format(sTIME_FORMAT.parse(dateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String changeTimeStandardFromAMPM(String dateTime) {
        try {
            return sDATE_TIME_FORMAT1format(sTIME_FORMAT_AM_PM.parse(dateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String changeStandardToHours(String dateTime) {
        try {
            return sTIME_FORMAT.format(sDATE_TIME_FORMAT1parse(dateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static Date changeStandardToHoursDate(String dateTime) {
        try {
            return sTIME_FORMAT_AM_PM.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }


    public static Date changeStringToHours(String time) {
        try {
            return sTIME_FORMAT.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }


    public static String currentDateStringWithZ() {

        return sDATE_TIME_FORMAT1format(currentDateWithZ());
    }

    public static String currentDateStringWithZ(Long additionInMilliSeconds) {

        return sDATE_TIME_FORMAT1format(currentDateWithZ(additionInMilliSeconds));
    }

    public static String onlyDateFromDateWithZ(String date) {


        try {
            return sSTANDARD_DATE_FORMAT.format(sDATE_TIME_FORMAT1parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return sSTANDARD_DATE_FORMAT.format(new Date());
    }

    public static String fromTimeFormat1ToTimeFormat(String time) {


        try {
            return sTIME_FORMAT.format(sTIME_FORMAT2parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return sTIME_FORMAT.format(new Date());
    }

    public static String toHourMin(String time) {


        try {
            return sTIME_FORMAT_AM_PM_WITHOUT_AMPM.format(sTIME_FORMAT2parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return sTIME_FORMAT_AM_PM_WITHOUT_AMPM.format(new Date());
    }

    public static String fromTimeFormat1ToAMPM(String time) {


        try {
            return sTIME_FORMAT_AM_PM.format(sTIME_FORMAT.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return sTIME_FORMAT_AM_PM.format(new Date());
    }

    public static String getYYYYMMDDTODDMMMYY(String time) {
        try {
            String s = sENGLISH_MONTH.format(sSIMPLE_DATE_FORMAT.parse(time));
            return s;
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return sENGLISH_MONTH.format(new Date());

    }

    public static String getDateEngMonth(String time) {
        try {
            return sDATE_ENGLISH_MONTH.format(sSTANDARD_DATE_FORMAT.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return sDATE_ENGLISH_MONTH.format(new Date());

    }

    public static String convertTimeToLocal(String time) {

        Date date = null;
        try {
            sTIME_FORMAT_AM_PM.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sTIME_FORMAT_AM_PM.parse(time);
            sTIME_FORMAT_AM_PM.setTimeZone(TimeZone.getDefault());
            return sTIME_FORMAT_AM_PM.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sTIME_FORMAT_AM_PM.format(new Date());
    }


    public static String covertTimeTo_Local(String updatedAt){

        Date date = null;
        try {
            sDATE_AND_TIME_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sDATE_AND_TIME_FORMAT.parse(updatedAt);
            sDATE_AND_TIME_FORMAT.setTimeZone(TimeZone.getDefault());
            return sDATE_AND_TIME_FORMAT.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sDATE_AND_TIME_FORMAT.format(new Date());
    }


    /*public static String getTimeOrDate(String updatedAt){

        Date date = null;
        try {
            sSIMPLE_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sSIMPLE_DATE_FORMAT.parse(updatedAt);
            sSIMPLE_DATE_FORMAT.setTimeZone(TimeZone.getDefault());
            sENGLISH_MONTH.setTimeZone(TimeZone.getDefault());
            Log.e("DATEDATE","Pass: " + date + " Cur: " + sSIMPLE_DATE_FORMAT.format(new Date()));
            if(sSIMPLE_DATE_FORMAT.format(date).equals(sSIMPLE_DATE_FORMAT.format(new Date()))){
                sDATE_AND_TIME_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
                date = sDATE_AND_TIME_FORMAT.parse(updatedAt);
                return sTIME_FORMAT_AM_PM.format(date);
            }else{
                return sENGLISH_MONTH.format(date);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return sENGLISH_MONTH.format(new Date());
    }
*/
    public static String getTimeOrDateTime(String updatedAt){

        Date date = null;
        try {
            sDATE_AND_TIME_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sDATE_AND_TIME_FORMAT.parse(updatedAt);
            sSIMPLE_DATE_FORMAT.setTimeZone(TimeZone.getDefault());
            sENGLISH_MONTH_TIME.setTimeZone(TimeZone.getDefault());

            boolean isDateEqual = sSIMPLE_DATE_FORMAT.format(date).equals(sSIMPLE_DATE_FORMAT.format(new Date()));
            Log.e("DATEDATE","Pass: " + sSIMPLE_DATE_FORMAT.format(date) + " Cur: " + sSIMPLE_DATE_FORMAT.format(new Date()));
            sDATE_AND_TIME_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sDATE_AND_TIME_FORMAT.parse(updatedAt);

            if(isDateEqual){
                return sTIME_FORMAT_AM_PM.format(date);
            }else{
                return sENGLISH_MONTH_TIME.format(date);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return sTIME_FORMAT_AM_PM.format(new Date());
    }

    public static String getDayOfWeek(String updatedAt){

        Date date = null;
        try {
            sSIMPLE_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sSIMPLE_DATE_FORMAT.parse(updatedAt);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            Integer day = cal.get(Calendar.DAY_OF_WEEK);
            switch (day){
                case Calendar.SUNDAY:
                    return "SUN";
                case Calendar.MONDAY:
                    return "MON";
                case Calendar.TUESDAY:
                    return "TUE";
                case Calendar.WEDNESDAY:
                    return "WED";
                case Calendar.THURSDAY:
                    return "THU";
                case Calendar.FRIDAY:
                    return "FRI";
                case Calendar.SATURDAY:
                    return "SAT";

            }
        } catch (ParseException e) {
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public static boolean isValidForCheckIn(String date_time){

        Date startDate = null;
        if(date_time != null) {
            try {
                sDATE_AND_TIME_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
                startDate = sDATE_AND_TIME_FORMAT.parse(date_time);

                Log.e("DATETIME","START DATE: "+sDATE_AND_TIME_FORMAT.format(startDate));
                Date currentDate = sDATE_AND_TIME_FORMAT.parse(sDATE_AND_TIME_FORMAT.format(new Date()));
                Log.e("DATETIME","End DATE: "+sDATE_AND_TIME_FORMAT.format(currentDate));

                if(startDate.getTime() - currentDate.getTime() > 3600*1000){
                    return false;
                }else {
                    return true;
                }
            }catch (Exception e){
                e.printStackTrace();
                return false;
            }
        }else {
            return false;
        }
    }


}
