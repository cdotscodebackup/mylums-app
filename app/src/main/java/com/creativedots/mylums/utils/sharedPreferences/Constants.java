package com.creativedots.mylums.utils.sharedPreferences;

public abstract class Constants {
    public static final String KEY_POP_TYPE = "popup" ;

    private Constants(){

    }

    public static final String KEY_IS_LOGGED_IN = "loggedIn";

    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_ROLL_NO = "roll_number";
    public static final String KEY_STATUS = "status";
    public static final String KEY_ID = "id";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_IMAGE_PATH = "image_path";

    public static final String KEY_FIRST_NAME = "first_name";
    public static final String KEY_LAST_NAME = "last_name";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_DEPARTMENT = "department";
    public static final String KEY_COMPANY = "company";
    public static final String KEY_INDUSTRY = "industry";
    public static final String KEY_DESIGNATION = "designation";
    public static final String KEY_COUNTRY = "country";
    public static final String KEY_CITY = "city";
    public static final String KEY_DELETED_AT = "deleted_at";
    public static final String KEY_CREATED_AT = "created_at";
    public static final String KEY_UPDATED_AT = "updated_at";
    public static final String KEY_QR_CODE = "qrcode";

    public static final String KEY_HELP_MENU = "help_menu";

    public static final String KEY_TINDER_TYPE = "tinder";
    public static final String KEY_EVENT_TYPE = "event";
    public static final String KEY_MESSAGE_TYPE = "message";
    public static final String KEY_DISCOUNT_TYPE = "discount";
    public static final String KEY_PROFILE_TYPE = "profile";




    public static final String KEY_ARCHIVE = "archive";
    public static final String KEY_READ = "read";
    public static final String KEY_BADGE_REGULAR = "regular";
    public static final String KEY_BADGE_PLATINUM = "platinum";
    public static final String KEY_BADGE_SILVER = "silver";
    public static final String KEY_BADGE_GOLD = "gold";



}
