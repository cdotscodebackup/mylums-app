package com.creativedots.mylums.utils.sharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.creativedots.mylums.model.NotificationModel;
import com.creativedots.mylums.model.retroFit.Data;
import com.creativedots.mylums.model.retroFit.helpMenu.MenuItem;
import com.creativedots.mylums.model.retroFit.profile.User;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class SessionManager {

    public static final String TAG = SessionManager.class.getSimpleName();

    private Context mContext;

    /*
     * Name of the file where all data is stored
     */
    public static final String SHARED_PREF_USER_DATA = "user.conf";

    public static final String DEFAULT_VALUE = "N/A";

    public static final String USERNAME = "admin@admin.com";
    public static final String PASSWORD = "admin";
    private static final String SESSION_TOKEN = "token";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "lonigtude";
    public static boolean openChat = false;
    public static NotificationModel notificationModel;

    private static Context context;
    private static SharedPreferences pref;
    private static SharedPreferences.Editor editor;

    private static final SessionManager ourInstance = new SessionManager();

    private SessionManager() {

    }

    public SessionManager(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    public static SessionManager from(Context context) {
        if (context != null) {
            SessionManager.context = context;
            pref = context.getSharedPreferences(SHARED_PREF_USER_DATA, Context.MODE_PRIVATE);
            editor = pref.edit();
        }
        return ourInstance;
    }

    public void clear() {
        editor.clear().apply();
    }

    public void saveToken(String token) {
        Log.e(TAG, "Token Saved!");
        editor.putString(SESSION_TOKEN, token);
        editor.apply();
    }

    public String getToken() {
        return pref.getString(SESSION_TOKEN, "");
    }

    public void createUserLogin(User data) {

        editor.putString(Constants.KEY_USER_ID, data.getUserId());
        editor.putString(Constants.KEY_EMAIL, data.getEmail());
        editor.putString(Constants.KEY_ROLL_NO, data.getRollNumber());
        editor.putString(Constants.KEY_STATUS, data.getStatus());
        editor.putString(Constants.KEY_ID, data.getId());
        editor.putString(Constants.KEY_IMAGE, data.getImagepath());
        editor.putString(Constants.KEY_FIRST_NAME, data.getFirstName());
        editor.putString(Constants.KEY_LAST_NAME, data.getLastName());
        editor.putString(Constants.KEY_GENDER, data.getGender());
        editor.putString(Constants.KEY_PHONE, data.getPhone());
        editor.putString(Constants.KEY_DEPARTMENT, data.getCurrentDepartment());
        editor.putString(Constants.KEY_COMPANY, data.getCurrentCompany());
        editor.putString(Constants.KEY_DESIGNATION, data.getCurrentDesignation());
        editor.putString(Constants.KEY_COUNTRY, data.getCountry());
        editor.putString(Constants.KEY_CITY, data.getCity());
        editor.putString(Constants.KEY_DELETED_AT, data.getDeletedAt());
        editor.putString(Constants.KEY_CREATED_AT, data.getCreatedAt());
        editor.putString(Constants.KEY_UPDATED_AT, data.getUpdatedAt());
        editor.putString(Constants.KEY_QR_CODE, data.getQrcode());
        editor.putString(Constants.KEY_INDUSTRY, data.getCurrentIndustry());


        /**
         * Setting value to know if user is logged in or not.
         */
        editor.putBoolean(Constants.KEY_IS_LOGGED_IN, true);

        editor.apply();
    }

    public Data getUser() {
        return new Data(
                pref.getString(Constants.KEY_USER_ID, DEFAULT_VALUE),
                pref.getString(Constants.KEY_EMAIL, DEFAULT_VALUE),
                pref.getString(Constants.KEY_ROLL_NO, DEFAULT_VALUE),
                pref.getString(Constants.KEY_STATUS, DEFAULT_VALUE),
                pref.getString(Constants.KEY_ID, DEFAULT_VALUE),
                pref.getString(Constants.KEY_IMAGE, DEFAULT_VALUE),
                pref.getString(Constants.KEY_FIRST_NAME, DEFAULT_VALUE),
                pref.getString(Constants.KEY_LAST_NAME, DEFAULT_VALUE),
                pref.getString(Constants.KEY_GENDER, DEFAULT_VALUE),
                pref.getString(Constants.KEY_PHONE, DEFAULT_VALUE),
                pref.getString(Constants.KEY_DEPARTMENT, DEFAULT_VALUE),
                pref.getString(Constants.KEY_COMPANY, DEFAULT_VALUE),
                pref.getString(Constants.KEY_DESIGNATION, DEFAULT_VALUE),
                pref.getString(Constants.KEY_COUNTRY, DEFAULT_VALUE),
                pref.getString(Constants.KEY_CITY, DEFAULT_VALUE),
                pref.getString(Constants.KEY_DELETED_AT, DEFAULT_VALUE),
                pref.getString(Constants.KEY_CREATED_AT, DEFAULT_VALUE),
                pref.getString(Constants.KEY_UPDATED_AT, DEFAULT_VALUE),
                pref.getString(Constants.KEY_QR_CODE, DEFAULT_VALUE),
                pref.getString(Constants.KEY_INDUSTRY, DEFAULT_VALUE)
        );
    }

    public Boolean isLoggedIn() {
        return pref.getBoolean(Constants.KEY_IS_LOGGED_IN, false);
    }

    public void saveHelpMenu(String jsonHelp) {

        editor.putString(Constants.KEY_HELP_MENU, jsonHelp);
        editor.apply();

    }

    public ArrayList<MenuItem> getHelpMenu() {

        String jsonHelpMenu = pref.getString(Constants.KEY_HELP_MENU, DEFAULT_VALUE);

        if (!jsonHelpMenu.equals(DEFAULT_VALUE)) {

            ArrayList<MenuItem> menuItems = new ArrayList<>();

            try {
                JSONArray jsonMenuItems = new JSONArray(jsonHelpMenu);

                menuItems.removeAll(menuItems);
                for (int i = 0; i < jsonMenuItems.length(); i++) {
                    String temp = new Gson().toJson(jsonMenuItems.getJSONObject(i));
                    menuItems.add(new Gson().fromJson(temp.replace("{\"nameValuePairs\":", "").replace("}}", "}"), MenuItem.class));
                }

                return menuItems;
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        } else
            return null;
    }

    public static NotificationModel getNotificationModel() {
        if (notificationModel == null)
            notificationModel = new NotificationModel();
        return notificationModel;
    }

    public static void setNotificationModel(NotificationModel notificationModel) {
        SessionManager.notificationModel = notificationModel;
    }

    public void setWorkManagerID(String workID) {
        editor.putString("workManagerID", workID).commit();
    }

    public String getWorkManagerID() {
        return pref.getString("workManagerID", "");
    }

    public void setShowWizard(boolean show) {
        editor.putBoolean("show_wizard", show).commit();
    }

    public Boolean getShowWizard() {
        return pref.getBoolean("show_wizard", true);
    }

    public void setLinkedInConnected(boolean show) {
        editor.putBoolean("linked_in", show).commit();
    }

    public Boolean getLinkedInConnected() {
        return pref.getBoolean("linked_in", false);
    }

    public String getLatOrLong(String key) {
        return pref.getString(key, "0.0");
    }

    public void setLatOrLong(String key, String value) {
        editor.putString(key, value).commit();
    }
}
