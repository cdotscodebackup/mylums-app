package com.creativedots.mylums.utils;

import android.databinding.BindingAdapter;
import android.widget.TextView;

public class ProfileConstants {

    private static final String BTN_EDIT = "EDIT";
    private static final String BTN_SAVE = "SAVE";



    @BindingAdapter("app:setEditButtonText")
    public static void setEditButtonText(TextView view, boolean viewState){
        view.setText((viewState)?BTN_SAVE:BTN_EDIT);
    }

}
