package com.creativedots.mylums.utils;

/*
 * Created by regbits on 11/30/17.
 */

import java.util.HashMap;

public class Event {


    public static final int REFRESH_NOTIFICATION_LIST = -10;
    public static final int UPDATE_PROFILE_BACK = -11;
    public static final int FINISH_ACTIVITY = -12;
    public static final int RESEND_EMAIL = -13;

    private int eventType;
    private Object value;
    private int position;
    private boolean changeSelection;

    public Event() {
        properties = new HashMap<>();
    }

    private HashMap<String, String> properties;

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public Object getContainedObject() {
        return value;
    }

    public void addObject(Object value) {
        this.value = value;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isChangeSelection() {
        return changeSelection;
    }

    public void setChangeSelection(boolean changeSelection) {
        this.changeSelection = changeSelection;
    }

    public void putProperties(String key, String value) {
        properties.put(key, value);
    }

    public String getProperty(String key, String defaultValue) {
        if (properties.containsKey(key))
            return properties.get(key);
        return defaultValue;
    }
}
