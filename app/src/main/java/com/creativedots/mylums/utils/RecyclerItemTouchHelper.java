package com.creativedots.mylums.utils;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.creativedots.mylums.adapter.messages.MessagesAdapter;
import com.creativedots.mylums.adapter.profile.WorkExperienceAdapter;


public class RecyclerItemTouchHelper extends ItemTouchHelper.SimpleCallback {
    private RecyclerItemTouchHelperListener listener;

    public RecyclerItemTouchHelper(int dragDirs, int swipeDirs, RecyclerItemTouchHelperListener listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return true;
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder != null) {
            View foregroundView;
            if (viewHolder instanceof WorkExperienceAdapter.ViewHolder)
                foregroundView = ((WorkExperienceAdapter.ViewHolder) viewHolder).viewForeground;
            else
                foregroundView = ((MessagesAdapter.ViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onSelected(foregroundView);
        }
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {
        View foregroundView;
        if (viewHolder instanceof WorkExperienceAdapter.ViewHolder)
            foregroundView = ((WorkExperienceAdapter.ViewHolder) viewHolder).viewForeground;
        else
            foregroundView = ((MessagesAdapter.ViewHolder) viewHolder).viewForeground;
        //final View foregroundView = ((MessagesAdapter.ViewHolder) viewHolder).viewForeground;
        getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY,
                actionState, isCurrentlyActive);
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        View foregroundView;
        if (viewHolder instanceof WorkExperienceAdapter.ViewHolder)
            foregroundView = ((WorkExperienceAdapter.ViewHolder) viewHolder).viewForeground;
        else
            foregroundView = ((MessagesAdapter.ViewHolder) viewHolder).viewForeground;
        //final View foregroundView = ((MessagesAdapter.ViewHolder) viewHolder).viewForeground;
        getDefaultUIUtil().clearView(foregroundView);
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {
        View foregroundView;
        if (viewHolder instanceof WorkExperienceAdapter.ViewHolder)
            foregroundView = ((WorkExperienceAdapter.ViewHolder) viewHolder).viewForeground;
        else
            foregroundView = ((MessagesAdapter.ViewHolder) viewHolder).viewForeground;
       // final View foregroundView = ((MessagesAdapter.ViewHolder) viewHolder).viewForeground;

        getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY,
                actionState, isCurrentlyActive);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        listener.onSwiped(viewHolder, direction, viewHolder.getAdapterPosition());
    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    public interface RecyclerItemTouchHelperListener {
        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return super.isItemViewSwipeEnabled();
    }
}
