package com.creativedots.mylums.utils;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;

import com.creativedots.mylums.activity.base.BaseActivity;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.model.retroFit.partnersNearBy.PartnersNearByResponse;
import com.creativedots.mylums.network.ApiClient;
import com.creativedots.mylums.network.ApiInterface;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import retrofit2.Call;
import retrofit2.Callback;

public class UploadSystemParamsTask {

    Context mContext;

    public UploadSystemParamsTask(Context context) {

    }

    public UploadSystemParamsTask(Context context, Location location) {
        mContext = context;
        makePartnersNearByRequest(location.getLatitude()+"",location.getLongitude()+"","");
    }

    private void makePartnersNearByRequest(String lat, String lng, String query) {

        ApiInterface apiService = ApiClient.getRetrofitClient(SessionManager.from(mContext).getToken()).create(ApiInterface.class);
        Call<PartnersNearByResponse> call = apiService.partnersNearBy(
                lat,
                lng,
                query,
                Api.PartnersNearBy.DEFAULT_ALL, Api.PartnersNearBy.DEFAULT_PUSH
        );

        /**
         * Hiding Loading View
         */

        Log.e("URL", "" + call.request().url());

        call.enqueue(new Callback<PartnersNearByResponse>() {
            @Override
            public void onResponse(@NonNull Call<PartnersNearByResponse> call, @NonNull retrofit2.Response<PartnersNearByResponse> response) {


                Log.e("BACKGROUND","Success call");
                if (response.isSuccessful()) {

                }
            }

            @Override
            public void onFailure(@NonNull Call<PartnersNearByResponse> call, @NonNull Throwable t) {
                /**
                 * Hiding Loading View
                 */
                t.printStackTrace();
            }
        });
    }
}