package com.creativedots.mylums.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.EditText;

public abstract class ValidationTools {

    private ValidationTools(){

    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target)
                && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean isValidEmail(EditText email) {
        return (!TextUtils.isEmpty(email.getText().toString().trim())
                && Patterns.EMAIL_ADDRESS.matcher(email.getText().toString().trim()).matches());
    }

    public static boolean allFieldsFilled(EditText... fields){

        for(EditText field : fields){
            if(field.getText().toString().equals("")){
                return false;
            }
        }

        return true;
    }

    public static boolean allFieldsFilled(String... fields){

        for(String field : fields){
            if(field.equals("")){
                return false;
            }
        }

        return true;
    }

    public static boolean minOneFieldFilled(String... fields){

        for(String field : fields){
            if(!field.equals("")){
                return true;
            }
        }

        return false;
    }

    public static boolean isValidPhone(EditText phoneNumber) {
        return (!TextUtils.isEmpty(phoneNumber.getText().toString().trim())
                && Patterns.PHONE.matcher(phoneNumber.getText().toString().trim()).matches()
                && phoneNumber.getText().toString().length() >= 6);
    }


    public static boolean isPermissionGranted(Activity context, String permission) {
        // String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }


    public static void requestPermission(Fragment fragment, String... permissions) {
        //ActivityCompat.requestPermissions(fragment.getActivity(), permissions, 50);
        fragment.requestPermissions(permissions, 50);
    }

    public static void requestPermission(Activity fragment, String... permissions) {
        ActivityCompat.requestPermissions(fragment, permissions, 50);
        //fragment.requestPermissions(permissions, 50);
    }
}
