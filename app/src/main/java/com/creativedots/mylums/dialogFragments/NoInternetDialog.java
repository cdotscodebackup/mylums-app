package com.creativedots.mylums.dialogFragments;

import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.databinding.DialogFragmentNoInternetBinding;
import com.creativedots.mylums.utils.AppSettings;

public class NoInternetDialog extends DialogFragment implements View.OnClickListener {
    public static final String TAG = NoInternetDialog.class.getSimpleName();

    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private DialogFragmentNoInternetBinding binding;

    private void setInfo() {

        binding.avi.hide();

        binding.llMobileData.setOnClickListener(this);
        binding.llWifi.setOnClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_no_internet, container, false);
        setInfo();
        try{
            getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        return binding.getRoot();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.llMobileData:
                AppSettings.turnOn3g(context);
                break;
            case R.id.llWifi:
                binding.avi.show();
                AppSettings.turnOnWifi(context);
                break;
        }
    }
}
