package com.creativedots.mylums.fragment.dashboardFragments;

import android.Manifest;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.adapter.discountAdapters.DiscountPartnersAdapter;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.model.retroFit.partnersList.Partner;
import com.creativedots.mylums.model.retroFit.partnersList.PartnersListResponse;
import com.creativedots.mylums.model.retroFit.partnersNearBy.PartnerNearBy;
import com.creativedots.mylums.model.retroFit.partnersNearBy.PartnersNearByResponse;
import com.creativedots.mylums.utils.AppSettings;
import com.creativedots.mylums.utils.GPSTracker;
import com.creativedots.mylums.utils.ValidationTools;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiscountPartnersFragments extends BaseFragment implements View.OnClickListener {
    public static final String TAG = DiscountPartnersFragments.class.getSimpleName();

    private GPSTracker gps;

    private AVLoadingIndicatorView aviLoading;
    private ImageView ivBack, ivSearch;
    private TextView tvAll, tvNearMe;
    private RelativeLayout rlSearch;
    private EditText etQuery;
    private LinearLayout allSelected, nearSelected;
    private TextView txtNoData;
    private boolean isFromNotification;
    private ArrayList<Partner> partners = new ArrayList<>();
    private ArrayList<PartnerNearBy> partnersNearBy = new ArrayList<>();
    private RecyclerView rvAll, rvNearMe;
    private DiscountPartnersAdapter allDiscountPartnersAdapter, nearMeDiscountPartnersAdapter;

    private void getInfo(View view) {
        aviLoading = view.findViewById(R.id.aviLoading);
        ivBack = view.findViewById(R.id.ivBack);
        ivSearch = view.findViewById(R.id.ivSearch);

        tvAll = view.findViewById(R.id.tvAll);
        tvNearMe = view.findViewById(R.id.tvNearMe);

        rvAll = view.findViewById(R.id.rvAll);
        rvNearMe = view.findViewById(R.id.rvNearMe);

        etQuery = view.findViewById(R.id.etQuery);
        rlSearch = view.findViewById(R.id.rlSearch);

        allSelected = view.findViewById(R.id.allSelected);
        nearSelected = view.findViewById(R.id.nearSelected);
        txtNoData = view.findViewById(R.id.txtNoData);
    }

    private void setInfo() {
        ivBack.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        tvAll.setOnClickListener(this);
        tvNearMe.setOnClickListener(this);
        rlSearch.setOnClickListener(this);

        /**
         * Getting EventDetail from Database
         */
        makeDiscountPartnersRequest(Api.PartnersList.DEFAULT_QUERY);


        /**
         * Setting Discount Partners Adapter
         */
        initDiscountPartnersAdapter();

        etQuery.setHint("Search Partner Name");

        etQuery.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() < 1) {
                    makeDiscountPartnersRequest(Api.PartnersList.DEFAULT_QUERY);
                }
            }
        });

        if (isFromNotification)
            nearByClicked();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_discount_partners, container, false);
        isFromNotification = false;
        if (getArguments() != null)
            if (getArguments().containsKey("notification"))
                isFromNotification = getArguments().getBoolean("notification");

        getInfo(view);
        setInfo();
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                AppSettings.hideKeyboard(context, view);
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.ivSearch:
                AppSettings.hideKeyboard(context, view);
                if (ValidationTools.allFieldsFilled(etQuery)) {
                    if (nearSelected.getVisibility() == View.VISIBLE) {
                        checkLocation(etQuery.getText().toString().trim());

                    } else
                        makeDiscountPartnersRequest(etQuery.getText().toString().trim());
                }
                break;
            case R.id.tvAll:
                handleTabClicks(view);
                break;
            case R.id.tvNearMe:
                handleTabClicks(view);
                break;
        }
    }

    private void makeDiscountPartnersRequest(String query) {

        Call<PartnersListResponse> call = apiService.partnersList(
                query, Api.PartnersList.DEFAULT_PAGE_NUMBER, Api.PartnersList.DEFAULT_ALL);

        /**
         * Hiding Loading View
         */
        aviLoading.smoothToShow();

        call.enqueue(new Callback<PartnersListResponse>() {
            @Override
            public void onResponse(@NonNull Call<PartnersListResponse> call, @NonNull retrofit2.Response<PartnersListResponse> response) {

                if (response.isSuccessful()) {

                    PartnersListResponse myResponse = response.body();

                    partners.removeAll(partners);

                    partners.addAll(myResponse.getData());

                    allDiscountPartnersAdapter.notifyDataSetChanged();
//                    nearMeDiscountPartnersAdapter.notifyDataSetChanged();

                    /**
                     * Hiding Loading View
                     */
                    aviLoading.hide();
                    displayListing();

                } else {
                    /**
                     * Hiding Loading View
                     */
                    aviLoading.hide();
                    try {
                        //getErrorMessage();
                        getDisplayMessage(getErrorMessage(response.errorBody().string()));

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<PartnersListResponse> call, @NonNull Throwable t) {
                /**
                 * Hiding Loading View
                 */
                aviLoading.hide();
                t.printStackTrace();

                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });
    }

    private void makePartnersNearByRequest(String lat, String lng, String query) {

        Call<PartnersNearByResponse> call = apiService.partnersNearBy(
                lat,
                lng,
                query,
                Api.PartnersList.DEFAULT_ALL, ""
        );

        /**
         * Hiding Loading View
         */

        Log.e("URL", "" + call.request().url());
        aviLoading.smoothToShow();

        call.enqueue(new Callback<PartnersNearByResponse>() {
            @Override
            public void onResponse(@NonNull Call<PartnersNearByResponse> call, @NonNull Response<PartnersNearByResponse> response) {

                if (response.isSuccessful()) {

                    PartnersNearByResponse myResponse = response.body();

                    partnersNearBy.clear();
                    partnersNearBy.addAll(myResponse.getData());

                    nearMeDiscountPartnersAdapter.notifyDataSetChanged();
                    displayListing();
                    /**
                     * Hiding Loading View
                     */
                    aviLoading.hide();
                } else {
                    /**
                     * Hiding Loading View
                     */
                    aviLoading.hide();
                    try {
                        getDisplayMessage(getErrorMessage(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<PartnersNearByResponse> call, @NonNull Throwable t) {
                /**
                 * Hiding Loading View
                 */
                aviLoading.hide();
                t.printStackTrace();

                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });
    }

    private void initDiscountPartnersAdapter() {
        allDiscountPartnersAdapter = new DiscountPartnersAdapter(((MainActivity) context),
                partners, null, false);
        allDiscountPartnersAdapter.setOnItemClickedListener(onItemClickedListener);
        rvAll.setLayoutManager(new LinearLayoutManager(context));
        rvAll.setAdapter(allDiscountPartnersAdapter);

        nearMeDiscountPartnersAdapter = new DiscountPartnersAdapter(((MainActivity) context),
                null, partnersNearBy, true);
        nearMeDiscountPartnersAdapter.setOnItemClickedListener(onItemClickedListener);
        rvNearMe.setLayoutManager(new LinearLayoutManager(context));
        rvNearMe.setAdapter(nearMeDiscountPartnersAdapter);

        rvAll.setNestedScrollingEnabled(false);
        rvNearMe.setNestedScrollingEnabled(false);
    }

    private DiscountPartnersAdapter.OnItemClickedListener onItemClickedListener = new DiscountPartnersAdapter.OnItemClickedListener() {
        @Override
        public void onDiscountSelected(String partnerID) {
            DiscountDetailsFragment discountDetailsFragment = new DiscountDetailsFragment();
            discountDetailsFragment.setPartnerID(partnerID);
            ((MainActivity) context).replaceFragment(
                    discountDetailsFragment,
                    DiscountDetailsFragment.TAG,
                    true,
                    false
            );
        }
    };

    private void handleTabClicks(View view) {
        switch (view.getId()) {
            case R.id.tvAll:
                setTabSelected(tvAll, allSelected, true);
                setTabSelected(tvNearMe, nearSelected, false);
                rvAll.setVisibility(View.VISIBLE);
                rvNearMe.setVisibility(View.GONE);
                break;
            case R.id.tvNearMe:
                nearByClicked();
                break;
        }
    }

    private void checkLocation(String q) {
        gps = new GPSTracker(context);
        if (gps.canGetLocation()) {
            String lat = String.valueOf(gps.getLatitude());
            String lng = String.valueOf(gps.getLongitude());

            if (q.isEmpty())
                makePartnersNearByRequest(lat, lng, Api.PartnersList.DEFAULT_QUERY);
            else
                makePartnersNearByRequest(lat, lng, q);
        } else {
            gps.showSettingsAlert();
        }
    }

    private void setTabSelected(TextView tab, LinearLayout layout, Boolean selected) {
        if (selected) {
            tab.setTextColor(context.getResources().getColor(R.color.colorPrimaryLight));
            layout.setVisibility(View.VISIBLE);
        } else {
            layout.setVisibility(View.GONE);
            tab.setTextColor(context.getResources().getColor(android.R.color.black));
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 50) {
            checkLocation("");
        }
    }

    public void getDisplayMessage(String error) {


        rvAll.setVisibility(View.GONE);
        rvNearMe.setVisibility(View.GONE);

        txtNoData.setText(error);
        txtNoData.setVisibility(View.VISIBLE);
    }

    public void displayListing() {

        if (allSelected.getVisibility() == View.VISIBLE)
            rvAll.setVisibility(View.VISIBLE);
        else
            rvNearMe.setVisibility(View.VISIBLE);

        txtNoData.setVisibility(View.GONE);
    }

    private void nearByClicked() {
        setTabSelected(tvNearMe, nearSelected, true);
        setTabSelected(tvAll, allSelected, false);

        rvNearMe.setVisibility(View.VISIBLE);
        rvAll.setVisibility(View.GONE);
        if (partnersNearBy.size() == 0)
            if (ValidationTools.isPermissionGranted(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION))
                checkLocation("");
            else
                ValidationTools.requestPermission(DiscountPartnersFragments.this, Manifest.permission.ACCESS_FINE_LOCATION);

    }
}
