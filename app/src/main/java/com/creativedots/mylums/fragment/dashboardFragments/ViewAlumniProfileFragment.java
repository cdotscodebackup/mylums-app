package com.creativedots.mylums.fragment.dashboardFragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.adapter.InterestsAdapter;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.model.retroFit.Alumni.AlumniProfileResponse;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.model.retroFit.profile.Interest;
import com.creativedots.mylums.model.retroFit.profile.User;
import com.creativedots.mylums.utils.AppSettings;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class ViewAlumniProfileFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = ViewAlumniProfileFragment.class.getSimpleName();

    private String almuniUserID;
    private String userType;
    private User mUser = new User();
    private AVLoadingIndicatorView aviLoading;

    public void setAlmuniUserID(String almuniUserID, String usertype) {
        this.almuniUserID = almuniUserID;
        this.userType = usertype;
    }

    private ImageView ivBack, imgCover, imgInvite;
    private CircleImageView civProfile;
    private TextView tvName, tvProgram, tvYear, tvCompany, tvDesignation, tvLocation, tvWebsite, tvEmail, tvContact, txtClick, tvProgramDate;
    private RelativeLayout rlContact, rlContentContainer;
    private RecyclerView rvInterests;

    private void getInfo(View view) {
        aviLoading = view.findViewById(R.id.aviLoading);
        ivBack = view.findViewById(R.id.ivBack);
        imgCover = view.findViewById(R.id.imgCover);
        civProfile = view.findViewById(R.id.civProfile);
        tvName = view.findViewById(R.id.tvName);
        tvProgram = view.findViewById(R.id.tvProgram);
        tvYear = view.findViewById(R.id.tvYear);
        tvCompany = view.findViewById(R.id.tvCompany);
        tvDesignation = view.findViewById(R.id.tvDesignation);
        tvLocation = view.findViewById(R.id.tvLocation);
        tvWebsite = view.findViewById(R.id.tvWebsite);
        tvEmail = view.findViewById(R.id.tvEmail);
        tvContact = view.findViewById(R.id.tvContact);
        imgInvite = view.findViewById(R.id.imgInvite);
        txtClick = view.findViewById(R.id.txtClick);
        rlContact = view.findViewById(R.id.rlContact);
        rlContentContainer = view.findViewById(R.id.rlContentContainer);
        rvInterests = view.findViewById(R.id.rvInterests);
        tvProgramDate = view.findViewById(R.id.tvProgramDate);

    }

    private void setInfo() {

        ivBack.setOnClickListener(this);
        rlContact.setOnClickListener(this);

        if (almuniUserID != null) {
            /**
             * Getting Profile data with user-id
             * And updating UI.
             */
            makeAlumniProfileRequest(almuniUserID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_view_alumni_profile, container, false);
        getInfo(view);
        setInfo();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /**
         * Hiding Keyboard
         */
        AppSettings.hideKeyboard(context.getApplicationContext(), view);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:

                if (SessionManager.openChat) {
                    SessionManager.openChat = false;
                    ((MainActivity) getActivity()).moveToMessageScreen(
                            SessionManager.getNotificationModel()
                    );
                }
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.rlContact:

                if (userType != null && userType.equalsIgnoreCase("profile")) {
                    Fragment fragment = new SayHiFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("user", mUser);
                    fragment.setArguments(bundle);
                    ((MainActivity) context).addFragment(fragment, SayHiFragment.TAG, true, false);
                } else {
                    makeInviteCall(almuniUserID);
                }
                break;

        }
    }

    private void makeAlumniProfileRequest(String userId) {
        Call<AlumniProfileResponse> call;
        if (userType != null && userType.equalsIgnoreCase("profile"))
            call = apiService.alumniProfile(Api.AlumniProfile.GET_URL + "/" + userId);
        else
            call = apiService.alumniProload(userId);
        aviLoading.smoothToShow();
        call.enqueue(new Callback<AlumniProfileResponse>() {
            @Override
            public void onResponse(@NonNull Call<AlumniProfileResponse> call, @NonNull retrofit2.Response<AlumniProfileResponse> response) {

                if (response.isSuccessful()) {

                    AlumniProfileResponse myResponse = response.body();

                    User data = myResponse.getUser();
                    mUser = data;

                    String userName = data.getFirstName() + " " + data.getLastName();

                    Glide.with(context).load(data.getImagepath())
                            .apply(new RequestOptions().circleCrop()
                                    .placeholder(R.drawable.ic_user_tab_primary).error(R.drawable.ic_user_tab_primary))
                            .into(civProfile);

                    Glide.with(context).load(data.getImagepath()).apply(new RequestOptions()
                            .placeholder(R.drawable.bg_dashboard).error(R.drawable.bg_dashboard))
                            .into(imgCover);

                    if (!userType.equalsIgnoreCase("profile")) {
                        txtClick.setText("Invite");
                        imgInvite.setVisibility(View.VISIBLE);
                    }
                    //initInterestsRecyclerView(data.get);
                    tvName.setText(userName);
                    tvCompany.setText(data.getCurrentCompany());
                    tvDesignation.setText(data.getCurrentDesignation());
                    tvLocation.setText(data.getLocation());
                    tvProgram.setText(data.getProgram());
                    tvYear.setText(data.getYear());
                    tvEmail.setText(data.getEmail());
                    tvContact.setText(data.getPhone());
                    tvProgramDate.setText(data.getProgram() + " " + data.getYear());

                    if (mUser.getInterests().size() > 0) {
                        interests.removeAll(interests);
                        interests.addAll(mUser.getInterests());
                    }

                    interestsAdapter.notifyDataSetChanged();


                    rlContentContainer.setVisibility(View.VISIBLE);

                } else {

                    try {
                        if (response.errorBody() != null) {
                            rlContentContainer.setVisibility(View.GONE);
                            Log.e(TAG, "Almuni-Profile Error Body: " + response.errorBody().string());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                aviLoading.hide();
            }

            @Override
            public void onFailure(@NonNull Call<AlumniProfileResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                aviLoading.hide();

                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });

        /**
         * Setting Layout Manager for Interests Spinner
         */
        initInterestsRecyclerView();
    }

    private void makeInviteCall(String userId) {
        Call<Response> call;
        call = apiService.alumniInvite(userId);
        aviLoading.smoothToShow();
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                if (response.isSuccessful()) {
                    Response myResponse = response.body();
                    txtClick.setText("invited");
                    rlContact.setOnClickListener(null);
                } else {

                    try {
                        if (response.errorBody() != null) {
                            rlContentContainer.setVisibility(View.GONE);
                            Log.e(TAG, "Almuni-Profile Error Body: " + response.errorBody().string());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                aviLoading.hide();
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                aviLoading.hide();

                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });

        /**
         * Setting Layout Manager for Interests Spinner
         */
        initInterestsRecyclerView();
    }

    private InterestsAdapter interestsAdapter;
    private ArrayList<Interest> interests;

    private void initInterestsRecyclerView() {
        /*interests.add("Racing");
        interests.add("Gaming");
        interests.add("Hunting");*/
        interests = new ArrayList<>(mUser.getInterests());
        interestsAdapter = new InterestsAdapter(((MainActivity) context), interests, false);
        rvInterests.setLayoutManager(ChipsLayoutManager.newBuilder(context).build());
        rvInterests.setAdapter(interestsAdapter);

    }


    @NonNull
    private String getInviteMessage(/*String inviteCode, String url*/) {
        return SessionManager.from(getActivity()).getUser().getFirstName().trim() + " has invited you to download "
                + getString(R.string.app_name)
                + " application.";
    }

}
