package com.creativedots.mylums.fragment.dashboardFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.adapter.discountAdapters.DiscountsAdapter;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.model.retroFit.offer.OfferResponse;
import com.creativedots.mylums.model.retroFit.partnersDetails.Offer;
import com.creativedots.mylums.model.retroFit.partnersDetails.PartnersDetailsResponse;
import com.creativedots.mylums.model.retroFit.partnersNearBy.PartnersNearByResponse;
import com.creativedots.mylums.utils.GPSTracker;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiscountDetailsFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = DiscountDetailsFragment.class.getSimpleName();

    private String partnerID;

    public void setPartnerID(String partnerID) {
        this.partnerID = partnerID;
    }

    private AVLoadingIndicatorView aviLoading;
    private ImageView ivBack, ivPremium;
    private CircleImageView civThumbnail;
    private TextView tvAll, tvNearMe;
    private TextView tvCompanyName, tvDescription;
    private LinearLayout allSelected, nearSelected,lnContentContainer;

    private ArrayList<Offer> offers = new ArrayList<>();
    private ArrayList<Offer> nearOffers = new ArrayList<>();
    private DiscountsAdapter allDiscountsAdapter, nearMeDiscountsAdapter;
    private RecyclerView rvAll, rvNearMe;
    private GPSTracker gps;

    private void getInfo(View view) {
        aviLoading = view.findViewById(R.id.aviLoading);
        ivBack = view.findViewById(R.id.ivBack);

        ivPremium = view.findViewById(R.id.ivPremium);
        civThumbnail = view.findViewById(R.id.civThumbnail);

        tvAll = view.findViewById(R.id.tvAll);
        tvNearMe = view.findViewById(R.id.tvNearMe);
        tvCompanyName = view.findViewById(R.id.tvCompanyName);
        tvDescription = view.findViewById(R.id.tvDescription);

        rvAll = view.findViewById(R.id.rvAll);
        rvNearMe = view.findViewById(R.id.rvNearMe);

        allSelected = view.findViewById(R.id.allSelected);
        nearSelected = view.findViewById(R.id.nearSelected);
        lnContentContainer = view.findViewById(R.id.lnContentContainer);

    }

    private void setInfo() {
        ivBack.setOnClickListener(this);

        tvAll.setOnClickListener(this);
        tvNearMe.setOnClickListener(this);

        /**
         * Getting data from database
         */

        makePartnersDetailsRequest(partnerID);
        //  checkLocation();
        //    fetchAllOffers(partnerID,"","");

        /**
         * Setting Adapter
         */
        initDiscountAdapter();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_discount_details, container, false);
        getInfo(view);
        setInfo();
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.tvAll:
                //makePartnersDetailsRequest(partnerID);
                handleTabClicks(view);
                break;
            case R.id.tvNearMe:
//                //makePartnersDetailsRequest(partnerID);
                //Toast.makeText(context, "Near By", Toast.LENGTH_SHORT).show();
                // TODO : Not Ready yet
                handleTabClicks(view);
                break;
        }
    }


    private void makePartnersDetailsRequest(String partnerID) {

        Call<PartnersDetailsResponse> call = apiService.partnersDetails(Api.PartnersDetails.GET_URL + "/" + partnerID);

        /**
         * Hiding Loading View
         */
        aviLoading.smoothToShow();

        call.enqueue(new Callback<PartnersDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<PartnersDetailsResponse> call, @NonNull Response<PartnersDetailsResponse> response) {

                if (response.isSuccessful()) {

                    PartnersDetailsResponse myResponse = response.body();

                    tvCompanyName.setText(myResponse.getData().getName());
                    tvDescription.setText(myResponse.getData().getDescription());

                    ivPremium.setVisibility(myResponse.getData().getPremium()
                            .equals(Api.PartnersDetails.IS_PRIMIUM)
                            ? View.VISIBLE : View.INVISIBLE
                    );

                    Glide.with(context).load(myResponse.getData().getImagepath())
                            .apply(new RequestOptions().centerCrop())
                            .into(civThumbnail);

                    offers.addAll(myResponse.getData().getOffers());
                    allDiscountsAdapter.notifyDataSetChanged();

                    /**
                     * Hiding Loading View
                     */
                    aviLoading.hide();
                    lnContentContainer.setVisibility(View.VISIBLE);

                } else {

                    /**
                     * Hiding Loading View
                     */
                    aviLoading.hide();
                    displayErrorMessage(response.errorBody().toString());
                }

            }

            @Override
            public void onFailure(@NonNull Call<PartnersDetailsResponse> call, @NonNull Throwable t) {
                /**
                 * Hiding Loading View
                 */
                aviLoading.hide();
                t.printStackTrace();

                if(t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });

    }

    private void fetchNearOffers(String partnerID, String lat, String lng) {

        Call<OfferResponse> call = apiService.partnerOfferNear(partnerID, lat, lng, Api.SinglePartner.DEFAULT_ALL);

//        displayErrorMessage(call.request().url().toString());
        /**
         * Hiding Loading View
         */
        aviLoading.smoothToShow();

        call.enqueue(new Callback<OfferResponse>() {
            @Override
            public void onResponse(@NonNull Call<OfferResponse> call, @NonNull Response<OfferResponse> response) {

                if (response.isSuccessful()) {
                    OfferResponse myResponse = response.body();

                    nearOffers.clear();
                    nearOffers.addAll(myResponse.getData());
                    nearMeDiscountsAdapter.notifyDataSetChanged();
                    aviLoading.hide();

                } else {

                    aviLoading.hide();
                    displayErrorMessage(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(@NonNull Call<OfferResponse> call, @NonNull Throwable t) {
                aviLoading.hide();
                t.printStackTrace();
                if(t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });
    }

    private void initDiscountAdapter() {
        allDiscountsAdapter = new DiscountsAdapter(((MainActivity) context), offers);
        // TODO : Not Ready yet
        nearMeDiscountsAdapter = new DiscountsAdapter(((MainActivity) context), nearOffers);

        rvAll.setLayoutManager(new LinearLayoutManager(context));
        rvAll.setAdapter(allDiscountsAdapter);

        // TODO : Not Ready yet
        rvNearMe.setLayoutManager(new LinearLayoutManager(context));
        rvNearMe.setAdapter(nearMeDiscountsAdapter);
    }

    private void handleTabClicks(View view) {
        switch (view.getId()) {
            case R.id.tvAll:
                setTabSelected(tvAll, allSelected, true);
                setTabSelected(tvNearMe, nearSelected, false);
                rvAll.setVisibility(View.VISIBLE);
                rvNearMe.setVisibility(View.GONE);
                break;
            case R.id.tvNearMe:
                setTabSelected(tvNearMe, nearSelected, true);
                setTabSelected(tvAll, allSelected, false);
                rvNearMe.setVisibility(View.VISIBLE);
                rvAll.setVisibility(View.GONE);
                if (nearOffers.size() == 0)
                    checkLocation();
                break;
        }
    }

    private void setTabSelected(TextView tab, LinearLayout linearLayout, Boolean selected) {
        if (selected) {
            linearLayout.setVisibility(View.VISIBLE);
            tab.setTextColor(context.getResources().getColor(R.color.colorPrimaryLight));
            //  tab.setBackground(new ColorDrawable(context.getResources().getColor(android.R.color.white)));
        } else {
            linearLayout.setVisibility(View.GONE);
            //tab.setBackground(new ColorDrawable(context.getResources().getColor(R.color.colorEventTabNoSelected)));
            tab.setTextColor(context.getResources().getColor(android.R.color.black));
        }
    }

    private void checkLocation() {
        gps = new GPSTracker(context);
        if (nearSelected.getVisibility() == View.VISIBLE)
            if (gps.canGetLocation()) {

                String lat = String.valueOf(gps.getLatitude());
                String lng = String.valueOf(gps.getLongitude());

                fetchNearOffers(partnerID, lat, lng);
            } else {
                gps.showSettingsAlert();
            }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (nearOffers.size() == 0)
            checkLocation();
    }
}
