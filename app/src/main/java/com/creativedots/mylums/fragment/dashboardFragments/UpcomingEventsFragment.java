package com.creativedots.mylums.fragment.dashboardFragments;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.adapter.event.EventsAdapter;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.model.retroFit.Data;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.model.retroFit.profile.User;
import com.creativedots.mylums.utils.AppSettings;
import com.creativedots.mylums.utils.ValidationTools;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class UpcomingEventsFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = UpcomingEventsFragment.class.getSimpleName();

    private ImageView ivBack, ivSearch;
    private EditText etQuery;
    private TextView txtNoData;
    private RelativeLayout rlSearch;
    private AVLoadingIndicatorView aviLoading;
    private TextView tvUpComingTab, tvPastTab;
    private AppCompatAutoCompleteTextView actvQuery;
    private Response myResponse, upcomingResponse, pastResponse;
    private ArrayList<Data> upcomingEvents = new ArrayList<>();
    private ArrayList<Data> pastEvents = new ArrayList<>();
    private RecyclerView rvUpcomingEvents, rvPastEvents;
    private LinearLayout lnUpcomingSelected, lnPastSelected;
    private EventsAdapter upcomingEventsAdapter, pastEventsAdapter;
    private EventsAdapter.OnItemClickedListener onItemClickedListener = new EventsAdapter.OnItemClickedListener() {
        @Override
        public void onEventClicked(Data event) {
            EventDetailsFragment eventDetailsFragment = new EventDetailsFragment();
            eventDetailsFragment.setEventID(event.getId());
            ((MainActivity) context).addFragment(eventDetailsFragment, eventDetailsFragment.TAG, true, false);
//            ((MainActivity) context).replaceFragment(eventDetailsFragment, eventDetailsFragment.TAG, true, false);
        }
    };
    //SwipeRefreshLayout mSwipeRefreshLayout;

    private void getInfo(View view) {

        lnUpcomingSelected = view.findViewById(R.id.upcomingSelected);
        lnPastSelected = view.findViewById(R.id.pastSelected);

        actvQuery = view.findViewById(R.id.actvQuery);
        aviLoading = view.findViewById(R.id.aviLoading);
        ivBack = view.findViewById(R.id.ivBack);
        ivSearch = view.findViewById(R.id.ivSearch);


        etQuery = view.findViewById(R.id.etQuery);
        txtNoData = view.findViewById(R.id.txtNoData);
        rlSearch = view.findViewById(R.id.rlSearch);

        tvUpComingTab = view.findViewById(R.id.tvUpComingTab);
        tvPastTab = view.findViewById(R.id.tvPastTab);

        rvUpcomingEvents = view.findViewById(R.id.rvUpcomingEvents);
        rvPastEvents = view.findViewById(R.id.rvPastEvents);

        // mSwipeRefreshLayout = view.findViewById(R.id.swiperefresh);

    }

    private void setInfo() {
        ivBack.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        rlSearch.setOnClickListener(this);
        tvUpComingTab.setOnClickListener(this);
        tvPastTab.setOnClickListener(this);

        /**
         * Getting Events List from database
         */
        etQuery.setHint(R.string.search_events);
        if (upcomingEvents.isEmpty())
            makeUpcomingEventsRequest(Api.Events.DEFAULT_QUERY, Api.Events.DEFAULT_PAGE_NUMBER);

        if (pastEvents.isEmpty())
            makePastEventsRequest(Api.Events.DEFAULT_QUERY, Api.Events.DEFAULT_PAGE_NUMBER);
        /**
         * Setting Events Adapter
         */
        initUpcomingEventsAdapter();

        etQuery.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() < 1) {
                    makeSearchRequest(Api.Events.DEFAULT_QUERY, Api.Events.DEFAULT_PAGE_NUMBER);
                }
            }
        });


     /*   mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        mSwipeRefreshLayout.setRefreshing(true);
                        makeSearchRequest(Api.Events.DEFAULT_QUERY, Api.Events.DEFAULT_PAGE_NUMBER);
                    }
                }
        );*/

        txtNoData.setOnClickListener(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null)
            if (savedInstanceState.containsKey("upcoming")) {
                if (savedInstanceState.getBoolean("upcoming")) {
                    tvUpComingTab.performLongClick();
                } else {
                    tvPastTab.performLongClick();
                }
            }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_upcoming_events, container, false);
        getInfo(view);
        setInfo();
        if (savedInstanceState != null)
            if (savedInstanceState.containsKey("upcoming")) {
                if (savedInstanceState.getBoolean("upcoming")) {
                    tvUpComingTab.performLongClick();
                } else {
                    tvPastTab.performLongClick();
                }
            }
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                AppSettings.hideKeyboard(context, view);
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.ivSearch:
                AppSettings.hideKeyboard(context, view);
                if (ValidationTools.allFieldsFilled(etQuery)) {
                    makeSearchRequest(etQuery.getText().toString(), Api.Events.DEFAULT_PAGE_NUMBER);
                } else {
                    Toast.makeText(context, R.string.warn_enter_query, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tvUpComingTab:
            case R.id.tvPastTab:
                handleTabClicks(view);
                break;
            case R.id.txtNoData:
                if (txtNoData.getText().toString().equalsIgnoreCase(getResources().getString(R.string.no_upcoming_event))) {
                    if (upcomingEvents.isEmpty())
                        makeUpcomingEventsRequest(Api.Events.DEFAULT_QUERY, Api.Events.DEFAULT_PAGE_NUMBER);
                } else {
                    if (pastEvents.isEmpty())
                        makePastEventsRequest(Api.Events.DEFAULT_QUERY, Api.Events.DEFAULT_PAGE_NUMBER);
                }
                break;
        }
    }

    private void handleTabClicks(View view) {
        switch (view.getId()) {
            case R.id.tvUpComingTab:
                lnUpcomingSelected.setVisibility(View.VISIBLE);
                lnPastSelected.setVisibility(View.GONE);
                // tvPastTab.setBackground(new ColorDrawable(getResources().getColor(R.color.colorEventTabNoSelected)));
                tvPastTab.setTextColor(getResources().getColor(android.R.color.black));
                tvUpComingTab.setBackground(new ColorDrawable(getResources().getColor(android.R.color.white)));
                tvUpComingTab.setTextColor(getResources().getColor(R.color.colorPrimaryLight));
                rvUpcomingEvents.setVisibility(View.VISIBLE);
                rvPastEvents.setVisibility(View.GONE);
                checkData("upcoming");
                break;
            case R.id.tvPastTab:
                lnUpcomingSelected.setVisibility(View.GONE);
                lnPastSelected.setVisibility(View.VISIBLE);
                tvPastTab.setBackground(new ColorDrawable(getResources().getColor(android.R.color.white)));
                tvPastTab.setTextColor(getResources().getColor(R.color.colorPrimaryLight));
                tvUpComingTab.setTextColor(getResources().getColor(android.R.color.black));
                rvUpcomingEvents.setVisibility(View.GONE);
                rvPastEvents.setVisibility(View.VISIBLE);
                checkData("past");
                if (pastEvents.isEmpty())
                    makePastEventsRequest(Api.Events.DEFAULT_QUERY, Api.Events.DEFAULT_PAGE_NUMBER);
                break;
        }
    }

    private LinearLayoutManager upcomingLinearManager, pastLinearManager;
    private boolean loading = true;
    private int upcomingVisibleItemCount, upcomingPastVisiblesItems, upcomingTotalItemCount;
    private int pastPastVisiblesItems, pastVisibleItemCount, pastTotalItemCount;

    private void initUpcomingEventsAdapter() {

        upcomingLinearManager = new LinearLayoutManager(context);
        pastLinearManager = new LinearLayoutManager(context);

        /**
         * Setting Adapter for Upcoming trips
         */
        upcomingEventsAdapter = new EventsAdapter(((MainActivity) context), upcomingEvents);
        upcomingEventsAdapter.setOnItemClickedListener(onItemClickedListener);
        rvUpcomingEvents.setLayoutManager(upcomingLinearManager);
        rvUpcomingEvents.setAdapter(upcomingEventsAdapter);
        rvUpcomingEvents.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                getNextPageForUpcomingEvents(dy);
            }
        });
        rvUpcomingEvents.setNestedScrollingEnabled(false);

        /**
         * Setting Adapter for Past trips
         */
        pastEventsAdapter = new EventsAdapter(((MainActivity) context), pastEvents);
        pastEventsAdapter.setOnItemClickedListener(onItemClickedListener);

        rvPastEvents.setLayoutManager(new LinearLayoutManager(context));
        rvPastEvents.setAdapter(pastEventsAdapter);
        rvPastEvents.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                getNextPageForPastEvents(dy);
            }
        });
        rvPastEvents.setNestedScrollingEnabled(false);
    }

    private void getNextPageForUpcomingEvents(int dy) {
        if (dy > 0) //check for scroll down
        {
            upcomingVisibleItemCount = upcomingLinearManager.getChildCount();
            upcomingTotalItemCount = upcomingLinearManager.getItemCount();
            upcomingPastVisiblesItems = upcomingLinearManager.findFirstVisibleItemPosition();

            if (loading) {
                if ((upcomingVisibleItemCount + upcomingPastVisiblesItems) >= upcomingTotalItemCount) {
                    loading = false;
                    if (myResponse != null) {
                        String query = etQuery.getText().toString();
                        String pageToCall;

                        if (myResponse.getPages().equals(Api.Events.DEFAULT_PAGE_NUMBER)) {
                            pageToCall = String.valueOf(myResponse.getPages() + 1);
                        } else {
                            pageToCall = String.valueOf(upcomingResponse.getPages() + 1);
                        }

                        makeUpcomingEventsRequest(
                                (query.equals("") ? Api.Events.DEFAULT_QUERY : query),
                                pageToCall
                        );
                    }
                    Log.v(TAG, "Last Item Wow !");
                    // TODO : Do pagination.. i.e. fetch new data
                }
            }
        }
    }

    private void getNextPageForPastEvents(int dy) {
        if (dy > 0) //check for scroll down
        {
            pastVisibleItemCount = pastLinearManager.getChildCount();
            pastTotalItemCount = pastLinearManager.getItemCount();
            pastPastVisiblesItems = pastLinearManager.findFirstVisibleItemPosition();

            if (loading) {
                if ((pastVisibleItemCount + pastPastVisiblesItems) >= pastTotalItemCount) {
                    loading = false;
                    if (myResponse != null) {
                        String query = etQuery.getText().toString();
                        String pageToCall;

                        if (myResponse.getPages().equals(Api.Events.DEFAULT_PAGE_NUMBER)) {
                            pageToCall = String.valueOf(myResponse.getPages() + 1);
                        } else {
                            pageToCall = String.valueOf(pastResponse.getPages() + 1);
                        }

                        makePastEventsRequest(
                                (query.equals("") ? Api.Events.DEFAULT_QUERY : query),
                                pageToCall
                        );
                    }
                    Log.v(TAG, "Last Item Wow !");
                    // TODO : Do pagination.. i.e. fetch new data
                }
            }
        }
    }

    private void makeUpcomingEventsRequest(String query, String pageToCall) {

        Call<Response> call = apiService.upcomingEventsList(
                pageToCall,
                Api.UpcomingEvents.DEFAULT_ALL,
                query
        );

        // mSwipeRefreshLayout.setRefreshing(true);
        aviLoading.smoothToShow();
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                if (response.isSuccessful()) {

                    upcomingResponse = response.body();

                    upcomingEvents.clear();
                    for (Data event : upcomingResponse.getDataList()) {
                        if (event.getStatus().equals(Api.UpcomingEvents.STATUS_ACTIVE)) {
                            upcomingEvents.add(event);
                        }
                    }
                } else {
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                checkData("upcoming");
                aviLoading.hide();

            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                aviLoading.hide();
                checkData("upcoming");

                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });

    }

    private void makePastEventsRequest(String query, String pageToCall) {

        Call<Response> call = apiService.pastEventsList(
                pageToCall,
                Api.UpcomingEvents.DEFAULT_ALL,
                query
        );
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                pastEvents.clear();
                if (response.isSuccessful()) {

                    pastResponse = response.body();
                    pastEvents.addAll(pastResponse.getDataList());
                    checkData("past");


                } else {
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                pastEventsAdapter.notifyDataSetChanged();

                //  aviLoading.hide();
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                checkData("past");
                // aviLoading.hide();

                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });

    }

    private void makeSearchRequest(String query, String pageToCall) {

        if (lnUpcomingSelected.getVisibility() == View.VISIBLE) {
            makeUpcomingEventsRequest(query, pageToCall);
        } else {
            makePastEventsRequest(query, pageToCall);
        }

    }

    private void checkData(String val) {


        try {
            if (val.equalsIgnoreCase("upcoming") &&
                    lnUpcomingSelected.getVisibility() == View.VISIBLE) {
                if (upcomingEvents.size() > 0) {
                    rvUpcomingEvents.setVisibility(View.VISIBLE);
                    txtNoData.setVisibility(View.GONE);
                    upcomingEventsAdapter.notifyDataSetChanged();
                } else {
                    rvUpcomingEvents.setVisibility(View.GONE);
                    if (lnUpcomingSelected.getVisibility() == View.VISIBLE) {
                        txtNoData.setText(getResources().getString(R.string.no_upcoming_event));
                        txtNoData.setVisibility(View.VISIBLE);
                    }
                }
            } else if (val.equalsIgnoreCase("past") &&
                    lnPastSelected.getVisibility() == View.VISIBLE) {
                if (pastEvents.size() > 0) {
                    rvPastEvents.setVisibility(View.VISIBLE);
                    txtNoData.setVisibility(View.GONE);
                    pastEventsAdapter.notifyDataSetChanged();
                } else {
                    rvPastEvents.setVisibility(View.GONE);
                    if (lnPastSelected.getVisibility() == View.VISIBLE) {
                        txtNoData.setText(getResources().getString(R.string.no_past_event));
                        txtNoData.setVisibility(View.VISIBLE);
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("Exception", "" + ex);
        }
        // mSwipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (lnPastSelected.getVisibility() == View.VISIBLE) {
            outState.putBoolean("upcoming", false);
        } else {
            outState.putBoolean("upcoming", true);
        }

//        getActivity().getSupportFragmentManager().putFragment(outState, UpcomingEventsFragment.TAG, UpcomingEventsFragment.this);

        //Save the fragment's state here
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            //Restore the fragment's state here

        }
    }


    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if (savedInstanceState == null)
            return;
        if (savedInstanceState.containsKey("upcoming")) {
            if (savedInstanceState.getBoolean("upcoming")) {
                tvUpComingTab.performLongClick();
            } else {
                tvPastTab.performLongClick();
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (txtNoData != null)
            txtNoData.setVisibility(View.GONE);

    }
}
