package com.creativedots.mylums.fragment.dashboardFragments;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.adapter.AlumniListAdapter;
import com.creativedots.mylums.adapter.autoComplete.DirectoryAutoCompleteArrayAdapter;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.model.retroFit.Data;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.utils.AppSettings;
import com.creativedots.mylums.utils.ValidationTools;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;

public class DirectorySearchFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = DirectorySearchFragment.class.getSimpleName();
    public static final String ADVANCE_SEARCH = "Advance search";

    private Response myResponse;
    private RecyclerView rvAlumniList;
    private ArrayList<Data> data = new ArrayList<>();
    private GridLayoutManager gridLayoutManager;
    private AlumniListAdapter alumniListAdapter;
    private boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private AVLoadingIndicatorView aviLoading;
    private boolean isAdvanceSearch = false;
    private TextView clearQuery, txtNoData;
    private ImageView ivSearch;
    private ImageView ivBack;
    private ImageView ivFilter;

    private LinearLayout llProgram, llYear;
    private TextView tvProgram, txtClear, txtApply, tvYear, txtClearAdv;
    private Spinner sProgram;
    //private EditText etQuery;
    private EditText etCountry, etCity, etCompanyName, etDesignation, etDepartment;
    private RelativeLayout rlSearch, rlShowAdvancedSearch;
    private AppCompatAutoCompleteTextView actvQuery;

    private LinearLayout llAdvancedSearchView, llRecyclerView, lnSearchBar;
    private boolean showingFilteredResult = false;

    private void getInfo(View view) {

        aviLoading = view.findViewById(R.id.aviLoading);
        ivBack = view.findViewById(R.id.ivBack);
        ivFilter = view.findViewById(R.id.ivFilter);

        rvAlumniList = view.findViewById(R.id.rvAlumniList);

        llRecyclerView = view.findViewById(R.id.llRecyclerView);
        llAdvancedSearchView = view.findViewById(R.id.llAdvancedSearchView);
        lnSearchBar = view.findViewById(R.id.lnSearchBar);

        llProgram = view.findViewById(R.id.llProgram);
        llYear = view.findViewById(R.id.llYear);
        tvProgram = view.findViewById(R.id.tvProgram);
        tvYear = view.findViewById(R.id.tvYear);
        txtClearAdv = view.findViewById(R.id.txtClearAdv);
        sProgram = view.findViewById(R.id.sProgram);

        //etQuery = view.findViewById(R.id.etQuery);

        actvQuery = view.findViewById(R.id.actvQuery);
        clearQuery = view.findViewById(R.id.clear_query);
        txtNoData = view.findViewById(R.id.txtNoData);

        ivSearch = view.findViewById(R.id.ivSearch);

        etCountry = view.findViewById(R.id.etCountry);
        etCity = view.findViewById(R.id.etCity);
        etCompanyName = view.findViewById(R.id.etCompanyName);
        etDesignation = view.findViewById(R.id.etDesignation);
        etDepartment = view.findViewById(R.id.etDepartment);

        rlShowAdvancedSearch = view.findViewById(R.id.rlShowAdvancedSearch);
        rlSearch = view.findViewById(R.id.rlSearch);
        //rlAdvancedSearch = view.findViewById(R.id.rlAdvancedSearch);
        txtApply = view.findViewById(R.id.txtApply);
        txtClear = view.findViewById(R.id.txtClear);
    }

    private void setInfo() {

        /**
         * Setting Program Items
         */
        actvQuery.setHint("Search Alumni Name");
        initProgramAdapter();

        llProgram.setOnClickListener(this);
        llYear.setOnClickListener(this);
        ivFilter.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        rlSearch.setOnClickListener(this);
        txtClear.setOnClickListener(this);
        txtApply.setOnClickListener(this);
        rlShowAdvancedSearch.setOnClickListener(this);
        txtClearAdv.setOnClickListener(this);

        View.OnClickListener searchClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == ivSearch) {
                    //Getting filtered result from autocomplete and setting in recycler view

                    if (ValidationTools.allFieldsFilled(actvQuery)) {
                        ArrayList<Data> filteredList = new ArrayList<>();
                        for (int i = 0; i < directoryAutoCompleteArrayAdapter.getCount(); i++) {
                            filteredList.add(directoryAutoCompleteArrayAdapter.getItem(i));
                        }
                        data.clear();
                        data.addAll(filteredList);
                        if (data.size() == 0) {
                            //txtNoData.setText(getString(R.string.advance_search));
                            txtNoData.setVisibility(View.VISIBLE);
                        } else {
                            errorGone();
                        }
                        alumniListAdapter.notifyDataSetChanged();
                        actvQuery.dismissDropDown();
                        //Saving Search State
                        showingFilteredResult = true;
                        AppSettings.hideKeyboard(getActivity(), actvQuery);
                        clearQuery.setVisibility(View.VISIBLE);
                    }
                } else if (view == clearQuery) {
                    //Saving Search State
                    showingFilteredResult = false;
                    actvQuery.setText("");
                    clearQuery.setVisibility(View.GONE);
                    //Clearing Recycler view
                    data.clear();
                    alumniListAdapter.notifyDataSetChanged();
                    //Fetching default data from page 1
                    fetchAlumniList("1");
                }
            }
        };
        ivSearch.setOnClickListener(searchClickListener);
        clearQuery.setOnClickListener(searchClickListener);

        /**
         * Setting RecyclerView to get AlumniList
         */
        initPaginationRecyclerView();

        initAutoCompleteAdapter();
    }

    private DirectoryAutoCompleteArrayAdapter directoryAutoCompleteArrayAdapter;

    private void initAutoCompleteAdapter() {
        directoryAutoCompleteArrayAdapter =
                new DirectoryAutoCompleteArrayAdapter(
                        ((MainActivity) context),
                        R.layout.actv_directory,
                        new ArrayList<Data>(),
                        apiService,
                        this);

        actvQuery.setAdapter(directoryAutoCompleteArrayAdapter);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_directory_search, container, false);
        getInfo(view);
        setInfo();
        setupAdvanceSearch();

        return view;
    }

    @Override
    public void onClick(View view) {
        AppSettings.hideKeyboard(context, view);

        switch (view.getId()) {
            case R.id.ivBack:
                if(llAdvancedSearchView.getVisibility() != View.VISIBLE)
                    getActivity().getSupportFragmentManager().popBackStack();
                else {
                    llAdvancedSearchView.setVisibility(View.GONE);
                    setViewVisibility(false, true);
                }
                break;
            case R.id.llProgram:
                pickProgram();
                break;
            case R.id.llYear:
                pickYear();
                break;
            case R.id.ivFilter:
                setViewVisibility(llAdvancedSearchView.getVisibility() != View.VISIBLE, llRecyclerView.getVisibility() != View.VISIBLE);
                break;
            case R.id.rlSearch:

                handleSearchEvent(Api.AlumniSearch.DEFAULT_PAGE_NUMBER, isAdvanceSearch, false);
                break;
            case R.id.txtApply:
                txtClearAdv.setVisibility(View.VISIBLE);
                isAdvanceSearch = true;
                if (!ValidationTools.minOneFieldFilled(etCountry.getText().toString(),
                        etCity.getText().toString(), tvProgram.getText().toString(),
                        etCompanyName.getText().toString(), etDesignation.getText().toString(),
                        etDepartment.getText().toString(), actvQuery.getText().toString())) {
                    Toast.makeText(context, R.string.warn_fill_at_least_one, Toast.LENGTH_SHORT).show();
                } else {
                    setViewVisibility(false, true);
                    handleSearchEvent(Api.AlumniSearch.DEFAULT_PAGE_NUMBER, isAdvanceSearch, false);
                }
                break;
            case R.id.txtClear:
                clearFields(true);
                isAdvanceSearch = false;
                handleSearchEvent("1", false, false);

                break;
            case R.id.rlShowAdvancedSearch:
                break;
            case R.id.txtClearAdv:
                txtClearAdv.setVisibility(View.GONE);
                clearFields(false);
                isAdvanceSearch = false;
                if (actvQuery.getText().toString().isEmpty()) {
                    fetchAlumniList("1");
                } else
                    handleSearchEvent("1", false, false);
                break;
        }
    }

    private ArrayList<String> sPrograms = new ArrayList<>();

    public void pickYear() {

        sPrograms.removeAll(sPrograms);
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        for (int i = year; i >= 1985; i--) {
            sPrograms.add("" + i);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Education Year").setAdapter(new ArrayAdapter<>(getActivity(),
                        android.R.layout.simple_list_item_1, sPrograms),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String program = sPrograms.get(i);
                        tvYear.setText(program);
                        tvYear.setTextColor(getResources().getColor(android.R.color.black));
                    }
                }).show();
    }

    public void pickProgram() {

        sPrograms.removeAll(sPrograms);
        sPrograms.add("PhD");
        sPrograms.add("EMBA");
        sPrograms.add("MBA");
        sPrograms.add("BSc");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Programs").setAdapter(new ArrayAdapter<>(getActivity(),
                        android.R.layout.simple_list_item_1, sPrograms),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String program = sPrograms.get(i);
                        tvProgram.setText(program);
                        tvProgram.setTextColor(getResources().getColor(android.R.color.black));
                    }
                }).show();
    }

    private void fetchAlumniList(String pageToCall) {
        Call<Response> call = apiService.alumniList("" + pageToCall);

        Log.e("URL", "" + call.request().url());
        aviLoading.smoothToShow();
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                handleSearchApiResponse(response, false);
                aviLoading.hide();
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                aviLoading.hide();
                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });
    }

    private void handleSearchEvent(String pageToCall, Boolean advancedSearch, Boolean isPagination) {


        if (advancedSearch) {

            //   if (handleAdvancedSearchEvent()) {
            if (!isPagination)
                data.clear();

            String country = etCountry.getText().toString();
            String city = etCity.getText().toString();
            String program = tvProgram.getText().toString();//String.valueOf(sProgram.getSelectedItem());

            String company = etCompanyName.getText().toString();
            String designation = etDesignation.getText().toString();
            String department = etDepartment.getText().toString();

            Call<Response> call = apiService.alumniSearch(actvQuery.getText().toString(), pageToCall,
                    Api.AlumniSearch.DEFAULT_ADVANCED,
                    country, city, program, company, designation, department, "");

            Log.e("URL", call.request().url() + "");
            aviLoading.smoothToShow();
            call.enqueue(new Callback<Response>() {
                @Override
                public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                    handleSearchApiResponse(response, true);
                    aviLoading.hide();
                }

                @Override
                public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    aviLoading.hide();
                    if (t.getLocalizedMessage().contains("No address associated with hostname"))
                        showNoNetworkDialog(context);
                }
            });

//            }

        } else if (ValidationTools.allFieldsFilled(actvQuery)) {

            if (!isPagination)
                data.clear();

            String query = actvQuery.getText().toString().trim();
            Call<Response> call;
            if (query.isEmpty()) {
                call = apiService.alumniList("" + pageToCall);
            } else {
                call = apiService.alumniSearch(query, pageToCall,
                        "",
                        "", "", "",
                        "", "", "", "");
            }
            call.enqueue(new Callback<Response>() {
                @Override
                public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                    handleSearchApiResponse(response, false);
                    aviLoading.hide();
                }

                @Override
                public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    aviLoading.hide();
                    Log.e("LOGLOG", "" + t.getMessage());
                    if (t.getLocalizedMessage().contains("No address associated with hostname"))
                        showNoNetworkDialog(context);
                }
            });

        } else if (isPagination) {
            fetchAlumniList(pageToCall + "");
        } else {
            Toast.makeText(context, R.string.warn_enter_query, Toast.LENGTH_SHORT).show();
        }
    }

    private void handleSearchApiResponse(@NonNull retrofit2.Response<Response> response, boolean flag) {
        if (response.isSuccessful()) {

            errorGone();
            /**
             * Setting UI to show alumni list.
             */
            myResponse = response.body();

            data.addAll(myResponse.getDataList());

            refreshList();
            /**
             * Resetting Pagination.
             */
            loading = true;

        } else {

            /**
             * Showing Error SayHiMessage to User.
             */


            try {
                if (data.size() == 0) {
                    txtNoData.setText(getErrorMessage(response.errorBody().string()));
                    txtNoData.setVisibility(View.VISIBLE);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @NonNull
    private Boolean handleAdvancedSearchEvent() {
        if (ValidationTools.allFieldsFilled(etCountry, etCity, etCompanyName, etDesignation, etDepartment)) {

            if (sProgram.getSelectedItemPosition() < sAdapter.getCount()) {
                return true;
            } else {
                Toast.makeText(context, R.string.warn_select_program, Toast.LENGTH_SHORT).show();
                return false;
            }

        } else {
            Toast.makeText(context, R.string.warn_fill_all_fields, Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void initPaginationRecyclerView() {
        gridLayoutManager = new GridLayoutManager(context, 2);
        rvAlumniList.setLayoutManager(gridLayoutManager);
        rvAlumniList.setNestedScrollingEnabled(false);

        alumniListAdapter = new AlumniListAdapter(((MainActivity) context), data);
        alumniListAdapter.setOnItemSelectedListener(new AlumniListAdapter.OnItemSelectedListener() {
            @Override
            public void onProfileSelected(int position) {

                /**
                 * Hiding Keyboard
                 */
                AppSettings.hideKeyboard(context.getApplicationContext(), ((MainActivity) context).getWindow().getDecorView());

                ViewAlumniProfileFragment viewAlumniProfileFragment = new ViewAlumniProfileFragment();
                viewAlumniProfileFragment.setAlmuniUserID(data.get(position).getUserId(), data.get(position).getUsertype());
                ((MainActivity) context).replaceFragment(
                        viewAlumniProfileFragment,
                        ViewAlumniProfileFragment.TAG,
                        true,
                        false
                );
            }
        });
        rvAlumniList.setAdapter(alumniListAdapter);

        rvAlumniList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            /*    if (showingFilteredResult)
                    return;*/

                visibleItemCount = gridLayoutManager.getChildCount();
                totalItemCount = gridLayoutManager.getItemCount();
                pastVisiblesItems = gridLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        loading = false;
                        int value = (totalItemCount / 10) + (totalItemCount % 10 > 0 ? 1 : 0);
                        if (totalItemCount >= 10 && value < myResponse.getPages())
                            handleSearchEvent((value + 1) + "", isAdvanceSearch, true);

                        Log.v(TAG, "Last Item Wow !");
                        // TODO : Do pagination.. i.e. fetch new data
                    }
                }
                //}
            }
        });
    }

    private ArrayAdapter<String> sAdapter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (data.size() == 0)
            fetchAlumniList("1");
        else
            refreshList();
    }

    private void initProgramAdapter() {

        sAdapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(R.id.text1)).setText("");
                    ((TextView) v.findViewById(R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };

        sAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        sAdapter.add("BSCS");
        sAdapter.add("BSIT");
        sAdapter.add("BSSE");
        sAdapter.add("BSEE");
        sAdapter.add("MSCS");
        sAdapter.add(getString(R.string.program));

        sProgram.setAdapter(sAdapter);
        sProgram.setSelection(sAdapter.getCount()); //display hint
    }

    private void setViewVisibility(Boolean showAdvancedSearch, Boolean showList) {

        llAdvancedSearchView.setVisibility(showAdvancedSearch ? View.VISIBLE : View.GONE);
        llRecyclerView.setVisibility(showList ? View.VISIBLE : View.GONE);
        lnSearchBar.setVisibility(showList ? View.VISIBLE : View.GONE);
        //txtNoData.setVisibility(showAdvancedSearch ? View.GONE : View.GONE);
        if (showAdvancedSearch)
            errorGone();
        else if (data.size() == 0)
            txtNoData.setVisibility(View.VISIBLE);
    }

    private void refreshList() {

        setViewVisibility(false, true);

        alumniListAdapter.notifyDataSetChanged();

        /**
         * For Search Filter
         */
        directoryAutoCompleteArrayAdapter.notifyDataSetChanged();

    }

    private void clearFields(boolean flag) {

        if (flag)
            data.clear();
        etCountry.setText("");
        etCity.setText("");
        tvProgram.setText("");
        etCompanyName.setText("");
        etDesignation.setText("");
        etDepartment.setText("");
        actvQuery.setText("");
    }

    public void errorGone() {
        txtNoData.setVisibility(View.GONE);
    }

    public boolean getIsAdvanceSearch() {
        return isAdvanceSearch;
    }


    public String getTvProgram() {
        return tvProgram.getText().toString();
    }

    public String getEtCountry() {
        return etCountry.getText().toString();
    }

    public String getEtCity() {
        return etCity.getText().toString();
    }

    public String getEtCompanyName() {
        return etCompanyName.getText().toString();
    }

    public String getEtDesignation() {
        return etDesignation.getText().toString();
    }

    public String getEtDepartment() {
        return etDepartment.getText().toString();
    }

    public void showCrossIcon(int visibility) {
        clearQuery.setVisibility(visibility);

    }


    private void setupAdvanceSearch() {
        ClickableSpan advSearch = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                //startBrowserForUrl(getString(R.string.const_terms_and_conditions), Api.URL_TERMS_AND_CONDITIONS);
                setViewVisibility(llAdvancedSearchView.getVisibility() != View.VISIBLE, llRecyclerView.getVisibility() != View.VISIBLE);

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(getResources().getColor(R.color.colorPrimary)); // specific color for this link
                ds.setFakeBoldText(true);
            }
        };

        makeLinks(txtNoData, new String[]{
                ADVANCE_SEARCH
        }, new ClickableSpan[]{
                advSearch
        });
    }


    public void makeLinks(TextView textView, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(textView.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            String startInda = textView.getText().toString();
            int startIndexOfLink = textView.getText().toString().toLowerCase().indexOf(link.toLowerCase());

            if (startIndexOfLink > -1)
                spannableString.setSpan(clickableSpan, startIndexOfLink,
                        startIndexOfLink + link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        textView.setHighlightColor(
                Color.TRANSPARENT); // prevent TextView change background when highlight
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
    }
}