package com.creativedots.mylums.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.messages.ChatMessageActivity;
import com.creativedots.mylums.adapter.MeetRequestsCardAdapter;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.model.retroFit.Message.MessageChannel;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.model.retroFit.notification.Notification;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.SwipeDirection;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class MeetRequestFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = MeetRequestFragment.class.getSimpleName();

    private ImageView ivBack;
    private CardStackView csvSwipeView;
    private MeetRequestsCardAdapter adapter;
    private ArrayList<Notification> mNotifications;
    private TextView txtNoData;

    private void getInfo(View view) {
        ivBack = view.findViewById(R.id.ivBack);
        csvSwipeView = view.findViewById(R.id.csvSwipeView);
        txtNoData = view.findViewById(R.id.txtNoData);
    }

    private void setInfo() {

        ivBack.setOnClickListener(this);

        setup();
        reload();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_meet_request, container, false);
        mNotifications = getArguments().getParcelableArrayList("notifications");
        getInfo(view);
        setInfo();

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }

    private MeetRequestsCardAdapter createMeetRequestsCardAdapter() {
        final MeetRequestsCardAdapter adapter = new MeetRequestsCardAdapter(context);

        adapter.addAll(mNotifications);

        return adapter;
    }

    private void setup() {
        csvSwipeView.setCardEventListener(new CardStackView.CardEventListener() {
            @Override
            public void onCardDragging(float percentX, float percentY) {
                Log.d("CardStackView", "onCardDragging");
            }

            @Override
            public void onCardSwiped(SwipeDirection direction) {
                Log.e("CardStackView", "onCardSwiped: " + direction.toString());
                Log.e("CardStackView", "topIndex: " + csvSwipeView.getTopIndex());
                Log.e("CardStackView", "getcount: " + csvSwipeView.getChildCount());

                if (csvSwipeView.getTopIndex() ==  1 ) {
                    Log.e("CardStackView", "Paginate: " + csvSwipeView.getTopIndex());
                    getActivity().getSupportFragmentManager().popBackStack();
//                    paginate();
                }


                switch (direction.toString()) {
                    case "Left"/*SwipeDirection.Left*/:
                        makeTinderResponse(csvSwipeView.getTopIndex() - 1, "reject");
                        Toast.makeText(context, "Decline", Toast.LENGTH_SHORT).show();
                        break;
                    case "Right"/*SwipeDirection.Right*/:
                        makeTinderResponse(csvSwipeView.getTopIndex() - 1, "accept");
                        moveToMessageScreen(csvSwipeView.getTopIndex() - 1);
                        Toast.makeText(context, "Accept", Toast.LENGTH_SHORT).show();
                        break;
                    case "Top"/*SwipeDirection.Top*/:
                        Toast.makeText(context, "Top", Toast.LENGTH_SHORT).show();
                        break;
                    case "Bottom"/*SwipeDirection.Bottom*/:
                        makeTinderResponse(csvSwipeView.getTopIndex() - 1, "later");
                        Toast.makeText(context, "Later", Toast.LENGTH_SHORT).show();
                        break;
                }
                if (csvSwipeView.getChildCount() == csvSwipeView.getTopIndex()) {
                    getActivity().getSupportFragmentManager().popBackStack();
                    txtNoData.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCardReversed() {
                Log.d("CardStackView", "onCardReversed");
            }

            @Override
            public void onCardMovedToOrigin() {
                Log.d("CardStackView", "onCardMovedToOrigin");
            }

            @Override
            public void onCardClicked(int index) {
                Log.d("CardStackView", "onCardClicked: " + index);
            }
        });
    }

    private void reload() {
        csvSwipeView.setVisibility(View.GONE);
        adapter = createMeetRequestsCardAdapter();
        csvSwipeView.setAdapter(adapter);
        csvSwipeView.setVisibility(View.VISIBLE);
    }

    private void makeTinderResponse(int pos, String status) {

        //will implement tinder today
        Call<Response> call = apiService.tinderMessage(
                status,
                mNotifications.get(pos).getSenderId()

        );
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                if (response.isSuccessful()) {


                } else {
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });
    }

    private void moveToMessageScreen(int pos) {

        MessageChannel messageChannel = new MessageChannel();
        messageChannel.setId(mNotifications.get(pos).getTypeId());
        messageChannel.setProfile(mNotifications.get(pos).getProfile());

        Intent startChatMessageActivity = new Intent(context, ChatMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("channel", messageChannel);
        startChatMessageActivity.putExtras(bundle);
        getActivity().startActivityForResult(startChatMessageActivity, 5001);
    }
}
