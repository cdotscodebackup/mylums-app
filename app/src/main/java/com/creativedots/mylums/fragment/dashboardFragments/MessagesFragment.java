package com.creativedots.mylums.fragment.dashboardFragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.activity.home.messages.ChatMessageActivity;
import com.creativedots.mylums.activity.home.messages.ComposeMessageActivity;
import com.creativedots.mylums.adapter.messages.MessagesAdapter;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.model.NotificationModel;
import com.creativedots.mylums.model.retroFit.Message.ChannelsResponse;
import com.creativedots.mylums.model.retroFit.Message.MessageChannel;
import com.creativedots.mylums.model.retroFit.Message.Messages;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.utils.AppSettings;
import com.creativedots.mylums.utils.DateConversion;
import com.creativedots.mylums.utils.Event;
import com.creativedots.mylums.utils.RecyclerItemTouchHelper;
import com.creativedots.mylums.utils.RxBus;
import com.creativedots.mylums.utils.sharedPreferences.Constants;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;

public class MessagesFragment extends BaseFragment implements View.OnClickListener, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {
    public static final String TAG = MessagesFragment.class.getSimpleName();
    private static final int RC_COMPOSE_MESSAGE_REQUEST = 909;
    public static final int RC_CHAT_ACTIVITY_REQUEST = 5001;

    private ImageView ivBack;
    private ImageView cvCompose;
    private EditText etQuery;
    //    private RelativeLayout rlSearch;
    private TextView txtNoData;

    private TabLayout tlTabs;
    private ImageView ivSearch;
    private int page;
    private ArrayList<MessageChannel> readMessages;
    private ArrayList<MessageChannel> unreadMessages;
    private ArrayList<MessageChannel> archiveMessages;
    private ArrayList<MessageChannel> tempArray;


    private RecyclerView rvMessages;
    private MessagesAdapter messagesAdapter;
    private LinearLayout coordinatorLayout;

    private AVLoadingIndicatorView aviLoading;

    private MessagesAdapter.OnItemClickedListener onItemClickedListener = new MessagesAdapter.OnItemClickedListener() {
        @Override
        public void onMessageClicked(int i) {

            if(i > tempArray.size()-1){
                Log.e(TAG, "Invalid index : " + String.valueOf(i));
                fetchMessagesList(false);
                return;
            }


            Intent startChatMessageActivity = new Intent(context, ChatMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelable("channel",tempArray.get(i));
            changeStatus(i);
            startChatMessageActivity.putExtras(bundle);
            startActivityForResult(startChatMessageActivity, RC_CHAT_ACTIVITY_REQUEST);
        }
    };

    private void getInfo(View view) {
        ivBack = view.findViewById(R.id.ivBack);

        tlTabs = view.findViewById(R.id.tabs);

        cvCompose = view.findViewById(R.id.cvCompose);

        etQuery = view.findViewById(R.id.etQuery);

        ivSearch = view.findViewById(R.id.ivSearch);

        rvMessages = view.findViewById(R.id.rvMessages);

        coordinatorLayout = view.findViewById(R.id.lnMainContainer);

        txtNoData = view.findViewById(R.id.txtNoData);

        aviLoading = view.findViewById(R.id.aviLoading);

        readMessages = new ArrayList<>();
        unreadMessages = new ArrayList<>();
        archiveMessages = new ArrayList<>();
        tempArray = new ArrayList<>();
    }

    private void setInfo() {


        registerWithBus();
        /**
         * initializing click listeners
         */
        initClickListeners();

        /**
         * initializing TabsLayout
         */
        initTabLayout();

        initMessagesAdapter();

        fetchMessagesList(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_messages, container, false);
        getInfo(view);
        setInfo();
        return view;
    }

    private void initClickListeners() {
        ivBack.setOnClickListener(this);
        cvCompose.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        etQuery.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filterList();
            }
        });
    }

    public void filterList(){
        ArrayList<MessageChannel> filterArray = new ArrayList<>();
        tempArray.clear();
        if (tlTabs.getSelectedTabPosition() == 0) {
            filterArray.addAll(readMessages);
        } else if (tlTabs.getSelectedTabPosition() == 1) {
            filterArray.addAll(unreadMessages);
        } else if (tlTabs.getSelectedTabPosition() == 2) {
            filterArray.addAll(archiveMessages);
        }

        for(int i =0; i < filterArray.size(); i++){
            if(filterArray.get(i).getProfile().getFirstName().toLowerCase().contains(etQuery.getText().toString().toLowerCase())
                    || filterArray.get(i).getProfile().getLastName().toLowerCase().contains(etQuery.getText().toString().toLowerCase()))
            tempArray.add(filterArray.get(i));
        }

        if (tempArray.size() == 0) {
            txtNoData.setText("No Message Found");
            txtNoData.setVisibility(View.VISIBLE);
            rvMessages.setVisibility(View.GONE);
        } else {
            txtNoData.setVisibility(View.GONE);
            rvMessages.setVisibility(View.VISIBLE);
        }
        messagesAdapter.notifyDataSetChanged();
        rvMessages.scrollToPosition(messagesAdapter.getItemCount()-1);
    }

    private void initTabLayout() {

        tlTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                assignList();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                AppSettings.hideKeyboard(context, view);
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.cvCompose:
                Intent startComposeMessageActivity = new Intent(context, ComposeMessageActivity.class);
                startComposeMessageActivity.putParcelableArrayListExtra("channel_list",tempArray);
                startActivityForResult(startComposeMessageActivity, RC_COMPOSE_MESSAGE_REQUEST);
                break;

        }
    }

    private void fetchMessagesList(Boolean showLoading) {

        Call<ChannelsResponse> call;
        call = apiService.ChannelsList(Api.Message.GET_URL_ALL);

        if(showLoading)
            aviLoading.smoothToShow();

        call.enqueue(new Callback<ChannelsResponse>() {
            @Override
            public void onResponse(@NonNull Call<ChannelsResponse> call, @NonNull retrofit2.Response<ChannelsResponse> response) {
                if (response.isSuccessful()) {
                    ChannelsResponse myResponse = response.body();
                    readMessages.clear();
                    unreadMessages.clear();
                    archiveMessages.clear();

                    /**
                     * Merge Both Read/Unread here
                     * myResponse.getData().getRead() : ArrayList<MessageChannel>
                     * myResponse.getData().getUnread() : ArrayList<MessageChannel>
                     */

                    readMessages.addAll(myResponse.getData().getRead());
                    unreadMessages.addAll(myResponse.getData().getUnread());
                    readMessages.addAll(unreadMessages);

                    Collections.sort(readMessages, new Comparator<MessageChannel>() {
                        public int compare(MessageChannel o1, MessageChannel o2) {
                            return o2.getTimeInMili() > o1.getTimeInMili() ? -1 : 1;
                        }
                    });

                    archiveMessages.addAll(myResponse.getData().getArchive());
                    assignList();

                } else {
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                aviLoading.hide();
            }

            @Override
            public void onFailure(@NonNull Call<ChannelsResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                aviLoading.hide();

                if(t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });
    }


    private void updateMsgStatus(String id,String status) {

        Call<Response> call;
        call = apiService.updateMessageStatus(id,status);
        //aviLoading.smoothToShow();

        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                if (response.isSuccessful()) {
                    Response myResponse = response.body();
                    fetchMessagesList(false);

                } else {
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
               // aviLoading.hide();
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
             //   aviLoading.hide();

                if(t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });
    }

    private void initMessagesAdapter() {
        // TODO : Update this Adapter to receive and display MessageChannel ArrayList.
        messagesAdapter = new MessagesAdapter(((MainActivity) context), tempArray);
        messagesAdapter.setOnItemClickedListener(onItemClickedListener);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setReverseLayout(true);
        rvMessages.setLayoutManager(layoutManager);
        rvMessages.setAdapter(messagesAdapter);
        rvMessages.scrollToPosition(messagesAdapter.getItemCount()-1);
         //  itemTouchHelper.attachToRecyclerView(rvMessages);
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rvMessages);

    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof MessagesAdapter.ViewHolder) {

            removeItem(viewHolder.getAdapterPosition());
            messagesAdapter.removeItem(viewHolder.getAdapterPosition());


        }
    }

    private void removeItem(int pos) {
        MessageChannel messageChannel = tempArray.get(pos);
        if (tlTabs.getSelectedTabPosition() == 0) {
            //messageChannel.setStatus("archive");
            updateMsgStatus(messageChannel.getId(),"archive");
            archiveMessages.add(messageChannel);
            readMessages.remove(pos);
        } else if (tlTabs.getSelectedTabPosition() == 1) {
           // messageChannel.setStatus("archive");
            updateMsgStatus(messageChannel.getId(),"archive");
            archiveMessages.add(messageChannel);
            unreadMessages.remove(pos);
        } else if (tlTabs.getSelectedTabPosition() == 2) {
            //messageChannel.setStatus("read");
            updateMsgStatus(messageChannel.getId(),"unarchive");
            readMessages.add(messageChannel);
            archiveMessages.remove(pos);
        }
    }

    private void assignList() {
        tempArray.clear();
        if (tlTabs.getSelectedTabPosition() == 0) {
            tempArray.addAll(readMessages);
        } else if (tlTabs.getSelectedTabPosition() == 1) {
            tempArray.addAll(unreadMessages);
        } else if (tlTabs.getSelectedTabPosition() == 2) {
            tempArray.addAll(archiveMessages);
        }
        if (tempArray.size() == 0) {
            txtNoData.setText("No Message Found");
            txtNoData.setVisibility(View.VISIBLE);
            rvMessages.setVisibility(View.GONE);
        } else {
            txtNoData.setVisibility(View.GONE);
            rvMessages.setVisibility(View.VISIBLE);
        }
        messagesAdapter.notifyDataSetChanged();
        rvMessages.scrollToPosition(messagesAdapter.getItemCount()-1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode != RESULT_OK){
            return;
        }

        if(requestCode == RC_COMPOSE_MESSAGE_REQUEST
                || requestCode == RC_CHAT_ACTIVITY_REQUEST){

            if(data != null){

                Bundle bundle = data.getExtras();
                if (bundle.containsKey("userId")) {
                    moveToAlumniProfile(bundle.getString("userId"));
                }

            } else{
                fetchMessagesList(false);
            }
        }
    }

    private void moveToAlumniProfile(String userId) {
        ViewAlumniProfileFragment viewAlumniProfileFragment = new ViewAlumniProfileFragment();
        viewAlumniProfileFragment.setAlmuniUserID(userId, "profile");
        ((MainActivity)context).replaceFragment(
                viewAlumniProfileFragment,
                ViewAlumniProfileFragment.TAG,
                true,
                false
        );
    }

    public void changeStatus(int pos){

        if(tempArray.size() > pos) {
            if(tempArray.get(pos).getStatus().equalsIgnoreCase("unread")) {
                tempArray.get(pos).setStatus("read");
                unreadMessages.remove(tempArray.get(pos));
                messagesAdapter.notifyDataSetChanged();
                Event event = new Event();
                event.setEventType(Event.REFRESH_NOTIFICATION_LIST);
                RxBus.defaultInstance().send(event);
            }
        }
    }

    private void registerWithBus() {
        RxBus.defaultInstance().toObservable().
                observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object event) {

                        if (event instanceof NotificationModel) {
                            NotificationModel notificationModel = (NotificationModel) event;
                            if (notificationModel.getType().equalsIgnoreCase(Constants.KEY_MESSAGE_TYPE)) {
                                if(getActivity() != null){
                                    fetchMessagesList(false);
                                }
                            }
                        }

                    }
                });
    }
}
