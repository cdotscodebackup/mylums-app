package com.creativedots.mylums.fragment.bottomBarFragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.activity.home.messages.ChatMessageActivity;
import com.creativedots.mylums.adapter.AttendeesAdapter;
import com.creativedots.mylums.adapter.NotificationsAdapter;
import com.creativedots.mylums.fragment.MeetRequestFragment;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.fragment.dashboardFragments.DiscountPartnersFragments;
import com.creativedots.mylums.fragment.dashboardFragments.EventDetailsFragment;
import com.creativedots.mylums.fragment.dashboardFragments.SayHiFragment;
import com.creativedots.mylums.fragment.dashboardFragments.ViewAlumniProfileFragment;
import com.creativedots.mylums.model.NotificationModel;
import com.creativedots.mylums.model.retroFit.Message.MessageChannel;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.model.retroFit.notification.Notification;
import com.creativedots.mylums.model.retroFit.notification.NotificationResponse;
import com.creativedots.mylums.utils.AppSettings;
import com.creativedots.mylums.utils.Event;
import com.creativedots.mylums.utils.RxBus;
import com.creativedots.mylums.utils.sharedPreferences.Constants;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;
import static com.creativedots.mylums.fragment.dashboardFragments.MessagesFragment.RC_CHAT_ACTIVITY_REQUEST;

public class NotificationsFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = NotificationsFragment.class.getSimpleName();

    private NotificationsAdapter notificationsAdapter;
    ArrayList<Notification> notificationList;
    private static final int RC_COMPOSE_MESSAGE_REQUEST = 909;
    private ImageView ivBack;
    private RecyclerView rvNotifications;
    private AVLoadingIndicatorView aviLoading;

    private void getInfo(View view) {

        ivBack = view.findViewById(R.id.ivBack);
        rvNotifications = view.findViewById(R.id.rvNotifications);
        aviLoading = view.findViewById(R.id.aviLoading);
    }

    private void setInfo() {
        ivBack.setOnClickListener(this);
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            activity.changeBottomTabs(false);
        }

        /**
         * Setting data for notifications
         */
        initNotificationsAdapter();

        fetchNotifications(true);

        registerWithBus();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);
        getInfo(view);
        setInfo();

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }

    private void initNotificationsAdapter() {

        notificationList = new ArrayList<>();
        notificationsAdapter = new NotificationsAdapter(((MainActivity) context), notificationList);
        notificationsAdapter.setOnItemClickedListener(onItemClickedListener);
        rvNotifications.setLayoutManager(new LinearLayoutManager(context));
        rvNotifications.setAdapter(notificationsAdapter);
    }


    private void fetchNotifications(Boolean flag) {
        Call<NotificationResponse> call = apiService.notifcationList();
        if(flag)
         aviLoading.smoothToShow();
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(@NonNull Call<NotificationResponse> call, @NonNull retrofit2.Response<NotificationResponse> response) {
                //  handleSearchApiResponse(response);
                 aviLoading.hide();

                if (response.isSuccessful()) {
                    NotificationResponse myResponse = response.body();

                    notificationList.clear();
                    notificationList.addAll(myResponse.getData());
                    notificationsAdapter.notifyDataSetChanged();

                } else {
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<NotificationResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                 aviLoading.hide();
                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });
    }

    private NotificationsAdapter.OnItemClickedListener onItemClickedListener = new NotificationsAdapter.OnItemClickedListener() {
        @Override
        public void onNotificationClicked(int pos) {

            String type = notificationList.get(pos).getType();
            try {
                if (notificationList.get(pos).getStatus().equalsIgnoreCase("unread")) {
                    markAsRead(notificationList.get(pos).getId());
                    notificationList.get(pos).setStatus("read");
                }
            } catch (Exception ex) {
                Log.e(NotificationsFragment.TAG + "Exception", ex + "");
            }


            notificationsAdapter.notifyDataSetChanged();
            if (type.equalsIgnoreCase(Constants.KEY_TINDER_TYPE)) {
                meetRequest();
            } else if (type.equalsIgnoreCase(Constants.KEY_EVENT_TYPE)) {
                EventDetailsFragment eventDetailsFragment = new EventDetailsFragment();
                eventDetailsFragment.setEventID(notificationList.get(pos).getTypeId());
                ((MainActivity) context).replaceFragment(eventDetailsFragment, EventDetailsFragment.TAG, true, false);
            } else if (type.equalsIgnoreCase(Constants.KEY_MESSAGE_TYPE)) {
                moveToMessageScreen(notificationList.get(pos));
            } else if (type.equalsIgnoreCase(Constants.KEY_DISCOUNT_TYPE)) {
                DiscountPartnersFragments discountPartnersFragments = new DiscountPartnersFragments();
                Bundle bundle = new Bundle();
                bundle.putBoolean("notification", true);
                discountPartnersFragments.setArguments(bundle);
                ((MainActivity) getActivity()).replaceFragment(discountPartnersFragments, DiscountPartnersFragments.TAG, true, false);

            } else if (type.equalsIgnoreCase(Constants.KEY_PROFILE_TYPE)) {
                ViewAlumniProfileFragment viewAlumniProfileFragment = new ViewAlumniProfileFragment();
                viewAlumniProfileFragment.setAlmuniUserID(notificationList.get(pos).getProfile().getUserId(), "profile");
                ((MainActivity) getActivity()).replaceFragment(
                        viewAlumniProfileFragment,
                        ViewAlumniProfileFragment.TAG,
                        true,
                        false
                );
            }else if(type.equalsIgnoreCase(Constants.KEY_POP_TYPE)){
                AppSettings.popUpDialog(getActivity(),notificationList.get(pos).getNotificationBody(),notificationList.get(pos).getNotificationTitle(),
                        notificationList.get(pos).getImagepath());
            }
        }
    };

    public void meetRequest() {
        ArrayList<Notification> notifications = new ArrayList<>();
        for (int i = 0; i < notificationList.size(); i++) {
            if (notificationList.get(i).getType().equalsIgnoreCase(Constants.KEY_TINDER_TYPE)) {
                notifications.add(notificationList.get(i));
            }
        }
        MeetRequestFragment fragment = new MeetRequestFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("notifications", notifications);
        fragment.setArguments(bundle);
        ((MainActivity) getActivity()).replaceFragment(fragment, MeetRequestFragment.TAG, true, false);

    }

    private void moveToMessageScreen(Notification notificationModel) {

        MessageChannel messageChannel = new MessageChannel();
        messageChannel.setId(notificationModel.getTypeId());
        messageChannel.setProfile(notificationModel.getProfile());

        Intent startChatMessageActivity = new Intent(getActivity(), ChatMessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("channel", messageChannel);
        startChatMessageActivity.putExtras(bundle);
        startActivityForResult(startChatMessageActivity, 5001);
    }


    private void markAsRead(String id) {

        Call<Response> call;
        call = apiService.readNotificationStatus(id);

        // aviLoading.smoothToShow();
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@io.reactivex.annotations.NonNull Call<Response> call, @io.reactivex.annotations.NonNull retrofit2.Response<Response> response) {
                //  handleSearchApiResponse(response);

                if (response.isSuccessful()) {

                    MainActivity activity = ((MainActivity) getActivity());
                    Response myResponse = response.body();
                    if (activity != null) {
                        if(!activity.tvNotifications.getText().toString().isEmpty()) {
                            int num = Integer.parseInt(activity.tvNotifications.getText().toString());
                           if(num <= 1)
                            activity.changeNotificationCount( "");
                        else
                            activity.changeNotificationCount((num-1)+"");
                        }
                    }
                } else {
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(@io.reactivex.annotations.NonNull Call<Response> call, @io.reactivex.annotations.NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            activity.changeBottomTabs(true);
        }

        Log.e("Destroy", "Destroy called");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == RC_COMPOSE_MESSAGE_REQUEST
                || requestCode == RC_CHAT_ACTIVITY_REQUEST) {

            if (data != null) {

                Bundle bundle = data.getExtras();
                if (bundle.containsKey("userId")) {
                    moveToAlumniProfile(bundle.getString("userId"));
                }

            }
        }
    }

    private void moveToAlumniProfile(String userId) {
        ViewAlumniProfileFragment viewAlumniProfileFragment = new ViewAlumniProfileFragment();
        viewAlumniProfileFragment.setAlmuniUserID(userId, "profile");
        ((MainActivity) context).replaceFragment(
                viewAlumniProfileFragment,
                ViewAlumniProfileFragment.TAG,
                true,
                false
        );
    }

    private void registerWithBus() {
        RxBus.defaultInstance().toObservable().
                observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object event) {

                        if (event instanceof Event) {

                            if (((Event) event).getEventType() == Event.REFRESH_NOTIFICATION_LIST) {
                                fetchNotifications(false);
                            }
                        }
                    }
                });
    }
}
