package com.creativedots.mylums.fragment.dashboardFragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.adapter.messages.MessagesAdapter;
import com.creativedots.mylums.adapter.profile.InterestsAdapter;
import com.creativedots.mylums.adapter.profile.WorkExperienceAdapter;
import com.creativedots.mylums.databinding.FragmentUpdateProfileBinding;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.model.NotificationModel;
import com.creativedots.mylums.model.retroFit.profile.City;
import com.creativedots.mylums.model.retroFit.profile.CityResponse;
import com.creativedots.mylums.model.retroFit.profile.Country;
import com.creativedots.mylums.model.retroFit.profile.CountryResponse;
import com.creativedots.mylums.model.retroFit.profile.Industry;
import com.creativedots.mylums.model.retroFit.profile.Interest;
import com.creativedots.mylums.model.retroFit.profile.InterestIndustryResponse;
import com.creativedots.mylums.model.retroFit.profile.ProfileResponse;
import com.creativedots.mylums.model.retroFit.profile.State;
import com.creativedots.mylums.model.retroFit.profile.StateResponse;
import com.creativedots.mylums.model.retroFit.profile.WorkExperience;
import com.creativedots.mylums.service.LinkedinController;
import com.creativedots.mylums.utils.Event;
import com.creativedots.mylums.utils.LinkedInHelper;
import com.creativedots.mylums.utils.RecyclerItemTouchHelper;
import com.creativedots.mylums.utils.RxBus;
import com.creativedots.mylums.utils.ValidationTools;
import com.creativedots.mylums.utils.sharedPreferences.Constants;
import com.creativedots.mylums.utils.sharedPreferences.LinkedInResponse;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.linkedin.platform.LISession;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class UpdateProfileFragment extends BaseFragment implements View.OnClickListener, WorkExperienceAdapter.OnItemSelectedListener, LinkedInResponse, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    public static final String TAG = UpdateProfileFragment.class.getSimpleName();

    private final String TEXT_EDIT = "EDIT";
    private final String TEXT_SAVE = "SAVE";
    private ProfileResponse profileResponse;
    private ArrayList<String> sPrograms = new ArrayList<>();
    private FragmentUpdateProfileBinding binding;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE_ADDRESS = 2;
    private InterestsAdapter interestsAdapter;
    private RecyclerView.LayoutManager layoutManagerInterests;
    private LinkedInHelper mLinkedInHelper;
    private WorkExperienceAdapter workExperienceAdapter;
    private RecyclerView.LayoutManager layoutManagerWorkExperience;
    private static final int IMAGE_PICK_REQUEST = 12345;
    private WorkExperience mWorkExperince = new WorkExperience();
    ItemTouchHelper.SimpleCallback itemTouchHelperCallback;
    ItemTouchHelper mItemTouchHelper;

    private String imagePath;
    private boolean isEditable = false;
    private boolean firstStateSetup = true;
    private boolean firstCitySetup = true;

    private void getInfo() {

        binding.headerPersonalDetail.setOnClickListener(this);
        binding.headerContactDetail.setOnClickListener(this);
        binding.headerEducatonDetail.setOnClickListener(this);
        binding.headerWorkDetail.setOnClickListener(this);
        binding.headerInterestDetail.setOnClickListener(this);
        binding.interest.setOnClickListener(this);
        binding.ivBack.setOnClickListener(this);
        binding.editPicture.setOnClickListener(this);
        binding.save.setOnClickListener(this);
        binding.connectWithLinkedin.setOnClickListener(this);
        binding.addWork.setOnClickListener(this);
        binding.workIndustry.setOnClickListener(this);
        binding.workTo.setOnClickListener(this);
        binding.workFrom.setOnClickListener(this);
        binding.editPicture.setOnClickListener(this);
        binding.workLocation.setOnClickListener(this);
        binding.program.setOnClickListener(this);
        binding.educationYear.setOnClickListener(this);

        binding.checkboxCurrentlyWorkHere.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    binding.lnTo.setVisibility(View.GONE);
                } else {
                    binding.lnTo.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.W_SHARE);
    }

    private void setInfo() {



       /* LISessionManager.getInstance(getActivity()).init(getActivity(), buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {
                setUpdateState();

            }

            @Override
            public void onAuthError(LIAuthError error) {

            }
        },true);*/


        Glide.with(this).load(profileResponse.getUser().getImagepath())
                .apply(new RequestOptions()
                        .centerCrop()
                        .dontTransform()
                        .placeholder(R.drawable.ic_user_place_holder))
                .into(binding.civProfileImage);

        //Setting Interests
        interestsAdapter = new InterestsAdapter(getActivity(), profileResponse.getUser().getInterests());
        layoutManagerInterests = new LinearLayoutManager(getActivity());
        binding.recyclerInterest.setLayoutManager(layoutManagerInterests);
        binding.recyclerInterest.setAdapter(interestsAdapter);
        binding.recyclerInterest.setNestedScrollingEnabled(false);
        //

        binding.save.setText(TEXT_EDIT);
        ChangeViewType(false);
        if (mWorkExperince != null) {
            binding.workCompanyName.setText(mWorkExperince.getCompanyName());
            binding.workIndustry.setText(mWorkExperince.getName());
            binding.workDestination.setText(mWorkExperince.getDesignation());
            binding.workFrom.setText(mWorkExperince.getFromDate());
            binding.workTo.setText(mWorkExperince.getToDate());
            binding.workLocation.setText(mWorkExperince.getCity());
            binding.checkboxCurrentlyWorkHere.setChecked(mWorkExperince.getIsCurrentJob().equals("yes"));
        }

        workExperienceAdapter = new WorkExperienceAdapter(getActivity(), profileResponse.getUser().getWorkExperience(), this, UpdateProfileFragment.this);
        layoutManagerWorkExperience = new LinearLayoutManager(getActivity());
        binding.recyclerWork.setLayoutManager(layoutManagerWorkExperience);
        binding.recyclerWork.setAdapter(workExperienceAdapter);
        binding.recyclerWork.setNestedScrollingEnabled(false);
        itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        mItemTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);

        registerWithBus();
        setupLocations();
    }

    @Override
    public void onClick(View view) {
        if (view == binding.headerPersonalDetail) {
            toggle(binding.expandableLayoutPersonalDetail);
        } else if (view == binding.headerContactDetail) {
            toggle(binding.expandableLayoutContactDetail);
        } else if (view == binding.headerEducatonDetail) {
            toggle(binding.expandableLayoutEducationDetail);
        } else if (view == binding.headerWorkDetail) {
            toggle(binding.expandableLayoutWorkDetail);
        } else if (view == binding.headerInterestDetail) {
            toggle(binding.expandableLayoutInterestDetail);
        } else if (view == binding.ivBack) {
            getActivity().onBackPressed();
        } else if (view == binding.interest) {
            pickInterest();
        } else if (view == binding.addWork) {
            addWorkExperience(false);
        } else if (view == binding.workIndustry) {
            pickIndustry();
        } else if (view == binding.workFrom) {
            pickWorkPeriod(0);
        } else if (view == binding.workTo) {
            pickWorkPeriod(1);
        } else if (view == binding.save) {

            if (binding.save.getText().toString().equalsIgnoreCase(TEXT_EDIT)) {
                ChangeViewType(true);
                binding.save.setText(TEXT_SAVE);
            } else {
                if (checkValidation()) {
                    if (addWorkExperience(true))
                        ChangeViewType(false);
                } else {
                    Toast.makeText(getActivity(), R.string.warn_fill_all_fields_2, Toast.LENGTH_SHORT).show();
                }
            }

        } else if (view == binding.editPicture) {

            displayChoiceDialog();
        } else if (view == binding.connectWithLinkedin) {
            mLinkedInHelper = new LinkedInHelper(getActivity(), this);
            mLinkedInHelper.performSignIn();
            //  LinkedinController linkedin= new LinkedinController(getActivity());
        } else if (view == binding.workLocation) {
            try {
                Intent intent =
                        new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                .build(getActivity());
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException e) {
                // TODO: Handle the error.
            } catch (GooglePlayServicesNotAvailableException e) {
                // TODO: Handle the error.
            }
        } else if (view == binding.program) {
            pickProgram();
        } else if (view == binding.educationYear) {
            pickYear();
        }
    }

    private void pickWorkPeriod(final int i) {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        new SpinnerDatePickerDialogBuilder()
                .context(getContext())
                .callback(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        String date = simpleDateFormat.format(calendar.getTime());
                        if (i == 0) {
                            mWorkExperince.setFromDate(date);
                            binding.workFrom.setText(date);
                        } else {
                            mWorkExperince.setToDate(date);
                            binding.workTo.setText(date);
                        }
                    }
                })
                .showTitle(true)
                .showDaySpinner(true)
                .defaultDate(mYear, mMonth, mDay)
                .maxDate(mYear, mMonth, mDay)
                .build()
                .show();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_update_profile, container, false);


        /**
         * Setting form to NOT_EDIT_ABLE state.
         */
        ChangeViewType(false);

        if (profileResponse != null && profileResponse.getUser() != null) {
            binding.setUser(profileResponse.getUser());
        }

        getInfo();

        if (profileResponse != null && profileResponse.getUser() != null)
            setInfo();

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (profileResponse == null) {

            binding.aviLoading.smoothToShow();
            apiService.getProfile().enqueue(new Callback<ProfileResponse>() {
                @Override
                public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                    if (getActivity() == null)
                        return;
                    binding.aviLoading.hide();
                    if (response.isSuccessful()) {
                        profileResponse = response.body();


                        if (profileResponse != null && profileResponse.getUser() != null) {
                            binding.setUser(profileResponse.getUser());
                            setInfo();
                        }
                        binding.executePendingBindings();

                    } else {

                    }
                }

                @Override
                public void onFailure(Call<ProfileResponse> call, Throwable t) {

                    if (getActivity() == null)
                        return;
                    binding.aviLoading.hide();
//                    if (t != null)
//                        Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                    if (t.getLocalizedMessage().contains("No address associated with hostname"))
                        showNoNetworkDialog(context);
                }
            });
        }


        if (mAllInterestList.isEmpty() || mAllIndustryList.isEmpty()) {
            apiService.getInterestIndustry().enqueue(new Callback<InterestIndustryResponse>() {
                @Override
                public void onResponse(Call<InterestIndustryResponse> call, Response<InterestIndustryResponse> response) {
                    if (getActivity() == null)
                        return;
                    if (response.isSuccessful()) {

                        if (response.body().getData().getIndustry() != null)
                            mAllIndustryList = response.body().getData().getIndustry();

                        if (response.body().getData().getInterests() != null)
                            mAllInterestList = response.body().getData().getInterests();

                    } else {

                    }
                }

                @Override
                public void onFailure(Call<InterestIndustryResponse> call, Throwable t) {
                    t.printStackTrace();
                    if (t.getLocalizedMessage().contains("No address associated with hostname"))
                        showNoNetworkDialog(context);
                }
            });
        }


    }


    void toggle(ExpandableLayout layout) {
        layout.toggle();
    }

    private ArrayList<Interest> mAllInterestList = new ArrayList<>();
    private ArrayList<Industry> mAllIndustryList = new ArrayList<>();
    private ArrayList<Country> mAllCountriesList = new ArrayList<>();
    private ArrayList<State> mAllStateList = new ArrayList<>();
    private ArrayList<City> mAllCityList = new ArrayList<>();

    public void pickInterest() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Interests").setAdapter(new ArrayAdapter<Interest>(getActivity(), android.R.layout.simple_list_item_1, mAllInterestList), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Interest selected = mAllInterestList.get(i);
                boolean exist = false;
                for (Interest interest : profileResponse.getUser().getInterests()) {
                    if (selected.getId().equalsIgnoreCase(interest.getId())) {
                        exist = true;
                        break;
                    }
                }
                if (!exist)
                    profileResponse.getUser().getInterests().add(selected);
                interestsAdapter.notifyDataSetChanged();
            }
        }).show();
    }

    public void pickIndustry() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Industry").setAdapter(new ArrayAdapter<Industry>(getActivity(), android.R.layout.simple_list_item_1, mAllIndustryList), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mWorkExperince.setName(mAllIndustryList.get(i).getName());
                mWorkExperince.setIndustryId(mAllIndustryList.get(i).getId());
                binding.workIndustry.setText(mAllIndustryList.get(i).getName());
                workExperienceAdapter.notifyDataSetChanged();
            }
        }).show();
    }

    public boolean addWorkExperience(boolean flag) {
        String companyName = binding.workCompanyName.getText().toString();
        String industry = binding.workIndustry.getText().toString();
        String destination = binding.workDestination.getText().toString();
        String from = binding.workFrom.getText().toString();
        String to = binding.workTo.getText().toString();
        String location = binding.workLocation.getText().toString();
        String industryId = mWorkExperince.getIndustryId();
        String currentlyWorkHere = binding.checkboxCurrentlyWorkHere.isChecked() ? "yes" : "no";
        if (currentlyWorkHere.equalsIgnoreCase("yes"))
            to = "";

        /**
         * If no field is edited then update profile and change viewState to ViewOnly
         */
        if (companyName.isEmpty() && industry.isEmpty() && destination.isEmpty() && from.isEmpty() && (to.isEmpty() && currentlyWorkHere.equalsIgnoreCase("no")) && location.isEmpty()) {
            if (flag)
                uploadProfileData();

            return true;
        }

        /**
         * If any field is edited and all fields are not filled then show error message and keep viewState to EditOnly
         */
        if (companyName.isEmpty() || industry.isEmpty() || destination.isEmpty() || from.isEmpty() || (to.isEmpty() && currentlyWorkHere.equalsIgnoreCase("no")) || location.isEmpty()) {

            Toast.makeText(getContext(), "Fill Required Fields of Work Experience", Toast.LENGTH_SHORT).show();
            return false;
        }/*else if (companyName.isEmpty() && industry.isEmpty() && destination.isEmpty() && from.isEmpty() && (to.isEmpty() && currentlyWorkHere.equalsIgnoreCase("no")) && location.isEmpty()) {
            //Toast.makeText(getContext(), "Fill Required Fields", Toast.LENGTH_SHORT).show();
            if(flag)
                uploadProfileData();
        }  */ else {
            WorkExperience workExperience = new WorkExperience();
            workExperience.setCompanyName(companyName);
            workExperience.setName(industry);
            workExperience.setDesignation(destination);
            workExperience.setFromDate(from);
            workExperience.setToDate(to);
            workExperience.setCity(mWorkExperince.getCity());
            workExperience.setStateProvince(mWorkExperince.getStateProvince());
            workExperience.setCountry(mWorkExperince.getCountry());
            workExperience.setLocation(location);

            workExperience.setIsCurrentJob(currentlyWorkHere);
            workExperience.setIndustryId(industryId);

            profileResponse.getUser().getWorkExperience().add(workExperience);
            workExperienceAdapter.notifyDataSetChanged();

            //Clearing input fields
            mWorkExperince = new WorkExperience();
            binding.workCompanyName.setText("");
            binding.workIndustry.setText("");
            binding.workDestination.setText("");
            binding.workFrom.setText("");
            binding.workTo.setText("");
            binding.workLocation.setText("");
            binding.checkboxCurrentlyWorkHere.setChecked(false);

            if (flag)
                uploadProfileData();
        }

        return true;
    }

    @Override
    public void onEditWorkExperience(int position) {
        WorkExperience workExperience = profileResponse.getUser().getWorkExperience().get(position);
        mWorkExperince = workExperience;
        binding.workCompanyName.setText(workExperience.getCompanyName());
        binding.workIndustry.setText(workExperience.getName());
        binding.workDestination.setText(workExperience.getDesignation());
        binding.workFrom.setText(workExperience.getFromDate());
        binding.workTo.setText(workExperience.getToDate());
        binding.workLocation.setText(workExperience.getCity());
        binding.checkboxCurrentlyWorkHere.setChecked(workExperience.getIsCurrentJob().equals("yes"));
        profileResponse.getUser().getWorkExperience().remove(position);
        workExperienceAdapter.notifyDataSetChanged();
    }

    private void setupLocations() {

    }

    private void uploadProfileData() {


        saveDataToObject();
        Call<ProfileResponse> call;
        call = apiService.updateProfile(profileResponse.getUser().getUserObject());

        binding.aviLoading.smoothToShow();
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(@io.reactivex.annotations.NonNull Call<ProfileResponse> call, @io.reactivex.annotations.NonNull retrofit2.Response<ProfileResponse> response) {
                //  handleSearchApiResponse(response);
                binding.aviLoading.hide();

                if (response.isSuccessful()) {
                    ProfileResponse myResponse = response.body();
                    SessionManager.from(getActivity()).saveToken(myResponse.getToken());
                    SessionManager.from(getActivity()).createUserLogin(myResponse.getUser());
                    binding.save.setText(TEXT_EDIT);
                    ChangeViewType(false);
                    if (getActivity() != null) {
                        ((MainActivity) getActivity()).initUserInfo();
                    }


                } else {
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(@io.reactivex.annotations.NonNull Call<ProfileResponse> call, @io.reactivex.annotations.NonNull Throwable t) {
                t.printStackTrace();
                binding.aviLoading.hide();
                //   aviLoading.hide();
                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });

        Log.e("HASHMAP", profileResponse.getUser().getUserObject().toString());
    }

    private void uploadImage(File file) {


        Call<ProfileResponse> call;
        if (file == null)
            file = new File(imagePath);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", "image", RequestBody.create(MediaType.parse("*/*"), file));
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), profileResponse.getUser().getFirstName());
        call = apiService.uploadProfileImage(filePart, name);

        //    aviLoading.smoothToShow();
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(@io.reactivex.annotations.NonNull Call<ProfileResponse> call, @io.reactivex.annotations.NonNull retrofit2.Response<ProfileResponse> response) {
                //  handleSearchApiResponse(response);
                //          aviLoading.hide();

                if (getActivity() == null)
                    return;
                if (response.isSuccessful()) {
                    ProfileResponse myResponse = response.body();
                    SessionManager.from(getActivity()).saveToken(myResponse.getToken());
                    SessionManager.from(getActivity()).createUserLogin(myResponse.getUser());
                    if (getActivity() != null) {
                        ((MainActivity) getActivity()).initUserInfo();
                    }

                } else {
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@io.reactivex.annotations.NonNull Call<ProfileResponse> call, @io.reactivex.annotations.NonNull Throwable t) {
                t.printStackTrace();
                //   aviLoading.hide();
                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });

        Log.e("HASHMAP", profileResponse.getUser().getUserObject().toString());
    }

    private void ChangeViewType(boolean flag) {

        if (SessionManager.from(getActivity()).getLinkedInConnected()) {
            binding.connectWithLinkedin.setText("Connected");
            binding.connectWithLinkedin.setOnClickListener(null);
        }

        isEditable = flag;
        if (interestsAdapter != null)
            interestsAdapter.setIS_EDITABLE(isEditable);

        binding.setViewState(flag);

        if (flag) {

            if(itemTouchHelperCallback != null)
                itemTouchHelperCallback.setDefaultSwipeDirs(ItemTouchHelper.LEFT);
            mItemTouchHelper.attachToRecyclerView(binding.recyclerWork);
            binding.expandableLayoutContactDetail.collapse();
            binding.expandableLayoutEducationDetail.collapse();
            binding.expandableLayoutInterestDetail.collapse();
            binding.expandableLayoutPersonalDetail.collapse();
            binding.expandableLayoutWorkDetail.collapse();

        } else {
            if(itemTouchHelperCallback != null)
            itemTouchHelperCallback.setDefaultSwipeDirs(0);
            binding.expandableLayoutContactDetail.expand();
            binding.expandableLayoutEducationDetail.expand();
            binding.expandableLayoutInterestDetail.expand();
            binding.expandableLayoutPersonalDetail.expand();
            binding.expandableLayoutWorkDetail.expand();
        }

        if (workExperienceAdapter != null)
            workExperienceAdapter.notifyDataSetChanged();
    }

    private void displayChoiceDialog() {
        String choiceString[] = new String[]{"Gallery", "Camera"};
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.setTitle("Select image from");
        dialog.setItems(choiceString,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent;
                        ArrayList<String> permissions = new ArrayList<>();
                        if (!ValidationTools.isPermissionGranted(getActivity(), Manifest.permission.CAMERA)) {
                            permissions.add(Manifest.permission.CAMERA);
                        }
                        if (!ValidationTools.isPermissionGranted(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                        }

                        String[] array = permissions.toArray(new String[permissions.size()]);

                        if (array.length == 0) {

                            if (which == 0) {

                                intent = new Intent(
                                        Intent.ACTION_PICK,
                                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(
                                        Intent.createChooser(intent, "Select profile picture"), IMAGE_PICK_REQUEST);
                            } else {

                                intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(
                                        Intent.createChooser(intent, "Select profile picture"), IMAGE_PICK_REQUEST);
                            }
                        } else
                            ValidationTools.requestPermission(UpdateProfileFragment.this, array);


                    }
                }).

                show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_PICK_REQUEST)
            if (resultCode == RESULT_OK && data != null) {
                Bitmap bitmap;
                Uri selectedImageUri = data.getData();
                if (selectedImageUri != null) {
                    imagePath = getRealPathFromURI(getActivity(), selectedImageUri);

                    Glide.with(context)
                            .load(imagePath)
                            .apply(new RequestOptions().centerCrop().circleCrop()
                                    .error(R.drawable.ic_user_tab_primary).placeholder(R.drawable.ic_user_tab_primary))
                            .into(binding.civProfileImage);
                    uploadImage(null);
                } else {
                    bitmap = (Bitmap) data.getExtras().get("data");
                    Glide.with(this).load(bitmap)
                            .apply(new RequestOptions()
                                    .centerCrop()
                                    .dontTransform()
                                    .placeholder(R.drawable.ic_user_place_holder))
                            .into(binding.civProfileImage);
                    Uri tempUri = getImageUri(getActivity(), bitmap);

                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    imagePath = getRealPathFromURI(tempUri);
                    uploadImage(null);

                }
            }

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                binding.workLocation.setText(getCountryCity(place, PLACE_AUTOCOMPLETE_REQUEST_CODE));
                Log.e(TAG, "Place: " + place.toString());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_ADDRESS) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                //      binding.address.setText(getCountryCity(place, PLACE_AUTOCOMPLETE_REQUEST_CODE_ADDRESS));
                Log.e(TAG, "Place: " + place.toString());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    /**
     * get actual path from uri
     *
     * @param context    context
     * @param contentUri uri
     * @return actual path
     */
    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] projection = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, projection, null, null, null);

            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getActivity().getContentResolver() != null) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void saveDataToObject() {
        if (profileResponse.getUser() == null)
            return;
        profileResponse.getUser().setFirstName(binding.firstName.getText().toString());
        profileResponse.getUser().setLastName(binding.lastName.getText().toString());
        profileResponse.getUser().setGender(getSelectedGender());
//        profileResponse.getUser().setAddress(binding.address.getText().toString());
        profileResponse.getUser().setAlternativeEmail(binding.email.getText().toString());
        profileResponse.getUser().setPhone(binding.mobileNumber.getText().toString());
        profileResponse.getUser().setLandline(binding.landlineNumber.getText().toString());
        profileResponse.getUser().setProgram(binding.program.getText().toString());
        profileResponse.getUser().setYear(binding.educationYear.getText().toString());

    }

    private String getSelectedGender() {
        int id = binding.genderGroup.getCheckedRadioButtonId();
        View radioButton = binding.genderGroup.findViewById(id);
        int radioId = binding.genderGroup.indexOfChild(radioButton);
        RadioButton btn = (RadioButton) binding.genderGroup.getChildAt(radioId);
        String selection = (String) btn.getText();
        return selection;
    }

    private void setUpdateState() {
        LISessionManager sessionManager = LISessionManager.getInstance(getActivity());
        LISession session = sessionManager.getSession();
        boolean accessTokenValid = session.isValid();
    }

    @Override
    public void onLinkedInSignInFail() {
        //   Toast.makeText(getActivity(), "LinkedIn sign in failed.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLinkedInSignInSuccess(String accessToken) {
        //  Toast.makeText(getActivity(), "Linked in signin successful.\n Getting user profile...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLinkedInProfileReceived(LinkedInHelper.LinkedInUser user) {

        if (user.name != null) {
            String[] name = user.name.split(" ");
            if (binding.firstName.getText().toString().isEmpty()) {
                if (name.length > 0)
                    binding.firstName.setText(name[0]);
            }
            if (binding.lastName.getText().toString().isEmpty()) {
                if (name.length > 1)
                    binding.lastName.setText(name[1]);
            }
        }
        if (binding.email.getText().toString().isEmpty())
            if (user.email != null) {
                binding.email.setText(user.email);
            }
        if (user.pictureUrl != null) {
            Glide.with(this).load(user.pictureUrl)
                    .apply(new RequestOptions()
                            .centerCrop()
                            .dontTransform()
                            .placeholder(R.drawable.ic_user_place_holder))
                    .into(binding.civProfileImage);
            new DownloadImageTask().execute(user.pictureUrl);

        }
        SessionManager.from(getActivity()).setLinkedInConnected(true);
        //   Toast.makeText(getActivity(), "LinkedIn user data: name= " + user.name + " email= " + user.email, Toast.LENGTH_SHORT).show();
    }

    public boolean checkValidation() {

        if (binding.firstName.getText().toString().isEmpty())
            return false;

        if (binding.lastName.getText().toString().isEmpty())
            return false;

        if (binding.program.getText().toString().isEmpty())
            return false;

        if (binding.educationYear.getText().toString().isEmpty())
            return false;

        if (binding.mobileNumber.getText().toString().isEmpty())
            return false;


        return true;
    }

    public String getCountryCity(Place place, int type) {
        Geocoder geocoder = new Geocoder(getActivity());
        try {
            List<Address> addresses = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
            String address = addresses.get(0).getAddressLine(0);
            String city = addresses.get(0).getLocality();
            String country = addresses.get(0).getCountryName();
            String state = addresses.get(0).getAdminArea();
            if (type == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
                mWorkExperince.setCity(city);
                mWorkExperince.setStateProvince(state);
                mWorkExperince.setCountry(country);
                return city + ", " + country;
            } else {

                return address;
            }
        } catch (IOException e) {

            e.printStackTrace();
        }
        return "";
    }

    public void pickProgram() {

        sPrograms.removeAll(sPrograms);
        sPrograms.add("PhD");
        sPrograms.add("EMBA");
        sPrograms.add("MBA");
        sPrograms.add("BSc");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Programs").setAdapter(new ArrayAdapter<>(getActivity(),
                        android.R.layout.simple_list_item_1, sPrograms),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String program = sPrograms.get(i);
                        binding.program.setText(program);
                        binding.program.setTextColor(getResources().getColor(android.R.color.black));
                    }
                }).show();
    }

    public void pickYear() {

        sPrograms.removeAll(sPrograms);
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        for (int i = year; i >= 1985; i--) {
            sPrograms.add("" + i);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Education Year").setAdapter(new ArrayAdapter<>(getActivity(),
                        android.R.layout.simple_list_item_1, sPrograms),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String program = sPrograms.get(i);
                        binding.educationYear.setText(program);
                        binding.educationYear.setTextColor(getResources().getColor(android.R.color.black));
                    }
                }).show();
    }

    private void registerWithBus() {
        RxBus.defaultInstance().toObservable().
                observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object event) {

                        if (event instanceof Event) {
                            if (((Event) event).getEventType() == Event.UPDATE_PROFILE_BACK) {
                                if (binding.save.getText().toString().equalsIgnoreCase(TEXT_SAVE)) {
                                    binding.save.setText(TEXT_EDIT);
                                    ChangeViewType(false);
                                } else {
                                    if (getActivity() != null)
                                        getActivity().getSupportFragmentManager().popBackStack();
                                }
                            }
                        }

                    }
                });
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof WorkExperienceAdapter.ViewHolder) {
            workExperienceAdapter.removeItem(viewHolder.getAdapterPosition());
        }
    }

    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        public DownloadImageTask() {

        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap bmp = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                bmp = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return bmp;
        }

        protected void onPostExecute(Bitmap result) {

            getLocalBitmapUri(result);
        }
    }

    public void getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {

            File cachePath = new File(context.getCacheDir(), "images");
            cachePath.mkdirs(); // don't forget to make the directory
            FileOutputStream stream = new FileOutputStream(cachePath + "/image.png"); // overwrites this image every time
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            stream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        File imagePath = new File(context.getCacheDir(), "images");
        File newFile = new File(imagePath, "image.png");


        if (newFile != null) {

            uploadImage(newFile);
        }
        //   return bmpUri;
    }
}
