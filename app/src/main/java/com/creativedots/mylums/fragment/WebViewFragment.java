package com.creativedots.mylums.fragment;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.databinding.FragmentWebViewBinding;
import com.creativedots.mylums.fragment.base.BaseFragment;

public class WebViewFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = WebViewFragment.class.getSimpleName();

    private String TITLE, URL;

    public static WebViewFragment newInstance(String TITLE, String URL) {
        WebViewFragment fragment = new WebViewFragment();
        fragment.TITLE = TITLE;
        fragment.URL = URL;
        return fragment;
    }

    private FragmentWebViewBinding binding;

    @SuppressLint("SetJavaScriptEnabled")
    private void setInfo() {

        /**
         * Showing loading view
         */
        binding.aviLoading.show();

        binding.tvTitle.setText(TITLE);

        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(context, description, Toast.LENGTH_SHORT).show();
            }
            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });
        binding.webView.loadUrl(URL);

        binding.webView.setWebViewClient(new WebViewClient(){

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                /**
                 * Hiding Loading view on finished webpage loading
                 */
                binding.aviLoading.hide();
            }
        });

        binding.ivBack.setOnClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_web_view, container, false);

        setInfo();
        return binding.getRoot();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivBack:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }
}
