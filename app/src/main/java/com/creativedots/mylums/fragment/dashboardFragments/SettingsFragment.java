package com.creativedots.mylums.fragment.dashboardFragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.fragment.WebViewFragment;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.model.retroFit.Data;
import com.creativedots.mylums.model.retroFit.settings.Setting;
import com.creativedots.mylums.model.retroFit.settings.SettingsGetResponse;
import com.creativedots.mylums.model.retroFit.settings.SettingsUpdateResponse;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;
import com.wang.avi.AVLoadingIndicatorView;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = SettingsFragment.class.getSimpleName();

    private ImageView ivBack;
    private CircleImageView civProfileImage;
    private TextView tvName, tvDesignation, tvCompany;
    private TextView tvUserName, tvRollNo;
    private LinearLayout llChangePassword;

    private Switch sMuteNotifications, sLocation;

    private TextView tvDiscountRadius;
    private SeekBar sbDiscountRadius;

    private TextView tvTermsAndConditions, tvPrivacyPolicy;

    private AVLoadingIndicatorView avi;

    private void getInfo(View view){
        ivBack = view.findViewById(R.id.ivBack);

        avi = view.findViewById(R.id.avi);

        civProfileImage = view.findViewById(R.id.civProfileImage);
        tvName = view.findViewById(R.id.tvName);
        tvDesignation = view.findViewById(R.id.tvDesignation);
        tvCompany = view.findViewById(R.id.tvCompany);

        tvUserName = view.findViewById(R.id.tvUserName);
        tvRollNo = view.findViewById(R.id.tvRollNo);
        llChangePassword = view.findViewById(R.id.llChangePassword);

        sMuteNotifications = view.findViewById(R.id.sMuteNotifications);
        sLocation = view.findViewById(R.id.sLocation);

        tvDiscountRadius = view.findViewById(R.id.tvDiscountRadius);
        sbDiscountRadius = view.findViewById(R.id.sbDiscountRadius);

        tvTermsAndConditions = view.findViewById(R.id.tvTermsAndConditions);
        tvPrivacyPolicy = view.findViewById(R.id.tvPrivacyPolicy);
    }

    private void setInfo(){

        /**
         * Getting settings from database
         */
        getSettingsFromDatabase();

        /**
         * Updating UI with user's data
         */
        initUserData();

        /**
         * Initializing Click Listeners
         */
        initClickListeners();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        getInfo(view);
        setInfo();

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivBack:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.llChangePassword:
                ((MainActivity) context)
                        .addFragment(new ChangePasswordFragment(),
                                ChangePasswordFragment.TAG, true, false);
                break;
            case R.id.tvTermsAndConditions:
                startBrowserForUrl(getString(R.string.const_terms_and_conditions), Api.URL_TERMS_AND_CONDITIONS);
                break;
            case R.id.tvPrivacyPolicy:
                startBrowserForUrl(getString(R.string.label_privacy_policy), Api.URL_PRIVACY_POLICY);
                break;

        }
    }

    private void startBrowserForUrl(String title, String url) {
        WebViewFragment webViewFragment = WebViewFragment.newInstance(title, url);
        ((MainActivity)context).addFragment(webViewFragment, WebViewFragment.TAG, true, false);
    }

    private void getSettingsFromDatabase() {
        avi.show();

        Call<SettingsGetResponse> call = apiService.getUserSettings();
        call.enqueue(new Callback<SettingsGetResponse>() {
            @Override
            public void onResponse(@NonNull Call<SettingsGetResponse> call, @NonNull Response<SettingsGetResponse> response) {
                avi.hide();
                if(response.isSuccessful()){

                    SettingsGetResponse myResponse = response.body();

                    Setting setting = myResponse.getSetting();

                    if(setting.getMute() == null){
                        setting.setMute("");
                    }
                    if(setting.getLocation() == null){
                        setting.setLocation("");
                    }
                    if(setting.getOfferradius() == null){
                        setting.setOfferradius("5");
                    }


                    sMuteNotifications.setChecked(setting.getMute().equals(Api.SettingsGet.KEY_NOTIFICATIONS_YES));
                    sLocation.setChecked(!setting.getLocation().equals(Api.SettingsGet.KEY_LOCATION_NO));
                    sbDiscountRadius.setProgress(Integer.valueOf(setting.getOfferradius()));

                    BOOL_READY_TO_UPDATE = true;


                }else{
                    displayErrorMessage(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(@NonNull Call<SettingsGetResponse> call, @NonNull Throwable t) {
                avi.hide();
                t.printStackTrace();

                if(t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });
    }

    private static final int UPDATE_TYPE_NOTIFICATION = 1;
    private static final int UPDATE_TYPE_LOCATION = 2;
    private static final int UPDATE_TYPE_OFFER_RADIUS = 3;

    private Boolean BOOL_READY_TO_UPDATE = false;

    private void updateSettingsOnDatabase(final int updateID){

        if(BOOL_READY_TO_UPDATE){
            avi.show();

            Call<SettingsUpdateResponse> call = apiService.updateUserSettings(
                    sMuteNotifications.isChecked()?Api.SettingsUpdate.KEY_NOTIFICATIONS_YES:Api.SettingsUpdate.KEY_NOTIFICATIONS_NO,
                    sLocation.isChecked()?Api.SettingsUpdate.KEY_LOCATION_YES:Api.SettingsUpdate.KEY_LOCATION_NO,
                    String.valueOf(sbDiscountRadius.getProgress()));

            call.enqueue(new Callback<SettingsUpdateResponse>() {
                @Override
                public void onResponse(@NonNull Call<SettingsUpdateResponse> call,@NonNull Response<SettingsUpdateResponse> response) {
                    avi.hide();
                    if(response.isSuccessful()){

//                        Boolean updated;

//                        updated = response.body().getStatus().equalsIgnoreCase(Api.SettingsUpdate.RESPONSE_SUCCESSFULL);

                        switch (updateID){
                            case UPDATE_TYPE_NOTIFICATION:
//                                Toast.makeText(context,
//                                        (updated) ? R.string.warn_notification_enabled
//                                                : R.string.warn_notifications_disabled,
//                                        Toast.LENGTH_SHORT
//                                ).show();
                                break;
                            case UPDATE_TYPE_LOCATION:
//                                Toast.makeText(context,
//                                        (updated) ? R.string.warn_location_enabled
//                                                : R.string.warn_location_disabled,
//                                        Toast.LENGTH_SHORT
//                                ).show();

                                break;
                            case UPDATE_TYPE_OFFER_RADIUS:

                                break;
                        }

                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }else{
                        displayErrorMessage(response.errorBody().toString());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SettingsUpdateResponse> call, @NonNull Throwable t) {
                    avi.hide();
                    t.printStackTrace();

                    if(t.getLocalizedMessage().contains("No address associated with hostname"))
                        showNoNetworkDialog(context);
                }
            });
        }
    }

    private void initUserData() {

        Data userData = SessionManager.from(context).getUser();

        Glide.with(context).load(userData.getImage())
                .apply(new RequestOptions().placeholder(R.drawable.ic_user_place_holder))
                .into(civProfileImage);
        tvName.setText(userData.getFirstName() + " " + userData.getLastName());
        tvDesignation.setText(userData.getDesignation());
        tvCompany.setText(userData.getCompany());
        tvUserName.setText(userData.getEmail());
        tvRollNo.setText(userData.getRollNumber());
    }

    private void initClickListeners() {
        ivBack.setOnClickListener(this);
        llChangePassword.setOnClickListener(this);
        tvTermsAndConditions.setOnClickListener(this);
        tvPrivacyPolicy.setOnClickListener(this);

        sMuteNotifications.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                updateSettingsOnDatabase(UPDATE_TYPE_NOTIFICATION);
            }
        });
        sLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {


                updateSettingsOnDatabase(UPDATE_TYPE_LOCATION);
            }
        });

        sbDiscountRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(i ==0)
                    i = 1;
                tvDiscountRadius.setText(i + " km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                updateSettingsOnDatabase(UPDATE_TYPE_OFFER_RADIUS);
            }
        });
    }
}
