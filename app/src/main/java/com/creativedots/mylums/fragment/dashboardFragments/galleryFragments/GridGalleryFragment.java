package com.creativedots.mylums.fragment.dashboardFragments.galleryFragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.adapter.event.gallery.GridAdapter;
import com.creativedots.mylums.databinding.FragmentGridGalleryBinding;
import com.creativedots.mylums.activity.YoutubePlayActivity;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.model.eventDetail.EventDetail;
import com.creativedots.mylums.model.eventDetail.GalleryProduct;

import java.util.ArrayList;

public class GridGalleryFragment extends BaseFragment {
    public static final String TAG = GridGalleryFragment.class.getSimpleName();

    private FragmentGridGalleryBinding binding;

    private String type;
    private EventDetail mEventDetail = new EventDetail();
    private ArrayList<GalleryProduct> mGalleryProducts = new ArrayList<>();

    public void setGalleryProducts(ArrayList<GalleryProduct> mGalleryProducts, EventDetail mEventDetail, String type) {
        this.mGalleryProducts = mGalleryProducts;
        this.mEventDetail = mEventDetail;
        this.type = type;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_grid_gallery, container, false);
        setInfo();
        return binding.getRoot();
    }

    private void setInfo() {

        GridAdapter gridAdapter = new GridAdapter(context, mGalleryProducts);
        gridAdapter.setOnItemClickListener(new GridAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int pos) {

               GalleryFragment fragment = new GalleryFragment();
                Bundle bundle = new Bundle();
                if (type.equalsIgnoreCase("photos")){
                    bundle.putParcelableArrayList("images", mEventDetail.getGallery().getPhotosList());
                    bundle.putInt(GalleryFragment.SELECTED_IMAGE_POS, pos);
                    fragment.setArguments(bundle);
                    ((MainActivity) context).addFragment(fragment, GalleryFragment.TAG, true, false);
                }
                else{
                    bundle.putParcelableArrayList(YoutubePlayActivity.KEY_VIDEOS, mEventDetail.getGallery().getVideoList());
                    bundle.putInt(YoutubePlayActivity.KEY_SELECTED_VIDEO, pos);

                    Intent iYoutubeActivity = new Intent(getActivity(), YoutubePlayActivity.class);

                    iYoutubeActivity.putExtra(YoutubePlayActivity.KEY_BUNDLE, bundle);

                    startActivity(iYoutubeActivity);

//                    startActivity(new Intent(getActivity(),YoutubePlayActivity.class));
                }
            }
        });
        binding.rvGrid.setLayoutManager(new GridLayoutManager(context, 2));
        binding.rvGrid.setAdapter(gridAdapter);
    }
}
