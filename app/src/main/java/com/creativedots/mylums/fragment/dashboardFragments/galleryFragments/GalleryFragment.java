package com.creativedots.mylums.fragment.dashboardFragments.galleryFragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.model.eventDetail.GalleryProduct;
import com.creativedots.mylums.utils.TouchImageView;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

public class GalleryFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = GalleryFragment.class.getSimpleName();
    public static final String SELECTED_IMAGE_POS = "pos";

    //    private PlayerView playerView;
//    private SimpleExoPlayer player;
    private ViewPager mPager;
    private AVLoadingIndicatorView aviLoading;
    private ArrayList<GalleryProduct> mGalleryProducts = new ArrayList<>();
    private CustomPagerAdapter mCustomPagerAdapter;
    private int SELECTED_IMAGE_POSITION = 0;
    long playbackPosition;
    int currentWindow;
    boolean playWhenReady;

    private void getInfo(View view) {

        aviLoading = view.findViewById(R.id.aviLoading);
        mPager = view.findViewById(R.id.pager);
    }

    private void setInfo() {

        if (getArguments() != null) {
            if (getArguments().containsKey("images")) {
                mGalleryProducts = getArguments().getParcelableArrayList("images");
                SELECTED_IMAGE_POSITION = getArguments().getInt(SELECTED_IMAGE_POS);
            }
        }

        mCustomPagerAdapter = new CustomPagerAdapter(getActivity());
        mPager.setAdapter(mCustomPagerAdapter);
        mPager.setCurrentItem(SELECTED_IMAGE_POSITION);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        getInfo(view);
        setInfo();
        return view;
    }

    @Override
    public void onClick(View view) {

    }

    public class CustomPagerAdapter extends PagerAdapter {

        private Context mContext;
        private LayoutInflater mLayoutInflater;


        public CustomPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            //return mResources.length;
            return mGalleryProducts.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

            if (mGalleryProducts.get(position).getImagePath() != null && !mGalleryProducts.get(position).getImagePath().isEmpty()) {
                TouchImageView imageView = itemView.findViewById(R.id.imageView);
                Glide.with(context).load(mGalleryProducts.get(position).getImagePath())
                        .apply(new RequestOptions().fitCenter().placeholder(android.R.color.transparent).error(android.R.color.transparent))
                        .into(imageView);
                imageView.setVisibility(View.VISIBLE);
                container.addView(itemView);
            } else {

            }

            return itemView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((LinearLayout) object);
        }

    }


}
