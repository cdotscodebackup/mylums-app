package com.creativedots.mylums.fragment.dashboardFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.fragment.base.BaseFragment;

public class ComposeMessageFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = ComposeMessageFragment.class.getSimpleName();

    private ImageView ivBack;
    private EditText etQuery, etMessage;
    private RelativeLayout rlSend;

    private void getInfo(View view){
        ivBack = view.findViewById(R.id.ivBack);

        etQuery = view.findViewById(R.id.etQuery);
        etMessage = view.findViewById(R.id.etMessage);

        rlSend = view.findViewById(R.id.rlSend);
    }

    private void setInfo(){
        ivBack.setOnClickListener(this);
        rlSend.setOnClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_message_fragment, container,false);
        getInfo(view);
        setInfo();
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivBack:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.rlSend:
                Toast.makeText(context, "Send", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
