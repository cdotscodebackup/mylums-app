package com.creativedots.mylums.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.creativedots.mylums.R;
import com.creativedots.mylums.fragment.base.BaseFragment;

public class DashboardFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = DashboardFragment.class.getSimpleName();

    private CardView cvEvents, cvDirectory, cvProfile,
            cvDiscounts, cvInvites, cvMessages;
    private ImageView ivEvents, ivDirectory, ivProfile,
            ivDiscounts, ivInvites, ivMessages;
    private TextView tvEvents, tvDirectory, tvProfile,
            tvDiscounts, tvInvites, tvMessages;

    private void getInfo(View view){

        cvEvents = view.findViewById(R.id.cvEvents);
        cvDirectory = view.findViewById(R.id.cvDirectory);
        cvProfile = view.findViewById(R.id.cvProfile);
        cvDiscounts = view.findViewById(R.id.cvDiscounts);
        cvInvites = view.findViewById(R.id.cvInvites);
        cvMessages = view.findViewById(R.id.cvMessages);

        ivEvents = view.findViewById(R.id.ivEvents);
        ivDirectory = view.findViewById(R.id.ivDirectory);
        ivProfile = view.findViewById(R.id.ivProfile);
        ivDiscounts = view.findViewById(R.id.ivDiscounts);
        ivInvites = view.findViewById(R.id.ivInvites);
        ivMessages = view.findViewById(R.id.ivMessages);

        tvEvents = view.findViewById(R.id.tvEvents);
        tvDirectory = view.findViewById(R.id.tvDirectory);
        tvProfile = view.findViewById(R.id.tvProfile);
        tvDiscounts = view.findViewById(R.id.tvDiscounts);
        tvInvites = view.findViewById(R.id.tvInvites);
        tvMessages = view.findViewById(R.id.tvMessages);
    }

    private void setInfo(){

        /**
         * Setting click listeners for buttons (CardViews)
         */
     //   initClickListeners();

        cvEvents.setOnClickListener(this);
        cvDirectory.setOnClickListener(this);
        cvProfile.setOnClickListener(this);
        cvDiscounts.setOnClickListener(this);
        cvInvites.setOnClickListener(this);
        cvMessages.setOnClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        getInfo(view);
        setInfo();
        return view;
    }

    @Override
    public void onClick(View view) {
        onItemClickedListener.onItemClicked(view);
    }

    private void handleClickEvents(CardView cvBackground) {
        onItemClickedListener.onItemClicked(cvBackground);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initClickListeners() {
        cvEvents.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent e) {
                handleTouchEvents(e, cvEvents, ivEvents, tvEvents);
                return true;
            }
        });
        cvDirectory.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent e) {
                handleTouchEvents(e, cvDirectory, ivDirectory, tvDirectory);
                return true;
            }
        });
        cvProfile.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent e) {
                handleTouchEvents(e, cvProfile, ivProfile, tvProfile);
                return true;
            }
        });
        cvDiscounts.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent e) {
                handleTouchEvents(e, cvDiscounts, ivDiscounts, tvDiscounts);
                return true;
            }
        });
        cvInvites.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent e) {
                handleTouchEvents(e, cvInvites, ivInvites, tvInvites);
                return true;
            }
        });
        cvMessages.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent e) {
                handleTouchEvents(e, cvMessages, ivMessages, tvMessages);
                return true;
            }
        });
    }

    private void handleTouchEvents(MotionEvent e, CardView cvBackground, ImageView ivIcon, TextView tvTitle) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                pressStartTime = System.currentTimeMillis();
                pressedX = e.getX();
                pressedY = e.getY();

                setIconSelected(cvBackground, ivIcon, tvTitle, true);
                break;
            }
            case MotionEvent.ACTION_MOVE:
                setIconSelected(cvBackground, ivIcon, tvTitle, false);
                break;
            case MotionEvent.ACTION_UP: {
                long pressDuration = System.currentTimeMillis() - pressStartTime;
                if (pressDuration < MAX_CLICK_DURATION && distance(pressedX, pressedY, e.getX(), e.getY()) < MAX_CLICK_DISTANCE) {
                    setIconSelected(cvBackground, ivIcon, tvTitle, false);

                    /**
                     * Handling Click events here...
                     */
                    handleClickEvents(cvBackground);
                }
            }
        }
    }

    private void setIconSelected(CardView cvBackground, ImageView ivIcon, TextView tvTitle, Boolean isSelected) {

        if(isSelected){
            cvBackground.setCardBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
            ivIcon.setColorFilter(getResources().getColor(android.R.color.white));
            tvTitle.setTextColor(getResources().getColor(android.R.color.white));
        }else {
            cvBackground.setCardBackgroundColor(getResources().getColor(R.color.colorDashboardButtons));
            ivIcon.setColorFilter(getResources().getColor(android.R.color.black));
            tvTitle.setTextColor(getResources().getColor(android.R.color.black));
        }
    }

    /**
     * Max allowed duration for a "click", in milliseconds.
     */
    private static final int MAX_CLICK_DURATION = 1000;

    /**
     * Max allowed distance to move during a "click", in DP.
     */
    private static final int MAX_CLICK_DISTANCE = 15;

    private long pressStartTime;
    private float pressedX;
    private float pressedY;

    private float distance(float x1, float y1, float x2, float y2) {
        float dx = x1 - x2;
        float dy = y1 - y2;
        float distanceInPx = (float) Math.sqrt(dx * dx + dy * dy);
        return pxToDp(distanceInPx);
    }

    private float pxToDp(float px) {
        return px / getResources().getDisplayMetrics().density;
    }

    private OnItemClickedListener onItemClickedListener;

    public void setOnItemClickedListener(OnItemClickedListener onItemClickedListener) {
        this.onItemClickedListener = onItemClickedListener;
    }

    public interface OnItemClickedListener{
        void onItemClicked(View view);
    }
}
