package com.creativedots.mylums.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.activity.login.HelpActivity;
import com.creativedots.mylums.adapter.helpMenu.HelpMenuAdapter;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.model.retroFit.helpMenu.HelpMenuResponse;
import com.creativedots.mylums.model.retroFit.helpMenu.MenuItem;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class HelpAndSupportFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = HelpAndSupportFragment.class.getSimpleName();

    private HelpMenuAdapter helpMenuAdapter;
    private RecyclerView rvHelpMenu;

    private TextView txtContent;
    private AVLoadingIndicatorView aviLoading;
    private ImageView ivBack;
    private RelativeLayout rlContactSupport;
    private NestedScrollView nsvHelp;

    private void getInfo(View view){
        ivBack = view.findViewById(R.id.ivBack);
        rlContactSupport = view.findViewById(R.id.rlContactSupport);

        rvHelpMenu = view.findViewById(R.id.rvHelpMenu);

        aviLoading = view.findViewById(R.id.aviLoading);

        txtContent = view.findViewById(R.id.txtContent);

        nsvHelp = view.findViewById(R.id.nsvHelp);
    }

    private void setInfo(){
        ivBack.setOnClickListener(this);
        rlContactSupport.setOnClickListener(this);

        /**
         * Getting data from Database
         */
        MainActivity activity = (MainActivity)getActivity();
        if(activity != null){
            activity.changeBottomTabs(false);
        }
        makeHelpMenuRequest();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_help_and_support, container, false);

        getInfo(view);
        setInfo();

        return view;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_help_and_support);

        //getInfo();
        //setInfo();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivBack:
                getFragmentManager().popBackStack();
                break;
            case R.id.rlContactSupport:
                startActivity(new Intent(getActivity(), HelpActivity.class));
                break;
        }
    }

    private void makeHelpMenuRequest(){

        /**
         * Setting Adapter to display Menu options
         */
       // initHelpMenuAdapter(SessionManager.from(getActivity()).getHelpMenu());

        Call<HelpMenuResponse> call = apiService.helpMenu();

        /**
         * Showing loading animation
         */
        aviLoading.smoothToShow();

        call.enqueue(new Callback<HelpMenuResponse>() {
            @Override
            public void onResponse(@NonNull Call<HelpMenuResponse> call, @NonNull retrofit2.Response<HelpMenuResponse> response) {

                if(response.isSuccessful()){

                    HelpMenuResponse helpMenuResponse = response.body();

                    txtContent.setText(helpMenuResponse.getContent().getContent());
                    /**
                     * Setting Adapter to display Menu options
                     */
                    initHelpMenuAdapter(helpMenuResponse.getData());

                    /**
                     * Hiding loading animation
                     */
                    aviLoading.hide();
                }else{

                    /**
                     * Hiding loading animation
                     */
                    aviLoading.hide();

                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<HelpMenuResponse> call, @NonNull Throwable t) {
                /**
                 * Hiding loading animation
                 */
                aviLoading.hide();
                t.printStackTrace();

                if(t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });
    }

    private void initHelpMenuAdapter(ArrayList<MenuItem> menuItems) {
        if(menuItems == null)
            menuItems = new ArrayList<>();

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        helpMenuAdapter = new HelpMenuAdapter(getActivity(), menuItems,HelpAndSupportFragment.this);
//        helpMenuAdapter.setOnItemClickedListener(new HelpMenuAdapter.OnItemClickedListener() {
//            @Override
//            public void onItemClicked(int pos, final int totalItems) {
//
//                if(pos == totalItems-1){
//                    layoutManager.setReverseLayout(true);
//                }else{
//                    layoutManager.setReverseLayout(false);
//                }
//
//                helpMenuAdapter.notifyDataSetChanged();
//            }
//        });
        rvHelpMenu.setLayoutManager(layoutManager);
        rvHelpMenu.setAdapter(helpMenuAdapter);
        rvHelpMenu.setNestedScrollingEnabled(false);
        helpMenuAdapter.notifyDataSetChanged();
    }

    public void smoothScrool(int pos, int count){
            rvHelpMenu.smoothScrollToPosition(pos);

        /*if(pos == count){
            rvHelpMenu.scrollToPosition(pos);
        }*/
    }

    public HelpMenuAdapter.ViewHolder viewHolderForAdapterPos(int pos){
        return (HelpMenuAdapter.ViewHolder)rvHelpMenu.findViewHolderForAdapterPosition(pos);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e("Destroy","Destroy called");
        MainActivity activity = (MainActivity)getActivity();
        if(activity != null){
            activity.changeBottomTabs(true);
        }
    }
}
