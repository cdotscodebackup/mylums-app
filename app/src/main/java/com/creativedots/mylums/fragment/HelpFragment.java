package com.creativedots.mylums.fragment;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.SplashActivity;
import com.creativedots.mylums.activity.login.LoginActivity;

public class HelpFragment extends Fragment {


    private static String EXTRA_POSITION = "EXTRA_CAT_ID";


    private TextView txtWelcome, txtDesc, txtTitle;
    private int mPosition;
    private ImageView mHelpImage, imgLogo;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPosition = getArguments().getInt(EXTRA_POSITION);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.help_fragment, container, false);

        imgLogo = root.findViewById(R.id.imgLogo);
        txtWelcome = root.findViewById(R.id.txtWelcome);
        txtDesc = root.findViewById(R.id.txtDesc);
        txtTitle = root.findViewById(R.id.txtTitle);

        switch (mPosition) {
            case 0:

                txtWelcome.setVisibility(View.VISIBLE);
                imgLogo.setImageDrawable(getResources().getDrawable(R.drawable.logo_mini));
                txtDesc.setText(getResources().getString(R.string.help_01));
                txtTitle.setVisibility(View.GONE);
                break;
            case 1:
                txtWelcome.setVisibility(View.INVISIBLE);
                imgLogo.setImageDrawable(getResources().getDrawable(R.drawable.help_search));
                txtDesc.setText(getResources().getString(R.string.help_02));
                txtTitle.setVisibility(View.VISIBLE);
                txtTitle.setText(getResources().getText(R.string.help_02_title));
                break;
            case 2:
                txtWelcome.setVisibility(View.INVISIBLE);
                imgLogo.setImageDrawable(getResources().getDrawable(R.drawable.help_event));
                txtDesc.setText(getResources().getString(R.string.help_03));
                txtTitle.setVisibility(View.VISIBLE);
                txtTitle.setText(getResources().getText(R.string.help_03_title));
                break;
            case 3:
                txtWelcome.setVisibility(View.INVISIBLE);
                imgLogo.setImageDrawable(getResources().getDrawable(R.drawable.help_meet));
                txtDesc.setText(getResources().getString(R.string.help_04));
                txtTitle.setVisibility(View.VISIBLE);
                txtTitle.setText(getResources().getText(R.string.help_04_title));
                break;
            case 4:
                txtWelcome.setVisibility(View.INVISIBLE);
                imgLogo.setImageDrawable(getResources().getDrawable(R.drawable.help_search));
                txtDesc.setText(getResources().getString(R.string.help_05));
                txtTitle.setVisibility(View.VISIBLE);
                txtTitle.setText(getResources().getText(R.string.help_05_title));
                break;
            default:
                break;
        }

        return root;
    }


    public static HelpFragment newInstance(int position) {
        HelpFragment f = new HelpFragment();

        Bundle args = new Bundle();
        args.putInt(EXTRA_POSITION, position);

        f.setArguments(args);

        return f;
    }


}
