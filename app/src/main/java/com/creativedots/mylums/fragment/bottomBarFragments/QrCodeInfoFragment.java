package com.creativedots.mylums.fragment.bottomBarFragments;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.model.retroFit.profile.User;

public class QrCodeInfoFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = QrCodeInfoFragment.class.getSimpleName();

    private ImageView ivBack;
    private RelativeLayout rlAddToContacts, rlClose;
    private TextView txtName,txtCompany,txtDesignation,txtWebsite,txtEmail,txtPhone;
    private User mUser ;

    private void getInfo(View view){
        ivBack = view.findViewById(R.id.ivBack);
        txtName = view.findViewById(R.id.txtName);
        txtCompany = view.findViewById(R.id.txtCompany);
        txtDesignation = view.findViewById(R.id.txtDesignation);
        txtWebsite = view.findViewById(R.id.txtWebsite);
        txtEmail = view.findViewById(R.id.txtEmail);
        txtPhone = view.findViewById(R.id.txtPhone);
        rlAddToContacts = view.findViewById(R.id.rlAddToContacts);
        rlClose = view.findViewById(R.id.rlClose);
    }

    private void setInfo(){
        updateValues();
        ivBack.setOnClickListener(this);
        rlAddToContacts.setOnClickListener(this);
        rlClose.setOnClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_qr_code_info, container, false);
       mUser = new User();
        if(getArguments() != null)
            if(getArguments().containsKey("user"))
                mUser = getArguments().getParcelable("user");
        getInfo(view);
        setInfo();

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rlClose:
            case R.id.ivBack:
                getActivity().getSupportFragmentManager().popBackStack();
                break;

            case R.id.rlAddToContacts:
                addContact();
                //Toast.makeText(context, "Add to contacts", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void updateValues(){
        txtName.setText(mUser.getFirstName() + " " + mUser.getLastName());
        txtCompany.setText(mUser.getCurrentCompany());
        txtDesignation.setText(mUser.getCurrentDesignation());
        txtWebsite.setText("");
        txtEmail.setText(mUser.getEmail());
        txtPhone.setText(mUser.getPhone());
    }

    private void addContact() {


        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, mUser.getEmail());
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, mUser.getPhone());
        intent.putExtra(ContactsContract.Intents.Insert.NAME, mUser.getFirstName() + " "
        + mUser.getLastName());
        intent.putExtra(ContactsContract.Intents.Insert.COMPANY, mUser.getCurrentCompany());
        intent.putExtra(ContactsContract.Intents.Insert.JOB_TITLE, mUser.getCurrentDesignation());

        startActivity(intent);

    }
}
