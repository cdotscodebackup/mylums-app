package com.creativedots.mylums.fragment.base;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.network.ApiClient;
import com.creativedots.mylums.network.ApiInterface;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class BaseFragment extends Fragment {

    public static final String TAG = BaseFragment.class.getSimpleName();

    protected Context context;
    private AlertDialog alertDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        /**
         * initializing Retro-Fit.
         */
        initRetroFit();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    protected ApiInterface apiService;

    private void initRetroFit() {
        Log.e("BaseFragment",SessionManager.from(context).getToken());
        apiService = ApiClient.getRetrofitClient(SessionManager.from(context).getToken()).create(ApiInterface.class);
    }

    protected void displayErrorMessage(String errorBody) {
        try {
            JSONObject jsonResponse = new JSONObject(errorBody);
            String message = jsonResponse.getString(Api.KEY_MESSAGE);
            if(jsonResponse.has("logout")){
                if(jsonResponse.getString("logout").equalsIgnoreCase("yes")){
                    SessionManager.from(getActivity()).saveToken("");
                   if(getActivity() != null)
                    forceRunApp(getActivity());
                }
            }
         //   Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getErrorMessage(String errorBody) {
        try {
            JSONObject jsonResponse = new JSONObject(errorBody);
            String message = jsonResponse.getString(Api.KEY_MESSAGE);
            if(jsonResponse.has("logout")){
                if(jsonResponse.getString("logout").equalsIgnoreCase("yes")){
                    SessionManager.from(getActivity()).saveToken("");
                    forceRunApp(getActivity());
                }
            }
            return message;
            //   Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void forceRunApp(Context context) {
        Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        launchIntent.setFlags(
                Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS
                        | Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP
        );
        context.startActivity(launchIntent);
    }

    public void showNoNetworkDialog(final Context context){
        alertDialog = new AlertDialog.Builder(context)
                .setTitle("No Internet")
                .setMessage("Please check you network!")
                .setPositiveButton("OK", null)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
//                        if(!AppSettings.isNetworkConnected(context)){
//                            alertDialog.show();
//
//                        }
                    }
                })
                .show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(android.R.color.black));
    }
}
