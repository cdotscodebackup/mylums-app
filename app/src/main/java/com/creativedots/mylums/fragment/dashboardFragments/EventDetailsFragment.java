package com.creativedots.mylums.fragment.dashboardFragments;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.adapter.AttendeesAdapter;
import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.fragment.dashboardFragments.galleryFragments.GalleryFragment;
import com.creativedots.mylums.fragment.dashboardFragments.galleryFragments.GridGalleryFragment;
import com.creativedots.mylums.model.eventDetail.EventAttendResponse;
import com.creativedots.mylums.model.eventDetail.EventDetail;
import com.creativedots.mylums.model.eventDetail.EventDetailResponse;
import com.creativedots.mylums.model.eventDetail.InterestType;
import com.creativedots.mylums.model.retroFit.profile.User;
import com.creativedots.mylums.utils.AppSettings;
import com.creativedots.mylums.utils.DateConversion;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class EventDetailsFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = EventDetailsFragment.class.getSimpleName();

    private String selectedTab = "";
    private String eventID;
    private EventDetail mEventDetail = new EventDetail();
    private AVLoadingIndicatorView aviLoading;

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }

    private LinearLayout checkedInSelected, goingSelected, interestedSelected;
    private LinearLayout lnGallery;

    private final String GOING = "going";
    private final String INTERESTED = "interested";
    private final String NOT_INTERESTED = "not-interested";

    ArrayList<InterestType> interestTypes;
    private ImageView ivBack, ivEventThumbnail, imgGoing, imgInterested, imgNotInterested, selectedInterest;

    private TextView tvDate, tvMonth, tvYear, tvEventTitle, tvNotInterested, tvTime, tvVenue,
            tvInterested, tvTotalInterested, tvGoing, tvTotalGoing, tvTotalCheckedIn,
            tvCheckedInTab, tvGoingTab, tvInterestedTab, tvDay;

    private LinearLayout lnCheckedInTab, lnGoingTab, lnInterestedTab;

    private ImageView ivImages, ivVideos;
    private RelativeLayout rlPhotos, rlVideos;
    private TextView tvTotalPhotos, tvTotalVideos;

    private LinearLayout lnContentContainer;
    private View llCheckIn, layout_checkin;
    private TextView tvCheckIn;
    private ExpandableTextView tvDescription;
    private RecyclerView rvCheckedIn, rvGoing, rvInterested;
    private View layoutCheckinMain;

    int selectedIcon;
    private LinearLayout lnInterested, lnGoing, lnNotInterested;

    private void getInfo(View view) {

        aviLoading = view.findViewById(R.id.aviLoading);
        ivBack = view.findViewById(R.id.ivBack);
        ivEventThumbnail = view.findViewById(R.id.ivEventThumbnail);

        tvDate = view.findViewById(R.id.tvDate);
        tvMonth = view.findViewById(R.id.tvMonth);
        tvYear = view.findViewById(R.id.tvYear);
        tvEventTitle = view.findViewById(R.id.tvEventTitle);
        tvNotInterested = view.findViewById(R.id.tvNotInterested);
        tvTime = view.findViewById(R.id.tvTime);
        tvVenue = view.findViewById(R.id.tvVenue);

        tvInterested = view.findViewById(R.id.tvInterested);
        tvTotalInterested = view.findViewById(R.id.tvTotalInterested);
        tvGoing = view.findViewById(R.id.tvGoing);
        tvTotalGoing = view.findViewById(R.id.tvTotalGoing);
        tvTotalCheckedIn = view.findViewById(R.id.tvTotalCheckedIn);
        llCheckIn = view.findViewById(R.id.llCheckIn);
        tvCheckIn = view.findViewById(R.id.tvCheckIn);
        layoutCheckinMain = view.findViewById(R.id.layout_checkin_main);
        tvCheckedInTab = view.findViewById(R.id.tvCheckedInTab);
        tvGoingTab = view.findViewById(R.id.tvGoingTab);
        tvInterestedTab = view.findViewById(R.id.tvInterestedTab);

        lnCheckedInTab = view.findViewById(R.id.lnCheckedInTab);
        lnGoingTab = view.findViewById(R.id.lnGoingTab);
        lnInterestedTab = view.findViewById(R.id.lnInterestedTab);

        tvDescription = view.findViewById(R.id.tvAbout);
        rvCheckedIn = view.findViewById(R.id.rvCheckedIn);
        rvGoing = view.findViewById(R.id.rvGoing);
        rvInterested = view.findViewById(R.id.rvInterested);
        lnInterested = view.findViewById(R.id.lnInterested);
        lnNotInterested = view.findViewById(R.id.lnNotInterested);
        lnGoing = view.findViewById(R.id.lnGoing);

        imgGoing = view.findViewById(R.id.imgGoing);
        imgInterested = view.findViewById(R.id.imgStar);
        imgNotInterested = view.findViewById(R.id.imgNotInterested);

        checkedInSelected = view.findViewById(R.id.checkedInSelected);
        goingSelected = view.findViewById(R.id.goingSelected);
        interestedSelected = view.findViewById(R.id.interestedSelected);

        lnContentContainer = view.findViewById(R.id.lnContentContainer);

        lnGallery = view.findViewById(R.id.lnGallery);

        ivImages = view.findViewById(R.id.ivImages);
        ivVideos = view.findViewById(R.id.ivVideos);

        rlPhotos = view.findViewById(R.id.rlPhotos);
        rlVideos = view.findViewById(R.id.rlVideos);

        tvTotalPhotos = view.findViewById(R.id.tvTotalPhotos);
        tvTotalVideos = view.findViewById(R.id.tvTotalVideos);
        tvDay = view.findViewById(R.id.tvDay);


    }

    private void setInfo() {

        /**
         * Setting Click Listeners
         */
        initClickListeners();

        /**
         * Getting EventDetail from database
         */

        if (mEventDetail == null || mEventDetail.getName() == null)
            makeEventDetailsRequest();
        else
            updateEventData(mEventDetail);


        /**
         * Initialiaing AttendeesAdapter
         */
        initializeAttendeesAdapter();
    }

    private void makeEventDetailsRequest() {

        Log.e(TAG, "onResponse: API" + Api.EventDetails.GET_URL + "/" + eventID);

        Call<EventDetailResponse> call = apiService.eventDetails(Api.EventDetails.GET_URL + "/" + eventID);

        aviLoading.smoothToShow();
        call.enqueue(new Callback<EventDetailResponse>() {
            @Override
            public void onResponse(@NonNull Call<EventDetailResponse> call, @NonNull retrofit2.Response<EventDetailResponse> response) {
                Log.e(TAG, "onResponse: " + response.toString());
                aviLoading.hide();
                if (response.isSuccessful()) {

                    EventDetailResponse eventDetailsResponseResponse = response.body();
                    mEventDetail = eventDetailsResponseResponse.getData();

                    if (mEventDetail != null) {

                        updateEventData(mEventDetail);

                    }
                } else {

                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<EventDetailResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Log.e(TAG, "onFailure: " + t.getMessage());
                aviLoading.hide();

                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_event_details, container, false);
        getInfo(view);
        setInfo();
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                AppSettings.hideKeyboard(context, view);
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.tvInterested:
                Toast.makeText(context, "InterestType", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tvGoing:
                Toast.makeText(context, "Going", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tvNotInterested:
                Toast.makeText(context, "Not InterestType", Toast.LENGTH_SHORT).show();
                break;
            case R.id.llCheckIn:
                //Toast.makeText(context, "Check In", Toast.LENGTH_SHORT).show();
                if (mEventDetail.isActive() && mEventDetail.isAlreadyCheckIn() && DateConversion.isValidForCheckIn(mEventDetail.getDate_time())) {
                    checkin(false);
                } else if (mEventDetail.isActive() && !mEventDetail.isAlreadyCheckIn() && DateConversion.isValidForCheckIn(mEventDetail.getDate_time())) {
                    checkin(true);
                }
                break;
            case R.id.lnCheckedInTab:
            case R.id.lnGoingTab:
            case R.id.lnInterestedTab:
                handleTabClicks(view);
                break;
            case R.id.lnInterested:
                if (selectedInterest != imgInterested)
                    changeEventInterest(imgInterested, R.drawable.ic_star_blue, R.drawable.ic_star_black);
                break;
            case R.id.lnGoing:
                if (selectedInterest != imgGoing)
                    changeEventInterest(imgGoing, R.drawable.ic_going_blue, R.drawable.ic_going_black);
                break;
            case R.id.lnNotInterested:
                if (selectedInterest != imgNotInterested)
                    changeEventInterest(imgNotInterested, R.drawable.ic_not_interested_blue, R.drawable.ic_not_interested_black);
                break;
            case R.id.rlPhotos:
                if (Integer.parseInt(mEventDetail.getGallery().getPhotoTotal()) > 0)
                    openPhotoList("photos");
                break;
            case R.id.rlVideos:
                if (Integer.parseInt(mEventDetail.getGallery().getVideoTotal()) > 0)
                    openPhotoList("videos");
                break;
        }
    }

    private void handleTabClicks(View view) {
        switch (view.getId()) {
            case R.id.lnCheckedInTab:
                if (!DateConversion.isValidForCheckIn(mEventDetail.getDate_time())) {
                    return;
                }
                setTabSelected(lnCheckedInTab, true);
                selectedTab = getResources().getString(R.string.checked_in);
                initializeAttendeesAdapter();
                break;
            case R.id.lnGoingTab:
                if (mEventDetail.isActive()) {
                    setTabSelected(lnGoingTab, true);
                    selectedTab = getResources().getString(R.string._going);
                    initializeAttendeesAdapter();
                }
                break;
            case R.id.lnInterestedTab:
                if (mEventDetail.isActive()) {

                    setTabSelected(lnInterestedTab, true);
                    selectedTab = getResources().getString(R.string.interested);
                    initializeAttendeesAdapter();
                }
                break;
        }
    }

    private void setTabSelected(LinearLayout tab, Boolean selected) {

        int color = R.color.colorBlack;
        if (!mEventDetail.isActive()) {
            color = R.color.colorGrayText;
            layoutCheckinMain.setVisibility(View.GONE);
        }
        tvGoingTab.setTextColor(context.getResources().getColor(color));
        tvInterestedTab.setTextColor(context.getResources().getColor(color));
        tvCheckedInTab.setTextColor(context.getResources().getColor(color));

        goingSelected.setVisibility(View.GONE);
        interestedSelected.setVisibility(View.GONE);
        checkedInSelected.setVisibility(View.GONE);

        if (tab.getId() == lnGoingTab.getId()) {
            tvGoingTab.setTextColor(context.getResources().getColor(R.color.colorPrimaryLight));
            tvTotalGoing.setTextColor(context.getResources().getColor(R.color.colorPrimaryLight));
            goingSelected.setVisibility(View.VISIBLE);
        } else if (tab.getId() == lnInterestedTab.getId()) {
            tvInterestedTab.setTextColor(context.getResources().getColor(R.color.colorPrimaryLight));
            tvTotalInterested.setTextColor(context.getResources().getColor(R.color.colorPrimaryLight));
            interestedSelected.setVisibility(View.VISIBLE);
        } else if (tab.getId() == lnCheckedInTab.getId()) {
            tvCheckedInTab.setTextColor(context.getResources().getColor(R.color.colorPrimaryLight));
            tvTotalCheckedIn.setTextColor(context.getResources().getColor(R.color.colorPrimaryLight));
            checkedInSelected.setVisibility(View.VISIBLE);
        }

    }

    private void updateEventData(EventDetail eventDetail) {
        try {


            Glide.with(context).load(mEventDetail.getImagepath())
                    .apply(new RequestOptions().centerCrop()
                            .placeholder(R.drawable.bg_dashboard).error(R.drawable.bg_dashboard))
                    .into(ivEventThumbnail);
            //eventDetail.setStatus("not_active");
            tvEventTitle.setText(eventDetail.getName());
            tvTime.setText(DateConversion.convertTimeToLocal(eventDetail.getStartTime()) + "-" + DateConversion.convertTimeToLocal(eventDetail.getEndTime()));
            tvVenue.setText(eventDetail.getVenue());
            tvTotalInterested.setText("" + eventDetail.getUsers().getInterestedTotal());
            tvTotalGoing.setText("" + eventDetail.getUsers().getGoingTotal());

            if (eventDetail.isActive() && !DateConversion.isValidForCheckIn(mEventDetail.getDate_time())) {
                tvTotalCheckedIn.setVisibility(View.INVISIBLE);
                llCheckIn.setVisibility(View.VISIBLE);
            } else {
                llCheckIn.setVisibility(View.GONE);
                tvTotalCheckedIn.setText("" + eventDetail.getUsers().getCheckedinTotal());
            }

            tvTotalInterested.setText("" + eventDetail.getUsers().getInterestedTotal());
            String split[] = DateConversion.getYYYYMMDDTODDMMMYY(eventDetail.getDates()).split(" ");
            if (split.length > 0) {
                tvDate.setText("" + split[0]);
            }
            tvMonth.setText((split.length > 1 ? split[1] : ""));
            tvYear.setText((split.length > 2 ? split[2] : ""));
            tvDay.setText(DateConversion.getDayOfWeek(eventDetail.getDates()));
            tvTotalInterested.setText("" + eventDetail.getUsers().getInterestedTotal());

            tvDescription.setText(eventDetail.getDescription());
            initializeAttendeesAdapter();

            if (mEventDetail.getMystatus().equalsIgnoreCase(INTERESTED)) {
                selectedInterest = imgInterested;
                selectedIcon = R.drawable.ic_star_black;
                imgInterested.setImageResource(R.drawable.ic_star_blue);
                imgGoing.setImageResource(R.drawable.ic_not_interested_black);
                imgNotInterested.setImageResource(R.drawable.ic_not_interested_black);
                tvInterested.setTextColor(context.getResources().getColor(R.color.colorPrimaryLight));
                tvNotInterested.setTextColor(context.getResources().getColor(R.color.colorBlack));
                tvGoing.setTextColor(context.getResources().getColor(R.color.colorBlack));

            } else if (mEventDetail.getMystatus().equalsIgnoreCase(GOING)) {
                selectedInterest = imgGoing;
                selectedIcon = R.drawable.ic_going_black;
                imgInterested.setImageResource(R.drawable.ic_star_black);
                imgGoing.setImageResource(R.drawable.ic_going_blue);
                imgNotInterested.setImageResource(R.drawable.ic_not_interested_black);
                tvGoing.setTextColor(context.getResources().getColor(R.color.colorPrimaryLight));
                tvNotInterested.setTextColor(context.getResources().getColor(R.color.colorBlack));
                tvInterested.setTextColor(context.getResources().getColor(R.color.colorBlack));

            } else if (mEventDetail.getMystatus().equalsIgnoreCase(NOT_INTERESTED)) {
                selectedIcon = R.drawable.ic_not_interested_black;
                selectedInterest = imgNotInterested;
                imgInterested.setImageResource(R.drawable.ic_star_black);
                imgGoing.setImageResource(R.drawable.ic_not_interested_black);
                imgNotInterested.setImageResource(R.drawable.ic_going_blue);
                tvNotInterested.setTextColor(context.getResources().getColor(R.color.colorPrimaryLight));
                tvInterested.setTextColor(context.getResources().getColor(R.color.colorBlack));
                tvGoing.setTextColor(context.getResources().getColor(R.color.colorBlack));

            } else {
                selectedInterest = null;
                imgInterested.setImageResource(R.drawable.ic_star_black);
                imgGoing.setImageResource(R.drawable.ic_not_interested_black);
                imgNotInterested.setImageResource(R.drawable.ic_not_interested_black);
                tvNotInterested.setTextColor(context.getResources().getColor(R.color.colorBlack));
                tvInterested.setTextColor(context.getResources().getColor(R.color.colorBlack));
                tvGoing.setTextColor(context.getResources().getColor(R.color.colorBlack));

            }

            if (!mEventDetail.isActive()) {
                setSetupCheckin(R.color.colorGrayText, R.drawable.gray_rect_hollow_corner_round, getString(R.string.check_in));
                layoutCheckinMain.setVisibility(View.GONE);
                llCheckIn.setVisibility(View.GONE);
            } else if (mEventDetail.isActive() && !DateConversion.isValidForCheckIn(mEventDetail.getDate_time())) {
                setSetupCheckin(R.color.colorGrayText, R.drawable.gray_rect_hollow_corner_round, getString(R.string.check_in));
                layoutCheckinMain.setVisibility(View.VISIBLE);
                llCheckIn.setVisibility(View.GONE);
            } else if (mEventDetail.isActive() && DateConversion.isValidForCheckIn(mEventDetail.getDate_time())) {
                llCheckIn.setVisibility(View.VISIBLE);
                if (mEventDetail.isAlreadyCheckIn()) {
                    setTabSelected(lnCheckedInTab, true);
                    selectedTab = getResources().getString(R.string.checked_in);
                    initializeAttendeesAdapter();
                    setSetupCheckin(R.color.colorPrimaryLight, R.drawable.blue_rect_hollow_corner_round, getString(R.string.checked_in));
                } else {
                    setSetupCheckin(android.R.color.black, R.drawable.black_rect_hollow_corner_round, getString(R.string.check_in));
                }
                layoutCheckinMain.setVisibility(View.VISIBLE);
            }

            lnContentContainer.setVisibility(View.VISIBLE);
            if (!mEventDetail.isActive()) {
                tvTotalGoing.setVisibility(View.INVISIBLE);
                tvTotalInterested.setVisibility(View.INVISIBLE);
                setTabSelected(lnCheckedInTab, true);
                selectedTab = getResources().getString(R.string.checked_in);
                initializeAttendeesAdapter();
                lnGallery.setVisibility(View.VISIBLE);
            } else {
                lnGallery.setVisibility(View.GONE);
            }
            if (Integer.parseInt(mEventDetail.getGallery().getPhotoTotal()) > 0) {
                tvTotalPhotos.setText(String.valueOf(mEventDetail.getGallery().getPhotosList().size()));
                Glide.with(context)
                        .load(mEventDetail.getGallery().getPhotosList().get(0).getImagePath())
                        .apply(new RequestOptions().centerCrop()
                                .error(R.drawable.bg_dashboard).placeholder(R.drawable.bg_dashboard))
                        .into(ivImages);
            } else {
                rlPhotos.setVisibility(View.GONE);
            }


            if (Integer.parseInt(mEventDetail.getGallery().getVideoTotal()) > 0) {


                tvTotalVideos.setText(String.valueOf(mEventDetail.getGallery().getVideoList().size()));
                String videoPath = mEventDetail.getGallery().getVideoList().get(0).getVideoPath();
                String videoId = videoPath.split("v=")[1];
                String thumbPath = "http://img.youtube.com/vi/" + videoId + "/0.jpg";

                Glide.with(context)
                        .load(thumbPath)
                        .apply(new RequestOptions().centerCrop()
                                .error(R.drawable.bg_dashboard).placeholder(R.drawable.bg_dashboard))
                        .into(ivVideos);
            } else {
                rlVideos.setVisibility(View.GONE);
            }

            if (rlPhotos.getVisibility() == View.GONE &&
                    rlVideos.getVisibility() == View.GONE) {
                lnGallery.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            Log.e("Exception", ex + "");
        }
    }


    public void setSetupCheckin(int colorRes, int backgroundRes, String text) {
        tvCheckIn.setTextColor(ContextCompat.getColor(context, colorRes));
        tvCheckIn.setText(text);
        if (text.equalsIgnoreCase(getString(R.string.checked_in)))
            llCheckIn.setOnClickListener(null);
        for (Drawable drawable : tvCheckIn.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(context, colorRes), PorterDuff.Mode.SRC_IN));
            }
        }
        llCheckIn.setBackgroundResource(backgroundRes);
    }

    private void initClickListeners() {

        ivBack.setOnClickListener(this);

        /*tvInterested.setOnClickListener(this);
        tvGoing.setOnClickListener(this);
        tvNotInterested.setOnClickListener(this);*/

        llCheckIn.setOnClickListener(this);

        lnCheckedInTab.setOnClickListener(this);
        lnGoingTab.setOnClickListener(this);
        lnInterestedTab.setOnClickListener(this);

        lnInterested.setOnClickListener(this);
        lnNotInterested.setOnClickListener(this);
        lnGoing.setOnClickListener(this);

        rlVideos.setOnClickListener(this);
        rlPhotos.setOnClickListener(this);


    }

    private void initializeAttendeesAdapter() {

        if (selectedTab.isEmpty() && DateConversion.isValidForCheckIn(mEventDetail.getDate_time())) {
            selectedTab = getResources().getString(R.string.checked_in);
        } else if (selectedTab.isEmpty()) {
            selectedTab = getResources().getString(R.string._going);
        }
        interestTypes = new ArrayList<>();
        if (mEventDetail.getUsers() != null) {
            if (tvCheckedInTab.getText().toString().equalsIgnoreCase(selectedTab) && mEventDetail.getUsers().getCheckedin() != null)
                interestTypes = mEventDetail.getUsers().getCheckedin();
            if (tvInterestedTab.getText().toString().equalsIgnoreCase(selectedTab) && mEventDetail.getUsers().getInterested() != null)
                interestTypes = mEventDetail.getUsers().getInterested();
            if (tvGoingTab.getText().toString().equalsIgnoreCase(selectedTab) && mEventDetail.getUsers().getGoing() != null)
                interestTypes = mEventDetail.getUsers().getGoing();
        }

        AttendeesAdapter checkedInAdapter = new AttendeesAdapter(context, interestTypes, mEventDetail.getStatus());
        checkedInAdapter.setOnItemClickedListener(onItemClickedListener);

        rvCheckedIn.setLayoutManager(new LinearLayoutManager(context));
        rvCheckedIn.setAdapter(checkedInAdapter);

        rvCheckedIn.setNestedScrollingEnabled(false);
    }

    private AttendeesAdapter.OnItemClickedListener onItemClickedListener = new AttendeesAdapter.OnItemClickedListener() {
        @Override
        public void onSayHiClicked(int pos) {

            User user = new User();
            if (interestTypes != null) {
                user = interestTypes.get(pos).getUser();
            }
            Fragment fragment = new SayHiFragment();
            Bundle bundle = new Bundle();
            bundle.putString("view", "tinder");
            bundle.putParcelable("user", user);
            fragment.setArguments(bundle);
            ((MainActivity) context).addFragment(fragment, SayHiFragment.TAG, true, false);


        }

        @Override
        public void viewProfileClicked(int pos) {
            ViewAlumniProfileFragment viewAlumniProfileFragment = new ViewAlumniProfileFragment();
            viewAlumniProfileFragment.setAlmuniUserID(interestTypes.get(pos).getUser().getUserId(), interestTypes.get(pos).getUser().getUsertype());

//            ((MainActivity) getActivity()).replaceFragment(viewAlumniProfileFragment, ViewAlumniProfileFragment.TAG, true, false);
            ((MainActivity) context).addFragment(viewAlumniProfileFragment, ViewAlumniProfileFragment.TAG, true, false);
        }
    };

    private void changeEventInterest(View now, int iconNow, int iconBlack) {


        if (selectedInterest != null)
            selectedInterest.setImageDrawable(getResources().getDrawable(selectedIcon));
        ((ImageView) now).setImageDrawable(getResources().getDrawable(iconNow));
        //imgG.setImageDrawable(getResources().getDrawable(R.drawable.ic_not_interested_black));

        selectedInterest = (ImageView) now;
        selectedIcon = iconBlack;

        if (selectedInterest == imgGoing) {
            updateAttendingStatus(GOING);
        } else if (selectedInterest == imgInterested) {
            updateAttendingStatus(INTERESTED);
        } else if (selectedInterest == imgNotInterested) {
            updateAttendingStatus(NOT_INTERESTED);
        }
    }

    private void updateAttendingStatus(String status) {
        Call<EventAttendResponse> call = apiService.attendEvent(eventID, status);
        aviLoading.smoothToShow();
        call.enqueue(new Callback<EventAttendResponse>() {
            @Override
            public void onResponse(@NonNull Call<EventAttendResponse> call, @NonNull retrofit2.Response<EventAttendResponse> response) {
                Log.e(TAG, "onResponse: " + response.toString());
                if (response.isSuccessful()) {
                    makeEventDetailsRequest();
                } else {
                    // makeEventDetailsRequest();
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                aviLoading.hide();
            }

            @Override
            public void onFailure(@NonNull Call<EventAttendResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                //  makeEventDetailsRequest();
                Log.e(TAG, "onFailure: " + t.getMessage());
                aviLoading.hide();

                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });

    }

    private void checkin(boolean checkin) {
        Call<EventAttendResponse> call = apiService.checkInEvent(eventID, checkin ? "in" : "out");
        aviLoading.smoothToShow();
        call.enqueue(new Callback<EventAttendResponse>() {
            @Override
            public void onResponse(@NonNull Call<EventAttendResponse> call, @NonNull retrofit2.Response<EventAttendResponse> response) {
                Log.e(TAG, "onResponse: " + response.toString());
                if (response.isSuccessful()) {
                    makeEventDetailsRequest();
                    handleTabClicks(lnCheckedInTab);
                } else {
                    //    makeEventDetailsRequest();
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                aviLoading.hide();
            }

            @Override
            public void onFailure(@NonNull Call<EventAttendResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                //   makeEventDetailsRequest();
                Log.e(TAG, "onFailure: " + t.getMessage());
                aviLoading.hide();

                if (t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });

    }

    private void openPhotoList(String type) {

//        GalleryFragment fragment = new GalleryFragment();
//        Bundle bundle = new Bundle();
//        if (type.equalsIgnoreCase("photos"))
//            bundle.putParcelableArrayList("images", mEventDetail.getGallery().getPhotosList());
//        else
//            bundle.putParcelableArrayList("images", mEventDetail.getGallery().getVideoList());
//
//        fragment.setArguments(bundle);
//        ((MainActivity) getActivity()).addFragment(fragment, GalleryFragment.TAG, true, false);

        GridGalleryFragment fragment = new GridGalleryFragment();
        if (type.equalsIgnoreCase("photos")) {
            fragment.setGalleryProducts(mEventDetail.getGallery().getPhotosList(), mEventDetail, type);
        } else {
            fragment.setGalleryProducts(mEventDetail.getGallery().getVideoList(), mEventDetail, type);
        }

        ((MainActivity) context).addFragment(fragment, GridGalleryFragment.TAG, true, false);
    }
}
