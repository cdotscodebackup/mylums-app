package com.creativedots.mylums.fragment.dashboardFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.model.retroFit.profile.User;
import com.creativedots.mylums.utils.sharedPreferences.Constants;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class SayHiFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = SayHiFragment.class.getSimpleName();

    private ImageView ivBack;
    private CircleImageView civProfile;
    private EditText etMessage;
    private RelativeLayout rlSend;
    private TextView txtName, txtCompany, txtDesignation;
    private String viewType = "";
    private AVLoadingIndicatorView aviLoading;
    private User mUser;

    private void getInfo(View view) {

        aviLoading = view.findViewById(R.id.aviLoading);
        ivBack = view.findViewById(R.id.ivBack);

        civProfile = view.findViewById(R.id.civProfile);
        etMessage = view.findViewById(R.id.etMessage);
        rlSend = view.findViewById(R.id.rlSend);

        txtName = view.findViewById(R.id.txtName);
        txtDesignation = view.findViewById(R.id.txtDesignation);
        txtCompany = view.findViewById(R.id.txtCompany);
    }

    private void setInfo() {
        ivBack.setOnClickListener(this);
        rlSend.setOnClickListener(this);

        if (mUser != null) {
            Glide.with(getContext())
                    .load(mUser.getImagepath())
                    .apply(new RequestOptions().centerCrop().circleCrop()
                            .error(R.drawable.ic_user_tab_primary).placeholder(R.drawable.ic_user_tab_primary))
                    .into(civProfile);

            txtName.setText(mUser.getFirstName() + " " + mUser.getLastName());
            txtCompany.setText(mUser.getCurrentCompany());
            txtDesignation.setText(mUser.getCurrentDesignation());

            if(!viewType.equalsIgnoreCase(Constants.KEY_TINDER_TYPE)){
                etMessage.setText("");
                etMessage.setHint("Type your message here");

            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_say_hi, container, false);

        if (getArguments() != null) {
            if (getArguments().containsKey("user"))
                mUser = getArguments().getParcelable("user");

            if (getArguments().containsKey("view"))
                viewType = getArguments().getString("view");

            // Student student = (Student) data.getParcelable("student");
        }
        getInfo(view);
        setInfo();
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.rlSend:


                sendMessage();
                break;
        }
    }

    private void sendMessage() {


        Call<Response> call;
        if (viewType.equalsIgnoreCase("tinder")) {
            call = apiService.sayHiTinder(("Hi \n" + etMessage.getText().toString()), mUser.getUserId());

        } else {
            call = apiService.sayHiMessage(("Hi \n" + etMessage.getText().toString()), mUser.getUserId());
        }

        Log.e("SERVER URL:",""+call.request().url());

        aviLoading.smoothToShow();
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                Log.e("SERVER Response:",""+response.toString());
                if (response.isSuccessful()) {
                    Response myResponse = response.body();
                    Toast.makeText(getActivity(), myResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    getActivity().onBackPressed();
                } else {
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                aviLoading.hide();
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                aviLoading.hide();

                if(t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });
    }
}
