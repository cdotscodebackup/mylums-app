package com.creativedots.mylums.fragment.dashboardFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.creativedots.mylums.R;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.model.retroFit.settings.Setting;
import com.creativedots.mylums.model.retroFit.settings.SettingsGetResponse;
import com.creativedots.mylums.utils.AppSettings;
import com.creativedots.mylums.utils.ValidationTools;
import com.wang.avi.AVLoadingIndicatorView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = ChangePasswordFragment.class.getSimpleName();

    private ImageView ivBack;
    private EditText etNewPassword, etConfirmPassword,etOldPassword;
    private RelativeLayout rlUpdatePassword, rlBackground;

    private AVLoadingIndicatorView aviLoading;

    private void getInfo(View view){
        aviLoading = view.findViewById(R.id.aviLoading);

        ivBack = view.findViewById(R.id.ivBack);
        rlBackground = view.findViewById(R.id.rlBackground);

        etNewPassword = view.findViewById(R.id.etNewPassword);
        etConfirmPassword = view.findViewById(R.id.etConfirmPassword);
        etOldPassword = view.findViewById(R.id.etOldPassword);
        rlUpdatePassword = view.findViewById(R.id.rlUpdatePassword);
    }

    private void setInfo(){
        ivBack.setOnClickListener(this);
        rlUpdatePassword.setOnClickListener(this);
        rlBackground.setOnClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        getInfo(view);
        setInfo();
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivBack:
            case R.id.rlBackground:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.rlUpdatePassword:
                handleUpdatePasswordEvent();
                break;
        }
    }

    private void handleUpdatePasswordEvent() {
        if(ValidationTools.allFieldsFilled(etConfirmPassword,etConfirmPassword,etOldPassword)){
            if(etConfirmPassword.getText().toString().equalsIgnoreCase(etNewPassword.getText().toString())){

                /**
                 * Updating password on Database
                 */
                makeUpdatePasswordRequest();

            }else{
                Toast.makeText(context, R.string.warn_passwords_do_not_match, Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(context, R.string.warn_fill_all_fields, Toast.LENGTH_SHORT).show();
        }
    }

    private void makeUpdatePasswordRequest() {
        aviLoading.show();
        Call<com.creativedots.mylums.model.retroFit.Response> call =
                apiService.updateUserPassword(etOldPassword.getText().toString(),etConfirmPassword.getText().toString());

        call.enqueue(new Callback<com.creativedots.mylums.model.retroFit.Response>() {
            @Override
            public void onResponse(@NonNull Call<com.creativedots.mylums.model.retroFit.Response> call, @NonNull Response<com.creativedots.mylums.model.retroFit.Response> response) {
                aviLoading.hide();
                if(response.isSuccessful()){
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    AppSettings.hideKeyboard(context,getView());
                    getActivity().getSupportFragmentManager().popBackStack();
                }else{
                    if(response.errorBody() != null) {
                        String error = response.errorBody().toString();
                        Toast.makeText(getActivity(), getErrorMessage(error), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<com.creativedots.mylums.model.retroFit.Response> call, @NonNull  Throwable t) {
                aviLoading.hide();
                t.printStackTrace();
                if(t.getLocalizedMessage().contains("No address associated with hostname"))
                    showNoNetworkDialog(context);
            }
        });
    }
}
