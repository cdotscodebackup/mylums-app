package com.creativedots.mylums.fragment.bottomBarFragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blikoon.qrcodescanner.QrCodeActivity;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.fragment.base.BaseFragment;
import com.creativedots.mylums.fragment.dashboardFragments.EventDetailsFragment;
import com.creativedots.mylums.model.retroFit.Data;
import com.creativedots.mylums.model.retroFit.profile.User;
import com.creativedots.mylums.utils.AppSettings;
import com.creativedots.mylums.utils.DownloadImageTask;
import com.creativedots.mylums.utils.ValidationTools;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;
import com.google.zxing.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static android.app.Activity.RESULT_OK;

public class QrCodeFragment extends BaseFragment implements
        View.OnClickListener {

    public static final String TAG = QrCodeFragment.class.getSimpleName();

    private static final String[] CAMERA_PERMISSIONS =
            {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private CodeScanner mCodeScanner;

    private static final int REQUEST_CODE_QR_SCAN = 101;
    private static final int REQUEST_CAMERA_PERMISSION = 103;

    private TextView tvScan;
    private ImageView ivBack, ivQrCode;
    private RelativeLayout rlRescan, rlShareQrCode;
    private LinearLayout lnScanner;
    private CodeScannerView scannerView;

    private void getInfo(View view) {
        ivBack = view.findViewById(R.id.ivBack);

        tvScan = view.findViewById(R.id.tvScan);
        ivQrCode = view.findViewById(R.id.ivQrCode);
        rlRescan = view.findViewById(R.id.rlRescan);
        rlShareQrCode = view.findViewById(R.id.rlShareQrCode);
        scannerView = view.findViewById(R.id.scanner_view);
        lnScanner = view.findViewById(R.id.lnScanner);
    }

    private void setInfo() {
        ivBack.setOnClickListener(this);
        ivQrCode.setOnClickListener(this);
        rlRescan.setOnClickListener(this);
        rlShareQrCode.setOnClickListener(this);

        /**
         * Setting QrCode Image
         */
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            activity.changeBottomTabs(false);
        }
        initQR();
    }

    private void initQR() {

        // TODO : Call API here to get QR-Code Image.

        String bQrCode = SessionManager.from(getActivity()).getUser().getQrcode();
        Glide.with(context).load(bQrCode)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true))
                .into(ivQrCode);


        mCodeScanner = new CodeScanner(getActivity(), scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("CONTACT", result.getText());
                        parseJson(result.getText());
                        mCodeScanner.releaseResources();
                        ivQrCode.setVisibility(View.VISIBLE);
                    }
                });
            }
        });
        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_qr_code, container, false);
        getInfo(view);
        setInfo();
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.rlRescan:
                if (ValidationTools.isPermissionGranted(getActivity(), Manifest.permission.CAMERA)) {
                    showQRCamera();
                } else {
                    ValidationTools.requestPermission(QrCodeFragment.this, Manifest.permission.CAMERA);
                }
                break;
            case R.id.rlShareQrCode:
                new DownloadImageTask().execute(SessionManager.from(getActivity()).getUser().getQrcode());
                break;
            case R.id.ivQrCode:
                try {
                    Data data = SessionManager.from(getActivity()).getUser();
                    User user = new User();
                    user.setId(data.userId);
                    user.setFirstName(data.firstName);
                    user.setLastName(data.lastName);
                    user.setEmail(data.email);
                    user.setPhone(data.phone);
                    user.setCurrentCompany(data.company);
                    user.setCurrentIndustry("");
                    user.setCurrentDesignation(data.designation);
                    user.setCurrentDepartment(data.department);
                    QrCodeInfoFragment fragment = new QrCodeInfoFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("user", user);
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()).replaceFragment(fragment, QrCodeInfoFragment.TAG, true, false);

                } catch (Exception ex) {
                    Log.e("Exception", "" + ex);
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // mCodeScanner.startPreview();
    }

    @Override
    public void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }

    private void showQRCamera() {
        lnScanner.setVisibility(View.VISIBLE);
        ivQrCode.setVisibility(View.GONE);
        mCodeScanner.releaseResources();
        mCodeScanner.startPreview();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (ValidationTools.isPermissionGranted(getActivity(), Manifest.permission.CAMERA)) {
            if (requestCode == 50) {
                showQRCamera();
            }
        }
    }

    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask() {

        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap bmp = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                bmp = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return bmp;
        }

        protected void onPostExecute(Bitmap result) {

            getLocalBitmapUri(result);
        }
    }


    public void getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {

            File cachePath = new File(context.getCacheDir(), "images");
            cachePath.mkdirs(); // don't forget to make the directory
            FileOutputStream stream = new FileOutputStream(cachePath + "/image.png"); // overwrites this image every time
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            stream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        File imagePath = new File(context.getCacheDir(), "images");
        File newFile = new File(imagePath, "image.png");
        Uri contentUri = FileProvider.getUriForFile(context, "com.creativedots.mylums.fileprovider", newFile);

        if (contentUri != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
            shareIntent.setDataAndType(contentUri, getActivity().getContentResolver().getType(contentUri));
            shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
            startActivity(Intent.createChooser(shareIntent, "Choose an app"));
        }
        //   return bmpUri;
    }


    public void parseJson(String json) {

        try {
            JSONObject jsonObject = new JSONObject(json).getJSONObject("user");
            User user = new User();
            user.setId(jsonKeyValue(jsonObject, "id"));
            user.setFirstName(jsonKeyValue(jsonObject, "name"));
            user.setLastName("");
            user.setEmail(jsonKeyValue(jsonObject, "email"));
            user.setPhone(jsonKeyValue(jsonObject, "phone"));
            user.setCurrentCompany(jsonKeyValue(jsonObject, "company"));
            user.setCurrentIndustry(jsonKeyValue(jsonObject, "industry"));
            user.setCurrentDesignation(jsonKeyValue(jsonObject, "designation"));
            user.setCurrentDepartment(jsonKeyValue(jsonObject, "department"));

            QrCodeInfoFragment fragment = new QrCodeInfoFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("user", user);
            fragment.setArguments(bundle);
            ((MainActivity) getActivity()).replaceFragment(fragment, QrCodeInfoFragment.TAG, true, false);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Toast.makeText(getActivity(), result.getText(), Toast.LENGTH_SHORT).show();

    }

    private String jsonKeyValue(JSONObject jsonObject, String key) {

        try {
            if (jsonObject.has(key)) {
                return jsonObject.getString(key);
            }
            return "";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e("Destroy", "Destroy called");
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            activity.changeBottomTabs(true);
        }
    }
}
