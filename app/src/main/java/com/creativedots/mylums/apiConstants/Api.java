package com.creativedots.mylums.apiConstants;

public class Api {

    public static final String BASE_URL = "https://mylums.pk/app/";
    //public static final String BASE_URL = "http://18.188.215.241/lums/app/";

    public static final String URL_TERMS_AND_CONDITIONS = BASE_URL + "terms-and-conditions";
    public static final String URL_PRIVACY_POLICY = BASE_URL + "privacy-policy";

    //    public static final String KEY_AUTHORIZATION = "Authorization : ";
    public static final String KEY_SUCCESSFULL = "Successful";
    public static final String KEY_MESSAGE = "message";

    public class Token {
        public static final String POST_URL = "auth/token";
        public static final String PARAM_USERNAME = "username";
        public static final String PARAM_PASSWORD = "password";
    }

    public class Login {
        public static final String POST_URL = "login";
        public static final String RESEND_URL = "resend";
        public static final String PARAM_USERNAME = "username";
        public static final String PARAM_PASSWORD = "password";
        public static final String PARAM_EMAIL = "email";
    }

    public class ForgotPassword {
        public static final String POST_URL = "forgotpassword";

        public static final String PARAM_EMAIL = "email";
    }

    public class Logout {
        public static final String POST_URL = "logout";


    }

    public class Register {
        public static final String POST_URL = "signup";

        public static final String PARAM_FIRST_NAME = "first_name";
        public static final String PARAM_LAST_NAME = "last_name";
        public static final String PARAM_ROLL_NO = "roll_number";
        public static final String PARAM_EMAIL = "email";
        public static final String PARAM_PASSWORD = "password";
        public static final String PARAM_CONFIRM_PASSWORD = "confirmpassword";
        public static final String PARAM_STATUS = "status";
    }

    public class PreLoad {
        public static final String POST_URL = "preload/verifyemail";

        public static final String PARAM_EMAIL = "email";
        public static final String PARAM_ROLL_NO = "roll_number";
    }

    public class HelpMenu {
        public static final String GET_URL = "help/menu";
    }

    public class Help {
        public static final String POST_URL = "help/desk";

        public static final String PARAM_FIRST_NAME = "first_name";
        public static final String PARAM_LAST_NAME = "last_name";
        public static final String PARAM_ROLL_NO = "roll_number";
        public static final String PARAM_PHONE_NO = "phone_number";
        public static final String PARAM_EMAIL = "email";
        public static final String PARAM_MESSAGE = "message";
    }

    public class AlumniList {
        public static final String GET_URL = "alumni/all";

        public static final String PARAM_PAGED = "paged";
        public static final String DEFAULT_PAGE_NUMBER = "1";
    }

    public class Notification {


        public static final String GET_URL = "notification/all";
        public static final String GET_READ_URL = "notification/read/{notification_id}";
        public static final String GET_DEVICE_REGISTRATION_URL = "device/registration";
        public static final String PARAM_FCM_TOKEN = "fcm_token";
        public static final String PARAM_TYPE = "type";
        public static final String PARAM_NOTIFICATION_ID = "notification_id";


    }

    public class AlumniProfile {
        public static final String GET_URL = "alumni/profile";

        public static final String PARAM_USERID = "userid";
    }

    public class AlumniSearch {
        public static final String GET_URL = "alumni/search";

        public static final String PARAM_QUERY = "q";
        public static final String PARAM_PAGED = "paged";
        public static final String PARAM_ADVANCED = "advance";
        public static final String PARAM_COUNTRY = "country";
        public static final String PARAM_CITY = "city";
        public static final String PARAM_PROGRAM = "program";
        public static final String PARAM_COMPANY = "company";
        public static final String PARAM_DESIGNATION = "designation";
        public static final String PARAM_DEPARTMENT = "department";
        public static final String PARAM_COMPOSE = "compose";

        public static final String DEFAULT_PAGE_NUMBER = "1";
        public static final String DEFAULT_ADVANCED = "yes";
    }

    public class Events {
        public static final String GET_URL = "events/all";

        public static final String PARAM_QUERY = "q";
        public static final String PARAM_ALL = "all";
        public static final String PARAM_PAGED = "paged";

        public static final String DEFAULT_QUERY = "";
        public static final String DEFAULT_ALL = "yes";
        public static final String DEFAULT_PAGE_NUMBER = "1";

        public static final String STATUS_ACTIVE = "active";
        public static final String STATUS_EXPIRE = "expire";
    }

    public class PastEvents {
        public static final String GET_URL = "events/past";

        public static final String PARAM_QUERY = "q";
        public static final String PARAM_ALL = "all";
        public static final String PARAM_PAGED = "paged";

        public static final String DEFAULT_QUERY = "";
        public static final String DEFAULT_ALL = "yes";
        public static final String DEFAULT_PAGE_NUMBER = "1";

        public static final String STATUS_ACTIVE = "active";
        public static final String STATUS_EXPIRE = "expire";
    }

    public class UpcomingEvents {
        public static final String GET_URL = "events/Upcoming";

        public static final String PARAM_QUERY = "q";
        public static final String PARAM_ALL = "all";
        public static final String PARAM_PAGED = "paged";

        public static final String DEFAULT_QUERY = "";
        public static final String DEFAULT_ALL = "yes";
        public static final String DEFAULT_PAGE_NUMBER = "1";

        public static final String STATUS_ACTIVE = "active";
        public static final String STATUS_EXPIRE = "expire";
    }

    public class EventDetails {

        public static final String GET_URL = "events/detail";
        public static final String PARAM_URL = "url";
        public static final String PARAM_EVENT_ID = "event_id";

    }

    public class EventAttending {
        public static final String GET_URL = "events/attending";
        public static final String EVENT_ID = "event_id";
        public static final String EVENT_STATUS = "status";
    }

    public class EventCheckin {
        public static final String GET_URL = "events/checkin";
        public static final String EVENT_ID = "event_id";
        public static final String STATUS = "status";
    }

    /*public class EventAttending{
        public static final String POST_URL = "events/attending";
        public static final String EVENT_ID = "event_id";
        public static final String EVENT_STATUS = "status";
    }

    public class EventCheckin{
        public static final String POST_URL = "events/checkin";
        public static final String EVENT_ID = "event_id";
        public static final String STATUS = "status";
    }*/

    public class PartnersList {
        public static final String GET_URL = "partners/all";

        public static final String PARAM_QUERY = "q";
        public static final String PARAM_ALL = "all";
        public static final String PARAM_PAGED = "paged";
        public static final String DEFAULT_QUERY = "";
        public static final String DEFAULT_ALL = "yes";
        public static final String DEFAULT_PAGE_NUMBER = "1";
    }

    public class PartnersDetails {
        public static final String GET_URL = "partners/detail";

        public static final String PARTNER_ID = "partner_id";

        public static final String IS_PRIMIUM = "1";
    }


    public class SinglePartner {

        public static final String GET_URL = "partners/near_offers/{partner_id}";

        public static final String PARAM_PARTNER_ID = "partner_id";
        public static final String PARAM_LAT = "lat";
        public static final String PARAM_LNG = "lng";
        public static final String PARAM_QUERY = "q";
        public static final String PARAM_ALL = "all";
        public static final String PARAM_PAGED = "paged";

        public static final String DEFAULT_QUERY = "";
        public static final String DEFAULT_ALL = "yes";
        public static final String DEFAULT_PAGE_NUMBER = "1";
    }

    public class PartnersNearBy {
        public static final String GET_URL = "partners/nearby";

        public static final String PARAM_LAT = "lat";
        public static final String PARAM_LNG = "lng";
        public static final String PARAM_QUERY = "q";
        public static final String PARAM_ALL = "all";
        public static final String PARAM_PAGED = "paged";
        public static final String PARAM_STATUS = "status";


        public static final String DEFAULT_QUERY = "";
        public static final String DEFAULT_ALL = "yes";
        public static final String DEFAULT_PAGE_NUMBER = "1";
        public static final String DEFAULT_PUSH = "push";
    }

    public class Message {
        public static final String ARCHIVE_UNARCHIVE_URL = "messages/archive/{thread_id}";
        public static final String GET_URL_ALL = "messages/all";
        public static final String GET_URL_READ = "messages/read/{thread_id}";
        public static final String GET_URL_UNREAD = "messages/threads/unread";
        public static final String GET_URL_ARCHIVE = "messages/threads/archive";
        public static final String GET_URL_MESSAGE_BOARD = "messages/messageboard/";
        public static final String GET_SEND_MESSAGE_URL = "messages/messagesent/";
        public static final String PARAM_MESSAGE = "message";
        public static final String PARAM_ROOM_ID = "room_id";
        public static final String RECEIVER_ID = "receiver_id";
        public static final String PARAM_STATUS = "status";
        public static final String DEFAULT_ARCHIVE = "archive";
        public static final String DEFAULT_UNARCHIVE = "unarchive";
        public static final String PARAM_THREAD_ID = "thread_id";

    }

    public class TinderResponse{
        public static final String GET_URL = "messages/tinder/";
        public static final String PARAM_USER_ID = "user_id";
        public static final String PARAM_STATUS = "status";

    }
    public class SayHiMessage {
        public static final String GET_URL = "messages/sayhimessage/";
        public static final String GET_TINDER_URL = "messages/tindermessage/";
        public static final String MESSAGE = "message";
        public static final String RECEIVER_ID = "receiver_id";
    }

    public class Profile{
        public static final String GET_URL = "users/profile";
        public static final String GET_UPDATE_URL_API = "users/profile/update";
    }

    public class InterestIndustry{
        public static final String GET_URL = "customs/interest_industry";
    }

    public class CountryStateCity{
        public static final String GET_COUNTIRES_URL = "location/countries";
        public static final String GET_STATES_URL = "location/states/{id}";
        public static final String GET_CITIES_URL = "location/cities/{id}";
        public static final String ID = "id";
    }

    public class SettingsGet{
        public static final String GET_URL = "setting/get";

        public static final String KEY_NOTIFICATIONS_YES = "yes";
        public static final String KEY_NOTIFICATIONS_NO = "no";
        public static final String KEY_LOCATION_YES = "yes";
        public static final String KEY_LOCATION_NO = "no";
    }

    public class SettingsUpdate{
        public static final String POST_URL = "setting/update";

        public static final String PARAM_MUTE = "mute";
        public static final String PARAM_LOCATION = "location";
        public static final String PARAM_OFFER_RADIUS = "offerradius";

        public static final String KEY_NOTIFICATIONS_YES = "yes";
        public static final String KEY_NOTIFICATIONS_NO = "no";
        public static final String KEY_LOCATION_YES = "yes";
        public static final String KEY_LOCATION_NO = "no";

        public static final String RESPONSE_SUCCESSFULL = "Successful";
    }

    public class UpdatePassword{
        public static final String POST_URL = "users/profile/update";

        public static final String PARAM_USER_PASSWORD = "users___password";
        public static final String PARAM_CURRENT_PASSWORD = "users___current_password";
    }

    public class Alumni{
        public static final String GET_URL_INVITE = "alumni/invite";
        public static final String GET_URL_PROLOAD = "alumni/preload/{user_id}";
        public static final String PARAM_USER_ID = "user_id";
    }
}
