package com.creativedots.mylums.network;

import android.support.annotation.NonNull;
import android.util.Log;

import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static final String BASE_URL = Api.BASE_URL;
    private static Retrofit retrofit = null;
    private static String token = "";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    public static Retrofit getRetrofitClient(String authToken) {
        if (retrofit == null || (!authToken.equalsIgnoreCase(token))) {
            token = authToken;
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            if (!authToken.equals(SessionManager.DEFAULT_VALUE)) {
                AuthenticationInterceptor interceptor = new AuthenticationInterceptor(authToken);
                httpClient.addInterceptor(interceptor);

                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .client(httpClient.build())
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();
            } else {
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();
            }
        }
        return retrofit;
    }

    public static class AuthenticationInterceptor implements Interceptor {

        private String authToken;

        AuthenticationInterceptor(String token) {
            this.authToken = token;
        }

        @Override
        public Response intercept(@NonNull Chain chain) throws IOException {
            Request original = chain.request();

            Request.Builder builder = original.newBuilder()
                    .header("Authorization", authToken);

            Log.e("AuthToken", "" + authToken);

            Request request = builder.build();
            return chain.proceed(request);
        }


    }
}
