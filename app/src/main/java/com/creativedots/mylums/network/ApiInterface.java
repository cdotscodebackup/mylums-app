package com.creativedots.mylums.network;

import com.creativedots.mylums.apiConstants.Api;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;


import com.creativedots.mylums.model.eventDetail.EventAttendResponse;
import com.creativedots.mylums.model.eventDetail.EventDetailResponse;
import com.creativedots.mylums.model.retroFit.Alumni.AlumniProfileResponse;
import com.creativedots.mylums.model.retroFit.Message.ChannelsResponse;
import com.creativedots.mylums.model.retroFit.Message.MessageBoardResponse;
import com.creativedots.mylums.model.retroFit.helpLogin.HelpLoginResponse;
import com.creativedots.mylums.model.retroFit.helpMenu.HelpMenuResponse;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.model.retroFit.notification.NotificationResponse;
import com.creativedots.mylums.model.retroFit.offer.OfferResponse;
import com.creativedots.mylums.model.retroFit.partnersDetails.PartnersDetailsResponse;
import com.creativedots.mylums.model.retroFit.partnersList.PartnersListResponse;
import com.creativedots.mylums.model.retroFit.partnersNearBy.PartnersNearByResponse;
import com.creativedots.mylums.model.retroFit.profile.CityResponse;
import com.creativedots.mylums.model.retroFit.profile.CountryResponse;
import com.creativedots.mylums.model.retroFit.profile.InterestIndustryResponse;
import com.creativedots.mylums.model.retroFit.profile.ProfileResponse;
import com.creativedots.mylums.model.retroFit.profile.StateResponse;
import com.creativedots.mylums.model.retroFit.settings.SettingsGetResponse;
import com.creativedots.mylums.model.retroFit.settings.SettingsUpdateResponse;

import java.util.HashMap;

import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiInterface {

    @FormUrlEncoded
    @POST(Api.Token.POST_URL)
    Call<Response> getToken(
            @Field(Api.Token.PARAM_USERNAME) String username,
            @Field(Api.Token.PARAM_PASSWORD) String password
    );

    @FormUrlEncoded
    @POST(Api.Login.POST_URL)
    Call<ProfileResponse> login(
            @Field(Api.Login.PARAM_USERNAME) String username,
            @Field(Api.Login.PARAM_PASSWORD) String password
    );

    @FormUrlEncoded
    @POST(Api.Login.RESEND_URL)
    Call<Response> resendEmail(
            @Field(Api.Login.PARAM_EMAIL) String username
    );

    @FormUrlEncoded
    @POST(Api.ForgotPassword.POST_URL)
    Call<Response> forgotPassword(@Field(Api.ForgotPassword.PARAM_EMAIL) String email);

    @FormUrlEncoded
    @POST(Api.PreLoad.POST_URL)
    Call<Response> verifyEmail(@Field(Api.PreLoad.PARAM_EMAIL) String email);

    @FormUrlEncoded
    @POST(Api.PreLoad.POST_URL)
    Call<Response> verifyRollNo(@Field(Api.PreLoad.PARAM_ROLL_NO) String rollNo);

    @FormUrlEncoded
    @POST(Api.Register.POST_URL)
    Call<Response> register(
            @Field(Api.Register.PARAM_FIRST_NAME) String firstName,
            @Field(Api.Register.PARAM_LAST_NAME) String lastName,
            @Field(Api.Register.PARAM_ROLL_NO) String rollNo,
            @Field(Api.Register.PARAM_EMAIL) String email,
            @Field(Api.Register.PARAM_PASSWORD) String password,
            @Field(Api.Register.PARAM_CONFIRM_PASSWORD) String confirmPassword,
            @Field(Api.Register.PARAM_STATUS) String status
    );

    @FormUrlEncoded
    @POST(Api.SayHiMessage.GET_URL)
    Call<Response> sayHiMessage(
            @Field(Api.SayHiMessage.MESSAGE) String message,
            @Field(Api.SayHiMessage.RECEIVER_ID) String recId
    );

    @FormUrlEncoded
    @POST(Api.SayHiMessage.GET_TINDER_URL)
    Call<Response> sayHiTinder(
            @Field(Api.SayHiMessage.MESSAGE) String message,
            @Field(Api.SayHiMessage.RECEIVER_ID) String recId
    );


    @FormUrlEncoded
    @POST(Api.TinderResponse.GET_URL)
    Call<Response> tinderMessage(
            @Field(Api.TinderResponse.PARAM_STATUS) String message,
            @Field(Api.TinderResponse.PARAM_USER_ID) String recId
    );


    @FormUrlEncoded
    @POST(Api.Help.POST_URL)
    Call<HelpLoginResponse> help(
            @Field(Api.Help.PARAM_FIRST_NAME) String firstName,
            @Field(Api.Help.PARAM_LAST_NAME) String lastName,
            @Field(Api.Help.PARAM_ROLL_NO) String rollNo,
            @Field(Api.Help.PARAM_PHONE_NO) String phoneNo,
            @Field(Api.Help.PARAM_EMAIL) String email,
            @Field(Api.Help.PARAM_MESSAGE) String message
    );


    @GET(Api.Notification.GET_URL)
    Call<NotificationResponse> notifcationList();


    @GET(Api.AlumniList.GET_URL)
    Call<Response> alumniList(@Query(Api.AlumniList.PARAM_PAGED) String paged);

    @GET(Api.Profile.GET_URL)
    Call<ProfileResponse> getProfile();

    @GET(Api.InterestIndustry.GET_URL)
    Call<InterestIndustryResponse> getInterestIndustry();

    @GET(Api.CountryStateCity.GET_COUNTIRES_URL)
    Call<CountryResponse> getCountries();

    @GET(Api.CountryStateCity.GET_STATES_URL)
    Call<StateResponse> getStates(@Path(Api.CountryStateCity.ID) String id);

    @GET(Api.CountryStateCity.GET_CITIES_URL)
    Call<CityResponse> getCities(@Path(Api.CountryStateCity.ID) String id);

    /*@GET(Api.AlumniProfile.POST_URL)
    Call<Response> alumniProfile(@Query (Api.AlumniProfile.PARAM_USERID) String userID);*/

    @GET
    Call<AlumniProfileResponse> alumniProfile(@Url String ApiURL);

    @GET(Api.AlumniSearch.GET_URL)
    Call<Response> alumniSearch(
            @Query(Api.AlumniSearch.PARAM_QUERY) String query,
            @Query(Api.AlumniSearch.PARAM_PAGED) String paged,
            @Query(Api.AlumniSearch.PARAM_ADVANCED) String advanced,
            @Query(Api.AlumniSearch.PARAM_COUNTRY) String country,
            @Query(Api.AlumniSearch.PARAM_CITY) String city,
            @Query(Api.AlumniSearch.PARAM_PROGRAM) String program,
            @Query(Api.AlumniSearch.PARAM_COMPANY) String company,
            @Query(Api.AlumniSearch.PARAM_DESIGNATION) String designation,
            @Query(Api.AlumniSearch.PARAM_DEPARTMENT) String department,
            @Query(Api.AlumniSearch.PARAM_COMPOSE) String compose
    );

    @GET(Api.Events.GET_URL)
    Call<Response> eventsList(
            @Query(Api.Events.PARAM_PAGED) String paged,
            @Query(Api.Events.PARAM_ALL) String all,
            @Query(Api.Events.PARAM_QUERY) String query
    );

    @GET(Api.UpcomingEvents.GET_URL)
    Call<Response> upcomingEventsList(
            @Query(Api.UpcomingEvents.PARAM_PAGED) String paged,
            @Query(Api.UpcomingEvents.PARAM_ALL) String all,
            @Query(Api.UpcomingEvents.PARAM_QUERY) String query
    );

    @GET(Api.PastEvents.GET_URL)
    Call<Response> pastEventsList(
            @Query(Api.PastEvents.PARAM_PAGED) String paged,
            @Query(Api.PastEvents.PARAM_ALL) String all,
            @Query(Api.PastEvents.PARAM_QUERY) String query
    );

    @GET
    Call<EventDetailResponse> eventDetails(@Url String ApiURL);

    @GET(Api.PartnersList.GET_URL)
    Call<PartnersListResponse> partnersList(
            @Query(Api.PartnersList.PARAM_QUERY) String query,
            @Query(Api.PartnersList.PARAM_PAGED) String paged,
            @Query(Api.PartnersList.PARAM_ALL) String all
    );


    @GET(Api.SinglePartner.GET_URL)
    Call<OfferResponse> partnerOfferNear(@Path(Api.SinglePartner.PARAM_PARTNER_ID) String id,
                                         @Query(Api.SinglePartner.PARAM_LAT) String lat,
                                         @Query(Api.SinglePartner.PARAM_LNG) String lng,
                                         @Query(Api.SinglePartner.PARAM_ALL) String all);

    Call<OfferResponse> partnerOfferAll(@Path(Api.SinglePartner.PARAM_PARTNER_ID) String id,
                                        @Query(Api.SinglePartner.PARAM_ALL) String all);

    /*@GET(Api.PartnersList.POST_URL)
    Call<PartnersListResponse> partnersListDetail(
            @Query(Api.PartnersList.PARAM_QUERY) String query,
            @Query(Api.PartnersList.PARAM_PAGED) String paged,
            @Query(Api.PartnersList.PARAM_ALL) String all
    );*/

    @GET
    Call<PartnersDetailsResponse> partnersDetails(@Url String ApiURL);

    @GET
    Call<ChannelsResponse> ChannelsList(@Url String ApiURL);

    @GET
    Call<MessageBoardResponse> MessagesList(@Url String ApiURL);

    @GET(Api.PartnersNearBy.GET_URL)
    Call<PartnersNearByResponse> partnersNearBy(
            @Query(Api.PartnersNearBy.PARAM_LAT) String lat,
            @Query(Api.PartnersNearBy.PARAM_LNG) String lng,
            @Query(Api.PartnersNearBy.PARAM_QUERY) String query,
            @Query(Api.PartnersNearBy.PARAM_ALL) String all,
            @Query(Api.PartnersNearBy.PARAM_STATUS) String status

    );

    @GET(Api.HelpMenu.GET_URL)
    Call<HelpMenuResponse> helpMenu();

    @POST(Api.Logout.POST_URL)
    Call<Response> logout();

    @GET(Api.EventAttending.GET_URL)
    Call<EventAttendResponse> attendEvent(
            @Query(Api.EventAttending.EVENT_ID) String eventId,
            @Query(Api.EventAttending.EVENT_STATUS) String eventStatus
    );

    @GET(Api.EventCheckin.GET_URL)
    Call<EventAttendResponse> checkInEvent(
            @Query(Api.EventCheckin.EVENT_ID) String eventId,
            @Query(Api.EventCheckin.STATUS) String status
    );

    @FormUrlEncoded
    @POST(Api.Notification.GET_DEVICE_REGISTRATION_URL)
    Call<Response> sendToken(
            @Field(Api.Notification.PARAM_FCM_TOKEN) String token,
            @Field(Api.Notification.PARAM_TYPE) String type
    );


    @FormUrlEncoded
    @POST(Api.Message.GET_SEND_MESSAGE_URL)
    Call<Response> sendMessage(
            @Field(Api.Message.PARAM_MESSAGE) String message,
            @Field(Api.Message.PARAM_ROOM_ID) String roomId
    );


    @GET(Api.Message.ARCHIVE_UNARCHIVE_URL)
    Call<Response> updateMessageStatus(
            @Path(Api.Message.PARAM_THREAD_ID) String id,
            @Query(Api.Message.PARAM_STATUS) String message
    );

    @GET(Api.Message.GET_URL_READ)
    Call<Response> readMessageStatus(
            @Path(Api.Message.PARAM_THREAD_ID) String id
    );

    @GET(Api.Notification.GET_READ_URL)
    Call<Response> readNotificationStatus(
            @Path(Api.Notification.PARAM_NOTIFICATION_ID) String id
    );

    @GET(Api.Alumni.GET_URL_INVITE)
    Call<Response> alumniInvite(
            @Query(Api.Alumni.PARAM_USER_ID) String userId

    );

    @GET(Api.Alumni.GET_URL_PROLOAD)
    Call<AlumniProfileResponse> alumniProload(
            @Path(Api.Alumni.PARAM_USER_ID) String userId
    );

    @FormUrlEncoded
    @POST(Api.Profile.GET_UPDATE_URL_API)
    Call<ProfileResponse> updateProfile(
            @FieldMap HashMap<String, String> data
    );


    @GET(Api.SettingsGet.GET_URL)
    Call<SettingsGetResponse> getUserSettings();

    @FormUrlEncoded
    @POST(Api.SettingsUpdate.POST_URL)
    Call<SettingsUpdateResponse> updateUserSettings(
            @Field(Api.SettingsUpdate.PARAM_MUTE) String mute,
            @Field(Api.SettingsUpdate.PARAM_LOCATION) String location,
            @Field(Api.SettingsUpdate.PARAM_OFFER_RADIUS) String offerRadius
    );

    @FormUrlEncoded
    @POST(Api.UpdatePassword.POST_URL)
    Call<Response> updateUserPassword(
            @Field(Api.UpdatePassword.PARAM_CURRENT_PASSWORD) String oldPassword,
            @Field(Api.UpdatePassword.PARAM_USER_PASSWORD) String userPassword);

    @Multipart
    @POST(Api.Profile.GET_UPDATE_URL_API)
    Call<ProfileResponse> uploadProfileImage(@Part MultipartBody.Part image, @Part("profile___first_name") RequestBody title);

}