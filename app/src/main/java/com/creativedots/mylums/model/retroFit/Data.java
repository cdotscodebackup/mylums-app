package com.creativedots.mylums.model.retroFit;

import com.creativedots.mylums.R;
import com.creativedots.mylums.model.eventDetail.Users;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Data {

    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("roll_number")
    @Expose
    public String rollNumber;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("gender")
    @Expose
    public String gender;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("current_department")
    @Expose
    public String department;
    @SerializedName("current_company")
    @Expose
    public String company;
    @SerializedName("current_designation")
    @Expose
    public String designation;
    @SerializedName("current_industry")
    @Expose
    private String industry;

    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("deleted_at")
    @Expose
    public String deletedAt;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("imagepath")
    @Expose
    public String imagePath;
    @SerializedName("profilepath")
    @Expose
    public String profilePath;
    @SerializedName("program")
    @Expose
    public String program;
    @SerializedName("year")
    @Expose
    public String year;
    @SerializedName("venue")
    @Expose
    public String venue;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("start_time")
    @Expose
    public String startTime;
    @SerializedName("end_time")
    @Expose
    public String endTime;
    @SerializedName("dates")
    @Expose
    public String dates;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("users")
    @Expose
    public Users users;

    /**
     * For PartnersList
     */
    @SerializedName("concerned_person")
    @Expose
    public String concernedPerson;
    @SerializedName("contact_detail")
    @Expose
    public String contactDetail;
    @SerializedName("premium")
    @Expose
    public String premium;
    @SerializedName("partnerspath")
    @Expose
    public String partnerspath;
    @SerializedName("qrcode")
    @Expose
    public String qrcode;
    @SerializedName("usertype")
    @Expose
    private String usertype;

    /***
     * Empty constructor required
     * @param s
     * @param s1
     * @param s2
     * @param expire
     * @param s3
     * @param s4
     * @param sample_name
     * @param sampleName
     * @param male
     * @param s5
     * @param name
     * @param sample_name1
     * @param sampleName1
     * @param pak
     * @param lhe
     * @param s6
     * @param s7
     * @param s8
     */
    public Data(String s, String s1, String s2, String expire, String s3, String s4, String sample_name, String sampleName, String male, String s5, String name, String sample_name1, String sampleName1, String pak, String lhe, String s6, String s7, String s8) {

    }

    public Data(String userId, String email, String rollNumber,
                String status, String id, String image, String firstName,
                String lastName, String gender, String phone,
                String department, String company, String designation,
                String country, String city, String deletedAt,
                String createdAt, String updatedAt, String qrcode, String industry) {
        this.userId = userId;
        this.email = email;
        this.rollNumber = rollNumber;
        this.status = status;
        this.id = id;
        this.image = image;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.phone = phone;
        this.department = department;
        this.company = company;
        this.designation = designation;
        this.country = country;
        this.city = city;
        this.deletedAt = deletedAt;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.qrcode = qrcode;
        this.industry = industry;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDates() {
        return dates;
    }

    public void setDates(String dates) {
        this.dates = dates;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public String getConcernedPerson() {
        return concernedPerson;
    }

    public void setConcernedPerson(String concernedPerson) {
        this.concernedPerson = concernedPerson;
    }

    public String getContactDetail() {
        return contactDetail;
    }

    public void setContactDetail(String contactDetail) {
        this.contactDetail = contactDetail;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    public String getPartnerspath() {
        return partnerspath;
    }

    public void setPartnerspath(String partnerspath) {
        this.partnerspath = partnerspath;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("userId", userId).append("email", email).append("rollNumber", rollNumber).append("status", status).append("id", id).append("image", image).append("firstName", firstName).append("lastName", lastName).append("name", name).append("gender", gender).append("phone", phone).append("department", department).append("company", company).append("designation", designation).append("country", country).append("city", city).append("deletedAt", deletedAt).append("createdAt", createdAt).append("updatedAt", updatedAt).toString();
    }

    public int getSelectedGender() {
        if (gender == null || gender.isEmpty()) {
            return R.id.male;
        } else if (gender.equalsIgnoreCase("male")) {
            return R.id.male;
        } else if (gender.equalsIgnoreCase("female")) {
            return R.id.female;
        } else {
            return R.id.male;
        }
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }
}