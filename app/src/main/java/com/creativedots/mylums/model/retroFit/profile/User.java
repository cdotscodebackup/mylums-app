
package com.creativedots.mylums.model.retroFit.profile;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.creativedots.mylums.R;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

public class User implements Parcelable {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("roll_number")
    @Expose
    private String rollNumber;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("personal_email")
    @Expose
    private String personalEmail;
    @SerializedName("alternative_email")
    @Expose
    private String alternativeEmail;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("landline")
    @Expose
    private String landline;
    @SerializedName("program")
    @Expose
    private String program;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("current_company")
    @Expose
    private String currentCompany;
    @SerializedName("current_industry")
    @Expose
    private String currentIndustry;
    @SerializedName("current_designation")
    @Expose
    private String currentDesignation;
    @SerializedName("current_department")
    @Expose
    private String currentDepartment;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("state_province")
    @Expose
    private String stateProvince;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("state_id")
    @Expose
    private String stateId;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("qrcode")
    @Expose
    private String qrcode;
    @SerializedName("imagepath")
    @Expose
    private String imagepath;
    @SerializedName("profilepath")
    @Expose
    private String profilepath;
    @SerializedName("interests")
    @Expose
    private ArrayList<Interest> interests = new ArrayList<Interest>();
    @SerializedName("workexperience")
    @Expose
    private ArrayList<WorkExperience> workExperience = new ArrayList<WorkExperience>();
    @SerializedName("proload")
    @Expose
    private Proload proload;
    @SerializedName("linkedin")
    @Expose
    private Linkedin linkedin;
    @SerializedName("setting")
    @Expose
    private Setting setting;
    @SerializedName("usertype")
    @Expose
    private String usertype;

    private HashMap<String, String> userObject;

    public User() {

    }


    protected User(Parcel in) {
        userId = in.readString();
        email = in.readString();
        rollNumber = in.readString();
        status = in.readString();
        id = in.readString();
        image = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        personalEmail = in.readString();
        alternativeEmail = in.readString();
        gender = in.readString();
        phone = in.readString();
        landline = in.readString();
        program = in.readString();
        year = in.readString();
        currentCompany = in.readString();
        currentIndustry = in.readString();
        currentDesignation = in.readString();
        currentDepartment = in.readString();
        country = in.readString();
        stateProvince = in.readString();
        city = in.readString();
        location = in.readString();
        address = in.readString();
        countryId = in.readString();
        stateId = in.readString();
        cityId = in.readString();
        deletedAt = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        qrcode = in.readString();
        imagepath = in.readString();
        profilepath = in.readString();
        usertype = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getUserId() {
        if(userId == null)
            return "";
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFirstName() {
        if(firstName == null)
            return "";
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        if(lastName == null)
            return "";
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPersonalEmail() {
        return personalEmail;
    }

    public void setPersonalEmail(String personalEmail) {
        this.personalEmail = personalEmail;
    }

    public String getAlternativeEmail() {
        return alternativeEmail;
    }

    public void setAlternativeEmail(String alternativeEmail) {
        this.alternativeEmail = alternativeEmail;
    }

    public String getGender() {
        return String.valueOf(gender.toCharArray()[0]).toUpperCase()
                + gender.substring(1);
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCurrentCompany() {
        return currentCompany;
    }

    public void setCurrentCompany(String currentCompany) {
        this.currentCompany = currentCompany;
    }

    public String getCurrentIndustry() {
        return currentIndustry;
    }

    public void setCurrentIndustry(String currentIndustry) {
        this.currentIndustry = currentIndustry;
    }

    public String getCurrentDesignation() {
        return currentDesignation;
    }

    public void setCurrentDesignation(String currentDesignation) {
        this.currentDesignation = currentDesignation;
    }

    public String getCurrentDepartment() {
        return currentDepartment;
    }

    public void setCurrentDepartment(String currentDepartment) {
        this.currentDepartment = currentDepartment;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getProfilepath() {
        return profilepath;
    }

    public void setProfilepath(String profilepath) {
        this.profilepath = profilepath;
    }

    public ArrayList<Interest> getInterests() {
        return interests;
    }

    public void setInterests(ArrayList<Interest> interests) {
        this.interests = interests;
    }

    public ArrayList<WorkExperience> getWorkExperience() {
        return workExperience;
    }

    public void setWorkExperience(ArrayList<WorkExperience> workExperience) {
        this.workExperience = workExperience;
    }

    public Proload getProload() {
        return proload;
    }

    public void setProload(Proload proload) {
        this.proload = proload;
    }

    public Linkedin getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(Linkedin linkedin) {
        this.linkedin = linkedin;
    }

    public Setting getSetting() {
        return setting;
    }

    public void setSetting(Setting setting) {
        this.setting = setting;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public int getSelectedGender() {
        if (gender == null || gender.isEmpty()) {
            return R.id.male;
        } else if (gender.equalsIgnoreCase("male")) {
            return R.id.male;
        } else if (gender.equalsIgnoreCase("female")) {
            return R.id.female;
        } else {
            return R.id.male;
        }
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public HashMap<String, String> getUserObject() {
        userObject = new HashMap();
        if (!firstName.isEmpty())
            userObject.put("profile___first_name", firstName);
        if (!lastName.isEmpty())
            userObject.put("profile___last_name", lastName);
        if (!gender.isEmpty())
            userObject.put("profile___gender", gender);
        if (!program.isEmpty())
            userObject.put("profile___program", program);
        if (!year.isEmpty())
            userObject.put("profile___year", year);
        if (!phone.isEmpty())
            userObject.put("profile___phone", phone);
        if (!landline.isEmpty())
            userObject.put("profile___landline", landline);
        else
            userObject.put("profile___landline", "");
        if (!personalEmail.isEmpty())
            userObject.put("profile___personal_email", personalEmail);
        if (!alternativeEmail.isEmpty())
            userObject.put("profile___alternative_email", alternativeEmail);
        /*if (!currentCompany.isEmpty())
            userObject.put("profile___current_company", currentCompany);
        if (!currentIndustry.isEmpty())
            userObject.put("profile___current_industry", currentIndustry);
        if (!currentDesignation.isEmpty())
            userObject.put("profile___current_designation", currentDesignation);
        if (!currentDepartment.isEmpty())
            userObject.put("profile___current_department", currentDepartment);*/
        if (!country.isEmpty())
            userObject.put("profile___country", country);
        if (!stateProvince.isEmpty())
            userObject.put("profile___state_province", stateProvince);
        if (!city.isEmpty())
            userObject.put("profile___city", city);
        if (!address.isEmpty())
            userObject.put("profile___address", address);
        if (!countryId.isEmpty())
            userObject.put("profile___country_id", countryId);
        if (!stateId.isEmpty())
            userObject.put("profile___state_id", stateId);
        if (!cityId.isEmpty())
            userObject.put("profile___city_id", cityId);
        //userObject.put("users___password","");
        if (interests.size() > 0)
            userObject.put("interests", new Interest().objectToHashMapList(interests));
        if (workExperience.size() > 0)
            userObject.put("work_experience", new WorkExperience().objectToHashMapList(workExperience));

        Log.e("UPDATE", userObject.toString());
        return userObject;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(userId);
        parcel.writeString(email);
        parcel.writeString(rollNumber);
        parcel.writeString(status);
        parcel.writeString(id);
        parcel.writeString(image);
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeString(personalEmail);
        parcel.writeString(alternativeEmail);
        parcel.writeString(gender);
        parcel.writeString(phone);
        parcel.writeString(landline);
        parcel.writeString(program);
        parcel.writeString(year);
        parcel.writeString(currentCompany);
        parcel.writeString(currentIndustry);
        parcel.writeString(currentDesignation);
        parcel.writeString(currentDepartment);
        parcel.writeString(country);
        parcel.writeString(stateProvince);
        parcel.writeString(city);
        parcel.writeString(location);
        parcel.writeString(address);
        parcel.writeString(countryId);
        parcel.writeString(stateId);
        parcel.writeString(cityId);
        parcel.writeString(deletedAt);
        parcel.writeString(createdAt);
        parcel.writeString(updatedAt);
        parcel.writeString(qrcode);
        parcel.writeString(imagepath);
        parcel.writeString(profilepath);
        parcel.writeString(usertype);
    }
}
