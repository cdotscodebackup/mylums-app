package com.creativedots.mylums.model.retroFit.notification;

import android.os.Parcel;
import android.os.Parcelable;

import com.creativedots.mylums.model.retroFit.profile.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("receiver_id")
    @Expose
    private String receiverId;
    @SerializedName("sender_id")
    @Expose
    private String senderId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("type_id")
    @Expose
    private String typeId;

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("read")
    @Expose
    private String read;
    @SerializedName("imagepath")
    @Expose
    private String imagepath;
    @SerializedName("notification_title")
    @Expose
    private String notificationTitle;
    @SerializedName("notification_body")
    @Expose
    private String notificationBody;

    @SerializedName("profile")
    @Expose
    private User profile;


    protected Notification(Parcel in) {
        id = in.readString();
        receiverId = in.readString();
        senderId = in.readString();
        type = in.readString();
        typeId = in.readString();
        status = in.readString();
        deletedAt = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        message = in.readString();
        url = in.readString();
        read = in.readString();
        imagepath = in.readString();
        notificationTitle = in.readString();
        notificationBody = in.readString();
        profile = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRead() {
        return read;
    }

    public void setRead(String read) {
        this.read = read;
    }

    public User getProfile() {
        return profile;
    }

    public void setProfile(User profile) {
        this.profile = profile;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    public String getNotificationBody() {
        return notificationBody;
    }

    public void setNotificationBody(String notificationBody) {
        this.notificationBody = notificationBody;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(receiverId);
        parcel.writeString(senderId);
        parcel.writeString(type);
        parcel.writeString(typeId);
        parcel.writeString(status);
        parcel.writeString(deletedAt);
        parcel.writeString(createdAt);
        parcel.writeString(updatedAt);
        parcel.writeString(message);
        parcel.writeString(url);
        parcel.writeString(read);
        parcel.writeString(imagepath);
        parcel.writeString(notificationTitle);
        parcel.writeString(notificationBody);
        parcel.writeParcelable(profile, i);
    }
}
