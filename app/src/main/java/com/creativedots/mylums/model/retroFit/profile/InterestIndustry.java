package com.creativedots.mylums.model.retroFit.profile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InterestIndustry implements Serializable {
    @SerializedName("interests")
    @Expose
    private ArrayList<Interest> interests = new ArrayList<Interest>();

    @SerializedName("industry")
    @Expose
    private ArrayList<Industry> industry = new ArrayList<Industry>();

    private final static long serialVersionUID = 7776369024488599326L;

    public ArrayList<Interest> getInterests() {
        return interests;
    }

    public void setInterests(ArrayList<Interest> interests) {
        this.interests = interests;
    }

    public ArrayList<Industry> getIndustry() {
        return industry;
    }

    public void setIndustry(ArrayList<Industry> industry) {
        this.industry = industry;
    }
}