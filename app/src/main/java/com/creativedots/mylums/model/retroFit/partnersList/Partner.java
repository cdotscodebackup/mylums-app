package com.creativedots.mylums.model.retroFit.partnersList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Partner {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("concerned_person")
    @Expose
    private String concernedPerson;
    @SerializedName("contact_detail")
    @Expose
    private String contactDetail;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("premium")
    @Expose
    private String premium;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("imagepath")
    @Expose
    private String imagepath;
    @SerializedName("partnerspath")
    @Expose
    private String partnerspath;
    @SerializedName("totaloffers")
    @Expose
    private String totalOffers;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("partner_type")
    @Expose
    private String partnerType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getConcernedPerson() {
        return concernedPerson;
    }

    public void setConcernedPerson(String concernedPerson) {
        this.concernedPerson = concernedPerson;
    }

    public String getContactDetail() {
        return contactDetail;
    }

    public void setContactDetail(String contactDetail) {
        this.contactDetail = contactDetail;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getPartnerspath() {
        return partnerspath;
    }

    public void setPartnerspath(String partnerspath) {
        this.partnerspath = partnerspath;
    }

    public String getTotalOffers() {
        return totalOffers;
    }

    public void setTotalOffers(String totalOffers) {
        this.totalOffers = totalOffers;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("description", description).append("image", image).append("concernedPerson", concernedPerson).append("contactDetail", contactDetail).append("email", email).append("premium", premium).append("deletedAt", deletedAt).append("createdAt", createdAt).append("updatedAt", updatedAt).append("imagepath", imagepath).append("partnerspath", partnerspath).toString();
    }

}
