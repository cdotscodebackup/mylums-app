package com.creativedots.mylums.model.retroFit.partnersList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;

public class PartnersListResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<Partner> data = new ArrayList<>();
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("pages")
    @Expose
    private Integer pages;
    @SerializedName("message")
    @Expose
    private String message;

    public ArrayList<Partner> getData() {
        return data;
    }

    public void setData(ArrayList<Partner> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("data", data).append("status", status).append("total", total).append("pages", pages).append("message", message).toString();
    }

}
