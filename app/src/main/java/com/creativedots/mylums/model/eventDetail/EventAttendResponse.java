package com.creativedots.mylums.model.eventDetail;

/**
 * Created by: Husnain Ali on 11/16/2018.
 * Company: Rapidzz Solutions
 * Email: husnain.ali02@gmail.com
 */
import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventAttendResponse implements Serializable {


    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}