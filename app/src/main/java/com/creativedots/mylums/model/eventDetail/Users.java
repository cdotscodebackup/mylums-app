
package com.creativedots.mylums.model.eventDetail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "going",
    "interested",
    "not-interested",
    "interested-total",
    "not-interested-total",
    "going-total",
    "checkedin",
    "checkedin-total"
})
public class Users {

    @JsonProperty("going")
    private ArrayList<InterestType> going = new ArrayList<InterestType>();
    @JsonProperty("interested")
    private ArrayList<InterestType> interested = new ArrayList<InterestType>();
    @SerializedName("not-interested")
    private ArrayList<InterestType> notInterested = new ArrayList<InterestType>();
    @SerializedName("interested-total")
    private Integer interestedTotal;
    @SerializedName("not-interested-total")
    private Integer notInterestedTotal;
    @SerializedName("going-total")
    private Integer goingTotal;
    @JsonProperty("checkedin")
    private ArrayList<InterestType> checkedin = null;
    @SerializedName("checkedin-total")
    private Integer checkedinTotal;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("going")
    public ArrayList<InterestType> getGoing() {
        return going;
    }

    @JsonProperty("going")
    public void setGoing(ArrayList<InterestType> going) {
        this.going = going;
    }

    @JsonProperty("interested")
    public ArrayList<InterestType> getInterested() {
        return interested;
    }

    @JsonProperty("interested")
    public void setInterested(ArrayList<InterestType> interested) {
        this.interested = interested;
    }

    @JsonProperty("not-interested")
    public ArrayList<InterestType> getNotInterested() {
        return notInterested;
    }

    @JsonProperty("not-interested")
    public void setNotInterested(ArrayList<InterestType> notInterested) {
        this.notInterested = notInterested;
    }

    @JsonProperty("interested-total")
    public Integer getInterestedTotal() {
        return interestedTotal;
    }

    @JsonProperty("interested-total")
    public void setInterestedTotal(Integer interestedTotal) {
        this.interestedTotal = interestedTotal;
    }

    @JsonProperty("not-interested-total")
    public Integer getNotInterestedTotal() {
        return notInterestedTotal;
    }

    @JsonProperty("not-interested-total")
    public void setNotInterestedTotal(Integer notInterestedTotal) {
        this.notInterestedTotal = notInterestedTotal;
    }

    @JsonProperty("going-total")
    public Integer getGoingTotal() {
        return goingTotal;
    }

    @JsonProperty("going-total")
    public void setGoingTotal(Integer goingTotal) {
        this.goingTotal = goingTotal;
    }

    @JsonProperty("checkedin")
    public ArrayList<InterestType> getCheckedin() {
        return checkedin;
    }

    @JsonProperty("checkedin")
    public void setCheckedin(ArrayList<InterestType> checkedin) {
        this.checkedin = checkedin;
    }

    @JsonProperty("checkedin-total")
    public Integer getCheckedinTotal() {
        return checkedinTotal;
    }

    @JsonProperty("checkedin-total")
    public void setCheckedinTotal(Integer checkedinTotal) {
        this.checkedinTotal = checkedinTotal;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
