
package com.creativedots.mylums.model.retroFit.profile;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Proload implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("roll_number")
    @Expose
    private String rollNumber;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("program")
    @Expose
    private String program;
    @SerializedName("major")
    @Expose
    private String major;
    @SerializedName("minor")
    @Expose
    private String minor;
    @SerializedName("graduation_year")
    @Expose
    private String graduationYear;
    @SerializedName("personal_email")
    @Expose
    private String personalEmail;
    @SerializedName("official_email")
    @Expose
    private String officialEmail;
    @SerializedName("campus_email")
    @Expose
    private String campusEmail;
    @SerializedName("alternate_email")
    @Expose
    private String alternateEmail;
    @SerializedName("landline")
    @Expose
    private String landline;
    @SerializedName("alternate_landline")
    @Expose
    private String alternateLandline;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("alternate_mobile")
    @Expose
    private String alternateMobile;
    @SerializedName("current_address")
    @Expose
    private String currentAddress;
    @SerializedName("current_city")
    @Expose
    private String currentCity;
    @SerializedName("current_state")
    @Expose
    private String currentState;
    @SerializedName("current_country")
    @Expose
    private String currentCountry;
    @SerializedName("permanent_address")
    @Expose
    private String permanentAddress;
    @SerializedName("permanent_city")
    @Expose
    private String permanentCity;
    @SerializedName("permanent_state")
    @Expose
    private String permanentState;
    @SerializedName("permanent_country")
    @Expose
    private String permanentCountry;
    @SerializedName("business_address")
    @Expose
    private String businessAddress;
    @SerializedName("business_city")
    @Expose
    private String businessCity;
    @SerializedName("business_state")
    @Expose
    private String businessState;
    @SerializedName("business_country")
    @Expose
    private String businessCountry;
    @SerializedName("current_experience_company")
    @Expose
    private String currentExperienceCompany;
    @SerializedName("current_experience_designation")
    @Expose
    private String currentExperienceDesignation;
    @SerializedName("current_experience_department")
    @Expose
    private String currentExperienceDepartment;
    @SerializedName("current_experience_industry")
    @Expose
    private String currentExperienceIndustry;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    private final static long serialVersionUID = 8636257397004076326L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getMinor() {
        return minor;
    }

    public void setMinor(String minor) {
        this.minor = minor;
    }

    public String getGraduationYear() {
        return graduationYear;
    }

    public void setGraduationYear(String graduationYear) {
        this.graduationYear = graduationYear;
    }

    public String getPersonalEmail() {
        return personalEmail;
    }

    public void setPersonalEmail(String personalEmail) {
        this.personalEmail = personalEmail;
    }

    public String getOfficialEmail() {
        return officialEmail;
    }

    public void setOfficialEmail(String officialEmail) {
        this.officialEmail = officialEmail;
    }

    public String getCampusEmail() {
        return campusEmail;
    }

    public void setCampusEmail(String campusEmail) {
        this.campusEmail = campusEmail;
    }

    public String getAlternateEmail() {
        return alternateEmail;
    }

    public void setAlternateEmail(String alternateEmail) {
        this.alternateEmail = alternateEmail;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getAlternateLandline() {
        return alternateLandline;
    }

    public void setAlternateLandline(String alternateLandline) {
        this.alternateLandline = alternateLandline;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAlternateMobile() {
        return alternateMobile;
    }

    public void setAlternateMobile(String alternateMobile) {
        this.alternateMobile = alternateMobile;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }

    public String getCurrentCity() {
        return currentCity;
    }

    public void setCurrentCity(String currentCity) {
        this.currentCity = currentCity;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public String getCurrentCountry() {
        return currentCountry;
    }

    public void setCurrentCountry(String currentCountry) {
        this.currentCountry = currentCountry;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getPermanentCity() {
        return permanentCity;
    }

    public void setPermanentCity(String permanentCity) {
        this.permanentCity = permanentCity;
    }

    public String getPermanentState() {
        return permanentState;
    }

    public void setPermanentState(String permanentState) {
        this.permanentState = permanentState;
    }

    public String getPermanentCountry() {
        return permanentCountry;
    }

    public void setPermanentCountry(String permanentCountry) {
        this.permanentCountry = permanentCountry;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessCity() {
        return businessCity;
    }

    public void setBusinessCity(String businessCity) {
        this.businessCity = businessCity;
    }

    public String getBusinessState() {
        return businessState;
    }

    public void setBusinessState(String businessState) {
        this.businessState = businessState;
    }

    public String getBusinessCountry() {
        return businessCountry;
    }

    public void setBusinessCountry(String businessCountry) {
        this.businessCountry = businessCountry;
    }

    public String getCurrentExperienceCompany() {
        return currentExperienceCompany;
    }

    public void setCurrentExperienceCompany(String currentExperienceCompany) {
        this.currentExperienceCompany = currentExperienceCompany;
    }

    public String getCurrentExperienceDesignation() {
        return currentExperienceDesignation;
    }

    public void setCurrentExperienceDesignation(String currentExperienceDesignation) {
        this.currentExperienceDesignation = currentExperienceDesignation;
    }

    public String getCurrentExperienceDepartment() {
        return currentExperienceDepartment;
    }

    public void setCurrentExperienceDepartment(String currentExperienceDepartment) {
        this.currentExperienceDepartment = currentExperienceDepartment;
    }

    public String getCurrentExperienceIndustry() {
        return currentExperienceIndustry;
    }

    public void setCurrentExperienceIndustry(String currentExperienceIndustry) {
        this.currentExperienceIndustry = currentExperienceIndustry;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
