package com.creativedots.mylums.model.retroFit.Message;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MessageChannelObject implements Parcelable {

    @SerializedName("read")
    @Expose
    private ArrayList<MessageChannel> read;

    @SerializedName("unread")
    @Expose
    private ArrayList<MessageChannel> unread;

    @SerializedName("archive")
    @Expose
    private ArrayList<MessageChannel> archive;

    public MessageChannelObject() {}

    public ArrayList<MessageChannel> getRead() {
        return read;
    }

    public void setRead(ArrayList<MessageChannel> read) {
        this.read = read;
    }

    public ArrayList<MessageChannel> getUnread() {
        return unread;
    }

    public void setUnread(ArrayList<MessageChannel> unread) {
        this.unread = unread;
    }

    public ArrayList<MessageChannel> getArchive() {
        return archive;
    }

    public void setArchive(ArrayList<MessageChannel> archive) {
        this.archive = archive;
    }

    protected MessageChannelObject(Parcel in) {
        read = in.createTypedArrayList(MessageChannel.CREATOR);
        unread = in.createTypedArrayList(MessageChannel.CREATOR);
        archive = in.createTypedArrayList(MessageChannel.CREATOR);
    }

    public static final Creator<MessageChannelObject> CREATOR = new Creator<MessageChannelObject>() {
        @Override
        public MessageChannelObject createFromParcel(Parcel in) {
            return new MessageChannelObject(in);
        }

        @Override
        public MessageChannelObject[] newArray(int size) {
            return new MessageChannelObject[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(read);
        parcel.writeTypedList(unread);
        parcel.writeTypedList(archive);
    }
}
