package com.creativedots.mylums.model.retroFit.Message;

import android.os.Parcel;
import android.os.Parcelable;

import com.creativedots.mylums.model.retroFit.profile.User;
import com.creativedots.mylums.utils.DateConversion;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;

public class MessageChannel implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("receiver_id")
    @Expose
    private String receiverId;

    @SerializedName("sender_id")
    @Expose
    private String senderId;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("archive")
    @Expose
    private String archive;

    @SerializedName("allmessages")
    @Expose
    private String allMessages;

    @SerializedName("latest_message")
    @Expose
    private String latestMessage;

    @SerializedName("created_at")
    @Expose
    private String createdAt;

    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    @SerializedName("sender")
    @Expose
    private String sender;

    @SerializedName("profile")
    @Expose
    private User profile;

    public static final Creator<MessageChannel> CREATOR = new Creator<MessageChannel>() {
        @Override
        public MessageChannel createFromParcel(Parcel in) {
            return new MessageChannel(in);
        }

        @Override
        public MessageChannel[] newArray(int size) {
            return new MessageChannel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getArchive() {
        return archive;
    }

    public void setArchive(String archive) {
        this.archive = archive;
    }

    public String getAllMessages() {
        return allMessages;
    }

    public void setAllMessages(String allMessages) {
        this.allMessages = allMessages;
    }

    public String getLatestMessage() {
        return latestMessage;
    }

    public void setLatestMessage(String latestMessage) {
        this.latestMessage = latestMessage;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public User getProfile() {
        return profile;
    }

    public void setProfile(User profile) {
        this.profile = profile;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public MessageChannel(){

    }

    protected MessageChannel(Parcel in) {
        id = in.readString();
        receiverId = in.readString();
        senderId = in.readString();
        status = in.readString();
        archive = in.readString();
        allMessages = in.readString();
        latestMessage = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        sender = in.readString();
       // profile = in.readValue();
        profile = (User) in.readParcelable(User.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(id);
        parcel.writeString(receiverId);
        parcel.writeString(senderId);
        parcel.writeString(status);
        parcel.writeString(archive);
        parcel.writeString(allMessages);
        parcel.writeString(latestMessage);
        parcel.writeString(createdAt);
        parcel.writeString(updatedAt);
        parcel.writeString(sender);
        parcel.writeParcelable(profile,i);

    }

    public long getTimeInMili(){
        try {
            return DateConversion.sDATE_AND_TIME_FORMAT.parse(updatedAt).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
