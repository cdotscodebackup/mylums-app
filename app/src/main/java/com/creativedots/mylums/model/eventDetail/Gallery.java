
package com.creativedots.mylums.model.eventDetail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "photos",
        "videos",
        "photo-total",
        "video-total"
})
public class Gallery {

    @SerializedName("videos")
    private ArrayList<GalleryProduct> videoList;
    @SerializedName("photos")
    private ArrayList<GalleryProduct> photosList;
    @SerializedName("photo-total")
    private String photoTotal;
    @SerializedName("video-total")
    private String videoTotal;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("videos")
    public ArrayList<GalleryProduct> getVideoList() {
        return videoList;
    }

    @JsonProperty("videos")
    public void setVideoList(ArrayList<GalleryProduct> videoList) {
        this.videoList = videoList;
    }

    @JsonProperty("photos")
    public ArrayList<GalleryProduct> getPhotosList() {
        return photosList;
    }

    @JsonProperty("photos")
    public void setPhotosList(ArrayList<GalleryProduct> photosList) {
        this.photosList = photosList;
    }

    public String getPhotoTotal() {
        return photoTotal;
    }

    public void setPhotoTotal(String photoTotal) {
        this.photoTotal = photoTotal;
    }

    public String getVideoTotal() {
        return videoTotal;
    }

    public void setVideoTotal(String videoTotal) {
        this.videoTotal = videoTotal;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
