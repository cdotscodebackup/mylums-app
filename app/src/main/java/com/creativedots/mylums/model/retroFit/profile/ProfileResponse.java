
package com.creativedots.mylums.model.retroFit.profile;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileResponse implements Serializable
{

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("homescreen")
    @Expose
    private String homescreen;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("status")
    @Expose
    private String status;
    private final static long serialVersionUID = 4941166868892910123L;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getHomescreen() {
        return homescreen;
    }

    public void setHomescreen(String homescreen) {
        this.homescreen = homescreen;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
