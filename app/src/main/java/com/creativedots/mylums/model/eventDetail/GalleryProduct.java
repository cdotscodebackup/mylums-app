
package com.creativedots.mylums.model.eventDetail;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "event_id",
        "path",
        "type",
        "imagepath",
        "videopath"
})
public class GalleryProduct implements Parcelable {


    @SerializedName("id")
    private String id;
    @SerializedName("event_id")
    private String eventId;
    @SerializedName("path")
    private String path;
    @SerializedName("type")
    private String type;
    @SerializedName("imagepath")
    private String imagePath;
    @SerializedName("videopath")
    private String videoPath;

    protected GalleryProduct(Parcel in) {
        id = in.readString();
        eventId = in.readString();
        path = in.readString();
        type = in.readString();
        imagePath = in.readString();
        videoPath = in.readString();
    }

    public static final Creator<GalleryProduct> CREATOR = new Creator<GalleryProduct>() {
        @Override
        public GalleryProduct createFromParcel(Parcel in) {
            return new GalleryProduct(in);
        }

        @Override
        public GalleryProduct[] newArray(int size) {
            return new GalleryProduct[size];
        }
    };

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("event_id")
    public String getEventId() {
        return eventId;
    }

    @JsonProperty("event_id")
    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    @JsonProperty("path")
    public String getPath() {
        return path;
    }

    @JsonProperty("path")
    public void setPath(String path) {
        this.path = path;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("imagepath")
    public String getImagePath() {
        return imagePath;
    }

    @JsonProperty("imagepath")
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @JsonProperty("videopath")
    public String getVideoPath() {
        return videoPath;
    }

    @JsonProperty("videopath")
    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(eventId);
        parcel.writeString(path);
        parcel.writeString(type);
        parcel.writeString(imagePath);
        parcel.writeString(videoPath);
    }
}
