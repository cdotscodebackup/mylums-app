package com.creativedots.mylums.model.retroFit.profile;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InterestIndustryResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private InterestIndustry data;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    private final static long serialVersionUID = -8609319493699685662L;

    public InterestIndustry getData() {
        return data;
    }

    public void setData(InterestIndustry data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}