
package com.creativedots.mylums.model.retroFit.profile;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Setting implements Serializable
{

    @SerializedName("mute")
    @Expose
    private String mute;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("offerradius")
    @Expose
    private String offerradius;
    private final static long serialVersionUID = 2722059817432975350L;

    public String getMute() {
        return mute;
    }

    public void setMute(String mute) {
        this.mute = mute;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOfferradius() {
        return offerradius;
    }

    public void setOfferradius(String offerradius) {
        this.offerradius = offerradius;
    }

}
