
package com.creativedots.mylums.model.retroFit.profile;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Linkedin implements Serializable
{

    @SerializedName("userinfo")
    @Expose
    private Object userinfo;
    private final static long serialVersionUID = 4053166965917454384L;

    public Object getUserinfo() {
        return userinfo;
    }

    public void setUserinfo(Object userinfo) {
        this.userinfo = userinfo;
    }

}
