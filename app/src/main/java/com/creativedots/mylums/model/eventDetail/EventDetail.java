
package com.creativedots.mylums.model.eventDetail;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "description",
    "dates",
    "start_time",
    "end_time",
    "full_time",
    "venue",
    "geo_location",
    "photo",
    "status",
    "lat",
    "lon",
    "deleted_at",
    "created_at",
    "updated_at",
    "member",
    "imagepath",
    "gallery",
    "interests",
    "users"
})
public class EventDetail {

    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("dates")
    private String dates;
    @SerializedName("start_time")
    private String startTime;
    @SerializedName("end_time")
    private String endTime;
    @SerializedName("full_time")
    private String fullTime;
    @SerializedName("venue")
    private String venue;
    @SerializedName("geo_location")
    private String geoLocation;
    @SerializedName("photo")
    private String photo;
    @SerializedName("status")
    private String status;
    @SerializedName("mystatus")
    private String mystatus;
    @SerializedName("mycheckin")
    private String mycheckin;
    @SerializedName("date_time")
    private String date_time;
    @SerializedName("lat")
    private String lat;
    @SerializedName("lon")
    private String lon;
    @SerializedName("deleted_at")
    private String deletedAt;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("member")
    private String member;
    @SerializedName("imagepath")
    private String imagepath;
    @SerializedName("gallery")
    private Gallery gallery;
    @SerializedName("interests")
    private List<Interest> interests = null;
    @SerializedName("users")
    private Users users;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("dates")
    public String getDates() {
        return dates;
    }

    @JsonProperty("dates")
    public void setDates(String dates) {
        this.dates = dates;
    }

    @JsonProperty("start_time")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("start_time")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @JsonProperty("end_time")
    public String getEndTime() {
        return endTime;
    }

    @JsonProperty("end_time")
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @JsonProperty("full_time")
    public String getFullTime() {
        return fullTime;
    }

    @JsonProperty("full_time")
    public void setFullTime(String fullTime) {
        this.fullTime = fullTime;
    }

    @JsonProperty("venue")
    public String getVenue() {
        return venue;
    }

    @JsonProperty("venue")
    public void setVenue(String venue) {
        this.venue = venue;
    }

    @JsonProperty("geo_location")
    public String getGeoLocation() {
        return geoLocation;
    }

    @JsonProperty("geo_location")
    public void setGeoLocation(String geoLocation) {
        this.geoLocation = geoLocation;
    }

    @JsonProperty("photo")
    public String getPhoto() {
        return photo;
    }

    @JsonProperty("photo")
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    @JsonProperty("lon")
    public String getLon() {
        return lon;
    }

    @JsonProperty("lon")
    public void setLon(String lon) {
        this.lon = lon;
    }

    @JsonProperty("deleted_at")
    public String getDeletedAt() {
        return deletedAt;
    }

    @JsonProperty("deleted_at")
    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonProperty("member")
    public String getMember() {
        return member;
    }

    @JsonProperty("member")
    public void setMember(String member) {
        this.member = member;
    }

    @JsonProperty("imagepath")
    public String getImagepath() {
        return imagepath;
    }

    @JsonProperty("imagepath")
    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    @JsonProperty("gallery")
    public Gallery getGallery() {
        return gallery;
    }

    @JsonProperty("gallery")
    public void setGallery(Gallery gallery) {
        this.gallery = gallery;
    }

    @JsonProperty("interests")
    public List<Interest> getInterests() {
        return interests;
    }

    @JsonProperty("interests")
    public void setInterests(List<Interest> interests) {
        this.interests = interests;
    }

    @JsonProperty("users")
    public Users getUsers() {
        return users;
    }

    @JsonProperty("users")
    public void setUsers(Users users) {
        this.users = users;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getMystatus() {
        return mystatus;
    }

    public void setMystatus(String mystatus) {
        this.mystatus = mystatus;
    }

    public String getMycheckin() {
        return mycheckin;
    }

    public void setMycheckin(String mycheckin) {
        this.mycheckin = mycheckin;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public boolean isActive(){
        if(status != null && status.equalsIgnoreCase("active")){
            return true;
        }else {
            return false;
        }
    }


    public boolean isAlreadyCheckIn() {
         if(mycheckin != null && mycheckin.equalsIgnoreCase("in")){
            return true;
         }else {
             return false;
         }
    }
}
