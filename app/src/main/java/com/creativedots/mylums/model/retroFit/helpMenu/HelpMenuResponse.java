package com.creativedots.mylums.model.retroFit.helpMenu;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class HelpMenuResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<MenuItem> data = new ArrayList<>();

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("content")
    @Expose
    private HelpContent content;

    public ArrayList<MenuItem> getData() {
        return data;
    }

    public void setData(ArrayList<MenuItem> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HelpContent getContent() {
        return content;
    }

    public void setContent(HelpContent content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("data", data).append("status", status).append("message", message).toString();
    }

    public class HelpContent{
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("content")
        @Expose
        private String content;

        public HelpContent(){

        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }


}


