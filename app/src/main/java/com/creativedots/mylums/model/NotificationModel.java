package com.creativedots.mylums.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.creativedots.mylums.model.notification.Message;
import com.creativedots.mylums.model.retroFit.profile.User;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class NotificationModel implements Parcelable {


    private String typeId;
    private String notificatoinId;
    private String type;
    private User profile;
    private String body;
    private Message message;

    public NotificationModel(){

    }

    protected NotificationModel(Parcel in) {
        typeId = in.readString();
        notificatoinId = in.readString();
        type = in.readString();
        profile = in.readParcelable(User.class.getClassLoader());
        body = in.readString();
        message = in.readParcelable(Message.class.getClassLoader());
    }

    public static final Creator<NotificationModel> CREATOR = new Creator<NotificationModel>() {
        @Override
        public NotificationModel createFromParcel(Parcel in) {
            return new NotificationModel(in);
        }

        @Override
        public NotificationModel[] newArray(int size) {
            return new NotificationModel[size];
        }
    };

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User getProfile() {
        return profile;
    }

    public void setProfile(User profile) {
        this.profile = profile;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getNotificatoinId() {
        return notificatoinId;
    }

    public void setNotificatoinId(String notificatoinId) {
        this.notificatoinId = notificatoinId;
    }

    public NotificationModel convertToNotification(Map<String, String> map) {

        NotificationModel notificationModel = new NotificationModel();
        notificationModel.setTypeId((String) getValueFromMap(map, "type_id"));
        notificationModel.setType((String) getValueFromMap(map, "type"));
        notificationModel.setNotificatoinId((String) getValueFromMap(map, "notification_id"));
        notificationModel.setProfile((NotificationModel.fromJson((String) getValueFromMap(map, "user"))));
        notificationModel.setMessage((Message) ConvertJsonToMessage((String) getValueFromMap(map, "message")));

        return notificationModel;
    }


    public NotificationModel convertToNotification(Bundle map) {

        NotificationModel notificationModel = new NotificationModel();
        notificationModel.setTypeId((String) getValueFromBundle(map, "type_id"));
        notificationModel.setType((String) getValueFromBundle(map, "type"));
        notificationModel.setNotificatoinId((String) getValueFromBundle(map, "notification_id"));
        notificationModel.setProfile((NotificationModel.fromJson((String) getValueFromBundle(map, "user"))));
        notificationModel.setMessage((Message) ConvertJsonToMessage((String) getValueFromBundle(map, "message")));

        return notificationModel;
    }

    public static User fromJson(String s) {
        return new Gson().fromJson(s, User.class);
    }
    private Object getValueFromMap(Map<String, String> map, String key) {


        if(map.containsKey(key)){
            return map.get(key);
        }
        return "";
    }

    private Object getValueFromBundle(Bundle map, String key) {


        if(map.containsKey(key)){
            return map.get(key);
        }
        return "";
    }


    private Object getValueFromJson(JSONObject jsonObject, String key) {
        if(jsonObject.has(key)){
            try {
                return jsonObject.getString(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    private Message ConvertJsonToMessage(String json) {
        Message message = new Message();
        try {

            JSONObject object = new JSONObject(json);
            message.setBody((String) getValueFromJson(object, "body"));
            message.setTitle((String) getValueFromJson(object, "title"));
            return message;

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        return message;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(typeId);
        parcel.writeString(notificatoinId);
        parcel.writeString(type);
        parcel.writeParcelable(profile, i);
        parcel.writeString(body);
        parcel.writeParcelable(message, i);
    }
}
