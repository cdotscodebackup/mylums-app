package com.creativedots.mylums.model.notification;

import android.os.Parcel;
import android.os.Parcelable;

public class Message implements Parcelable {
    private String title;
    private String body;

    protected Message(Parcel in) {
        title = in.readString();
        body = in.readString();
    }

    public Message(){

    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(body);
    }
}