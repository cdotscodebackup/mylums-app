
package com.creativedots.mylums.model.retroFit.profile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

public class WorkExperience implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id = "";
    @SerializedName("user_id")
    @Expose
    private String userId = "";
    @SerializedName("company_name")
    @Expose
    private String companyName = "";
    @SerializedName("industry_id")
    @Expose
    private String industryId = "";
    @SerializedName("designation")
    @Expose
    private String designation = "";
    @SerializedName("from_date")
    @Expose
    private String fromDate = "";
    @SerializedName("to_date")
    @Expose
    private String toDate = "";
    @SerializedName("is_current_job")
    @Expose
    private String isCurrentJob = "no";
    @SerializedName("city")
    @Expose
    private String city = "";
    @SerializedName("location")
    @Expose
    private String location = "";
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("state_province")
    @Expose
    private String stateProvince;
    @SerializedName("state")
    @Expose
    private String state = "";
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("name")
    @Expose
    private String name;
    private final static long serialVersionUID = 7277734146082523556L;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getIndustryId() {
        return industryId;
    }

    public void setIndustryId(String industryId) {
        this.industryId = industryId;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getIsCurrentJob() {
        return isCurrentJob;
    }

    public void setIsCurrentJob(String isCurrentJob) {
        this.isCurrentJob = isCurrentJob;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String objectToHashMapList(ArrayList<WorkExperience> workExperience) {

        ArrayList<String> workexp = new ArrayList<>();

        for (int i = 0; i < workExperience.size(); i++) {
            HashMap<String,String> hashMap = new HashMap<>();
            hashMap.put("company_name",workExperience.get(i).companyName);
            hashMap.put("industry",workExperience.get(i).industryId);
            hashMap.put("desingation",workExperience.get(i).designation);
            hashMap.put("from_date",workExperience.get(i).fromDate);
            hashMap.put("to_date",workExperience.get(i).toDate);
            hashMap.put("city",workExperience.get(i).city);
            hashMap.put("location",workExperience.get(i).location);
            hashMap.put("state",workExperience.get(i).state);
            hashMap.put("state_province",workExperience.get(i).stateProvince);
            hashMap.put("country",workExperience.get(i).country);
            hashMap.put("is_current_job",workExperience.get(i).isCurrentJob);
            workexp.add(new JSONObject(hashMap).toString());
        }
        return workexp.toString();

    }

}
