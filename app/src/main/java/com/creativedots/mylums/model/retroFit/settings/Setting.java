package com.creativedots.mylums.model.retroFit.settings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Setting {

    @SerializedName("mute")
    @Expose
    private String mute = "";
    @SerializedName("location")
    @Expose
    private String location = "";
    @SerializedName("offerradius")
    @Expose
    private String offerradius = "";

    public String getMute() {
        return mute;
    }

    public void setMute(String mute) {
        this.mute = mute;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOfferradius() {
        return offerradius;
    }

    public void setOfferradius(String offerradius) {
        this.offerradius = offerradius;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("mute", mute).append("location", location).append("offerradius", offerradius).toString();
    }

}
