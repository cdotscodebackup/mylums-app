package com.creativedots.mylums.model.eventDetail;

import java.util.ArrayList;
import java.util.List;

import com.creativedots.mylums.model.retroFit.partnersDetails.Datum;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class EventDetailResponse {

    @SerializedName("data")
    @Expose
    private EventDetail mEventDetail = new EventDetail();

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private String message;

    public EventDetail getData() {
        return mEventDetail;
    }

    public void setData(EventDetail data) {
        this.mEventDetail = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("data", mEventDetail).append("status", status).append("message", message).toString();
    }

}
