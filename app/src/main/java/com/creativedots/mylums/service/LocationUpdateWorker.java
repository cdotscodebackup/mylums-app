package com.creativedots.mylums.service;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.creativedots.mylums.utils.UploadSystemParamsTask;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.concurrent.TimeUnit;

import androidx.work.Worker;


public class LocationUpdateWorker extends Worker implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    Location mLocation;

    Context mContext;

    @Override
    public Result doWork() {
        try {

            boolean gpsStatus = ((LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER);
            mContext = getApplicationContext();
            if ((ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)) != PackageManager.PERMISSION_GRANTED || !gpsStatus)
                new UploadSystemParamsTask(getApplicationContext());
            else
                connectWithClient();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.SUCCESS;
    }

    @SuppressLint("RestrictedApi")
    private void connectWithClient() throws Exception {
        stopClient();
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(TimeUnit.MINUTES.toMillis(10));
        mLocationRequest.setFastestInterval(TimeUnit.MINUTES.toMillis(10)); //5 secs
        mLocationRequest.setSmallestDisplacement(100);
        int priority = LocationRequest.PRIORITY_HIGH_ACCURACY; //by default
        mLocationRequest.setPriority(priority);
        mGoogleApiClient.connect();
    }

    /*
     * LOCATION CALLBACKS
     */
    @Override
    public void onConnected(Bundle dataBundle) {
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(LocationUpdateWorker.class.getSimpleName(), "== Error On onConnected() Permission not granted");
            return;
        }
        try {

            if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(LocationUpdateWorker.class.getSimpleName(), "Connected to Google API");
    }

    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onConnectionSuspended(int i) {
        Log.d(LocationUpdateWorker.class.getSimpleName(), "Connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(LocationUpdateWorker.class.getSimpleName(), "Failed to connect to Google API");
    }

    //to get the location change
    @Override
    public void onLocationChanged(Location location) {
        Log.d(LocationUpdateWorker.class.getSimpleName(), "Location changed");

        //   if(location != null){


        mLocation = location;
        stopClient();
        if (location != null) {
            SessionManager sessionManager = SessionManager.from(mContext);
            Location location1 = new Location("");
            location1.setLatitude(Double.parseDouble(sessionManager.getLatOrLong(SessionManager.LATITUDE)));
            location1.setLongitude(Double.parseDouble(sessionManager.getLatOrLong(SessionManager.LONGITUDE)));

            Log.e("DISTANCE", "" + location.distanceTo(location1));
            if ((location.distanceTo(location1) / 1000) > 3) {
                new UploadSystemParamsTask(mContext, location);
                sessionManager.setLatOrLong(SessionManager.LATITUDE, location.getLatitude() + "");
                sessionManager.setLatOrLong(SessionManager.LONGITUDE, location.getLongitude() + "");

            }
        }


    }


    private void stopClient() {

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
        if (mLocationRequest != null) {
            mLocationRequest = null;
        }
    }
}
