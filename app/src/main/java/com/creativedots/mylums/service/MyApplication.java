package com.creativedots.mylums.service;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.widget.Toast;

import com.creativedots.mylums.model.retroFit.helpMenu.HelpMenuResponse;
import com.creativedots.mylums.network.ApiClient;
import com.creativedots.mylums.network.ApiInterface;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyApplication extends MultiDexApplication {
    public static final String TAG = MyApplication.class.getSimpleName();

    private static MyApplication ourInstance = new MyApplication();

    static MyApplication getInstance() {
        return ourInstance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public MyApplication() {
        ourInstance = this;
    }

    protected ApiInterface apiService;

    @Override
    public void onCreate() {
        super.onCreate();

        apiService = ApiClient.getRetrofitClient(SessionManager.from(ourInstance).getToken()).create(ApiInterface.class);
        apiService.helpMenu().enqueue(new Callback<HelpMenuResponse>() {
            @Override
            public void onResponse(@NonNull Call<HelpMenuResponse> call, @NonNull Response<HelpMenuResponse> response) {

                if(response.isSuccessful()){

                    String jsonHelp = new Gson().toJson(response.body().getData());
                    SessionManager.from(ourInstance).saveHelpMenu(jsonHelp);
                    Log.e(TAG, "Help-Menu Preloaded!");
                }

            }

            @Override
            public void onFailure(@NonNull Call<HelpMenuResponse> call, @NonNull Throwable t) {
                t.printStackTrace();

            }
        });

    }
}
