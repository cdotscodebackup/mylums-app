package com.creativedots.mylums.service;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LinkedinController implements AuthListener {

    Context context;
    // linkedin JSON key
    public static final String KEY_LINKED_IN_STATUS_CODE ="StatusCode";
    public static final String KEY_LINKED_IN_RESPONSE_DATA="responseData";
    public static final String KEY_LINKED_IN_EMAIL_ID ="emailAddress";
    public static final String KEY_LINKED_IN_FIRST_NAME ="firstName";
    public static final String KEY_LINKED_IN_LAST_NAME ="lastName";
    public static final String KEY_LINKED_IN_PROFILE_URL="pictureUrl";
    public static final String KEY_LINKED_IN_LOCATION ="location";
    private String email="",urlPath="",firstname="",lastname="",location_name="";
    private String linkedInAccessToken ="";
    private static final String host = "api.linkedin.com";
    private static final String topCardUrl = "https://" + host + "/v1/people/~:(first-name,last-name,picture-url,email-address,location)";
    public LinkedinController(Context con)
    {
        context = con;
        loginWithLinkedIn();
    }

    public void loginWithLinkedIn()
    {
        LISessionManager.getInstance(context).init((Activity) context, buildScope(), this, true);
        printKeyHash((Activity) context);
    }


    @Override
    public void onAuthSuccess() {
        linkedInAccessToken = LISessionManager.getInstance(context).getSession().getAccessToken().toString();
        APIHelper apiHelper = APIHelper.getInstance(context);

        apiHelper.getRequest(context, topCardUrl, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse s) {
                Log.e(" server response comes", s.getResponseDataAsJson().toString());
                try {
                    JSONObject obj = s.getResponseDataAsJson();
                    if (obj.has(KEY_LINKED_IN_EMAIL_ID)) {
                        email = obj.optString(KEY_LINKED_IN_EMAIL_ID);
                    }
                    if (obj.has(KEY_LINKED_IN_PROFILE_URL)) {
                        urlPath = obj.optString(KEY_LINKED_IN_PROFILE_URL);
                    }
                    if (obj.has(KEY_LINKED_IN_FIRST_NAME)) {
                        firstname = obj.optString(KEY_LINKED_IN_FIRST_NAME);
                    }
                    if (obj.has(KEY_LINKED_IN_LAST_NAME)) {
                        lastname = obj.optString(KEY_LINKED_IN_LAST_NAME);
                    }
                    JSONObject location_obj=obj.optJSONObject(KEY_LINKED_IN_LOCATION);
                    if(location_obj!=null)
                    {
                        location_name=location_obj.optString("name");
                    }

                    Log.e("First", firstname);
                    Log.e("last", lastname);
                    Log.e("email",email);
                    Log.e("path",urlPath);
                 //   new JsonUserLinkedInLogin().execute(new String[]{firstname,lastname,email,urlPath,location_name});
                } catch (Exception e) {
                    //  Debug.printLog(" problem comes to parse json "+e);
                }
            }

            @Override
            public void onApiError(LIApiError error) {


            }
        });
    }

    @Override
    public void onAuthError(LIAuthError error) {
        Toast.makeText(context,"failed", Toast.LENGTH_LONG).show();
    }


    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE,Scope.R_EMAILADDRESS);
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        }
        catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }
}
