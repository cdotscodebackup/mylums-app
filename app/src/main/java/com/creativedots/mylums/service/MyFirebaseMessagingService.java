package com.creativedots.mylums.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.model.NotificationModel;
import com.creativedots.mylums.utils.Event;
import com.creativedots.mylums.utils.RxBus;
import com.creativedots.mylums.utils.sharedPreferences.Constants;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage message) {
        sendMyNotification(message);
    }


    private void sendMyNotification(RemoteMessage message) {

        Map<String,String> msg = message.getData();
        Log.e("LOGLOG",msg.toString());
        NotificationModel notificationModel = new NotificationModel();
        notificationModel = notificationModel.convertToNotification(msg);
        Event event = new Event();
        event.setEventType(Event.REFRESH_NOTIFICATION_LIST);
        RxBus.defaultInstance().send(event);


        //On click of notification it redirect to this Activity
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("notification",notificationModel);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Log.e("LOGLOG",message.getNotification().getTitle());
        String channelid = "001";
        String channelName = getString(R.string.app_name);
        Uri soundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,channelid)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(notificationModel.getMessage().getTitle())
                .setContentText(notificationModel.getMessage().getBody())
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(channelid, channelName, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationBuilder.setChannelId(channelid);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notificationManager.notify(0, notificationBuilder.build());
    }
}