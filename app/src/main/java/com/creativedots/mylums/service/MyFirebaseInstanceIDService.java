package com.creativedots.mylums.service;

import android.support.annotation.NonNull;
import android.util.Log;

import com.creativedots.mylums.apiConstants.Api;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.network.ApiClient;
import com.creativedots.mylums.network.ApiInterface;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;
import com.google.firebase.messaging.FirebaseMessagingService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;

public class MyFirebaseInstanceIDService extends FirebaseMessagingService {

    public static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        sendTokenToServer(s);
        Log.e(TAG, "FCM Token: " + s);
    }

    private void sendTokenToServer(String token) {
        SessionManager.from(getApplicationContext()).saveToken(token);
        ApiInterface apiService = ApiClient.getRetrofitClient(token).create(ApiInterface.class);
        Call<Response> call = apiService.sendToken(
                token, "android"
        );
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                if (response.isSuccessful()) {
                    SessionManager.from(getApplicationContext()).clear();
                    Response myResponse = response.body();
                    Log.e(TAG, "myResponse.getStatus(): " + myResponse.toString());
                }else {
                    try {
                        displayErrorMessage(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }


            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    protected void displayErrorMessage(String errorBody) {
        try {
            JSONObject jsonResponse = new JSONObject(errorBody);
            String message = jsonResponse.getString(Api.KEY_MESSAGE);
            if(jsonResponse.has("logout")){

            }
            //   Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
