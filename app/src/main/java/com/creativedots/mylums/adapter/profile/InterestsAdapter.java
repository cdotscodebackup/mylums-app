package com.creativedots.mylums.adapter.profile;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.creativedots.mylums.R;
import com.creativedots.mylums.model.retroFit.profile.Interest;

import java.util.ArrayList;

public class InterestsAdapter extends RecyclerView.Adapter<InterestsAdapter.ViewHolder> {

    private Activity activity;
    private ArrayList<Interest> interests;

    private Boolean IS_EDITABLE = false;

    public InterestsAdapter(Activity activity, ArrayList<Interest> interests) {
        this.activity = activity;
        this.interests = interests;
    }

    public void setIS_EDITABLE(Boolean IS_EDITABLE) {
        this.IS_EDITABLE = IS_EDITABLE;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(activity).inflate(R.layout.item_interest, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {

        holder.tvTitle.setText(interests.get(pos).getName());

        if(!IS_EDITABLE){
            holder.ivDelete.setVisibility(View.GONE);
        }else{
            holder.ivDelete.setVisibility(View.VISIBLE);
        }

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    interests.remove(holder.getAdapterPosition());
                    notifyItemRemoved(holder.getAdapterPosition()); }
        });
    }

    @Override
    public int getItemCount() {
        return interests.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tvTitle;
        private ImageView ivDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.name);
            ivDelete = itemView.findViewById(R.id.remove);
        }
    }
}
