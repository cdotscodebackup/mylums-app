package com.creativedots.mylums.adapter.messages;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.model.retroFit.Message.MessageChannel;
import com.creativedots.mylums.utils.DateConversion;
import com.creativedots.mylums.utils.sharedPreferences.Constants;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {

    private AppCompatActivity activity;
    private ArrayList<MessageChannel> mMessages;

    public MessagesAdapter(AppCompatActivity activity, ArrayList<MessageChannel> messages) {
        this.activity = activity;
        mMessages = messages;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(activity).inflate(R.layout.rv_item_message, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {


        try {
            MessageChannel messages = mMessages.get(i);
            viewHolder.tvName.setText(messages.getProfile().getFirstName() + " " + messages.getProfile().getLastName());
            viewHolder.tvMessage.setText(messages.getLatestMessage());
            viewHolder.tvDate.setText(DateConversion.getTimeOrDateTime(messages.getUpdatedAt()));
            viewHolder.tvDesignation.setText(messages.getProfile().getCurrentDesignation());
            Glide.with(activity)
                    .load(messages.getProfile().getImagepath())
                    .apply(new RequestOptions().centerCrop().circleCrop().error(R.drawable.ic_user_tab_primary).placeholder(R.drawable.ic_user_tab_primary))
                    .into(viewHolder.civProfileImage);


            if (messages.getStatus().equalsIgnoreCase(Constants.KEY_READ)) {
                viewHolder.lnUnread.setVisibility(View.GONE);
                viewHolder.tvDesignation.setTypeface(viewHolder.tvDesignation.getTypeface(), Typeface.NORMAL);
                viewHolder.tvMessage.setTypeface(viewHolder.tvMessage.getTypeface(), Typeface.NORMAL);
            } else {
                viewHolder.lnUnread.setVisibility(View.VISIBLE);
                viewHolder.tvDesignation.setTypeface(viewHolder.tvDesignation.getTypeface(), Typeface.BOLD);
                viewHolder.tvMessage.setTypeface(viewHolder.tvMessage.getTypeface(), Typeface.BOLD);
            }

            if (!messages.getArchive().isEmpty()) {
                viewHolder.txtArchive.setText("Unarchive");
            } else {
                viewHolder.txtArchive.setText("Archive");
            }

            viewHolder.llMessageItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (onItemClickedListener != null)
                        onItemClickedListener.onMessageClicked(i);
                }
            });
        } catch (Exception ex) {
            Log.e("Exception", "" + ex);
        }
    }

    @Override
    public int getItemCount() {
        // TODO : Replace this with real data.
        return mMessages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName, tvDesignation, tvMessage, tvDate, txtArchive;
        private LinearLayout llMessageItem, lnUnread;
        private CircleImageView civProfileImage;
        public RelativeLayout viewBackground, viewForeground;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtArchive = itemView.findViewById(R.id.txtArchive);
            tvName = itemView.findViewById(R.id.tvName);
            tvDesignation = itemView.findViewById(R.id.tvDesignation);
            tvMessage = itemView.findViewById(R.id.tvMessage);
            tvDate = itemView.findViewById(R.id.tvDate);

            llMessageItem = itemView.findViewById(R.id.llMessageItem);
            lnUnread = itemView.findViewById(R.id.lnUnread);

            civProfileImage = itemView.findViewById(R.id.civProfileImage);
            viewBackground = itemView.findViewById(R.id.view_background);
            viewForeground = itemView.findViewById(R.id.view_foreground);
        }
    }

    private OnItemClickedListener onItemClickedListener;

    public void setOnItemClickedListener(OnItemClickedListener onItemClickedListener) {
        this.onItemClickedListener = onItemClickedListener;
    }

    public interface OnItemClickedListener {
        void onMessageClicked(int i);
    }

    public void removeItem(int position) {
        mMessages.remove(position);

        notifyItemRemoved(position);
    }

    public void restoreItem(MessageChannel item, int position) {
        mMessages.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }


}
