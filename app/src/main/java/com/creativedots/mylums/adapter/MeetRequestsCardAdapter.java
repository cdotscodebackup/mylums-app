package com.creativedots.mylums.adapter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.model.retroFit.Data;
import com.creativedots.mylums.model.retroFit.notification.Notification;
import com.creativedots.mylums.model.retroFit.profile.Interest;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MeetRequestsCardAdapter extends ArrayAdapter<Notification> {

    private AppCompatActivity activity;
    private ArrayList<Interest> interests;

    public MeetRequestsCardAdapter(Context context) {
        super(context, 0);
        activity = ((MainActivity)context);
        initInterestsRecyclerView();
    }

    @Override
    public View getView(int position, View contentView, ViewGroup parent) {
        ViewHolder holder;

        if (contentView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            contentView = inflater.inflate(R.layout.item_user_card, parent, false);
            holder = new ViewHolder(contentView);
            contentView.setTag(holder);
        } else {
            holder = (ViewHolder) contentView.getTag();
        }

        Notification user = getItem(position);

        holder.tvName.setText(user.getProfile().getFirstName() + " " + user.getProfile().getLastName());
        holder.tvCompany.setText(user.getProfile().getCurrentCompany());
        Glide.with(getContext()).load(user.getProfile().getImagepath())
                .into(holder.civProfile);
        Glide.with(getContext())
                .load(user.getProfile().getImagepath())
                .apply(new RequestOptions().centerCrop().circleCrop()
                        .error(R.drawable.ic_user_tab_primary).placeholder(R.drawable.ic_user_tab_primary))
                .into(holder.civProfile);
        InterestsAdapter interestsAdapter = new InterestsAdapter(activity, interests, false);
        interests.addAll(user.getProfile().getInterests());
        holder.rvInterests.setLayoutManager(ChipsLayoutManager.newBuilder(activity).build());
        holder.rvInterests.setAdapter(interestsAdapter);

        return contentView;
    }

    private static class ViewHolder {
        public TextView tvName;
        public TextView tvCompany;
        public CircleImageView civProfile;
        public RecyclerView rvInterests;

        public ViewHolder(View view) {
            tvName = view.findViewById(R.id.tvName);
            tvCompany = view.findViewById(R.id.tvCompany);
            civProfile = view.findViewById(R.id.civProfile);
            rvInterests = view.findViewById(R.id.rvInterests);
        }
    }

    private void initInterestsRecyclerView() {
        interests = new ArrayList<>();
       // interests.addAll()
       /* interests.add("Racing");
        interests.add("Gaming");
        interests.add("More");*/
    }

}
