package com.creativedots.mylums.adapter;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.model.retroFit.Data;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AlumniListAdapter extends RecyclerView.Adapter<AlumniListAdapter.ViewHolder> {

    private AppCompatActivity activity;
    private ArrayList<Data> data;
    private OnItemSelectedListener onItemSelectedListener;

    public AlumniListAdapter(AppCompatActivity activity, ArrayList<Data> data) {
        this.activity = activity;
        this.data = data;
    }

    public void setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.rv_item_alumni_list, parent,false);

       // CardView cvAlumniItem = view.findViewById(R.id.cvAlumniItem);
       // cvAlumniItem.getLayoutParams().width = (getScreenWidth()/2)-90;

        return new ViewHolder(view);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {

        Data data = this.data.get(pos);

        Glide.with(activity)
                .load(data.getImagePath())
                .apply(new RequestOptions().centerCrop().circleCrop()
                        .error(R.drawable.logo_mini).placeholder(R.drawable.logo_mini))
                .into(holder.civProfile);

        holder.tvName.setText(data.getFirstName() + " " + data.getLastName());

        holder.tvCompany.setText(data.getCompany());

        holder.tvDesignation.setText(data.getDesignation());

//        holder.cvAlumniItem.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent e) {
//
//                handleTouchEvents(holder, e, holder.cvAlumniItem,
//                        holder.tvName, holder.tvCompany, holder.tvDesignation);
//                return true;
//            }
//        });

        holder.cvAlumniItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemSelectedListener.onProfileSelected(holder.getAdapterPosition());
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView civProfile;
        private TextView tvName, tvCompany, tvDesignation;
        private CardView cvAlumniItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            civProfile = itemView.findViewById(R.id.civProfile);

            tvName = itemView.findViewById(R.id.tvName);
            tvCompany = itemView.findViewById(R.id.tvCompany);
            tvDesignation = itemView.findViewById(R.id.tvDesignation);

            cvAlumniItem = itemView.findViewById(R.id.cvAlumniItem);
        }
    }

    public interface OnItemSelectedListener{
        void onProfileSelected(int position);
    }

    private int getScreenWidth(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int height = displayMetrics.heightPixels;

        return displayMetrics.widthPixels;
    }

    private void handleTouchEvents(ViewHolder holder, MotionEvent e, CardView cvBackground, TextView... textViews) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                pressStartTime = System.currentTimeMillis();
                pressedX = e.getX();
                pressedY = e.getY();

                setIconSelected(cvBackground, true, textViews);
                break;
            }
            case MotionEvent.ACTION_SCROLL:
            case MotionEvent.ACTION_MOVE:
                setIconSelected(cvBackground, false, textViews);
                break;
            case MotionEvent.ACTION_UP: {
                setIconSelected(cvBackground, false, textViews);
                long pressDuration = System.currentTimeMillis() - pressStartTime;
                if (pressDuration < MAX_CLICK_DURATION && distance(pressedX, pressedY, e.getX(), e.getY()) < MAX_CLICK_DISTANCE) {

                    /**
                     * Handling Click events here...
                     */
//                    handleClickEvents(cvBackground);
                    onItemSelectedListener.onProfileSelected(holder.getAdapterPosition());
                }
            }
        }
    }

    private void setIconSelected(CardView cvBackground, Boolean isSelected,TextView... textViews) {

        if(isSelected){
            cvBackground.setCardBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryLight));

            for(TextView textView : textViews){
                textView.setTextColor(activity.getResources().getColor(android.R.color.white));
            }
        }else {
            cvBackground.setCardBackgroundColor(activity.getResources().getColor(R.color.colorNothingBackground));
            for(TextView textView : textViews){
                textView.setTextColor(activity.getResources().getColor(android.R.color.black));
            }
        }
    }

    /**
     * Max allowed duration for a "click", in milliseconds.
     */
    private static final int MAX_CLICK_DURATION = 1000;

    /**
     * Max allowed distance to move during a "click", in DP.
     */
    private static final int MAX_CLICK_DISTANCE = 15;

    private long pressStartTime;
    private float pressedX;
    private float pressedY;

    private float distance(float x1, float y1, float x2, float y2) {
        float dx = x1 - x2;
        float dy = y1 - y2;
        float distanceInPx = (float) Math.sqrt(dx * dx + dy * dy);
        return pxToDp(distanceInPx);
    }

    private float pxToDp(float px) {
        return px / activity.getResources().getDisplayMetrics().density;
    }
}
