package com.creativedots.mylums.adapter.profile;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.creativedots.mylums.R;
import com.creativedots.mylums.fragment.dashboardFragments.UpdateProfileFragment;
import com.creativedots.mylums.model.retroFit.profile.Interest;
import com.creativedots.mylums.model.retroFit.profile.WorkExperience;

import java.util.ArrayList;

public class WorkExperienceAdapter extends RecyclerView.Adapter<WorkExperienceAdapter.ViewHolder> {

    private Activity activity;
    private ArrayList<WorkExperience> interests;
    private OnItemSelectedListener mListener;
    private UpdateProfileFragment mFragment;

    public WorkExperienceAdapter(Activity activity, ArrayList<WorkExperience> interests, OnItemSelectedListener onItemSelectedListener,
                                 UpdateProfileFragment fragment) {
        this.activity = activity;
        this.interests = interests;
        this.mListener = onItemSelectedListener;
        mFragment = fragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_work_experience_ready_only, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {

        holder.destination.setText(interests.get(pos).getDesignation());
        holder.companyName.setText(interests.get(pos).getCompanyName());
        holder.companyLocation.setText(interests.get(pos).getCity());
        String period = interests.get(pos).getFromDate() + " - ";

        if (interests.get(pos).getIsCurrentJob().equalsIgnoreCase("yes")) {
            period = period + "Present";
        } else {
            period = period + interests.get(pos).getToDate();
        }
        holder.period.setText(period);

        if (mFragment.isEditable()) {
            holder.edit.setVisibility(View.VISIBLE);
        } else {
            holder.edit.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return interests.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView destination;
        private TextView companyName;
        private TextView companyLocation;
        private TextView period;
        private ImageView edit;
        public RelativeLayout viewBackground, viewForeground;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            viewBackground = itemView.findViewById(R.id.view_background);
            viewForeground = itemView.findViewById(R.id.view_foreground);
            destination = itemView.findViewById(R.id.work_destination);
            companyLocation = itemView.findViewById(R.id.work_location);
            companyName = itemView.findViewById(R.id.work_company_name);
            period = itemView.findViewById(R.id.work_period);
            edit = itemView.findViewById(R.id.edit_work);
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.onEditWorkExperience(getAdapterPosition());
                    }
                }
            });

        }
    }


    public void removeItem(int position) {
        interests.remove(position);

        notifyItemRemoved(position);
    }


    public interface OnItemSelectedListener {
        abstract void onEditWorkExperience(int position);
    }
}

