package com.creativedots.mylums.adapter.autoComplete;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.activity.home.messages.ComposeMessageActivity;
import com.creativedots.mylums.fragment.dashboardFragments.DirectorySearchFragment;
import com.creativedots.mylums.fragment.dashboardFragments.ViewAlumniProfileFragment;
import com.creativedots.mylums.model.retroFit.Data;
import com.creativedots.mylums.model.retroFit.Response;
import com.creativedots.mylums.network.ApiInterface;
import com.creativedots.mylums.utils.AppSettings;

import java.io.IOException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;

public class DirectoryAutoCompleteArrayAdapter extends ArrayAdapter<Data> {

    private AppCompatActivity activity;
    private int viewResourceId;
    public ApiInterface apiInterface;
    private OnItemClickListener onItemClickListener;
    private String compose = "";
    private DirectorySearchFragment mDirectorySearchFragment;

    public DirectoryAutoCompleteArrayAdapter(AppCompatActivity activity, int viewResourceId,
                                             ArrayList<Data> data, ApiInterface apiInterface, DirectorySearchFragment searchFragment){
        super(activity, viewResourceId, data);
        this.apiInterface = apiInterface;
        this.activity = activity;
        this.viewResourceId = viewResourceId;
        if(activity instanceof ComposeMessageActivity){
            compose = "yes";
        }
        mDirectorySearchFragment = searchFragment;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = LayoutInflater.from(activity).inflate(viewResourceId, null);
        }
        Data user = getItem(position);
        if (user != null) {
            LinearLayout llAlumniItem;
            CircleImageView civThumbnail;
            TextView tvName, tvDesignation, tvCompany;

            llAlumniItem = v.findViewById(R.id.llAlumniItem);
            civThumbnail = v.findViewById(R.id.civThumbnail);

            tvName = v.findViewById(R.id.tvName);
            tvDesignation = v.findViewById(R.id.tvDesignation);
            tvCompany = v.findViewById(R.id.tvCompany);

            Glide.with(activity).load(user.getImagePath())
                    .apply(new RequestOptions().centerCrop().placeholder(R.drawable.ic_user_place_holder))
                    .into(civThumbnail);

            tvName.setText(user.getFirstName() + " " + user.getLastName());
            tvDesignation.setText(user.getDesignation());
            tvCompany.setText(user.getCompany());
            llAlumniItem.setTag(position);
            llAlumniItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(activity.getClass().equals(ComposeMessageActivity.class)){
                        onItemClickListener.onItemClicked(getItem((int)view.getTag()));
                    }else if (activity.getClass().equals(MainActivity.class)){
                        // Toast.makeText(activity, "Test", Toast.LENGTH_SHORT).show();


                        AppSettings.hideKeyboard(view.getContext().getApplicationContext(),view);

                        ViewAlumniProfileFragment viewAlumniProfileFragment = new ViewAlumniProfileFragment();
                        viewAlumniProfileFragment.setAlmuniUserID(getItem((int)view.getTag()).getUserId(),getItem((int)view.getTag()).getUsertype());

                        ((MainActivity)activity).replaceFragment(
                                viewAlumniProfileFragment,
                                ViewAlumniProfileFragment.TAG,
                                true,
                                false
                        );
                    }
                }
            });
        }
        return v;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    private Filter nameFilter = new Filter() {

        @NonNull
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            Data user = (Data) resultValue;
            return user.getFirstName()+" "+user.getLastName();
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            if(charSequence != null) {
                ArrayList<Data> filteredList = new ArrayList<>();
                DirectoryAutoCompleteArrayAdapter frag = DirectoryAutoCompleteArrayAdapter.this;
                Call<Response> call;
                if(mDirectorySearchFragment != null && mDirectorySearchFragment.getIsAdvanceSearch()){
                    call = frag.apiInterface.alumniSearch(charSequence.toString(), "1",
                            "yes", mDirectorySearchFragment.getEtCountry(), mDirectorySearchFragment.getEtCity(), mDirectorySearchFragment.getTvProgram(), mDirectorySearchFragment.getEtCompanyName(), mDirectorySearchFragment.getEtDesignation(), mDirectorySearchFragment.getEtDepartment(), compose);
                }else {
                    call = frag.apiInterface.alumniSearch(charSequence.toString(), "1", "", "", "", "", "", "", "", compose);
                }
                        try {
                    retrofit2.Response<Response> response = call.execute();
                    if(response.isSuccessful()){
                        filteredList = response.body().getDataList();
                    }
                }catch (IOException ioe){
                    ioe.printStackTrace();
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                filterResults.count = filteredList.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            clear();
            if(filterResults.values != null) {
                ArrayList<Data> filteredList = (ArrayList<Data>) filterResults.values;
                addAll(filteredList);
            }
            notifyDataSetChanged();
        }
    };

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener{
        void onItemClicked(Data data);
    }
}
