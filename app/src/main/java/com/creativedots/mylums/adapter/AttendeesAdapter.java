package com.creativedots.mylums.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.model.eventDetail.InterestType;
import com.creativedots.mylums.model.retroFit.profile.User;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AttendeesAdapter extends RecyclerView.Adapter<AttendeesAdapter.ViewHolder> {

    private Context context;
    private ArrayList<InterestType> mInterestTypes;
    private String mStatus;

    public AttendeesAdapter(Context context, ArrayList<InterestType> list, String status) {
        this.context = context;
        mInterestTypes = list;
        mStatus = status;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.rv_item_event_attendees, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int pos) {


        User user = mInterestTypes.get(pos).getUser();

        try {


            holder.tvName.setText(user.getFirstName() + " " + user.getLastName());
            holder.tvCompany.setText(user.getCurrentCompany());
            holder.tvDesignation.setText(user.getCurrentDesignation());
            Glide.with(context)
                    .load(user.getImagepath())
                    .apply(new RequestOptions().centerCrop().circleCrop()
                            .error(R.drawable.ic_user_tab_primary).placeholder(R.drawable.ic_user_tab_primary))
                    .into(holder.civProfile);

            if (mInterestTypes.get(pos).getStatus().equalsIgnoreCase("in") && mStatus.equalsIgnoreCase("active") &&
                    !user.getUserId().equalsIgnoreCase(SessionManager.from(context).getUser().userId)) {
                holder.tvSayHi.setVisibility(View.VISIBLE);
            } else {
                holder.tvSayHi.setVisibility(View.GONE);
            }
            holder.tvSayHi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickedListener.onSayHiClicked(pos);
                }
            });
            holder.lnProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickedListener.viewProfileClicked(pos);
                }
            });
        } catch (Exception ex) {
            Log.e("Exception", "" + ex);
        }
    }

    @Override
    public int getItemCount() {
        return mInterestTypes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView civProfile;
        private TextView tvName, tvDesignation, tvSayHi,tvCompany;
        private LinearLayout lnProfile;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            civProfile = itemView.findViewById(R.id.civProfile);

            tvName = itemView.findViewById(R.id.tvName);
            tvCompany = itemView.findViewById(R.id.tvCompany);
            tvDesignation = itemView.findViewById(R.id.tvDesignation);
            tvSayHi = itemView.findViewById(R.id.tvSayHi);
            lnProfile = itemView.findViewById(R.id.lnProfile);
        }
    }

    private OnItemClickedListener onItemClickedListener;

    public void setOnItemClickedListener(OnItemClickedListener onItemClickedListener) {
        this.onItemClickedListener = onItemClickedListener;
    }

    public interface OnItemClickedListener {
        void onSayHiClicked(int pos);

        void viewProfileClicked(int pos);
    }

}
