package com.creativedots.mylums.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.creativedots.mylums.fragment.HelpFragment;



public class HelpPagerAdapter extends FragmentStatePagerAdapter {

    Context ctxt = null;


    public HelpPagerAdapter(Context ctxt, FragmentManager mgr) {
        super(mgr);
        this.ctxt = ctxt;

    }

    @Override
    public Fragment getItem(int position) {

        return HelpFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return 5;
    }


}
