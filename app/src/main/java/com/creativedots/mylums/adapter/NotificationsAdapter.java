package com.creativedots.mylums.adapter;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.model.retroFit.notification.Notification;
import com.creativedots.mylums.utils.DateConversion;

import java.util.ArrayList;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {

    private AppCompatActivity activity;
    private ArrayList<Notification> mNotifications;

    public NotificationsAdapter(AppCompatActivity activity, ArrayList<Notification> notifications) {
        this.activity = activity;
        mNotifications = notifications;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(activity).inflate(R.layout.rv_item_notification, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {

        try {
            Notification notification = mNotifications.get(pos);
            holder.tvTitle.setText(notification.getNotificationTitle());
            holder.tvSubtitle.setText(notification.getNotificationBody());
            holder.tvDate.setText(DateConversion.getTimeOrDateTime(notification.getCreatedAt()));
            Glide.with(activity)
                    .load(notification.getImagepath())
                    .apply(new RequestOptions().centerCrop().circleCrop().error(R.drawable.ic_user_place_holder).placeholder(R.drawable.ic_user_place_holder))
                    .into(holder.imgUser);

            if (notification.getStatus().equalsIgnoreCase("unread")) {
                holder.imgDot.setVisibility(View.VISIBLE);
            } else {
                holder.imgDot.setVisibility(View.INVISIBLE);
            }
            holder.llNotificationItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickedListener.onNotificationClicked(holder.getAdapterPosition());
                }
            });
        } catch (Exception ex) {
            Log.e("Exception", "" + ex);
        }

    }


    @Override
    public int getItemCount() {
        return mNotifications.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout llNotificationItem;
        private TextView tvTitle, tvSubtitle, tvDate;
        private ImageView imgUser, imgDot;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            llNotificationItem = itemView.findViewById(R.id.llNotificationItem);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvSubtitle = itemView.findViewById(R.id.tvSubtitle);
            tvDate = itemView.findViewById(R.id.tvDate);

            imgUser = itemView.findViewById(R.id.imgUser);
            imgDot = itemView.findViewById(R.id.imgDot);

        }
    }

    private NotificationsAdapter.OnItemClickedListener onItemClickedListener;

    public void setOnItemClickedListener(NotificationsAdapter.OnItemClickedListener onItemClickedListener) {
        this.onItemClickedListener = onItemClickedListener;
    }

    public interface OnItemClickedListener {
        void onNotificationClicked(int pos);
    }
}
