package com.creativedots.mylums.adapter;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.creativedots.mylums.R;
import com.creativedots.mylums.model.retroFit.profile.Interest;

import java.util.ArrayList;

public class InterestsAdapter extends RecyclerView.Adapter<InterestsAdapter.ViewHolder> {

    private AppCompatActivity activity;
    private ArrayList<Interest> interests;
    private Boolean shouldRemove;

    public InterestsAdapter(AppCompatActivity activity, ArrayList<Interest> interests, Boolean shouldRemove) {
        this.activity = activity;
        this.interests = interests;
        this.shouldRemove = shouldRemove;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(activity).inflate(R.layout.rv_item_interests, viewGroup, false);

        ImageView ivDelete = view.findViewById(R.id.ivDelete);

        if(shouldRemove){
            ivDelete.setVisibility(View.VISIBLE);
        }else{
            ivDelete.setVisibility(View.GONE);
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {

        holder.tvTitle.setText(interests.get(pos).getName());

        if(shouldRemove) {
            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    interests.remove(holder.getAdapterPosition());
                    notifyItemRemoved(holder.getAdapterPosition());
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return interests.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tvTitle;
        private ImageView ivDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            ivDelete = itemView.findViewById(R.id.ivDelete);
        }
    }
}
