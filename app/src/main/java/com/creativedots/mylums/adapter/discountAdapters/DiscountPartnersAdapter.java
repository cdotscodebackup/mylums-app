package com.creativedots.mylums.adapter.discountAdapters;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.model.retroFit.partnersList.Partner;
import com.creativedots.mylums.model.retroFit.partnersNearBy.PartnerNearBy;
import com.creativedots.mylums.utils.sharedPreferences.Constants;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

public class DiscountPartnersAdapter extends RecyclerView.Adapter<DiscountPartnersAdapter.ViewHolder> {

    private Boolean nearBy;
    private AppCompatActivity activity;
    private ArrayList<Partner> partners;
    private ArrayList<PartnerNearBy> partnersNearBy;

    public DiscountPartnersAdapter(AppCompatActivity activity, ArrayList<Partner> partners,
                                   ArrayList<PartnerNearBy> partnersNearBy, Boolean nearBy) {
        this.activity = activity;
        this.partners = partners;
        this.partnersNearBy = partnersNearBy;
        this.nearBy = nearBy;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(activity).inflate(R.layout.rv_item_discount_partners, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {

        if (nearBy) {

            holder.ivBadge.setVisibility(View.VISIBLE);
            if (partnersNearBy.get(pos).getPartnerType().equalsIgnoreCase(Constants.KEY_BADGE_GOLD)) {
                holder.ivBadge.setImageDrawable(activity.getResources().getDrawable(R.drawable.badge_gold));
            } else if (partnersNearBy.get(pos).getPartnerType().equalsIgnoreCase(Constants.KEY_BADGE_PLATINUM)) {
                holder.ivBadge.setImageDrawable(activity.getResources().getDrawable(R.drawable.badges_platinum));
            } else if (partnersNearBy.get(pos).getPartnerType().equalsIgnoreCase(Constants.KEY_BADGE_SILVER)) {
                holder.ivBadge.setImageDrawable(activity.getResources().getDrawable(R.drawable.badge_silver));
            } else {
                holder.ivBadge.setVisibility(View.INVISIBLE);
            }
            Glide.with(activity).load(partnersNearBy.get(pos).getImagepath().replace(" ", "%20"))
                    .apply(new RequestOptions()
                            .error(R.drawable.logo_mini).placeholder(R.drawable.logo_mini))

                    .into(holder.ivThumbnail);

            holder.tvTitle.setText(partnersNearBy.get(pos).getName());
            holder.tvDescription.setText(partnersNearBy.get(pos).getDescription());
            holder.tvDiscount.setText(partnersNearBy.get(pos).getDiscount());

            holder.llDiscountItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickedListener.onDiscountSelected(partnersNearBy.get(holder.getAdapterPosition()).getId());
                }
            });
        } else {

            holder.ivBadge.setVisibility(View.VISIBLE);
            if (partners.get(pos).getPartnerType().equalsIgnoreCase(Constants.KEY_BADGE_GOLD)) {
                holder.ivBadge.setImageDrawable(activity.getResources().getDrawable(R.drawable.badge_gold));
            } else if (partners.get(pos).getPartnerType().equalsIgnoreCase(Constants.KEY_BADGE_PLATINUM)) {
                holder.ivBadge.setImageDrawable(activity.getResources().getDrawable(R.drawable.badges_platinum));
            } else if (partners.get(pos).getPartnerType().equalsIgnoreCase(Constants.KEY_BADGE_SILVER)) {
                holder.ivBadge.setImageDrawable(activity.getResources().getDrawable(R.drawable.badge_silver));
            } else {
                holder.ivBadge.setVisibility(View.INVISIBLE);
            }
            Glide.with(activity).load(partners.get(pos).getImagepath().replace(" ", "%20"))
                    .apply(new RequestOptions()
                            .error(R.drawable.logo_mini).placeholder(R.drawable.logo_mini))

                    .into(holder.ivThumbnail);

            holder.tvTitle.setText(partners.get(pos).getName());
            holder.tvDescription.setText(partners.get(pos).getDescription());
            holder.tvDiscount.setText(partners.get(pos).getDiscount());

            holder.llDiscountItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickedListener.onDiscountSelected(partners.get(holder.getAdapterPosition()).getId());
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        if (nearBy) {
            return partnersNearBy.size();
        } else {
            return partners.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout llDiscountItem;
        private ImageView ivThumbnail, ivBadge;
        private TextView tvTitle, tvDescription, tvDiscount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            llDiscountItem = itemView.findViewById(R.id.llDiscountItem);

            ivThumbnail = itemView.findViewById(R.id.ivThumbnail);
            ivBadge = itemView.findViewById(R.id.imgBadge);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvDiscount = itemView.findViewById(R.id.tvDiscount);
        }
    }

    private OnItemClickedListener onItemClickedListener;

    public void setOnItemClickedListener(OnItemClickedListener onItemClickedListener) {
        this.onItemClickedListener = onItemClickedListener;
    }

    public interface OnItemClickedListener {
        void onDiscountSelected(String partnerID);
    }

    public int getBadgeDrawable() {
        return 0;
    }
}
