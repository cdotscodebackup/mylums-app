package com.creativedots.mylums.adapter.event;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.fragment.dashboardFragments.EventDetailsFragment;
import com.creativedots.mylums.model.retroFit.Data;
import com.creativedots.mylums.utils.DateConversion;

import java.util.ArrayList;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {

    private AppCompatActivity activity;
    private ArrayList<Data> events;

    public EventsAdapter(AppCompatActivity activity, ArrayList<Data> events) {
        this.activity = activity;
        this.events = events;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(activity).inflate(R.layout.rv_item_upcoming_events, viewGroup, false));
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {
        final Data event = events.get(pos);
//        holder.llBackground.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent e) {
//                handleTouchEvents(holder,e,holder.llBackground,
//                        holder.tvEventTitle, holder.tvLabelDate, holder.tvDate,
//                        holder.tvLabelTime, holder.tvTime, holder.tvLabelVenue,
//                        holder.tvVenue);
//                return true;
//            }
//        });

        Glide.with(activity).load(event.getImagePath())
                .apply(new RequestOptions().centerCrop()
                        .error(R.drawable.bg_dashboard).placeholder(R.drawable.bg_dashboard))
                .into(holder.ivThumbnail);
        holder.tvEventTitle.setText(event.getName());
        holder.tvDate.setText(DateConversion.getDateEngMonth(event.getDates()));
        holder.tvTime.setText(DateConversion.convertTimeToLocal(event.getStartTime()) + " to " + DateConversion.convertTimeToLocal(event.getEndTime()));
        holder.tvVenue.setText(event.getVenue());

        holder.llBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickedListener.onEventClicked(event);
            }
        });

    }

    @Override
    public int getItemCount() {
        return events.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout llBackground;
        private ImageView ivThumbnail;
        private TextView tvDate, tvTime, tvVenue;
        private TextView tvEventTitle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            llBackground = itemView.findViewById(R.id.llBackground);

            ivThumbnail = itemView.findViewById(R.id.ivThumbnail);

            tvDate = itemView.findViewById(R.id.tvDate);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvVenue = itemView.findViewById(R.id.tvVenue);

            tvEventTitle = itemView.findViewById(R.id.tvEventTitle);
        }
    }

    private OnItemClickedListener onItemClickedListener;

    public void setOnItemClickedListener(OnItemClickedListener onItemClickedListener) {
        this.onItemClickedListener = onItemClickedListener;
    }

    public interface OnItemClickedListener {
        void onEventClicked(Data event);
    }

    private void handleTouchEvents(ViewHolder holder, MotionEvent e, View vBackground, TextView... textViews) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                pressStartTime = System.currentTimeMillis();
                pressedX = e.getX();
                pressedY = e.getY();

                setIconSelected(vBackground, true, textViews);
                break;
            }
            case MotionEvent.ACTION_SCROLL:
            case MotionEvent.ACTION_MOVE:
                setIconSelected(vBackground, false, textViews);
                break;
            case MotionEvent.ACTION_UP: {
                setIconSelected(vBackground, false, textViews);
                long pressDuration = System.currentTimeMillis() - pressStartTime;
                if (pressDuration < MAX_CLICK_DURATION && distance(pressedX, pressedY, e.getX(), e.getY()) < MAX_CLICK_DISTANCE) {

                    /**
                     * Handling Click events here...
                     */
//                    handleClickEvents(cvBackground);

                    // TODO : create a click listener here...
                    EventDetailsFragment eventDetailsFragment = new EventDetailsFragment();
                    ((MainActivity) activity).replaceFragment(eventDetailsFragment, EventDetailsFragment.TAG, true, false);
                }
            }
        }
    }

    private void setIconSelected(View vBackground, Boolean isSelected, TextView... textViews) {

        if (isSelected) {
            vBackground.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryLight));

            for (TextView textView : textViews) {
                textView.setTextColor(activity.getResources().getColor(android.R.color.white));
            }
        } else {
            vBackground.setBackgroundColor(activity.getResources().getColor(android.R.color.white));
            for (TextView textView : textViews) {
                textView.setTextColor(activity.getResources().getColor(android.R.color.black));
            }
        }
    }

    /**
     * Max allowed duration for a "click", in milliseconds.
     */
    private static final int MAX_CLICK_DURATION = 1000;

    /**
     * Max allowed distance to move during a "click", in DP.
     */
    private static final int MAX_CLICK_DISTANCE = 15;

    private long pressStartTime;
    private float pressedX;
    private float pressedY;

    private float distance(float x1, float y1, float x2, float y2) {
        float dx = x1 - x2;
        float dy = y1 - y2;
        float distanceInPx = (float) Math.sqrt(dx * dx + dy * dy);
        return pxToDp(distanceInPx);
    }

    private float pxToDp(float px) {
        return px / activity.getResources().getDisplayMetrics().density;
    }
}
