package com.creativedots.mylums.adapter.messages;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.annotation.NonNull;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.creativedots.mylums.R;
import com.creativedots.mylums.model.retroFit.Message.Messages;
import com.creativedots.mylums.utils.DateConversion;
import com.creativedots.mylums.utils.sharedPreferences.SessionManager;

import java.util.ArrayList;

public class ChatMessageAdapter extends RecyclerView.Adapter<ChatMessageAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Messages> mMessages;
    private String fName;

    public ChatMessageAdapter(Context context, ArrayList<Messages> messages,String fName) {
        this.context = context;
        mMessages = messages;
        this.fName = fName;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.rv_item_chat_message, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int pos) {

        Messages messages = mMessages.get(pos);


        holder.tvMessage.setText(messages.getMessage());
        holder.tvTime.setText(DateConversion.getTimeOrDateTime(messages.getCreatedAt()));

        Log.e("LOGID","MYID: " + SessionManager.from(context).getUser().getUserId()
         + " SENDERID: " + messages.getSenderId());
        if (SessionManager.from(context).getUser().getUserId().equalsIgnoreCase(messages.getSenderId())) {

            holder.tvMessage.setTextColor(context.getResources().getColor(R.color.colorWhite));
            holder.tvTitle.setTextColor(context.getResources().getColor(R.color.colorWhite));
            holder.tvTime.setTextColor(context.getResources().getColor(R.color.colorWhite));

            holder.tvTitle.setText("You");
            holder.llMessageItem.setGravity(Gravity.END);
            holder.cvMessageBackground.setBackgroundColor(context.getResources().getColor(R.color.colorChatSent));
            holder.imgChatTriangle.setRotation(180);
            //holder.imgChatTriangle.setImageTintMode();
            ImageViewCompat.setImageTintList(holder.imgChatTriangle, ColorStateList.valueOf(
                    context.getResources().getColor(R.color.colorChatSent)));

        } else {
            holder.tvMessage.setTextColor(context.getResources().getColor(R.color.colorBlack));
            holder.tvTitle.setTextColor(context.getResources().getColor(R.color.colorBlack));
            holder.tvTime.setTextColor(context.getResources().getColor(R.color.colorBlack));


            holder.tvTitle.setText(fName);
            holder.llMessageItem.setGravity(Gravity.START);
            holder.cvMessageBackground.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
            holder.imgChatTriangle.setRotation(0);
            ImageViewCompat.setImageTintList(holder.imgChatTriangle, ColorStateList.valueOf(
                    context.getResources().getColor(R.color.colorWhite)));
        }
    }

    @Override
    public int getItemCount() {
        Log.e("Size","" + mMessages.size());
        return mMessages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout llMessageItem;
        private CardView cvMessageBackground;
        private ImageView imgChatTriangle;
        private TextView tvTitle, tvMessage, tvTime;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            llMessageItem = itemView.findViewById(R.id.llMessageItem);

            cvMessageBackground = itemView.findViewById(R.id.cvMessageBackground);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvMessage = itemView.findViewById(R.id.tvMessage);
            tvTime = itemView.findViewById(R.id.tvTime);

            imgChatTriangle = itemView.findViewById(R.id.imgChatTriangle);
        }
    }
}
