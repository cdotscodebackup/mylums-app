package com.creativedots.mylums.adapter.event.gallery;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.creativedots.mylums.R;
import com.creativedots.mylums.activity.home.MainActivity;
import com.creativedots.mylums.model.eventDetail.GalleryProduct;

import java.util.ArrayList;

public class GridAdapter extends RecyclerView.Adapter<GridAdapter.ViewHolder> {

    private Context context;
    private ArrayList<GalleryProduct> mGalleryProducts;
    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public GridAdapter(Context context, ArrayList<GalleryProduct> mGalleryProducts) {
        this.context = context;
        this.mGalleryProducts = mGalleryProducts;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.rv_item_gallery_grid, viewGroup, false);

//        ImageView imageView = view.findViewById(R.id.ivImage);
//
//        Display display = ((MainActivity)context).getWindowManager().getDefaultDisplay();
//
//        imageView.getLayoutParams().width = (display.getWidth()/2);
//        imageView.getLayoutParams().height = (display.getWidth()/2);
//        imageView.requestLayout();

        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {

        if(mGalleryProducts.get(i).getType().equals("video")){

            String videoPath = mGalleryProducts.get(i).getVideoPath();
            String videoId = videoPath.split("v=")[1];
            String thumbPath = "http://img.youtube.com/vi/"+videoId+"/0.jpg";

            Glide.with(context)
                    .load(thumbPath)
                    .apply(new RequestOptions().centerCrop()
                            .error(R.drawable.logo_mini).placeholder(R.drawable.logo_mini))
                    .into(viewHolder.ivImage);

            viewHolder.ivPlayIcon.setVisibility(View.VISIBLE);
        }else{
            viewHolder.ivPlayIcon.setVisibility(View.GONE);
            Glide.with(context)
                    .load(mGalleryProducts.get(i).getImagePath())
                    .apply(new RequestOptions().centerCrop()
                            .error(R.drawable.logo_mini).placeholder(R.drawable.logo_mini))
                    .into(viewHolder.ivImage);
        }

        viewHolder.ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onItemClicked(viewHolder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mGalleryProducts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView ivImage, ivPlayIcon;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivImage = itemView.findViewById(R.id.ivImage);
            ivPlayIcon = itemView.findViewById(R.id.ivPlayIcon);

        }
    }

    public interface OnItemClickListener{
        void onItemClicked(int pos);
    }
}
