package com.creativedots.mylums.adapter.helpMenu;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.creativedots.mylums.R;
import com.creativedots.mylums.fragment.HelpAndSupportFragment;
import com.creativedots.mylums.model.retroFit.helpMenu.MenuItem;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class HelpMenuAdapter extends RecyclerView.Adapter<HelpMenuAdapter.ViewHolder> {

    private Context context;
    private ArrayList<MenuItem> menuItems;
    HelpAndSupportFragment mHelpAndSupportFragment;
    private static final int UNSELECTED = -1;
    private int selectedItem = UNSELECTED;


    public HelpMenuAdapter(Context context, ArrayList<MenuItem> menuItems, HelpAndSupportFragment helpAndSupportFragment) {
        this.context = context;
        this.menuItems = menuItems;
        mHelpAndSupportFragment = helpAndSupportFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.rv_item_help_menu, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int pos) {

        if(menuItems.get(pos).getExpanded()){
            holder.elDescription.expand(true);
        }else{
            holder.elDescription.collapse();
        }

      //  holder.elDescription.setExpanded(menuItems.get(holder.getAdapterPosition()).getExpanded(),true);

        Glide.with(context).load(menuItems.get(pos).getImagepath()).into(holder.ivThumbnail);
        holder.tvTitle.setText(menuItems.get(pos).getTitle());
        holder.tvDescription.setText(menuItems.get(pos).getDescription());
        int position = holder.getAdapterPosition();
        boolean isSelected = position == selectedItem;

        //expandButton.setText(position + ". Tap to expand");
        holder.llHelpItem.setSelected(isSelected);
        holder.elDescription.setExpanded(isSelected, false);
    }

    @Override
    public int getItemCount() {
        return menuItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,ExpandableLayout.OnExpansionUpdateListener{

        private LinearLayout llHelpItem;
        private ExpandableLayout elDescription;
        private ImageView ivThumbnail;
        private TextView tvTitle, tvDescription;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            llHelpItem = itemView.findViewById(R.id.llHelpItem);
            elDescription = itemView.findViewById(R.id.elDescription);
            ivThumbnail = itemView.findViewById(R.id.ivThumbnail);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDescription = itemView.findViewById(R.id.tvDescription);


            elDescription.setInterpolator(new OvershootInterpolator());
            elDescription.setOnExpansionUpdateListener(this);

            llHelpItem.setOnClickListener(this);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Log.d("ExpandableLayout", "State: " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mHelpAndSupportFragment.smoothScrool(getAdapterPosition(),menuItems.size());
            }

          /*  if(state == ExpandableLayout.State.EXPANDED){
                mHelpAndSupportFragment.smoothScrool(getAdapterPosition(),menuItems.size());
            }*/
        }

        @Override
        public void onClick(View view) {

         //   ViewHolder holder = (ViewHolder) recyclerView.findViewHolderForAdapterPosition(selectedItem);
            ViewHolder holder = mHelpAndSupportFragment.viewHolderForAdapterPos(selectedItem);
            if (holder != null) {
                holder.llHelpItem.setSelected(false);
                holder.elDescription.collapse();
            }

            int position = getAdapterPosition();
//            onItemClickedListener.onItemClicked(position, menuItems.size());

            if (position == selectedItem) {
                selectedItem = UNSELECTED;
            } else {
                llHelpItem.setSelected(true);
                elDescription.expand();
                selectedItem = position;
            }
        }
    }

}
